package org.lhasalimited.knime.metabolism.encapsulated.smartcyp2;

import org.knime.core.node.NodeView;

/**
 * <code>NodeView</code> for the "SMARTCyp" Node.
 * SMARTCyp 2.4.2
 *
 * @author Lhasa Limited
 */
public class SMARTCypNodeView2 extends NodeView<SMARTCypNodeModel2> {

    /**
     * Creates a new view.
     * 
     * @param nodeModel The model (class: {@link SMARTCypNodeModel2})
     */
    protected SMARTCypNodeView2(final SMARTCypNodeModel2 nodeModel) {
        super(nodeModel);
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void modelChanged() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onClose() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onOpen() {
        // TODO: generated method stub
    }

}

