package org.lhasalimited.knime.metabolism.encapsulated.smartcyp2;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;


/**
 * <code>NodeFactory</code> for the "SMARTCyp" Node.
 * SMARTCyp 2.4.2
 *
 * @author Lhasa Limited
 */
public class SMARTCypNodeFactory2 
        extends NodeFactory<SMARTCypNodeModel2> {

    /**
     * {@inheritDoc}
     */
    @Override
    public SMARTCypNodeModel2 createNodeModel() {
        return new SMARTCypNodeModel2();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<SMARTCypNodeModel2> createNodeView(final int viewIndex,
            final SMARTCypNodeModel2 nodeModel) {
        return new SMARTCypNodeView2(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new SMARTCypNodeDialog2();
    }

}

