package org.lhasalimited.knime.metabolism.encapsulated.util;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import org.openscience.cdk.depict.Depiction;
import org.openscience.cdk.depict.DepictionGenerator;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.layout.StructureDiagramGenerator;
import org.openscience.cdk.renderer.AtomContainerRenderer;
import org.openscience.cdk.renderer.RendererModel;
import org.openscience.cdk.renderer.SymbolVisibility;
import org.openscience.cdk.renderer.color.ModCPKAtomColors;
import org.openscience.cdk.renderer.font.AWTFontManager;
import org.openscience.cdk.renderer.generators.BasicSceneGenerator;
import org.openscience.cdk.renderer.generators.IGenerator;
import org.openscience.cdk.renderer.generators.standard.StandardGenerator;
import org.openscience.cdk.renderer.visitor.AWTDrawVisitor;

/**
 * Render an image from a CDK Structure. Shows atom highlighting and annotations
 * that are set as properties within the {@link IAtomContainer}
 * 
 * @author Samuel
 * @since KNIME 3.1, plugin 1.0
 *
 */
public class CdkImageRender {

	private int width;
	private int height;
	private Color textColour;

	/**
	 * Default annotation colour of {@link Color.BLACK}
	 * 
	 * @param width
	 *            with to render the image to
	 * @param height
	 *            height to render the mage to
	 */
	public CdkImageRender(int width, int height) {
		this(width, height, Color.BLACK);
	}

	/**
	 * 
	 * @param width
	 *            Width to render the image to
	 * @param height
	 *            Height to render the image to
	 * @param col
	 *            Colour for the label annotations
	 */
	public CdkImageRender(int width, int height, Color col) {
		this.width = width;
		this.height = height;
		this.textColour = col;
	}

	/**
	 * Render the SMARTCyp result as an image. This is a workaround the the CDK
	 * Cell not keeping properties and therefore loosing the ranking
	 * annotations.
	 * 
	 * @param result
	 * @return
	 */
	public Image renderResult(IAtomContainer result) throws Exception {

		// The rendering can fail with 3D coordinates, we therefore regenerate
		// the coordintes prior to rendering
		StructureDiagramGenerator sdg = new StructureDiagramGenerator();
		sdg.setMolecule(result);
		sdg.generateCoordinates();
		result = sdg.getMolecule();

		List<IGenerator<IAtomContainer>> generators = new ArrayList<IGenerator<IAtomContainer>>();
		generators.add(new BasicSceneGenerator());
		generators.add(new StandardGenerator(new Font("Verdana", Font.PLAIN, 24)));
		AtomContainerRenderer renderer = new AtomContainerRenderer(generators, new AWTFontManager());

		RendererModel renderer2dModel = renderer.getRenderer2DModel();
		renderer2dModel.set(BasicSceneGenerator.UseAntiAliasing.class, true);
		renderer2dModel.set(StandardGenerator.AtomColor.class, new ModCPKAtomColors());
		renderer2dModel.set(StandardGenerator.AnnotationColor.class, textColour);
		renderer2dModel.set(StandardGenerator.Highlighting.class, StandardGenerator.HighlightStyle.OuterGlow);
		renderer2dModel.set(StandardGenerator.Visibility.class, SymbolVisibility.iupacRecommendations());

		Image image = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D g = (Graphics2D) image.getGraphics();
		g.setColor(Color.WHITE);
		g.fill(new Rectangle2D.Double(0, 0, width, height));

		renderer.paint(result, new AWTDrawVisitor(g), new Rectangle2D.Double(0, 0, width, height), true);
		g.dispose();

		return image;
	}

	// /**
	// * Render the SMARTCyp result as an image. This is a workaround the the
	// CDK
	// * Cell not keeping properties and therefore loosing the ranking
	// * annotations.
	// *
	// * @param result
	// * @return
	// */
	// public Image renderResultSvg(IAtomContainer result) throws Exception
	// {
	// // 26 pt is also okay
	// Font font = new Font("Arial", Font.PLAIN, 24);
	//
	// // note the font manager is not used
	// AtomContainerRenderer renderer = new
	// AtomContainerRenderer(Arrays.asList(new BasicSceneGenerator(), new
	// StandardGenerator(font)), new AWTFontManager());
	//
	// RendererModel rendererModel = renderer.getRenderer2DModel();
	// rendererModel.set(StandardGenerator.AtomColor.class, new
	// UniColor(Color.BLACK));
	// rendererModel.set(StandardGenerator.Visibility.class,
	// SymbolVisibility.iupacRecommendations());
	// rendererModel.set(StandardGenerator.StrokeRatio.class, 0.85);
	// rendererModel.set(StandardGenerator.SymbolMarginRatio.class, 4d);
	//
	// StandardGenerator generator = new StandardGenerator(new Font("Arial",
	// Font.PLAIN, 24));
	// IRenderingElement element = generator.generate(result, rendererModel);
	//
	//
	// return null;
	// }

	/**
	 * Static access to the image rendering
	 * 
	 * @param result
	 *            The structure to highlight (should have annotations already
	 *            added if desired)
	 * @param width
	 *            The width to render to
	 * @param height
	 *            The height to render to
	 * 
	 * @return The rendered image
	 * @throws Exception
	 */
	public static Image renderImage(IAtomContainer result, int width, int height) throws Exception 
	{
		return render(result, width, height, Color.BLACK);
	}

	/**
	 * 
	 * @param result
	 *            Structure / result to highlight. Should have properties set
	 *            for annotations and highlight
	 * @param width
	 *            Render width
	 * @param height
	 *            Render height
	 * @param colorValue
	 *            Text colour
	 * @return
	 * @throws Exception
	 */
	public static Image renderImage(IAtomContainer result, int width, int height, Color colorValue) throws Exception {
//		CdkImageRender renderer = new CdkImageRender(width, height, colorValue);
//		return renderer.renderResult(result);
		return render(result, width, height, colorValue);
	}

	//////////////////////
	//// Additions using the CDK depict functionality

	/**
	 * Get the depicted molecule as a {@link BufferedImage}
	 * @param container
	 * @param width
	 * @param height
	 * @param colour
	 * @return
	 * @throws Exception
	 */
	public static Image render(IAtomContainer container, int width, int height, Color colour) throws Exception
	{		
		return depict(container, width, height, colour).toImg();
	}
	
	/**
	 * Get the depicted molecule as an SVG String
	 * @param container
	 * @param width
	 * @param height
	 * @param colour
	 * @return
	 * @throws Exception
	 */
	public static String renderSvg(IAtomContainer container, int width, int height, Color colour) throws Exception
	{
		return depict(container, width, height, colour).toSvgStr();
	}
	
	
	/**
	 * Get the depiction of the molecule
	 * 
	 * @param container
	 * @param width
	 * @param height
	 * @param colour
	 * @return
	 * @throws Exception
	 */
	public static Depiction depict(IAtomContainer container, int width, int height, Color colour) throws Exception
	{
		DepictionGenerator dg = new DepictionGenerator().withSize(width, height).withAtomColors().withFillToFit().withAnnotationColor(colour).withOuterGlowHighlight().withMolTitle();
		
		return dg.depict(container);		
	}



}
