package org.lhasalimited.knime.metabolism.encapsulated.util;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.imageio.ImageIO;

import org.knime.base.data.xml.SvgCellFactory;
import org.knime.chem.types.MolCellFactory;
import org.knime.chem.types.MolValue;
import org.knime.chem.types.SdfValue;
import org.knime.core.data.AdapterValue;
import org.knime.core.data.DataCell;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataType;
import org.knime.core.data.MissingCell;
import org.knime.core.data.append.AppendedColumnRow;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.data.image.png.PNGImageCellFactory;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelColor;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.util.MultiThreadWorker;
import org.lhasalimited.cyp.util.smartcyp.AtomValues;
import org.lhasalimited.cyp.util.smartcyp.SmartCypColour;
import org.lhasalimited.cyp.util.smartcyp.SmartCypProcessor;
import org.lhasalimited.cyp.util.smartcyp.SmartCypResults;
import org.lhasalimited.knime.metabolism.encapsulated.smartcyp.SMARTCypNodeModel;
import org.lhasalimited.knime.metabolism.encapsulated.smartcyp2.SMARTCypNodeModel2;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.knime.commons.CDKNodeUtils;
import org.openscience.cdk.knime.type.CDKCell3;
import org.openscience.cdk.knime.type.CDKValue;
import org.openscience.cdk.renderer.generators.standard.StandardGenerator;

/**
 * A threaded worker class for processing {@link DataRow} objects through
 * SMARTCyp and adding their results to a summary and detailed output
 * {@link BufferedDataContainer}
 * 
 * @author Samuel
 * @since KNIMe 3.1, plugin 1.0
 *
 */
public class SmartCypWorker2 extends MultiThreadWorker<DataRow, KnimeSmartCypResults>
{

	private final SmartCypProcessor processor;
	private final ExecutionMonitor exec;
	private final double max;
	private final int columnIndex;
	private final BufferedDataContainer bdc;
	private final BufferedDataContainer bdcDetailed;
	private final SmartCypSettings settings;
	private final int idIndex;

	private int rankingCutoff;
	private Color labelColour;
	private int width;
	private int height;

	private boolean useAdapterApproach;

	/**
	 * 
	 * @param maxQueueSize
	 * @param maxActiveInstanceSize
	 * @param columnIndex
	 *            The structure column index
	 * @param exec
	 * @param max
	 * @param bdc
	 *            The summary output table
	 * @param bdcDetailed
	 *            The detailed output table
	 * @param settings
	 *            The settings (linked with the dialog)
	 * @param processor
	 *            The {@link SmartCypProcessor} instance
	 * @param idIndex
	 *            the index of the identifier column
	 */
	public SmartCypWorker2(final int maxQueueSize, final int maxActiveInstanceSize, final int columnIndex,
			final ExecutionMonitor exec, final long max, final BufferedDataContainer bdc,
			final BufferedDataContainer bdcDetailed, SmartCypSettings settings, SmartCypProcessor processor,
			int idIndex, boolean useAdapterApproach)
	{
		super(maxQueueSize, maxActiveInstanceSize);

		this.processor = processor;
		this.exec = exec;
		this.bdc = bdc;
		this.max = max;
		this.settings = settings;
		this.columnIndex = columnIndex;
		this.idIndex = idIndex;
		this.bdcDetailed = bdcDetailed;

		rankingCutoff = settings.getSetting(SmartCypSettings.CONFIG_RANKING_CUTOFF, SettingsModelInteger.class)
				.getIntValue();
		labelColour = settings.getSetting(SmartCypSettings.CONFIG_COLOUR_LABEL, SettingsModelColor.class)
				.getColorValue();

		width = settings.getSetting(SmartCypSettings.CONFIG_WIDTH, SettingsModelInteger.class).getIntValue();
		height = settings.getSetting(SmartCypSettings.CONFIG_HEIGHT, SettingsModelInteger.class).getIntValue();

		this.useAdapterApproach = useAdapterApproach;
	}

	@Override
	protected KnimeSmartCypResults compute(DataRow row, long index) throws Exception
	{
		// Setup variables for output
		DataRow summary = null;
		List<DataRow> detailedResults = null;

		DataCell outCell = null;
		DataCell outStructureCell = null;

		if (row.getCell(columnIndex).isMissing())
		{
			outCell = DataType.getMissingCell();
			outStructureCell = DataType.getMissingCell();
			summary = new AppendedColumnRow(row, new DataCell[] { outCell, outStructureCell });
			detailedResults = new ArrayList<DataRow>();
			SMARTCypNodeModel.LOGGER.debug("Structure is missing or not compatible");
		} else
		{
			if (useAdapterApproach)
			{
				if ((((AdapterValue) row.getCell(columnIndex)).getAdapterError(CDKValue.class) != null))
				{
					outCell = DataType.getMissingCell();
					outStructureCell = DataType.getMissingCell();
					summary = new AppendedColumnRow(row, new DataCell[] { outCell, outStructureCell });
					detailedResults = new ArrayList<DataRow>();
					SMARTCypNodeModel.LOGGER.debug("Column is not an adapter cell");
				}
			}
			
			
			List<SmartCypResults> results;
			try
			{
				String mdlRepresentation = null;
				if (!useAdapterApproach)
				{
					DataType type = row.getCell(columnIndex).getType();
					if (type.isCompatible(MolValue.class))
					{
						mdlRepresentation = ((MolValue) row.getCell(columnIndex)).getMolValue();
					} else if (type.isCompatible(SdfValue.class))
					{
						mdlRepresentation = ((SdfValue) row.getCell(columnIndex)).getSdfValue();
					} else
					{
						outCell = DataType.getMissingCell();
						outStructureCell = DataType.getMissingCell();
						summary = new AppendedColumnRow(row, new DataCell[] { outCell, outStructureCell });
						detailedResults = new ArrayList<DataRow>();
						SMARTCypNodeModel.LOGGER.debug(type.toString()
								+ " is not explicit handled and CDK Adapter cell should have been used. Eror in NodeModel");
					}
				} else
				{
					// The column is already expected to be compatible with the
					// CDK Adapter value
					// otherwise it would have thrown an exception in the
					// initial if
					CDKValue mol = ((AdapterValue) row.getCell(columnIndex)).getAdapter(CDKValue.class);
					IAtomContainer con = CDKNodeUtils.getFullMolecule(mol.getAtomContainer());
					mdlRepresentation = StructureConversion.writeMolfile(con);
				}

				String id = idIndex == -1 ? row.getKey().getString() : row.getCell(idIndex).toString();

				results = processor.process(mdlRepresentation, id);

				// Create the images for the first output table
				// There are three highlighting outputs from SMARTCyp. The
				// standard and 2 specific models
				DataCell imageCell;
				DataCell imageCellCyp2C;
				DataCell imageCellCyp2D6;

				SmartCypResults result = results.get(0);
				IAtomContainer structure = StructureConversion.readMolfile(result.getMolfile());
				// IAtomContainer structureCloned = structure.clone();

				IAtomContainer standard = createStandard(structure, result);
				try
				{

					if (settings.getSetting(SmartCypSettings.CONFIG_IMAGE_FORMAT, SettingsModelString.class)
							.getStringValue().equals("PNG"))
					{
						Image imgStandard = CdkImageRender.renderImage(standard, width, height, labelColour);
						Image imgCyp2c = CdkImageRender.renderImage(createCyp2c(structure, result), width, height,
								labelColour);
						Image imgCyp2d6 = CdkImageRender.renderImage(createCyp2d6(structure, result), width, height,
								labelColour);

						imageCell = createImageCell(imgStandard);
						imageCellCyp2C = createImageCell(imgCyp2c);
						imageCellCyp2D6 = createImageCell(imgCyp2d6);
					} else
					{
						imageCell = SvgCellFactory
								.create(CdkImageRender.renderSvg(standard, width, height, labelColour));
						imageCellCyp2C = SvgCellFactory.create(
								CdkImageRender.renderSvg(createCyp2c(structure, result), width, height, labelColour));
						imageCellCyp2D6 = SvgCellFactory.create(
								CdkImageRender.renderSvg(createCyp2d6(structure, result), width, height, labelColour));
					}

				} catch (Exception e)
				{
					SMARTCypNodeModel.LOGGER.error("Failed to render images for row: " + row.getKey());
					// SMARTCypNodeModel.LOGGER.debug(e.getClass().toString() +
					// ": " + e.getMessage());
					SMARTCypNodeModel.LOGGER.error("Failed: ", e);
					e.printStackTrace();

					imageCell = new MissingCell(e.getMessage());
					imageCellCyp2C = new MissingCell(e.getMessage());
					imageCellCyp2D6 = new MissingCell(e.getMessage());
				}

				outStructureCell = new CDKCell3(standard);

				if (useAdapterApproach)
				{
					summary = new AppendedColumnRow(row,
							new DataCell[] { imageCell, imageCellCyp2C, imageCellCyp2D6, outStructureCell });
				} else
				{
					summary = new AppendedColumnRow(row, new DataCell[] { imageCell, imageCellCyp2C, imageCellCyp2D6,// outStructureCell});
							new MolCellFactory().createCell(result.getMolfile())});
				}

				detailedResults = createRows(row, structure, result, rankingCutoff);

			} catch (Exception e)
			{
				e.printStackTrace();
				SMARTCypNodeModel.LOGGER.error("Failed to process row: " + row.getKey() + " appending missing values");
				SMARTCypNodeModel.LOGGER.debug(e.getMessage());
				exec.setMessage("Node ran with issues");
				summary = new AppendedColumnRow(row,
						new DataCell[] { new MissingCell(e.getMessage()), new MissingCell(e.getMessage()),
								new MissingCell(e.getMessage()), new MissingCell(e.getMessage()) });
				detailedResults = new ArrayList<DataRow>();
			}
		}

		return new KnimeSmartCypResults(summary, detailedResults);
	}

	/**
	 * Create the structure highlight properties to represent the ranking of the
	 * CYP2D6 model
	 * 
	 * @param structure
	 * @param result
	 * @return
	 * @throws CloneNotSupportedException
	 */
	private IAtomContainer createCyp2d6(IAtomContainer structure, SmartCypResults result)
			throws CloneNotSupportedException
	{
		IAtomContainer structureCloned = structure.clone();
		for (int atomIndex = 0; atomIndex < structureCloned.getAtomCount(); atomIndex++)
		{
			IAtom currentAtom = structureCloned.getAtom(atomIndex);
			String currentAtomType = currentAtom.getSymbol();

			if (currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P")
					|| currentAtomType.equals("S"))
			{
				int ranking = result.getAtom(atomIndex).getRanking2d6();
				if (ranking <= rankingCutoff || rankingCutoff == 0)
				{
					structureCloned.getAtom(atomIndex).setProperty(StandardGenerator.ANNOTATION_LABEL, ranking + "");
					structureCloned.getAtom(atomIndex).setProperty(StandardGenerator.HIGHLIGHT_COLOR,
							getColour(ranking));
				}
			}
		}

		return structureCloned;
	}

	/**
	 * Create the structure highlight properties to represent the ranking of the
	 * CYP2C model
	 * 
	 * @param structure
	 * @param result
	 * @return
	 * @throws CloneNotSupportedException
	 */
	private IAtomContainer createCyp2c(IAtomContainer structure, SmartCypResults result)
			throws CloneNotSupportedException
	{
		IAtomContainer structureCloned = structure.clone();
		for (int atomIndex = 0; atomIndex < structureCloned.getAtomCount(); atomIndex++)
		{
			IAtom currentAtom = structureCloned.getAtom(atomIndex);
			String currentAtomType = currentAtom.getSymbol();

			if (currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P")
					|| currentAtomType.equals("S"))
			{
				int ranking = result.getAtom(atomIndex).getRanking2C9();
				if (ranking <= rankingCutoff || rankingCutoff == 0)
				{
					structureCloned.getAtom(atomIndex).setProperty(StandardGenerator.ANNOTATION_LABEL, ranking + "");
					structureCloned.getAtom(atomIndex).setProperty(StandardGenerator.HIGHLIGHT_COLOR,
							getColour(ranking));
				}
			}
		}

		return structureCloned;
	}

	/**
	 * Set the highlight properties to represent the ranking of the standard
	 * model
	 * 
	 * @param structure
	 * @param result
	 * @return
	 * @throws CloneNotSupportedException
	 */
	private IAtomContainer createStandard(IAtomContainer structure, SmartCypResults result)
			throws CloneNotSupportedException
	{
		IAtomContainer structureCloned = structure.clone();
		for (int atomIndex = 0; atomIndex < structureCloned.getAtomCount(); atomIndex++)
		{
			IAtom currentAtom = structureCloned.getAtom(atomIndex);
			String currentAtomType = currentAtom.getSymbol();

			if (currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P")
					|| currentAtomType.equals("S"))
			{
				int ranking = result.getAtom(atomIndex).getRanking();
				if (ranking <= rankingCutoff || rankingCutoff == 0)
				{
					structureCloned.getAtom(atomIndex).setProperty(StandardGenerator.ANNOTATION_LABEL, ranking + "");
					structureCloned.getAtom(atomIndex).setProperty(StandardGenerator.HIGHLIGHT_COLOR,
							getColour(ranking));
				}
			}
		}

		return structureCloned;
	}

	/**
	 * Create the image cell with the highlighting from the given image
	 * 
	 * @param img
	 * @return
	 * @throws IOException
	 */
	private DataCell createImageCell(Image img) throws IOException
	{
		File tmp = File.createTempFile("temp-smartCyp", ".png");
		ImageIO.write((RenderedImage) img, "PNG", tmp);
		return new PNGImageCellFactory().createCell(new FileInputStream(tmp));
	}

	/**
	 * Get the colour for the highlight either based on the dialog settings or
	 * the automatic value
	 * 
	 * @param ranking
	 * @return
	 */
	private Color getColour(int ranking)
	{

		boolean overide = settings.getSetting(SmartCypSettings.CONFIG_SET_COLOUR, SettingsModelBoolean.class)
				.getBooleanValue();
		Color col = null;
		if (overide)
		{
			col = settings.getSetting(SmartCypSettings.CONFIG_COLOUR, SettingsModelColor.class).getColorValue();
		} else
		{
			col = SmartCypColour.getColourForRank(ranking);
		}

		return col;
	}

	/**
	 * Create the output rows for the given version of SmartCyp
	 * 
	 * @param source
	 * @param moleculeKU
	 * @param rankCutoff
	 * @return
	 * @throws CloneNotSupportedException
	 */
	private List<DataRow> createRows(DataRow source, IAtomContainer structure, SmartCypResults result, int rankCutoff)
			throws CloneNotSupportedException
	{
		double ERROR_VALUE = 999d;

		List<DataRow> rows = new ArrayList<DataRow>();

		for (int atomIndex = 0; atomIndex < structure.getAtomCount(); atomIndex++)
		{

			IAtom currentAtom = structure.getAtom(atomIndex);
			String currentAtomType = currentAtom.getSymbol();

			if (currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P")
					|| currentAtomType.equals("S"))
			{

				AtomValues values = result.getAtom(atomIndex);

				Number ranking = values.getRanking();

				// Highlight the atom
				IAtomContainer cloned = structure.clone();
				cloned.getAtom(atomIndex).setProperty(StandardGenerator.HIGHLIGHT_COLOR, getColour((int) ranking));

				DataCell[] cells = new DataCell[SMARTCypNodeModel2.NUM_OUT_CELLS];

				if (idIndex == -1)
				{
					cells[0] = new StringCell(source.getKey().toString());
				} else
				{
					cells[0] = source.getCell(idIndex);
				}

//				cells[1] = CDKCell3.createCDKCell(cloned);

				// TODO: refactor this to be more inline with the switch to the
				// encapsualted SmartCyp approach
				Number score = ERROR_VALUE;
				Number energy = ERROR_VALUE;
				if (values.getScore() != null)
				{
					score = values.getScore();
					energy = values.getEnergy();
				}

				Number accessibility = values.getAccessibility();

				Number score2d6 = ERROR_VALUE;
				Number ranking2d6 = ERROR_VALUE;
				if (values.getScore2D6() != null)
				{
					ranking2d6 = values.getRanking2d6();
					score2d6 = values.getScore2D6();
				}

				Number span2end = values.getSpan2end();

				Number dist2protamine = 0;
				if (values.getDist2ProtAmine() != null)
				{
					dist2protamine = values.getDist2ProtAmine();
				}

				Number ranking2c9 = ERROR_VALUE;
				Number score2c9 = ERROR_VALUE;
				if (values.getScore2C9() != null)
				{
					ranking2c9 = values.getRanking2C9();
					score2c9 = values.getScore2C9();
				}

				Number dist2cooh = 0;
				if (values.getDist2CarboxylicAcid() != null)
				{
					dist2cooh = values.getDist2CarboxylicAcid();
				}

				Number sasa2d = 0;
				if (values.getSASA2D() != null)
				{
					sasa2d = values.getSASA2D();
				}

				int indexToReport = settings
						.getSetting(SmartCypSettings.CONFIG_INDEX_FROM_ZERO, SettingsModelBoolean.class)
						.getBooleanValue() ? atomIndex : atomIndex + 1;

				cells[1] = new StringCell(currentAtom.getSymbol());
				cells[2] = new IntCell(indexToReport);
				cells[3] = new IntCell((int) ranking);
				cells[4] = new DoubleCell(score.doubleValue());
				cells[5] = new DoubleCell(energy.doubleValue());
				cells[6] = new DoubleCell(accessibility.doubleValue());
				cells[7] = new DoubleCell(score2d6.doubleValue());
				cells[8] = new IntCell((int) ranking2d6);
				cells[9] = new DoubleCell(span2end.doubleValue());
				cells[10] = new IntCell((int) dist2protamine);
				cells[11] = new IntCell((int) ranking2c9);
				cells[12] = new DoubleCell(score2c9.doubleValue());
				cells[13] = new IntCell((int) dist2cooh);
				cells[14] = new DoubleCell(sasa2d.doubleValue());

				boolean filter = settings
						.getSetting(SmartCypSettings.CONFIG_FILTER_DETAILED, SettingsModelBoolean.class)
						.getBooleanValue();
				if (((int) ranking <= (rankCutoff) && filter) || !filter)
				{
					rows.add(new DefaultRow(source.getKey() + "_" + (atomIndex + 1), cells));
				}
			}
		}

		return rows;
	}

	/**
	 * Handle adding the summary and detailed rows to the output tables. Also
	 * updates the progress of the node
	 */
	@Override
	protected void processFinished(ComputationTask task)
			throws ExecutionException, CancellationException, InterruptedException
	{

		KnimeSmartCypResults results = task.get();

		DataRow summary = results.getSummary();
		List<DataRow> detailed = results.getDetailed();

		if (!summary.getCell(columnIndex).isMissing())
		{
			bdc.addRowToTable(summary);

			long start = bdcDetailed.size();

			if (detailed != null)
			{
				for (DataRow row : detailed)
				{
					bdcDetailed.addRowToTable(row);
				}
				SMARTCypNodeModel.LOGGER
						.debug("Added: " + (bdcDetailed.size() - start) + " rows to the detailed output table");
			} else
			{
				SMARTCypNodeModel.LOGGER.debug("No detailed rows added for row: " + summary.getKey().toString());
			}
		}

		exec.setProgress(this.getFinishedCount() / max, this.getFinishedCount() + " (active/submitted: "
				+ this.getActiveCount() + "/" + (this.getSubmittedCount() - this.getFinishedCount()) + ")");

		try
		{
			exec.checkCanceled();
		} catch (CanceledExecutionException cee)
		{
			throw new CancellationException();
		}
	}
}
