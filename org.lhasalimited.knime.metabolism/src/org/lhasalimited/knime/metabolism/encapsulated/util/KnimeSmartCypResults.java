package org.lhasalimited.knime.metabolism.encapsulated.util;

import java.util.List;

import org.knime.core.data.DataRow;

/**
 * A temporary container to store the various output rows for a SMARTCyp run. The node has a summary output table which 
 * generates a single row per input row and a second output table with generates 1-N rows per output table. 
 * 
 * @author Samuel, Lhasa Limited
 * @since KNIME 3.1, plugin 1.0
 *
 */
public class KnimeSmartCypResults
{

	private DataRow summary;
	private List<DataRow> detailed;
	
	/**
	 * 
	 * @param summary			The single summary row
	 * @param detailed			The multiple detailed rows
	 */
	public KnimeSmartCypResults(DataRow summary, List<DataRow> detailed)
	{
		this.summary = summary;
		this.detailed = detailed;
	}


	/**
	 * Get the summary table row
	 * @return
	 */
	public DataRow getSummary()
	{
		return summary;
	}


	/**
	 * Set the summary table row
	 * @param summary
	 */
	public void setSummary(DataRow summary)
	{
		this.summary = summary;
	}


	/**
	 * Get the rows for the detailed table
	 * @return
	 */
	public List<DataRow> getDetailed()
	{
		return detailed;
	}


	/**
	 * Set the row for the detailed table
	 * @param detailed
	 */
	public void setDetailed(List<DataRow> detailed)
	{
		this.detailed = detailed;
	}
	
	
	
}
