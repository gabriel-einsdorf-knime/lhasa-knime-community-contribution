package org.lhasalimited.knime.metabolism.encapsulated.util;

import java.awt.Color;
import java.util.TreeMap;

import org.knime.core.node.NodeDialog;
import org.knime.core.node.NodeModel;
import org.knime.core.node.defaultnodesettings.SettingsModel;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelColor;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnName;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.openscience.cdk.knime.core.CDKSettings;

/**
 * The settings for the SMARTCyp node. No need to create the various
 * {@link SettingsModel} objects in the {@link NodeModel} and the
 * {@link NodeDialog} call the appropriate {@link #getSetting(String, Class)} to
 * retrieve the object. <br>
 * <br>
 * A series of static strings provide support for finding the appropriate
 * {@link SettingsModel} <br>
 * <br>
 * Uses the {@link CDKSettings} but overided the way the target structure column
 * is handled.
 * 
 * @author Samuel, Lhasa Limited
 * @since KNIME 3.1, plugin 1.0
 */
public class SmartCypSettings extends NodeSettingCollection
{


	public static final String CONFIG_STRUCTURE_COLUMN = "cfgStructureColumn";
	public static final String CONFIG_N_OXIDATION_CORRECTION = "cfgNOxidationCorrection";
	public static final String CONFIG_RANKING_CUTOFF = "cfgRankingCutoff";
	public static final String CONFIG_IDENTIFIER_COLUMN = "cfgIdentifierColumn";
	public static final String CONFIG_FILTER_DETAILED = "cfgFilterDetailed";

	public static final String CONFIG_WIDTH = "cfgWidth";
	public static final String CONFIG_HEIGHT = "cfgHeight";
	public static final String CONFIG_COLOUR = "cfgColour";
	public static final String CONFIG_SET_COLOUR = "cfgSetColour";
	public static final String CONFIG_COLOUR_LABEL = "cfgLabelColour";
	public static final String CONFIG_IMAGE_FORMAT = "cfgImageFormat";
	public static final String CONFIG_INDEX_FROM_ZERO = "cfgIndexFromZero";

	public static final int DEFAULT_RANKING_CUTOFF = 3;

	public SmartCypSettings()
	{
		super();

		addSettings();
	}



	@Override
	protected void addSettings()
	{
		settingMap = new TreeMap<String, SettingsModel>();

		settingMap.put(CONFIG_WIDTH, new SettingsModelInteger(CONFIG_WIDTH, 250));
		settingMap.put(CONFIG_HEIGHT, new SettingsModelInteger(CONFIG_HEIGHT, 250));
		settingMap.put(CONFIG_COLOUR, new SettingsModelColor(CONFIG_COLOUR, Color.RED));
		settingMap.put(CONFIG_SET_COLOUR, new SettingsModelBoolean(CONFIG_SET_COLOUR, false));
		settingMap.put(CONFIG_COLOUR_LABEL, new SettingsModelColor(CONFIG_COLOUR_LABEL, Color.BLACK));

		settingMap.put(CONFIG_STRUCTURE_COLUMN, new SettingsModelColumnName(CONFIG_STRUCTURE_COLUMN, "Structure"));
		settingMap.put(CONFIG_IDENTIFIER_COLUMN, new SettingsModelColumnName(CONFIG_IDENTIFIER_COLUMN, "ID"));
		settingMap.put(CONFIG_N_OXIDATION_CORRECTION, new SettingsModelBoolean(CONFIG_N_OXIDATION_CORRECTION, true));
		settingMap.put(CONFIG_RANKING_CUTOFF, new SettingsModelInteger(CONFIG_RANKING_CUTOFF, DEFAULT_RANKING_CUTOFF));
		settingMap.put(CONFIG_FILTER_DETAILED, new SettingsModelBoolean(CONFIG_FILTER_DETAILED, true));
		settingMap.put(CONFIG_IMAGE_FORMAT, new SettingsModelString(CONFIG_IMAGE_FORMAT, "PNG"));
		settingMap.put(CONFIG_INDEX_FROM_ZERO, new SettingsModelBoolean(CONFIG_INDEX_FROM_ZERO, false));
	}

}
