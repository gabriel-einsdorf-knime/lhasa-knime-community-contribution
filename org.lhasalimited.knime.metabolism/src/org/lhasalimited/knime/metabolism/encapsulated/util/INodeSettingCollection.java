package org.lhasalimited.knime.metabolism.encapsulated.util;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Iterator;

import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModel;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnName;
import org.openscience.cdk.knime.core.CDKSettings;

/**
 * Extension of {@link CDKSettings} providing a central store of settings to share between the {@link NodeDialog} and the {@link NodeModel}
 * <br><br>
 * Can delegate the the save, load and validate methods in the {@link NodeModel} to this object. Static strings provide the 
 * key String values for the {@link SettingsModel} objects. 
 * 
 * @author Samuel
 *
 */
public interface INodeSettingCollection extends CDKSettings
{
	public static final String CONFIG_STRUCTURE_COLUMN = "cfgStructureColumn";
	
	public <T> T getSetting(String key, Class<T> type);
	public Iterator<SettingsModel> getSettingIterator();
	
	default void validateSettings(NodeSettingsRO settings)
	{
		getSettingIterator().forEachRemaining(s ->
		{
			try
			{
				s.validateSettings(settings);
			} catch (Exception e)
			{
				e.printStackTrace();
				throw new UncheckedIOException("Error validating settings", new IOException(e));
			}
		});
		
		targetColumn(getSetting(CONFIG_STRUCTURE_COLUMN, SettingsModelColumnName.class).getColumnName());
	}
	
	default void loadValidatedSettingsFrom(NodeSettingsRO settings)
	{
		getSettingIterator().forEachRemaining(s ->
		{
			try
			{
				s.loadSettingsFrom(settings);
			} catch (Exception e)
			{
				e.printStackTrace();
				throw new UncheckedIOException("Error loading settings", new IOException(e));
			}
		});
		
		targetColumn(getSetting(CONFIG_STRUCTURE_COLUMN, SettingsModelColumnName.class).getColumnName());
	}
	
	default void saveSettingsTo(NodeSettingsWO settings)
	{
		targetColumn(getSetting(CONFIG_STRUCTURE_COLUMN, SettingsModelColumnName.class).getColumnName());
		getSettingIterator().forEachRemaining(s ->
		{
			try
			{
				s.saveSettingsTo(settings);
			} catch (Exception e)
			{
				e.printStackTrace();
				throw new UncheckedIOException("Error savings settings", new IOException(e));
			}
		});
	}
	
	default void loadSettingsForDialog(final NodeSettingsRO settings)
	{
		try
		{
			loadSettings(settings);
		} catch (InvalidSettingsException e)
		{
			e.printStackTrace();
		}

		targetColumn(((SettingsModelColumnName) getSetting(CONFIG_STRUCTURE_COLUMN, null)).getColumnName());
	}

}
