package org.lhasalimited.knime.metabolism.encapsulated.util;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Properties;

import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.io.MDLV2000Reader;
import org.openscience.cdk.io.MDLV2000Writer;
import org.openscience.cdk.io.listener.PropertiesListener;
import org.openscience.cdk.silent.AtomContainer;

/**
 * Some static support methods for converting {@link IAtomContainer} to and from molfile strings
 * @author Samuel
 * @since KNIME 3.1, plugin 1.0
 */
public class StructureConversion
{
	
	/**
	 * Convert a molfile to an AtomContainer
	 * @param molfile
	 * @return
	 * @throws IOException
	 * @throws CDKException
	 */
	public static IAtomContainer readMolfile(String molfile) throws IOException, CDKException
	{
		MDLV2000Reader reader = new MDLV2000Reader(new StringReader(molfile));
		IAtomContainer container = reader.read(new AtomContainer());
		reader.close();
		return container;
	}

	/**
	 * Convert an IAtomContainer to a molfile string
	 * @param structure
	 * @return
	 * @throws CDKException
	 * @throws IOException
	 */
	public static String writeMolfile(IAtomContainer structure) throws CDKException, IOException
	{
		StringWriter writer = new StringWriter();
		MDLV2000Writer molfileWriter = new MDLV2000Writer(writer);
		
//		Properties customSettings = new Properties();
//		customSettings.setProperty(
//		 "ForceWriteAs2DCoordinates", "true"
//		);
//		
//		PropertiesListener listener = new PropertiesListener(customSettings);
//		molfileWriter.addChemObjectIOListener(listener);
		
		
		molfileWriter.write(structure);
		molfileWriter.close();
		
		return writer.toString();
	}
	
}
