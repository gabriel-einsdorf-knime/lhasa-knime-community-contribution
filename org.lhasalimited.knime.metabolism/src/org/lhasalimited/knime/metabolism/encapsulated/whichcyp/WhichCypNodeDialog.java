package org.lhasalimited.knime.metabolism.encapsulated.whichcyp;

import java.util.Arrays;

import org.knime.core.data.DataTableSpec;
import org.knime.core.data.DataValue;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.NotConfigurableException;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentBoolean;
import org.knime.core.node.defaultnodesettings.DialogComponentColorChooser;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnNameSelection;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;
import org.knime.core.node.defaultnodesettings.DialogComponentString;
import org.knime.core.node.defaultnodesettings.DialogComponentStringSelection;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelColor;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnName;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.lhasalimited.knime.metabolism.encapsulated.util.INodeSettingCollection;
import org.lhasalimited.knime.metabolism.encapsulated.util.SmartCypSettings;

/**
 * <code>NodeDialog</code> for the "WhichCyp" Node. Runs WhichCyp 1.2
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more
 * complex dialog please derive directly from
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Lhasa Limited
 */
public class WhichCypNodeDialog extends DefaultNodeSettingsPane {

	private final INodeSettingCollection m_settings = new WhichCypSettings();

	/**
	 * New pane for configuring the WhichCyp node.
	 */
	protected WhichCypNodeDialog() {
		addDialogComponent(new DialogComponentColumnNameSelection(
				m_settings.getSetting(WhichCypSettings.CONFIG_STRUCTURE_COLUMN, SettingsModelColumnName.class),
				"Structure", 0, DataValue.class));

		createNewTab("Image settings");
		setHorizontalPlacement(true);
		addDialogComponent(new DialogComponentNumber(
				m_settings.getSetting(WhichCypSettings.CONFIG_WIDTH, SettingsModelInteger.class), "Width", 10));
		addDialogComponent(new DialogComponentNumber(
				m_settings.getSetting(WhichCypSettings.CONFIG_HEIGHT, SettingsModelInteger.class), "Height", 10));
		addDialogComponent(new DialogComponentStringSelection(
				m_settings.getSetting(WhichCypSettings.CONFIG_IMAGE_FORMAT, SettingsModelString.class), "Output format",
				Arrays.asList(new String[] { "PNG", "SVG" })));

		setHorizontalPlacement(false);
		createNewGroup("Structure highlight");
		setHorizontalPlacement(true);
		addDialogComponent(new DialogComponentColorChooser(
				m_settings.getSetting(WhichCypSettings.CONFIG_COLOUR, SettingsModelColor.class), "Colour", true));
		addDialogComponent(new DialogComponentBoolean(
				m_settings.getSetting(WhichCypSettings.CONFIG_USE_SENTIVITY_FOR_COLOUR, SettingsModelBoolean.class),
				"Use sensitivy for colouring"));
		setHorizontalPlacement(false);
		createNewGroup("Label settings");
		setHorizontalPlacement(true);
		addDialogComponent(new DialogComponentColorChooser(
				m_settings.getSetting(WhichCypSettings.CONFIG_COLOUR_LABEL, SettingsModelColor.class), "Label colour",
				true));
	
	}

	@Override
	public void loadAdditionalSettingsFrom(NodeSettingsRO settings, DataTableSpec[] specs)
			throws NotConfigurableException {
		// TODO Auto-generated method stub
		super.loadAdditionalSettingsFrom(settings, specs);

		m_settings.loadSettingsForDialog(settings);

	}

	@Override
	public void saveAdditionalSettingsTo(NodeSettingsWO settings) throws InvalidSettingsException {
		// TODO Auto-generated method stub
		super.saveAdditionalSettingsTo(settings);
		m_settings.saveSettingsTo(settings);
	}
}
