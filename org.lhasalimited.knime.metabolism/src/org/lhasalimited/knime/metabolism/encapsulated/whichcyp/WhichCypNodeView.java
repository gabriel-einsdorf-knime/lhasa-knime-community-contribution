package org.lhasalimited.knime.metabolism.encapsulated.whichcyp;

import org.knime.core.node.NodeView;

/**
 * <code>NodeView</code> for the "WhichCyp" Node.
 * Runs WhichCyp 1.2
 *
 * @author Lhasa Limited
 */
public class WhichCypNodeView extends NodeView<WhichCypNodeModel> {

    /**
     * Creates a new view.
     * 
     * @param nodeModel The model (class: {@link WhichCypNodeModel})
     */
    protected WhichCypNodeView(final WhichCypNodeModel nodeModel) {
        super(nodeModel);
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void modelChanged() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onClose() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onOpen() {
        // TODO: generated method stub
    }

}

