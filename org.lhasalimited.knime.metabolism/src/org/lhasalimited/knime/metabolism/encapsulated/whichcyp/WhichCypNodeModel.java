package org.lhasalimited.knime.metabolism.encapsulated.whichcyp;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.knime.base.data.xml.SvgCellFactory;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.DataTableSpecCreator;
import org.knime.core.data.DataType;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.image.png.PNGImageCellFactory;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeLogger;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.lhasalimited.cyp.whichcyp.WhichCypProcessor;
import org.lhasalimited.cyp.whichcyp.WhichCypPropertySupport;
import org.lhasalimited.knime.metabolism.encapsulated.util.INodeSettingCollection;
import org.openscience.cdk.knime.core.CDKAdapterNodeModel;

/**
 * This is the model implementation of WhichCyp. Runs WhichCyp 1.2
 *
 * @author Lhasa Limited
 */
public class WhichCypNodeModel extends CDKAdapterNodeModel {

	private INodeSettingCollection localSettings;
	public static NodeLogger LOGGER = NodeLogger.getLogger(WhichCypNodeModel.class);
	// private int structureColIndex;

	public static int NUM_IMAGE_CELLS = 5;
	public static int NUM_OUT_CELLS;

	/**
	 * Constructor for the node model.
	 */
	protected WhichCypNodeModel() {
		super(1, 1, new WhichCypSettings());
		localSettings = (INodeSettingCollection) settings;
	}

	@Override
	protected BufferedDataTable[] process(BufferedDataTable[] convertedTables, ExecutionContext exec) throws Exception {

		BufferedDataContainer outputTable = exec.createDataContainer(appendSpec(convertedTables[0].getDataTableSpec()));

		WhichCypProcessor processor = new WhichCypProcessor();

		WhichCypWorker worker = new WhichCypWorker(maxQueueSize, maxParallelWorkers, columnIndex,
				exec.createSubProgress(1), convertedTables[0].size(), outputTable, settings(WhichCypSettings.class),
				processor);

		try {
			worker.run(convertedTables[0]);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error thrown running multi thread worker", e);
			throw (e);
		} finally {
			try {
				outputTable.close();
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}
		}

		if (convertedTables[0].size() != outputTable.size())
			setWarningMessage("Some molecules couldn't be processed. Output table has "
					+ (convertedTables[0].size() - outputTable.size()) + " fewer rows.");

		return new BufferedDataTable[] { outputTable.getTable() };
	}

	private DataTableSpec appendSpec(DataTableSpec spec) {
		String targetCol = localSettings.targetColumn();
		String colA = DataTableSpec.getUniqueColumnName(spec, "1A2");
		String colB = DataTableSpec.getUniqueColumnName(spec, "2C9");
		String colC = DataTableSpec.getUniqueColumnName(spec, "2C19");
		String colD = DataTableSpec.getUniqueColumnName(spec, "2D6");
		String colE = DataTableSpec.getUniqueColumnName(spec, "3A4");

		DataType imageType = localSettings.getSetting(WhichCypSettings.CONFIG_IMAGE_FORMAT, SettingsModelString.class)
				.getStringValue().equals("PNG") ? PNGImageCellFactory.TYPE : SvgCellFactory.TYPE;

		DataColumnSpec creatorA = new DataColumnSpecCreator(colA, imageType).createSpec();
		DataColumnSpec creatorB = new DataColumnSpecCreator(colB, imageType).createSpec();
		DataColumnSpec creatorC = new DataColumnSpecCreator(colC, imageType).createSpec();
		DataColumnSpec creatorD = new DataColumnSpecCreator(colD, imageType).createSpec();
		DataColumnSpec creatorE = new DataColumnSpecCreator(colE, imageType).createSpec();

		DataTableSpecCreator creator = new DataTableSpecCreator(spec);
		creator.addColumns(new DataColumnSpec[] { creatorA, creatorB, creatorC, creatorD, creatorE });

		List<String> propNames = WhichCypPropertySupport.PROPERTY_NAMES;
		DataColumnSpec[] countCells = new DataColumnSpec[propNames.size()];

		for (int i = 0; i < propNames.size(); i++) {
			String colName = DataTableSpec.getUniqueColumnName(spec, propNames.get(i));
			countCells[i] = new DataColumnSpecCreator(colName, IntCell.TYPE).createSpec();
		}
		creator.addColumns(countCells);

		return creator.createSpec();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DataTableSpec[] configure(final DataTableSpec[] inSpecs) throws InvalidSettingsException {
		autoConfigure(inSpecs);
		// Configure the local settings object
		localSettings = (INodeSettingCollection) settings(WhichCypSettings.class);

		NUM_OUT_CELLS = WhichCypPropertySupport.PROPERTY_NAMES.size() + NUM_IMAGE_CELLS;

		// TODO: generated method stub
		return new DataTableSpec[] { appendSpec(convertTables(inSpecs)[0]) };
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveSettingsTo(final NodeSettingsWO settings) {
		localSettings.saveSettingsTo(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadValidatedSettingsFrom(final NodeSettingsRO settings) throws InvalidSettingsException {

		localSettings.loadValidatedSettingsFrom(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validateSettings(final NodeSettingsRO settings) throws InvalidSettingsException {
		localSettings.validateSettings(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadInternals(final File internDir, final ExecutionMonitor exec)
			throws IOException, CanceledExecutionException {
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveInternals(final File internDir, final ExecutionMonitor exec)
			throws IOException, CanceledExecutionException {
		// TODO: generated method stub
	}

}
