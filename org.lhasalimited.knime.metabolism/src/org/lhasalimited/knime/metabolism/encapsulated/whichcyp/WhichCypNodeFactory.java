package org.lhasalimited.knime.metabolism.encapsulated.whichcyp;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "WhichCyp" Node.
 * Runs WhichCyp 1.2
 *
 * @author Lhasa Limited
 */
public class WhichCypNodeFactory 
        extends NodeFactory<WhichCypNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public WhichCypNodeModel createNodeModel() {
        return new WhichCypNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<WhichCypNodeModel> createNodeView(final int viewIndex,
            final WhichCypNodeModel nodeModel) {
        return new WhichCypNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new WhichCypNodeDialog();
    }

}

