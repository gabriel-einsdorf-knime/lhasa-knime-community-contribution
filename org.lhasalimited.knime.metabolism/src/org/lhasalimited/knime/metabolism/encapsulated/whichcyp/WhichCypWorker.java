package org.lhasalimited.knime.metabolism.encapsulated.whichcyp;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.imageio.ImageIO;

import org.knime.base.data.xml.SvgCellFactory;
import org.knime.core.data.AdapterValue;
import org.knime.core.data.DataCell;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataType;
import org.knime.core.data.MissingCell;
import org.knime.core.data.append.AppendedColumnRow;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.image.png.PNGImageCellFactory;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelColor;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.util.MultiThreadWorker;
import org.lhasalimited.cyp.whichcyp.WhichCypProcessor;
import org.lhasalimited.cyp.whichcyp.WhichCypPropertySupport;
import org.lhasalimited.cyp.whichcyp.WhichCypResults;
import org.lhasalimited.knime.metabolism.encapsulated.util.CdkImageRender;
import org.lhasalimited.knime.metabolism.encapsulated.util.StructureConversion;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.knime.commons.CDKNodeUtils;
import org.openscience.cdk.knime.type.CDKValue;
import org.openscience.cdk.renderer.generators.standard.StandardGenerator;

public class WhichCypWorker extends MultiThreadWorker<DataRow, DataRow> {

	private int columnIndex;
	private BufferedDataContainer outputTable;
	private final ExecutionMonitor exec;
	private final double max;
	private WhichCypProcessor processor;
	private WhichCypSettings settings;

	private int width;
	private int height;
	private Color labelColour;
	private Color structureColour;
	private boolean useSensitivity;

	public WhichCypWorker(int maxQueueSize, int maxParallelWorkers, int structureColIndex, ExecutionMonitor exec,
			long max, BufferedDataContainer outputTable, WhichCypSettings settings, WhichCypProcessor processor) {
		super(maxQueueSize, maxParallelWorkers);
		this.columnIndex = structureColIndex;
		this.outputTable = outputTable;
		this.exec = exec;
		this.max = max;
		this.processor = processor;
		this.settings = settings;

		extractSettings();

	}

	private void extractSettings() {
		labelColour = this.settings.getSetting(WhichCypSettings.CONFIG_COLOUR_LABEL, SettingsModelColor.class)
				.getColorValue();
		structureColour = this.settings.getSetting(WhichCypSettings.CONFIG_COLOUR, SettingsModelColor.class)
				.getColorValue();

		width = this.settings.getSetting(WhichCypSettings.CONFIG_WIDTH, SettingsModelInteger.class).getIntValue();
		height = this.settings.getSetting(WhichCypSettings.CONFIG_HEIGHT, SettingsModelInteger.class).getIntValue();

		useSensitivity = settings
				.getSetting(WhichCypSettings.CONFIG_USE_SENTIVITY_FOR_COLOUR, SettingsModelBoolean.class)
				.getBooleanValue();
	}

	@Override
	protected DataRow compute(DataRow row, long index) throws Exception {
		String[] isoformsOrdering = { "1a2", "2c9", "2c19", "2d6", "3a4" };

		DataRow outRow = null;
		if (row.getCell(columnIndex).isMissing()
				|| (((AdapterValue) row.getCell(columnIndex)).getAdapterError(CDKValue.class) != null)) {
			DataCell[] cells = new DataCell[WhichCypNodeModel.NUM_OUT_CELLS];
			Arrays.fill(cells, DataType.getMissingCell());
			;

			outRow = new AppendedColumnRow(row, cells);
		} else {

			try {
				CDKValue mol = ((AdapterValue) row.getCell(columnIndex)).getAdapter(CDKValue.class);
				IAtomContainer con = CDKNodeUtils.getFullMolecule(mol.getAtomContainer());

				WhichCypResults whichCypResult = processor.process(StructureConversion.writeMolfile(con),
						row.getKey().toString());

				int[][] result = whichCypResult.getIsphormPredictions();
				Map<String, Integer> results = whichCypResult.getResults();

				DataCell[] cells = new DataCell[5 + results.size()];

				for (int i = 0; i < 5; i++) {
					// A clone is taken to allow the generation of different
					// highlights
					// alternatively the properties could be cleared at the end
					// of each
					// iteration

					IAtomContainer clone = StructureConversion.readMolfile(whichCypResult.getMolfile());
					for (int j = 0; j < clone.getAtomCount(); j++) {
						// I represents the isophorm index and j the atom index
						if (result[i][j] > 0) {
							Color tempColour = structureColour;

							// If using sensitivity to adjust the colour we need
							// to identify if this
							// isophorm has the sensitivity warning triggered
							if (useSensitivity) {
								int warning = results.get("SensitivityWarning" + isoformsOrdering[i]);

								if (warning == 1)
									tempColour = Color.GRAY;
							}

							clone.getAtom(j).setProperty(StandardGenerator.HIGHLIGHT_COLOR, tempColour);
							clone.setProperty(StandardGenerator.ANNOTATION_LABEL, "Isomorph: " + isoformsOrdering[i]);
						}

					}

					clone.setID(isoformsOrdering[i]);

					if (settings.getSetting(WhichCypSettings.CONFIG_IMAGE_FORMAT, SettingsModelString.class)
							.getStringValue().equals("PNG")) {
						Image img = CdkImageRender.renderImage(clone, width, height, labelColour);
						cells[i] = createImageCell(img);
					} else {
						cells[i] = new SvgCellFactory()
								.createCell(CdkImageRender.renderSvg(clone, width, height, labelColour));
					}

				}

				// Iterate through all the properties creating the cells. This
				// iteration should occur
				// in the same order as when the configuration for the output
				// table was done. So we
				// don't need to handle the order manually.
				int track = 5;
				for (String prop : WhichCypPropertySupport.PROPERTY_NAMES) {
					try {
						cells[track] = new IntCell(results.get(prop));
					} catch (Exception e) {
						e.printStackTrace();
						cells[track] = new MissingCell(e.getMessage());
					} finally {
						track++;
					}
				}

				outRow = new AppendedColumnRow(row, cells);
			} catch (Exception e) {
				e.printStackTrace();
				DataCell[] missing = new DataCell[5 + WhichCypPropertySupport.PROPERTY_NAMES.size()];
				Arrays.fill(missing, new MissingCell(e.getMessage()));

				outRow = new AppendedColumnRow(row, missing);
			}
		}

		return outRow;
	}

	/**
	 * Create the PNG image cell, SVG's are handled differently
	 * 
	 * @param img
	 * @return
	 * @throws IOException
	 */
	private DataCell createImageCell(Image img) throws IOException {
		File tmp = File.createTempFile("temp-smartCyp", ".png");
		ImageIO.write((RenderedImage) img, "PNG", tmp);
		return new PNGImageCellFactory().createCell(new FileInputStream(tmp));
	}

	@Override
	protected void processFinished(ComputationTask task)
			throws ExecutionException, CancellationException, InterruptedException {
		DataRow row = task.get();
		if (!row.getCell(columnIndex).isMissing()) {
			WhichCypNodeModel.LOGGER.debug("RowKey: " + row.getKey());
			outputTable.addRowToTable(row);
		} else {
			WhichCypNodeModel.LOGGER.error("CDK Cell was not created for row: " + row.getKey());
		}

		exec.setProgress(this.getFinishedCount() / max, this.getFinishedCount() + " (active/submitted: "
				+ this.getActiveCount() + "/" + (this.getSubmittedCount() - this.getFinishedCount()) + ")");

		try {
			exec.checkCanceled();
		} catch (CanceledExecutionException cee) {
			throw new CancellationException();
		}
	}

}
