package org.lhasalimited.knime.metabolism.encapsulated.whichcyp;

import java.awt.Color;
import java.util.TreeMap;

import org.knime.core.node.defaultnodesettings.SettingsModel;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelColor;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnName;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.lhasalimited.knime.metabolism.encapsulated.util.NodeSettingCollection;

public class WhichCypSettings extends NodeSettingCollection {

	public static final String CONFIG_WIDTH = "cfgWidth";
	public static final String CONFIG_HEIGHT = "cfgHeight";
	public static final String CONFIG_COLOUR = "cfgColour";
	public static final String CONFIG_USE_SENTIVITY_FOR_COLOUR = "cfgSetColour";
	public static final String CONFIG_COLOUR_LABEL = "cfgLabelColour";
	public static final String CONFIG_IMAGE_FORMAT = "cfgImageFormat";

	public WhichCypSettings() {
		super();
		addSettings();
	}

	@Override
	protected void addSettings() {
		settingMap = new TreeMap<String, SettingsModel>();
		settingMap.put(CONFIG_STRUCTURE_COLUMN, new SettingsModelColumnName(CONFIG_STRUCTURE_COLUMN, "Structure"));

		settingMap.put(CONFIG_WIDTH, new SettingsModelInteger(CONFIG_WIDTH, 250));
		settingMap.put(CONFIG_HEIGHT, new SettingsModelInteger(CONFIG_HEIGHT, 250));
		settingMap.put(CONFIG_COLOUR, new SettingsModelColor(CONFIG_COLOUR, Color.RED));
		settingMap.put(CONFIG_USE_SENTIVITY_FOR_COLOUR,
				new SettingsModelBoolean(CONFIG_USE_SENTIVITY_FOR_COLOUR, false));
		settingMap.put(CONFIG_COLOUR_LABEL, new SettingsModelColor(CONFIG_COLOUR_LABEL, Color.BLACK));
		settingMap.put(CONFIG_IMAGE_FORMAT, new SettingsModelString(CONFIG_IMAGE_FORMAT, "PNG"));
	}

}
