package org.lhasalimited.knime.metabolism.encapsulated.smartcyp;

import java.io.File;
import java.io.IOException;

import org.knime.base.data.xml.SvgCellFactory;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.DataType;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.data.image.png.PNGImageCellFactory;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeLogger;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnName;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.lhasalimited.cyp.util.smartcyp.SmartCypProcessor;
import org.lhasalimited.knime.metabolism.encapsulated.util.SmartCypSettings;
import org.lhasalimited.knime.metabolism.encapsulated.util.SmartCypWorkerDep;
import org.openscience.cdk.knime.core.CDKAdapterNodeModel;
import org.openscience.cdk.knime.type.CDKAdapterCell;
import org.openscience.cdk.knime.type.CDKCell3;

/**
 * This is the model implementation of SMARTCyp. SMARTCyp 2.4.2
 *
 * @author Lhasa Limited
 */
public class SMARTCypNodeModel extends CDKAdapterNodeModel
{
	
	public static final int NUM_OUT_CELLS = 16;
	public static final int NUM_EXTRA_CELLS = 4;

	public static NodeLogger LOGGER = NodeLogger.getLogger(SMARTCypNodeModel.class);

	private int structureColIndex;
	private int idColumnIndex;

	private SmartCypSettings localSettings;

	/**
	 * Constructor for the node model.
	 */
	protected SMARTCypNodeModel()
	{
		super(1, 2, new SmartCypSettings());
		localSettings = (SmartCypSettings) settings;
	}

	@Override
	protected BufferedDataTable[] process(BufferedDataTable[] convertedTables, ExecutionContext exec) throws Exception
	{
		BufferedDataContainer outputTable = exec.createDataContainer(appendSpec(convertedTables[0].getDataTableSpec()));
		BufferedDataContainer outputTableDetailed = exec.createDataContainer(createOutputSpecDetailed(convertedTables[0].getDataTableSpec()));

		
		SmartCypProcessor processor = new SmartCypProcessor(
				localSettings.getSetting(SmartCypSettings.CONFIG_N_OXIDATION_CORRECTION, SettingsModelBoolean.class).getBooleanValue());

		SmartCypWorkerDep worker = new SmartCypWorkerDep(maxQueueSize, maxParallelWorkers, columnIndex, exec.createSubProgress(1), convertedTables[0].size(),
				outputTable, outputTableDetailed, settings(SmartCypSettings.class), processor, idColumnIndex);
		
		try
		{
			worker.run(convertedTables[0]);
		} catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		finally
		{
			outputTable.close();
			outputTableDetailed.close();
		}

		if(convertedTables[0].size() != outputTable.size())
			setWarningMessage("Some molecules couldn't be processed. Output table has " + (convertedTables[0].size() - outputTable.size()) + " fewer rows.");
			
		return new BufferedDataTable[] { outputTable.getTable(), outputTableDetailed.getTable() };
	}

	/**
	 * Create the detailed output specification
	 * 
	 * @param inSpec
	 * @return
	 */
	private DataTableSpec createOutputSpecDetailed(DataTableSpec inSpec)
	{
		DataColumnSpec[] outSpecs = new DataColumnSpec[NUM_OUT_CELLS];

		outSpecs[0] = idColumnIndex == -1 ? new DataColumnSpecCreator("RowID", StringCell.TYPE).createSpec() : inSpec.getColumnSpec(idColumnIndex);
		outSpecs[1] = new DataColumnSpecCreator(
				localSettings.getSetting(SmartCypSettings.CONFIG_STRUCTURE_COLUMN, SettingsModelColumnName.class).getColumnName(), CDKAdapterCell.RAW_TYPE)
						.createSpec();
		outSpecs[2] = new DataColumnSpecCreator("Atom", StringCell.TYPE).createSpec();
		outSpecs[3] = new DataColumnSpecCreator("Atom ID", IntCell.TYPE).createSpec();
		outSpecs[4] = new DataColumnSpecCreator("Ranking", IntCell.TYPE).createSpec();
		outSpecs[5] = new DataColumnSpecCreator("Score", DoubleCell.TYPE).createSpec();
		outSpecs[6] = new DataColumnSpecCreator("Energy", DoubleCell.TYPE).createSpec();
		outSpecs[7] = new DataColumnSpecCreator("Accessibility", DoubleCell.TYPE).createSpec();
		outSpecs[8] = new DataColumnSpecCreator("2D6 score", DoubleCell.TYPE).createSpec();
		outSpecs[9] = new DataColumnSpecCreator("2D5 ranking", IntCell.TYPE).createSpec();
		outSpecs[10] = new DataColumnSpecCreator("Span 2 End", DoubleCell.TYPE).createSpec();
		outSpecs[11] = new DataColumnSpecCreator("N+Dist", IntCell.TYPE).createSpec();
		outSpecs[12] = new DataColumnSpecCreator("2C ranking", IntCell.TYPE).createSpec();
		outSpecs[13] = new DataColumnSpecCreator("2C score", DoubleCell.TYPE).createSpec();
		outSpecs[14] = new DataColumnSpecCreator("COO Dist", IntCell.TYPE).createSpec();
		outSpecs[15] = new DataColumnSpecCreator("2DSASA", DoubleCell.TYPE).createSpec();

		return new DataTableSpec(outSpecs);
	}

	private DataTableSpec appendSpec(DataTableSpec spec)
	{
		
		DataType imageType = localSettings.getSetting(SmartCypSettings.CONFIG_IMAGE_FORMAT, SettingsModelString.class)
				.getStringValue().equals("PNG") ? PNGImageCellFactory.TYPE : SvgCellFactory.TYPE;

		
		// Standard image
		String summaryImageColName = settings(SmartCypSettings.class).targetColumn() + " standard image";
		summaryImageColName = DataTableSpec.getUniqueColumnName(spec, summaryImageColName);

		DataColumnSpecCreator imageCellSpecCreator = new DataColumnSpecCreator(summaryImageColName, imageType);

		// CYP2C image
		String cyp2cImageColName = settings(SmartCypSettings.class).targetColumn() + " CYP2C image";
		cyp2cImageColName = DataTableSpec.getUniqueColumnName(spec, cyp2cImageColName);

		DataColumnSpecCreator cyp2cImageCellSpecCreator = new DataColumnSpecCreator(cyp2cImageColName, imageType);
		
		/// CPY2D6 image
		String cyp2D6ImageColName = settings(SmartCypSettings.class).targetColumn() + " CYP2D6 image";
		cyp2D6ImageColName = DataTableSpec.getUniqueColumnName(spec, cyp2D6ImageColName);

		DataColumnSpecCreator cyp2D6ImageCellSpecCreator = new DataColumnSpecCreator(cyp2D6ImageColName, imageType);
		
		// CDK Standard cell
		
		String cdkCellResultColName = settings(SmartCypSettings.class).targetColumn() + " standard";
		cdkCellResultColName = DataTableSpec.getUniqueColumnName(spec, cdkCellResultColName);

		DataColumnSpecCreator cdkCellSpecCreator = new DataColumnSpecCreator(cdkCellResultColName, CDKCell3.TYPE);

		
		
		DataColumnSpec imageCellSpec = imageCellSpecCreator.createSpec();
		DataColumnSpec image2CCellSpec = cyp2cImageCellSpecCreator.createSpec();
		DataColumnSpec image2D6CellSpec = cyp2D6ImageCellSpecCreator.createSpec();
		DataColumnSpec cdkCellSpec = cdkCellSpecCreator.createSpec();

		DataTableSpec intermediateSpec = new DataTableSpec(spec, new DataTableSpec(imageCellSpec, image2CCellSpec, image2D6CellSpec, cdkCellSpec));

		return intermediateSpec;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void reset()
	{
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DataTableSpec[] configure(final DataTableSpec[] inSpecs) throws InvalidSettingsException
	{
		autoConfigure(inSpecs);
		DataTableSpec outSpec = convertTables(inSpecs)[0];

		localSettings = settings(SmartCypSettings.class);

		idColumnIndex = inSpecs[0]
				.findColumnIndex(localSettings.getSetting(SmartCypSettings.CONFIG_IDENTIFIER_COLUMN, SettingsModelColumnName.class).getColumnName());
//		structureColIndex = inSpecs[0]
//				.findColumnIndex(localSettings.getSetting(SmartCypSettings.CONFIG_STRUCTURE_COLUMN, SettingsModelColumnName.class).getColumnName());

		return new DataTableSpec[] { appendSpec(outSpec), createOutputSpecDetailed(inSpecs[0]) };
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveSettingsTo(final NodeSettingsWO settings)
	{
		localSettings.saveSettingsTo(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadValidatedSettingsFrom(final NodeSettingsRO settings) throws InvalidSettingsException
	{
//		localSettings = new SmartCypSettings();
		localSettings.loadSettings(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validateSettings(final NodeSettingsRO settings) throws InvalidSettingsException
	{
		localSettings.validateSettings(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadInternals(final File internDir, final ExecutionMonitor exec) throws IOException, CanceledExecutionException
	{
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveInternals(final File internDir, final ExecutionMonitor exec) throws IOException, CanceledExecutionException
	{
		// TODO: generated method stub
	}

}
