package org.lhasalimited.knime.metabolism.encapsulated.smartcyp;

import java.util.Arrays;

import org.knime.core.data.DataTableSpec;
import org.knime.core.data.DataValue;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.NotConfigurableException;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentBoolean;
import org.knime.core.node.defaultnodesettings.DialogComponentColorChooser;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnNameSelection;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;
import org.knime.core.node.defaultnodesettings.DialogComponentStringSelection;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelColor;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnName;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.lhasalimited.knime.metabolism.encapsulated.util.SmartCypSettings;
import org.lhasalimited.knime.metabolism.encapsulated.whichcyp.WhichCypSettings;

/**
 * <code>NodeDialog</code> for the "SMARTCyp" Node. SMARTCyp 2.4.2
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more
 * complex dialog please derive directly from
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Lhasa Limited
 */
public class SMARTCypNodeDialog extends DefaultNodeSettingsPane
{

	private final SmartCypSettings m_settings = new SmartCypSettings();

	/**
	 * New pane for configuring the SMARTCyp node.
	 */
	protected SMARTCypNodeDialog()
	{
		createNewGroup("Input settings");
		setHorizontalPlacement(true);
		// Select a structure column TODO: make it only allow selection from a
		// chemistry adapter cell type
		addDialogComponent(new DialogComponentColumnNameSelection(
				m_settings.getSetting(SmartCypSettings.CONFIG_STRUCTURE_COLUMN, SettingsModelColumnName.class), "Structure", 0, DataValue.class));

		// Select a structure column TODO: make it only allow selection from a
		// chemistry adapter cell type
		addDialogComponent(new DialogComponentColumnNameSelection(
				m_settings.getSetting(SmartCypSettings.CONFIG_IDENTIFIER_COLUMN, SettingsModelColumnName.class), "ID", 0, DataValue.class));

		createNewGroup("SMARTCyp settings");
		setHorizontalPlacement(false);
		setHorizontalPlacement(true);
		// N-Oxidation correction
		addDialogComponent(new DialogComponentBoolean(m_settings.getSetting(SmartCypSettings.CONFIG_N_OXIDATION_CORRECTION, SettingsModelBoolean.class),
				"N Oxidation correction"));
		
		// SMARTCyp file output index's from 1, so the default behaviour is false to allow this to be recreated
		// and be consistent with the original release of this node
		addDialogComponent(new DialogComponentBoolean(m_settings.getSetting(SmartCypSettings.CONFIG_INDEX_FROM_ZERO, SettingsModelBoolean.class),
				"Index from 0"));

		setHorizontalPlacement(false);
		createNewGroup("Filter settings");
		setHorizontalPlacement(true);
		addDialogComponent(
				new DialogComponentNumber(m_settings.getSetting(SmartCypSettings.CONFIG_RANKING_CUTOFF, SettingsModelInteger.class), "Ranking cutoff", 1));

		addDialogComponent(new DialogComponentBoolean(m_settings.getSetting(SmartCypSettings.CONFIG_FILTER_DETAILED, SettingsModelBoolean.class),
				"Filter detailed table"));

		createNewTab("Image settings");
		setHorizontalPlacement(true);
		addDialogComponent(new DialogComponentNumber(m_settings.getSetting(SmartCypSettings.CONFIG_WIDTH, SettingsModelInteger.class), "Width", 10));
		addDialogComponent(new DialogComponentNumber(m_settings.getSetting(SmartCypSettings.CONFIG_HEIGHT, SettingsModelInteger.class), "Height", 10));
		addDialogComponent(new DialogComponentStringSelection(
				m_settings.getSetting(WhichCypSettings.CONFIG_IMAGE_FORMAT, SettingsModelString.class), "Output format",
				Arrays.asList(new String[] { "PNG", "SVG" })));

		setHorizontalPlacement(false);
		createNewGroup("Structure highlight");
		setHorizontalPlacement(true);
		addDialogComponent(new DialogComponentColorChooser(m_settings.getSetting(SmartCypSettings.CONFIG_COLOUR, SettingsModelColor.class), "Colour", true));
		addDialogComponent(
				new DialogComponentBoolean(m_settings.getSetting(SmartCypSettings.CONFIG_SET_COLOUR, SettingsModelBoolean.class), "Overide default colour"));
		setHorizontalPlacement(false);
		createNewGroup("Label settings");
		setHorizontalPlacement(true);
		addDialogComponent(
				new DialogComponentColorChooser(m_settings.getSetting(SmartCypSettings.CONFIG_COLOUR_LABEL, SettingsModelColor.class), "Label colour", true));

	}
	
	@Override
	public void loadAdditionalSettingsFrom(NodeSettingsRO settings, DataTableSpec[] specs) throws NotConfigurableException
	{
		// TODO Auto-generated method stub
		super.loadAdditionalSettingsFrom(settings, specs);

		m_settings.loadSettingsForDialog(settings);

	}

	@Override
	public void saveAdditionalSettingsTo(NodeSettingsWO settings) throws InvalidSettingsException
	{
		// TODO Auto-generated method stub
		super.saveAdditionalSettingsTo(settings);
		m_settings.saveSettingsTo(settings);
	}
}
