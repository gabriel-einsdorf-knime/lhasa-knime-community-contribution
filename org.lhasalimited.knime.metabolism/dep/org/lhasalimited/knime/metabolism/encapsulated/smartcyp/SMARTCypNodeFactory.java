package org.lhasalimited.knime.metabolism.encapsulated.smartcyp;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "SMARTCyp" Node.
 * SMARTCyp 2.4.2
 *
 * @author Lhasa Limited
 */
public class SMARTCypNodeFactory 
        extends NodeFactory<SMARTCypNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public SMARTCypNodeModel createNodeModel() {
        return new SMARTCypNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<SMARTCypNodeModel> createNodeView(final int viewIndex,
            final SMARTCypNodeModel nodeModel) {
        return new SMARTCypNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new SMARTCypNodeDialog();
    }

}

