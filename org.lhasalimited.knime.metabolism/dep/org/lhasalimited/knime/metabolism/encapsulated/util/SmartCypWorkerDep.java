package org.lhasalimited.knime.metabolism.encapsulated.util;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.imageio.ImageIO;

import org.knime.base.data.xml.SvgCellFactory;
import org.knime.core.data.AdapterValue;
import org.knime.core.data.DataCell;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataType;
import org.knime.core.data.MissingCell;
import org.knime.core.data.append.AppendedColumnRow;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.data.image.png.PNGImageCellFactory;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelColor;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.util.MultiThreadWorker;
import org.lhasalimited.cyp.util.smartcyp.AtomValues;
import org.lhasalimited.cyp.util.smartcyp.SmartCypColour;
import org.lhasalimited.cyp.util.smartcyp.SmartCypProcessor;
import org.lhasalimited.cyp.util.smartcyp.SmartCypResults;
import org.lhasalimited.knime.metabolism.encapsulated.smartcyp.SMARTCypNodeModel;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.knime.commons.CDKNodeUtils;
import org.openscience.cdk.knime.type.CDKCell3;
import org.openscience.cdk.knime.type.CDKValue;
import org.openscience.cdk.renderer.generators.standard.StandardGenerator;

/**
 * A threaded worker class for processing {@link DataRow} objects through
 * SMARTCyp and adding their results to a summary and detailed output
 * {@link BufferedDataContainer}
 * 
 * @author Samuel
 * @since KNIMe 3.1, plugin 1.0
 *
 */
public class SmartCypWorkerDep extends MultiThreadWorker<DataRow, KnimeSmartCypResults>
{

	private final SmartCypProcessor processor;
	private final ExecutionMonitor exec;
	private final double max;
	private final int columnIndex;
	private final BufferedDataContainer bdc;
	private final BufferedDataContainer bdcDetailed;
	private final SmartCypSettings settings;
	private final int idIndex;

	private int rankingCutoff;
	private Color labelColour;
	private int width;
	private int height;

	/**
	 * 
	 * @param maxQueueSize
	 * @param maxActiveInstanceSize
	 * @param columnIndex
	 *            The structure column index
	 * @param exec
	 * @param max
	 * @param bdc
	 *            The summary output table
	 * @param bdcDetailed
	 *            The detailed output table
	 * @param settings
	 *            The settings (linked with the dialog)
	 * @param processor
	 *            The {@link SmartCypProcessor} instance
	 * @param idIndex
	 *            the index of the identifier column
	 */
	public SmartCypWorkerDep(final int maxQueueSize, final int maxActiveInstanceSize, final int columnIndex,
			final ExecutionMonitor exec, final long max, final BufferedDataContainer bdc,
			final BufferedDataContainer bdcDetailed, SmartCypSettings settings, SmartCypProcessor processor,
			int idIndex)
	{
		super(maxQueueSize, maxActiveInstanceSize);

		this.processor = processor;
		this.exec = exec;
		this.bdc = bdc;
		this.max = max;
		this.settings = settings;
		this.columnIndex = columnIndex;
		this.idIndex = idIndex;
		this.bdcDetailed = bdcDetailed;

		rankingCutoff = settings.getSetting(SmartCypSettings.CONFIG_RANKING_CUTOFF, SettingsModelInteger.class)
				.getIntValue();
		labelColour = settings.getSetting(SmartCypSettings.CONFIG_COLOUR_LABEL, SettingsModelColor.class)
				.getColorValue();

		width = settings.getSetting(SmartCypSettings.CONFIG_WIDTH, SettingsModelInteger.class).getIntValue();
		height = settings.getSetting(SmartCypSettings.CONFIG_HEIGHT, SettingsModelInteger.class).getIntValue();

	}

	@Override
	protected KnimeSmartCypResults compute(DataRow row, long index) throws Exception
	{
		// Setup variables for output
		DataRow summary = null;
		List<DataRow> detailedResults = null;

		DataCell outCell = null;
		DataCell outStructureCell = null;
		if (row.getCell(columnIndex).isMissing()
				|| (((AdapterValue) row.getCell(columnIndex)).getAdapterError(CDKValue.class) != null))
		{
			outCell = DataType.getMissingCell();
			outStructureCell = DataType.getMissingCell();
			summary = new AppendedColumnRow(row, new DataCell[] { outCell, outStructureCell });
			detailedResults = new ArrayList<DataRow>();
			SMARTCypNodeModel.LOGGER.debug("CDK structure is missing or not compatible");
		} else
		{
			List<SmartCypResults> results;
			try
			{
				CDKValue mol = ((AdapterValue) row.getCell(columnIndex)).getAdapter(CDKValue.class);
				IAtomContainer con = CDKNodeUtils.getFullMolecule(mol.getAtomContainer());

				String id = idIndex == -1 ? row.getKey().getString() : row.getCell(idIndex).toString();

				results = processor.process(StructureConversion.writeMolfile(con), id);

				// Create the images for the first output table
				// There are three highlighting outputs from SMARTCyp. The
				// standard and 2 specific models
				// These are highlighted and then created as PNG images (TODO:
				// Update to allow SVG when CDK
				// is updated to include the depic packages)
				DataCell imageCell;
				DataCell imageCellCyp2C;
				DataCell imageCellCyp2D6;

				SmartCypResults result = results.get(0);
				IAtomContainer structure = StructureConversion.readMolfile(result.getMolfile());
				// IAtomContainer structureCloned = structure.clone();

				IAtomContainer standard = createStandard(structure, result);
				try
				{

					if (settings.getSetting(SmartCypSettings.CONFIG_IMAGE_FORMAT, SettingsModelString.class)
							.getStringValue().equals("PNG"))
					{
						Image imgStandard = CdkImageRender.renderImage(standard, width, height, labelColour);
						Image imgCyp2c = CdkImageRender.renderImage(createCyp2c(structure, result), width, height,
								labelColour);
						Image imgCyp2d6 = CdkImageRender.renderImage(createCyp2d6(structure, result), width, height,
								labelColour);

						imageCell = createImageCell(imgStandard);
						imageCellCyp2C = createImageCell(imgCyp2c);
						imageCellCyp2D6 = createImageCell(imgCyp2d6);
					} else
					{
						imageCell = SvgCellFactory
								.create(CdkImageRender.renderSvg(standard, width, height, labelColour));
						imageCellCyp2C = SvgCellFactory.create(
								CdkImageRender.renderSvg(createCyp2c(structure, result), width, height, labelColour));
						imageCellCyp2D6 = SvgCellFactory.create(
								CdkImageRender.renderSvg(createCyp2d6(structure, result), width, height, labelColour));
					}

				} catch (Exception e)
				{
					SMARTCypNodeModel.LOGGER.error("Failed to render images for row: " + row.getKey());
					// SMARTCypNodeModel.LOGGER.debug(e.getClass().toString() +
					// ": " + e.getMessage());
					SMARTCypNodeModel.LOGGER.error("Failed: ", e);
					e.printStackTrace();

					imageCell = new MissingCell(e.getMessage());
					imageCellCyp2C = new MissingCell(e.getMessage());
					imageCellCyp2D6 = new MissingCell(e.getMessage());
				}

				outStructureCell = new CDKCell3(standard);

				summary = new AppendedColumnRow(row,
						new DataCell[] { imageCell, imageCellCyp2C, imageCellCyp2D6, outStructureCell });
				detailedResults = createRows(row, structure, result, rankingCutoff);

				// Check that the right number of detailed output rows are
				// created
				// If now throw an exception
				// this check doesn't work as multiple atoms can have the same
				// rank
				// int atomCount = 0;
				// for (IAtom atom : structure.atoms())
				// {
				// if (atom.getSymbol().equals("C") ||
				// atom.getSymbol().equals("N") || atom.getSymbol().equals("P")
				// || atom.getSymbol().equals("S"))
				// atomCount++;
				// }
				//
				// if (detailedResults.size() != atomCount &&
				// detailedResults.size() != rankingCutoff)
				// throw new IOException("The detailed output row size (" +
				// detailedResults.size() + ") is not equal to the expected
				// count (" + atomCount + ")");

			} catch (Exception e)
			{
				e.printStackTrace();
				SMARTCypNodeModel.LOGGER.error("Failed to process row: " + row.getKey() + " appending missing values");
				SMARTCypNodeModel.LOGGER.debug(e.getMessage());
				exec.setMessage("Node ran with issues");
				summary = new AppendedColumnRow(row,
						new DataCell[] { new MissingCell(e.getMessage()), new MissingCell(e.getMessage()),
								new MissingCell(e.getMessage()), new MissingCell(e.getMessage()) });
				detailedResults = new ArrayList<DataRow>();
			}
		}

		return new KnimeSmartCypResults(summary, detailedResults);
	}

	/**
	 * Create the structure highlight properties to represent the ranking of the
	 * CYP2D6 model
	 * 
	 * @param structure
	 * @param result
	 * @return
	 * @throws CloneNotSupportedException
	 */
	private IAtomContainer createCyp2d6(IAtomContainer structure, SmartCypResults result)
			throws CloneNotSupportedException
	{
		IAtomContainer structureCloned = structure.clone();
		for (int atomIndex = 0; atomIndex < structureCloned.getAtomCount(); atomIndex++)
		{
			IAtom currentAtom = structureCloned.getAtom(atomIndex);
			String currentAtomType = currentAtom.getSymbol();

			if (currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P")
					|| currentAtomType.equals("S"))
			{
				int ranking = result.getAtom(atomIndex).getRanking2d6();
				if (ranking <= rankingCutoff || rankingCutoff == 0)
				{
					structureCloned.getAtom(atomIndex).setProperty(StandardGenerator.ANNOTATION_LABEL, ranking + "");
					structureCloned.getAtom(atomIndex).setProperty(StandardGenerator.HIGHLIGHT_COLOR,
							getColour(ranking));
				}
			}
		}

		return structureCloned;
	}

	/**
	 * Create the structure highlight properties to represent the ranking of the
	 * CYP2C model
	 * 
	 * @param structure
	 * @param result
	 * @return
	 * @throws CloneNotSupportedException
	 */
	private IAtomContainer createCyp2c(IAtomContainer structure, SmartCypResults result)
			throws CloneNotSupportedException
	{
		IAtomContainer structureCloned = structure.clone();
		for (int atomIndex = 0; atomIndex < structureCloned.getAtomCount(); atomIndex++)
		{
			IAtom currentAtom = structureCloned.getAtom(atomIndex);
			String currentAtomType = currentAtom.getSymbol();

			if (currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P")
					|| currentAtomType.equals("S"))
			{
				int ranking = result.getAtom(atomIndex).getRanking2C9();
				if (ranking <= rankingCutoff || rankingCutoff == 0)
				{
					structureCloned.getAtom(atomIndex).setProperty(StandardGenerator.ANNOTATION_LABEL, ranking + "");
					structureCloned.getAtom(atomIndex).setProperty(StandardGenerator.HIGHLIGHT_COLOR,
							getColour(ranking));
				}
			}
		}

		return structureCloned;
	}

	/**
	 * Set the highlight properties to represent the ranking of the standard
	 * model
	 * 
	 * @param structure
	 * @param result
	 * @return
	 * @throws CloneNotSupportedException
	 */
	private IAtomContainer createStandard(IAtomContainer structure, SmartCypResults result)
			throws CloneNotSupportedException
	{
		IAtomContainer structureCloned = structure.clone();
		for (int atomIndex = 0; atomIndex < structureCloned.getAtomCount(); atomIndex++)
		{
			IAtom currentAtom = structureCloned.getAtom(atomIndex);
			String currentAtomType = currentAtom.getSymbol();

			if (currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P")
					|| currentAtomType.equals("S"))
			{
				int ranking = result.getAtom(atomIndex).getRanking();
				if (ranking <= rankingCutoff || rankingCutoff == 0)
				{
					structureCloned.getAtom(atomIndex).setProperty(StandardGenerator.ANNOTATION_LABEL, ranking + "");
					structureCloned.getAtom(atomIndex).setProperty(StandardGenerator.HIGHLIGHT_COLOR,
							getColour(ranking));
				}
			}
		}

		return structureCloned;
	}

	/**
	 * Create the image cell with the highlighting from the given image
	 * 
	 * @param img
	 * @return
	 * @throws IOException
	 */
	private DataCell createImageCell(Image img) throws IOException
	{
		File tmp = File.createTempFile("temp-smartCyp", ".png");
		ImageIO.write((RenderedImage) img, "PNG", tmp);
		return new PNGImageCellFactory().createCell(new FileInputStream(tmp));
	}

	/**
	 * Get the colour for the highlight either based on the dialog settings or
	 * the automatic value
	 * 
	 * @param ranking
	 * @return
	 */
	private Color getColour(int ranking)
	{

		boolean overide = settings.getSetting(SmartCypSettings.CONFIG_SET_COLOUR, SettingsModelBoolean.class)
				.getBooleanValue();
		Color col = null;
		if (overide)
		{
			col = settings.getSetting(SmartCypSettings.CONFIG_COLOUR, SettingsModelColor.class).getColorValue();
		} else
		{
			col = SmartCypColour.getColourForRank(ranking);
		}

		return col;
	}

	/**
	 * Create the output rows for the given version of SmartCyp
	 * 
	 * @param source
	 * @param moleculeKU
	 * @param rankCutoff
	 * @return
	 * @throws CloneNotSupportedException
	 */
	private List<DataRow> createRows(DataRow source, IAtomContainer structure, SmartCypResults result, int rankCutoff)
			throws CloneNotSupportedException
	{
		double ERROR_VALUE = 999d;

		List<DataRow> rows = new ArrayList<DataRow>();

		for (int atomIndex = 0; atomIndex < structure.getAtomCount(); atomIndex++)
		{

			IAtom currentAtom = structure.getAtom(atomIndex);
			String currentAtomType = currentAtom.getSymbol();

			if (currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P")
					|| currentAtomType.equals("S"))
			{

				AtomValues values = result.getAtom(atomIndex);

				Number ranking = values.getRanking();

				// Highlight the atom
				IAtomContainer cloned = structure.clone();
				cloned.getAtom(atomIndex).setProperty(StandardGenerator.HIGHLIGHT_COLOR, getColour((int) ranking));

				DataCell[] cells = new DataCell[SMARTCypNodeModel.NUM_OUT_CELLS];

				if (idIndex == -1)
				{
					cells[0] = new StringCell(source.getKey().toString());
				} else
				{
					cells[0] = source.getCell(idIndex);
				}

				cells[1] = CDKCell3.createCDKCell(cloned);

				// TODO: refactor this to be more inline with the switch to the
				// encapsualted SmartCyp approach
				Number score = ERROR_VALUE;
				Number energy = ERROR_VALUE;
				if (values.getScore() != null)
				{
					score = values.getScore();
					energy = values.getEnergy();
				}

				Number accessibility = values.getAccessibility();

				Number score2d6 = ERROR_VALUE;
				Number ranking2d6 = ERROR_VALUE;
				if (values.getScore2D6() != null)
				{
					ranking2d6 = values.getRanking2d6();
					score2d6 = values.getScore2D6();
				}

				Number span2end = values.getSpan2end();

				Number dist2protamine = 0;
				if (values.getDist2ProtAmine() != null)
				{
					dist2protamine = values.getDist2ProtAmine();
				}

				Number ranking2c9 = ERROR_VALUE;
				Number score2c9 = ERROR_VALUE;
				if (values.getScore2C9() != null)
				{
					ranking2c9 = values.getRanking2C9();
					score2c9 = values.getScore2C9();
				}

				Number dist2cooh = 0;
				if (values.getDist2CarboxylicAcid() != null)
				{
					dist2cooh = values.getDist2CarboxylicAcid();
				}

				Number sasa2d = 0;
				if (values.getSASA2D() != null)
				{
					sasa2d = values.getSASA2D();
				}

				int indexToReport = settings
						.getSetting(SmartCypSettings.CONFIG_INDEX_FROM_ZERO, SettingsModelBoolean.class)
						.getBooleanValue() ? atomIndex : atomIndex + 1;

				cells[2] = new StringCell(currentAtom.getSymbol());
				cells[3] = new IntCell(indexToReport);
				cells[4] = new IntCell((int) ranking);
				cells[5] = new DoubleCell(score.doubleValue());
				cells[6] = new DoubleCell(energy.doubleValue());
				cells[7] = new DoubleCell(accessibility.doubleValue());
				cells[8] = new DoubleCell(score2d6.doubleValue());
				cells[9] = new IntCell((int) ranking2d6);
				cells[10] = new DoubleCell(span2end.doubleValue());
				cells[11] = new IntCell((int) dist2protamine);
				cells[12] = new IntCell((int) ranking2c9);
				cells[13] = new DoubleCell(score2c9.doubleValue());
				cells[14] = new IntCell((int) dist2cooh);
				cells[15] = new DoubleCell(sasa2d.doubleValue());

				boolean filter = settings
						.getSetting(SmartCypSettings.CONFIG_FILTER_DETAILED, SettingsModelBoolean.class)
						.getBooleanValue();
				if (((int) ranking <= (rankCutoff) && filter) || !filter)
				{
					rows.add(new DefaultRow(source.getKey() + "_" + (atomIndex + 1), cells));
				}
			}
		}

		return rows;
	}

	/**
	 * Handle adding the summary and detailed rows to the output tables. Also
	 * updates the progress of the node
	 */
	@Override
	protected void processFinished(ComputationTask task)
			throws ExecutionException, CancellationException, InterruptedException
	{

		KnimeSmartCypResults results = task.get();

		DataRow summary = results.getSummary();
		List<DataRow> detailed = results.getDetailed();

		if (!summary.getCell(columnIndex).isMissing())
		{
			bdc.addRowToTable(summary);

			long start = bdcDetailed.size();

			if (detailed != null)
			{
				for (DataRow row : detailed)
				{
					bdcDetailed.addRowToTable(row);
				}
				SMARTCypNodeModel.LOGGER
						.debug("Added: " + (bdcDetailed.size() - start) + " rows to the detailed output table");
			} else
			{
				SMARTCypNodeModel.LOGGER.debug("No detailed rows added for row: " + summary.getKey().toString());
			}
		}

		exec.setProgress(this.getFinishedCount() / max, this.getFinishedCount() + " (active/submitted: "
				+ this.getActiveCount() + "/" + (this.getSubmittedCount() - this.getFinishedCount()) + ")");

		try
		{
			exec.checkCanceled();
		} catch (CanceledExecutionException cee)
		{
			throw new CancellationException();
		}
	}
}
