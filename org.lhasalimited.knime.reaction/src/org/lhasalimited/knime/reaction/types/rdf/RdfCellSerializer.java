package org.lhasalimited.knime.reaction.types.rdf;

import java.io.IOException;

import org.knime.core.data.DataCellDataInput;
import org.knime.core.data.DataCellDataOutput;
import org.knime.core.data.DataCellSerializer;

/**
 * Serialises as a String (which it technically all an RdfCell is
 * @author Samuel
 *
 */
public class RdfCellSerializer implements DataCellSerializer<RdfCell>
{

	@Override
	public void serialize(RdfCell cell, DataCellDataOutput output) throws IOException
	{
		output.writeUTF(((RdfCell) cell).getRdfString());
	}

	@Override
	public RdfCell deserialize(DataCellDataInput input) throws IOException
	{
		String s = new String();
		try
		{
			s = input.readUTF();
		} catch (IOException ex)
		{
			throw ex;
		}

		return new RdfCell(s);
	}

}
