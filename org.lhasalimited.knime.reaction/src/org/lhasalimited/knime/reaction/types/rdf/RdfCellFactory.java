package org.lhasalimited.knime.reaction.types.rdf;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataCellFactory.FromComplexString;
import org.knime.core.data.DataType;
import org.knime.core.data.convert.DataCellFactoryMethod;

public final class RdfCellFactory implements FromComplexString
{

	/** Type for Rxn cells. */
	public static final DataType TYPE = RdfCell.TYPE;

	/**
	 * Factory method to create {@link DataCell} representing Rdf structures.
	 * The returned cell is either of type {@link RdfCell} 
	 * 
	 * @param string
	 *            String representing the Rdf content.
	 * @return DataCell representing Rxn content.
	 * @throws NullPointerException
	 *             if argument is null
	 */
	@DataCellFactoryMethod(name = "String (RDF)")
	public static DataCell create(final String string)
	{
		return new RdfCell(string);
	}
	
    public DataType getDataType() 
    {
        return TYPE;
    }

	@Override
	public DataCell createCell(final String input)
	{
		return create(input);
	}

	
}
