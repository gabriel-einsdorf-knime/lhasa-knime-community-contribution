package org.lhasalimited.knime.reaction.types.rdf;

import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.renderer.AbstractDataValueRendererFactory;
import org.knime.core.data.renderer.DataValueRenderer;


public final class RdfValueRenderFactory extends AbstractDataValueRendererFactory
{
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDescription()
	{
		return "RDF";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DataValueRenderer createRenderer(final DataColumnSpec colSpec)
	{
		return new RdfValueRenderer(getDescription());
	}
}