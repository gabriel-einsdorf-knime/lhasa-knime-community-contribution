package org.lhasalimited.knime.reaction.types.rdf;

import org.knime.core.data.DataValue;
import org.knime.core.data.DataValueComparator;

/**
 * Simply compares the size of the reaction by number of components
 * @author Samuel
 *
 */
public class RdfComparator extends DataValueComparator
{

	@Override
	protected int compareDataValues(DataValue v1, DataValue v2)
	{
		int comp = 0;
		
		if(v1 instanceof RdfValue && v2 instanceof RdfValue)
		{			
			String s1 = ((RdfValue)v1).getRdfString();
			String s2 = ((RdfValue)v2).getRdfString();
						
			comp = s1.compareTo(s2);
		}

		return comp;
	}

}
