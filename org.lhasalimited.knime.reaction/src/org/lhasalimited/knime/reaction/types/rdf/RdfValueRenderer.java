package org.lhasalimited.knime.reaction.types.rdf;

import org.knime.core.data.renderer.MultiLineStringValueRenderer;

@SuppressWarnings("serial")
public final class RdfValueRenderer extends MultiLineStringValueRenderer
{

	public RdfValueRenderer(String description)
	{
		super(description);
	}



	@Override
	protected void setValue(final Object value)
	{
		if (value instanceof RdfValue)
		{
			super.setValue(((RdfValue) value).getRdfString());
		} else
		{
			super.setValue(value);
		}
	}

}
