package org.lhasalimited.knime.reaction.types.rdf;

import javax.swing.Icon;

import org.knime.core.data.DataValue;
import org.knime.core.data.DataValueComparator;
import org.knime.core.data.ExtensibleUtilityFactory;
import org.knime.core.data.convert.DataValueAccessMethod;

/**
 * A Reaction Data File (RDF)  String
 * @author Samuel
 *
 */
public interface RdfValue extends DataValue
{

	public static final ExtensibleUtilityFactory UTILITY = new ExtensibleUtilityFactory(RdfValue.class)
	{
		private final Icon ICON = loadIcon(RdfValue.class, "/icons/rdf.png");

		protected DataValueComparator getComparator()
		{
			return new RdfComparator();
		}

		public Icon getIcon()
		{
			return ICON;
		}

		@Override
		public String getName()
		{
			return "RDF";
		}
		
		@Override
		public String getGroupName()
		{
			return "Chemistry";
		}
	};

	@DataValueAccessMethod(name = "String (RDF)")
	public String getRdfString();

}