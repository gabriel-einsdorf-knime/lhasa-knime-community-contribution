package org.lhasalimited.knime.reaction.types.rdf;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataType;
import org.knime.core.data.StringValue;

/**
 * A Reaction Data File (RDF) cell stores the String representation of a reaction in an MDL format
 * with attached data. This is the reaction equivalent of an SDF.
 * @author Samuel
 *
 */
public class RdfCell extends DataCell implements RdfValue, StringValue
{

	private static final long serialVersionUID = 1L;

	/** Type for Rxn cells. */
	public static final DataType TYPE = DataType.getType(RdfCell.class);

	private final String m_rdfString;

	/**
	 * Creates a new Rdf Cell based on the given String value. This constructor
	 * is used from the {@link RdfCellFactory#create(String) create} method of
	 * the accompanying {@link RdfCellFactory}.
	 * 
	 * @param str
	 *            the String value to store
	 * @throws NullPointerException
	 *             if the given String value is <code>null</code>
	 */
	RdfCell(final String str)
	{
		if (str == null)
		{
			throw new NullPointerException("RDF value must not be null.");
		}
		m_rdfString = str;
	}

	@Override
	public String getStringValue()
	{
		return m_rdfString;
	}

	@Override
	public String getRdfString()
	{
		return m_rdfString;
	}

	@Override
	public String toString()
	{
		return getStringValue();
	}

	@Override
	protected boolean equalsDataCell(DataCell dc)
	{
		if (dc.getType() == getType())
		{
			return ((RdfValue) dc).getRdfString().equals(getRdfString());
		}

		return false;
	}

	@Override
	public int hashCode()
	{
		return m_rdfString.hashCode();
	}

}
