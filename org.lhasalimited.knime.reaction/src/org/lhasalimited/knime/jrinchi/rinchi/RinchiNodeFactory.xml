<?xml version="1.0" encoding="UTF-8"?>
<knimeNode icon="./rinchi.png" type="Manipulator" xmlns="http://knime.org/node/v2.8" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://knime.org/node/v2.10 http://knime.org/node/v2.10.xsd">
    <name>Rxn to RInChI</name>
    
    <shortDescription>
        Generates RInChI and RInChI keys for RXN and RDF strings
    </shortDescription>
    
    <fullDescription>
        <intro>
        <p> 
	        In 2011, the first prototype of RInChI was introduced as version 0.02 based on Python programming  (Grethe, Goodman, Allen, 2013). 
	        To make the resulting libraries comparable with InChI deliveries, to increase the search ability of RInChIKeys in the Web by the 
	        introduction of the Web-RInChIKey and to fix a number of bugs and development gaps in the former version this new version (version 
	        number 1.00) has been created. This version is supposed to be published for general usage by the InChI Trust.
        </p>
        <p>
        	For more details see: <a href="http://www-rinchi.ch.cam.ac.uk/">http://www-rinchi.ch.cam.ac.uk/</a>.
        </p>


        <p>
            <b>RInchi</b><br></br>
            The RInChI format is a hierarchical, layered description of a reaction with different levels based on the Standard 
	        InChI representation (version 1.04) of each structural component participating in the reaction.
        </p>

        <p>
        	<b>Inchi Key</b><br></br>
            RInChIKeys are hashed representations of the RInChI strings. The hashing process creates shorter strings that are unique representations of the original (longer) RInChIs. However, RInChIs (and therefore the original RXN and/or RD file) cannot be rebuilt out of the hashed string. That makes RInChIKeys an encrypted unique depiction of chemical reactions especially suitable for database processes and web operations.
			For the anticipated usage, three different types of RInChIKeys have been developed:
			<ol>
				<li>Long-RInChIKeys are strings concatenated from the InChIKeys of each reaction component. The length of Long-RInChIKeys is flexible.</li>
				<li>Short-RInChIKeys are created by hashing the major and minor layers of InChIs for each group of RInChI to a fixed length string</li>
				<li>Web-RInChIKeys deduplicate InChIs over all groups and hash all major and minor InChI layers into a fixed length string ignoring the specific role of the reaction components.</li>
			</ol>
			All RInChIKeys are generated using the sha2 hashing functionality used by and provided with the InChI algorithm.
        </p>
        

        <p>
                <b>Input format</b><br></br>
         		Accepts RXN or RDF input. Note: If agents occur, the RXN file format is not sufficient as agents in an rxn string are ignored by the RInChI library. 
        </p>
        
        <p>
        	    Instead, the reaction must be described in the RD file format that contains catalysts, solvents and other reagents as molfiles following the reaction 
         		section in RXN format within the RD file section.
        </p>
        
        <p>
                	<br></br>
        	For example:<br></br><br></br>
        	$DTYPE RXN:AGENTS(1):MOLECULES(1):MOLSTRUCTURE <br></br>
        	$DATUM $MFMT <br></br>
        	... molfile.... <br></br>
        	$DTYPE RXN:AGENTS(1):MOLECULES(2):MOLSTRUCTURE <br></br>
        	$DATUM $MFMT <br></br>
        	... molfile.... <br></br>
        </p>


        </intro>
        
        
        <option name="RXN">The reaction (please note the requirement for agent handling))</option>
        <option name="Force equilibrium">To handle a reaction as equilibrium reaction, the related parameter “in_force_equilibrium” of the
			function rinchi_from_file_text or rinchikey_from_file_text must be set to TRUE (default FALSE).
			Note: There is no defined place for the equilibrium parameter in the RXN file definition.
			For equilibrium reactions the comment “NOTE: Reaction is an equilibrium reaction.” is returned in the
			comment section of the rebuilt RXN/RD file</option>
    </fullDescription>
    
    <ports>
        <inPort index="0" name="Reactions">Input reactions in RXN or RDF format. Please note the requirement for agent handling.</inPort>
        <outPort index="0" name="RInChIs and keys">The RInChI, AUX info, and all 3 keys (web, long, shorT))</outPort>
    </ports>    
</knimeNode>
