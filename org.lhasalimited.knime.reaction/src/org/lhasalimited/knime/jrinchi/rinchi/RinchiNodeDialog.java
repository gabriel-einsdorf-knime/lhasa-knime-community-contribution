package org.lhasalimited.knime.jrinchi.rinchi;

import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;

/**
 * <code>NodeDialog</code> for the "Rinchi" Node.
 * Converts a reaction string to a rinchi
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Lhasa Limited
 */
public class RinchiNodeDialog extends DefaultNodeSettingsPane {

    /**
     * New pane for configuring the Rinchi node.
     */
    protected RinchiNodeDialog() {
    	RinchiNodeSettings settings = new RinchiNodeSettings();
    	
    	addDialogComponent(settings.getColumnSelectionComponent());
    	addDialogComponent(settings.getDialogComponentForceEquilibrium());

    }
}

