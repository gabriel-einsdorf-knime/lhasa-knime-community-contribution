package org.lhasalimited.knime.jrinchi.rinchi;

import org.knime.chem.types.RxnValue;
import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.MissingCell;
import org.knime.core.data.container.AbstractCellFactory;
import org.knime.core.data.container.ColumnRearranger;
import org.knime.core.data.def.StringCell.StringCellFactory;
import org.knime.core.node.InvalidSettingsException;
import org.lhasalimited.generic.node.config.StreamableLocalSettingsNodeModel;
import org.lhasalimited.jrinchi.Rinchi;
import org.lhasalimited.jrinchi.RinchiKeyType;
import org.lhasalimited.jrinchi.RinchiUtil;
import org.lhasalimited.jrinchi.exception.RinchiException;
import org.lhasalimited.knime.reaction.types.rdf.RdfValue;

/**
 * This is the model implementation of Rinchi.
 * Converts a reaction string to a rinchi
 *
 * @author Lhasa Limited
 */
public class RinchiNodeModel extends StreamableLocalSettingsNodeModel<RinchiNodeSettings> {

	@Override
	protected ColumnRearranger createColumnRearrangerImplementation(DataTableSpec spec) throws InvalidSettingsException
	{
		ColumnRearranger rearranger = new ColumnRearranger(spec);
		
		rearranger.append(new RinchiCellFactory(false, localSettings.getColumnIndex(spec), createOutputSpecs(spec)));
		
		return rearranger;
	}
    
	
	private DataColumnSpec[] createOutputSpecs(DataTableSpec spec)
	{
		DataColumnSpec[] creator = new DataColumnSpec[5];
		
		creator[0] = new DataColumnSpecCreator(DataTableSpec.getUniqueColumnName(spec, "RInChI"), StringCellFactory.TYPE).createSpec();
		creator[1] = new DataColumnSpecCreator(DataTableSpec.getUniqueColumnName(spec, "AUX"), StringCellFactory.TYPE).createSpec();
		creator[2] = new DataColumnSpecCreator(DataTableSpec.getUniqueColumnName(spec, "Web Key"), StringCellFactory.TYPE).createSpec();
		creator[3] = new DataColumnSpecCreator(DataTableSpec.getUniqueColumnName(spec, "Short Key"), StringCellFactory.TYPE).createSpec();
		creator[4] = new DataColumnSpecCreator(DataTableSpec.getUniqueColumnName(spec, "Long Key"), StringCellFactory.TYPE).createSpec();
		
		return creator;
	}
	

	class RinchiCellFactory extends AbstractCellFactory
	{
		private int index;
		private int size;
		
		public RinchiCellFactory(boolean processConcurrently, int index, DataColumnSpec... colSpecs)
		{
			super(processConcurrently, colSpecs);
			this.index = index;
			this.size = colSpecs.length;
		}

		@Override
		public DataCell[] getCells(DataRow row)
		{
			DataCell[] out = null;
			
			DataCell cell = row.getCell(index);
			
			if(cell.isMissing())
			{
				out = createMissing("Missing input", size);
			} else
			{
				try
				{
					String rxn = null;
					
					if(cell instanceof RxnValue)
					{
						rxn = ((RxnValue)cell).getRxnValue();
					} 
					else if(cell instanceof RdfValue)
					{
						rxn = ((RdfValue)cell).getRdfString();
					}
					
					Rinchi rinchi = RinchiUtil.getRinchi(rxn, RinchiKeyType.LONG, localSettings.isForceEquilibrium());
					
					String shortKey = RinchiUtil.getRinchiKeyFromRinchi(rinchi, RinchiKeyType.SHORT);
					String webKey = RinchiUtil.getRinchiKeyFromRinchi(rinchi, RinchiKeyType.WEB);
					
					out = new DataCell[]{
							StringCellFactory.create(rinchi.getRinchi()),
							rinchi.getAux().isPresent() ? StringCellFactory.create(rinchi.getAux().get()) : new MissingCell("No aux info"),
							StringCellFactory.create(webKey),
							StringCellFactory.create(shortKey),
							StringCellFactory.create(rinchi.getKey().get())

					};
											
				} catch (Exception e)
				{
					e.printStackTrace();
					out = createMissing(e.getMessage(), size);
					getLogger().error(e);
				}
			}

			return out;
		}
		
	}

}


