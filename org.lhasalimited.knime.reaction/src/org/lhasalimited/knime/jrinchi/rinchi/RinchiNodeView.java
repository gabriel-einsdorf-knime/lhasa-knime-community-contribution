package org.lhasalimited.knime.jrinchi.rinchi;

import org.knime.core.node.NodeView;

/**
 * <code>NodeView</code> for the "Rinchi" Node.
 * Converts a reaction string to a rinchi
 *
 * @author Lhasa Limited
 */
public class RinchiNodeView extends NodeView<RinchiNodeModel> {

    /**
     * Creates a new view.
     * 
     * @param nodeModel The model (class: {@link RinchiNodeModel})
     */
    protected RinchiNodeView(final RinchiNodeModel nodeModel) {
        super(nodeModel);
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void modelChanged() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onClose() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onOpen() {
        // TODO: generated method stub
    }

}

