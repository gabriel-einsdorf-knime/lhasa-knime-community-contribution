package org.lhasalimited.knime.jrinchi.rinchi;

import org.knime.chem.types.RxnValue;
import org.knime.core.data.DataTableSpec;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.defaultnodesettings.DialogComponent;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnName;
import org.lhasalimited.generic.node.config.NodeSettingCollection;
import org.lhasalimited.knime.reaction.types.rdf.RdfValue;

/**
 * Stores and manages the settings for the Rinchi node
 * 
 * @author Samuel
 *
 */
public class RinchiNodeSettings extends NodeSettingCollection
{

	public static final String CONFIG_RXN_COLUMN =  "cfgRxnColumn";
	public static final String CONFIG_FORCE_EQUILIBRIUM = "cfgForceEquilibrium";
	
	@Override
	protected void addSettings()
	{
		addSetting(CONFIG_RXN_COLUMN, new SettingsModelColumnName(CONFIG_RXN_COLUMN, "RXN"));
		addSetting(CONFIG_FORCE_EQUILIBRIUM, new SettingsModelBoolean(CONFIG_FORCE_EQUILIBRIUM, false));
	}
	
	/**
	 * Get the selected column name
	 * @return
	 */
	public String getColumnName()
	{
		return getColumnName(CONFIG_RXN_COLUMN);
	}
	
	/**
	 * Get the index of the selected column in the table
	 * 
	 * @param spec
	 * @return
	 */
	public int getColumnIndex(DataTableSpec spec) throws InvalidSettingsException
	{
		int index = spec.findColumnIndex(getColumnName());
		
		if(index == -1)
			throw new InvalidSettingsException("No valid column name selected");
		
		return index;
	}

	/**
	 * Get a dialog component for the reaction column selection
	 * @return
	 */
	public DialogComponent getColumnSelectionComponent()
	{
		return getDialogColumnNameSelection(CONFIG_RXN_COLUMN, "RXN", 0, RxnValue.class, RdfValue.class);
	}
	
	/**
	 * Set whether force equilibrium should be set
	 * @return
	 */
	public boolean isForceEquilibrium()
	{
		return getBooleanValue(CONFIG_FORCE_EQUILIBRIUM);
	}
	
	/**
	 * Get a dialog component to select force equilibrium
	 * @return
	 */
	public DialogComponent getDialogComponentForceEquilibrium()
	{
		return getDialogComponentBoolean(CONFIG_FORCE_EQUILIBRIUM, "Force equilibrium");
	}

}
