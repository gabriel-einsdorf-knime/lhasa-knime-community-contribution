package org.lhasalimited.knime.jrinchi.rinchi;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "Rinchi" Node.
 * Converts a reaction string to a rinchi
 *
 * @author Lhasa Limited
 */
public class RinchiNodeFactory 
        extends NodeFactory<RinchiNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public RinchiNodeModel createNodeModel() {
        return new RinchiNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<RinchiNodeModel> createNodeView(final int viewIndex,
            final RinchiNodeModel nodeModel) {
        return new RinchiNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new RinchiNodeDialog();
    }

}

