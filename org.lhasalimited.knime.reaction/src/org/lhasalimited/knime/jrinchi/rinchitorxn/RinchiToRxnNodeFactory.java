package org.lhasalimited.knime.jrinchi.rinchitorxn;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "RinchiToRxn" Node.
 * Convert a reaction InChI to a reaction
 *
 * @author Lhasa Limited
 */
public class RinchiToRxnNodeFactory 
        extends NodeFactory<RinchiToRxnNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public RinchiToRxnNodeModel createNodeModel() {
        return new RinchiToRxnNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<RinchiToRxnNodeModel> createNodeView(final int viewIndex,
            final RinchiToRxnNodeModel nodeModel) {
        return new RinchiToRxnNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new RinchiToRxnNodeDialog();
    }

}

