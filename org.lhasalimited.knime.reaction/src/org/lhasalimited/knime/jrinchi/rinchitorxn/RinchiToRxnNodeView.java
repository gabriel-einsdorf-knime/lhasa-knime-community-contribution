package org.lhasalimited.knime.jrinchi.rinchitorxn;

import org.knime.core.node.NodeView;

/**
 * <code>NodeView</code> for the "RinchiToRxn" Node.
 * Convert a reaction InChI to a reaction
 *
 * @author Lhasa Limited
 */
public class RinchiToRxnNodeView extends NodeView<RinchiToRxnNodeModel> {

    /**
     * Creates a new view.
     * 
     * @param nodeModel The model (class: {@link RinchiToRxnNodeModel})
     */
    protected RinchiToRxnNodeView(final RinchiToRxnNodeModel nodeModel) {
        super(nodeModel);
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void modelChanged() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onClose() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onOpen() {
        // TODO: generated method stub
    }

}

