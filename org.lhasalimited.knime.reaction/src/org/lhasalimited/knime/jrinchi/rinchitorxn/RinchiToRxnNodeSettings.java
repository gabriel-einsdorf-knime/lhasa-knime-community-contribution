package org.lhasalimited.knime.jrinchi.rinchitorxn;

import org.knime.core.data.DataTableSpec;
import org.knime.core.data.StringValue;
import org.knime.core.node.defaultnodesettings.DialogComponent;
import org.knime.core.node.defaultnodesettings.DialogComponentButtonGroup;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnName;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.lhasalimited.generic.node.config.NodeSettingCollection;
import org.lhasalimited.jrinchi.ReactionFormat;

/**
 * Stores and manages the settings for the Rinchi node
 * 
 * @author Samuel
 *
 */
public class RinchiToRxnNodeSettings extends NodeSettingCollection {

	public static final String CONFIG_RINCHI_COLUMN = "cfgRinchiColumn";
	public static final String CONFIG_OUTPUT_TYPE = "cfgOutputType";

	@Override
	protected void addSettings() {
		addSetting(CONFIG_RINCHI_COLUMN, new SettingsModelColumnName(CONFIG_RINCHI_COLUMN, "RInChI"));
		addSetting(CONFIG_OUTPUT_TYPE, new SettingsModelString(CONFIG_OUTPUT_TYPE, "RXN"));
	}

	/**
	 * Get the selected column name
	 * 
	 * @return
	 */
	public String getColumnName() {
		return getColumnName(CONFIG_RINCHI_COLUMN);
	}

	/**
	 * Get the index of the selected column in the table
	 * 
	 * @param spec
	 * @return
	 */
	public int getColumnIndex(DataTableSpec spec) {
		return spec.findColumnIndex(getColumnName());
	}

	/**
	 * Get a dialog component for the reaction column selection
	 * 
	 * @return
	 */
	public DialogComponent getColumnSelectionComponent() {
		return getDialogColumnNameSelection(CONFIG_RINCHI_COLUMN, "RInChI", 0, StringValue.class);
	}

	/**
	 * The desired output format
	 * 
	 * @return
	 */
	public ReactionFormat getSelectedformat() {
		return ReactionFormat.valueOf(getSetting(CONFIG_OUTPUT_TYPE, SettingsModelString.class).getStringValue());
	}

	/**
	 * Reaction format selection 
	 * 
	 * @return
	 */
	public DialogComponent getDialogComponentFormatSelection() {
		return new DialogComponentButtonGroup(getSetting(CONFIG_OUTPUT_TYPE, SettingsModelString.class), false,
				"Output format", ReactionFormat.RXN.toString(), ReactionFormat.RD.toString());
	}

}
