package org.lhasalimited.knime.jrinchi.rinchitorxn;

import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;

/**
 * <code>NodeDialog</code> for the "RinchiToRxn" Node.
 * Convert a reaction InChI to a reaction
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author S. Webb, Lhasa Limited
 */
public class RinchiToRxnNodeDialog extends DefaultNodeSettingsPane {

    /**
     * New pane for configuring the RinchiToRxn node.
     */
    protected RinchiToRxnNodeDialog() {
    	RinchiToRxnNodeSettings settings = new RinchiToRxnNodeSettings();
    	
    	addDialogComponent(settings.getColumnSelectionComponent());
    	addDialogComponent(settings.getDialogComponentFormatSelection());
    }
}

