package org.lhasalimited.knime.jrinchi.rinchitorxn;

import org.knime.chem.types.RxnAdapterCell;
import org.knime.chem.types.RxnCell;
import org.knime.chem.types.RxnCellFactory;
import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.DataType;
import org.knime.core.data.MissingCell;
import org.knime.core.data.StringValue;
import org.knime.core.data.container.AbstractCellFactory;
import org.knime.core.data.container.ColumnRearranger;
import org.knime.core.node.InvalidSettingsException;
import org.lhasalimited.generic.node.config.StreamableLocalSettingsNodeModel;
import org.lhasalimited.jrinchi.ReactionFormat;
import org.lhasalimited.jrinchi.Rinchi;
import org.lhasalimited.jrinchi.RinchiUtil;
import org.lhasalimited.jrinchi.exception.RinchiException;
import org.lhasalimited.knime.reaction.types.rdf.RdfCell;
import org.lhasalimited.knime.reaction.types.rdf.RdfCellFactory;

/**
 * This is the model implementation of RinchiToRxn.
 * Convert a reaction InChI to a reaction
 *
 * @author Lhasa Limited
 */
public class RinchiToRxnNodeModel extends StreamableLocalSettingsNodeModel<RinchiToRxnNodeSettings> {

	private static final int NUM_OUT_CELL = 1;
	
	@Override
	protected ColumnRearranger createColumnRearrangerImplementation(DataTableSpec spec) throws InvalidSettingsException
	{
		ColumnRearranger rearranger = new ColumnRearranger(spec);
		
		rearranger.append(new RinchiCellFactory(false, localSettings.getColumnIndex(spec), createOutputSpecs(spec)));
		
		return rearranger;
	}
    
	
	private DataColumnSpec[] createOutputSpecs(DataTableSpec spec) throws InvalidSettingsException
	{
		DataColumnSpec[] creator = new DataColumnSpec[NUM_OUT_CELL];
		
		try {
			creator[0] = new DataColumnSpecCreator(DataTableSpec.getUniqueColumnName(spec, "Reaction"), getType(localSettings.getSelectedformat())).createSpec();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return creator;
	}
	

	private DataType getType(ReactionFormat selectedformat) throws InvalidSettingsException 
	{
		DataType type;
		
		switch(selectedformat) {
		case AUTO:
				throw new InvalidSettingsException("Reaction type not compatible, expecting RD of RXN");
		case RD:
				type = RdfCellFactory.TYPE;
			break;
		case RXN:
			type = RxnCellFactory.TYPE;
			break;
		default:
			throw new InvalidSettingsException("Reaction type not compatible, expecting RD of RXN");
		
		}
		

		return type;
	}


	class RinchiCellFactory extends AbstractCellFactory
	{
		private int index;
		private int size;
		
		public RinchiCellFactory(boolean processConcurrently, int index, DataColumnSpec... colSpecs)
		{
			super(processConcurrently, colSpecs);
			this.index = index;
			this.size = colSpecs.length;
		}

		@Override
		public DataCell[] getCells(DataRow row)
		{
			DataCell[] out = null;
			
			DataCell cell = row.getCell(index);
			
			if(cell.isMissing())
			{
				out = createMissing("Missing input", size);
			} else
			{
				try
				{
					String rinchi = null;
					
					if(cell instanceof StringValue)
					{
						rinchi = ((StringValue)cell).getStringValue();
					} 

					Rinchi rinchiObject = new Rinchi(rinchi);

					
					String rxn = RinchiUtil.getReactionTextFromRinchi(rinchiObject, localSettings.getSelectedformat());
					
					out = new DataCell[]{
							createCell(rxn, localSettings.getSelectedformat())
					};
											
				} catch (RinchiException e)
				{
					e.printStackTrace();
					out = createMissing(e.getMessage(), size);
					getLogger().error(e);
				}
			}

			return out;
		}

		/**
		 * Create the cell for the specified reaction format. Handles null and empty rxn strings, in these
		 * scenarios a {@link MissingCell} will be returned
		 * 
		 * @param rxn				the string representation of the reaction
		 * @param selectedformat	the reaction format of the string
		 * 
		 * @return	The appropriate {@link DataCell} for the given format e.g. {@link RxnCell}, {@link RxnAdapterCell} or {@link RdfCell}
		 */
		private DataCell createCell(String rxn, ReactionFormat selectedformat)
		{
			DataCell cell;
			
			if(rxn == null)
			{
				cell = new MissingCell("jrinchi library returned null");
			} else if(rxn.equals(""))
			{
				cell = new MissingCell("jrinchi library returned an empty string");
			} else if(selectedformat == ReactionFormat.RXN) {
				cell = RxnCellFactory.createAdapterCell(rxn);
			} else if(selectedformat == ReactionFormat.RD) {
				cell = RdfCellFactory.create(rxn);
			} else {
				cell = new MissingCell("Reaction type not handled");
				getLogger().error("Reaction format of " + selectedformat.toString() + " is not handled and a missing cell will be returned");
			}
			
			return cell;
		}
		
	}



}