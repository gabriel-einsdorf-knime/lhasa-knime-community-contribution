package org.lhasalimited.knime.gpl.rdt.nodes.mapper;

import org.knime.core.node.NodeView;

/**
 * <code>NodeView</code> for the "ReactionDecoderMapper" Node.
 * Uses the Reaction Decoder tool to perform reaction mapping
 *
 * @author Lhasa Limited
 */
public class ReactionDecoderMapperNodeView extends NodeView<ReactionDecoderMapperNodeModel> {

    /**
     * Creates a new view.
     * 
     * @param nodeModel The model (class: {@link ReactionDecoderMapperNodeModel})
     */
    protected ReactionDecoderMapperNodeView(final ReactionDecoderMapperNodeModel nodeModel) {
        super(nodeModel);
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void modelChanged() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onClose() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onOpen() {
        // TODO: generated method stub
    }

}

