package org.lhasalimited.knime.gpl.rdt.nodes.mapper;

import org.knime.chem.types.RxnValue;
import org.knime.chem.types.SmilesValue;
import org.knime.core.data.DataTableSpec;
import org.knime.core.node.defaultnodesettings.DialogComponent;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.lhasalimited.generic.node.config.NodeSettingCollection;

public class ReactionDecoderMapperSettings extends NodeSettingCollection {

	public static final String CONFIG_REACTION_COLUMN = "cfgReactionColumn";
		
	public static final String CONFIG_IMAGE_WIDTH = "cfgImageWidth";
	public static final String CONFIG_IMAGE_HEIGHT = "cfgImageHeight";
	
	@Override
	protected void addSettings() {
		addColumnNameSetting(CONFIG_REACTION_COLUMN, "Reaction");
		addSetting(CONFIG_IMAGE_HEIGHT, new SettingsModelInteger(CONFIG_IMAGE_HEIGHT, 600));
		addSetting(CONFIG_IMAGE_WIDTH, new SettingsModelInteger(CONFIG_IMAGE_WIDTH, 1400));
	}

	public DialogComponent getDialogComponentColumnName() {
		return getDialogColumnNameSelection(CONFIG_REACTION_COLUMN, "Reaction", 0, RxnValue.class, SmilesValue.class);
	}

	public String getReactionColumnName() {
		return getColumnName(CONFIG_REACTION_COLUMN);
	}

	public int getReactionColumnIndex(DataTableSpec spec) {
		return spec.findColumnIndex(getReactionColumnName());
	}
	
	public int getWidth()
	{
		return getSetting(CONFIG_IMAGE_WIDTH, SettingsModelInteger.class).getIntValue();
	}
	
	public int getHeight()
	{
		return getSetting(CONFIG_IMAGE_HEIGHT, SettingsModelInteger.class).getIntValue();
	}
	
	public DialogComponent getDialogComponentWidth()
	{
		return new DialogComponentNumber(getSetting(CONFIG_IMAGE_WIDTH, SettingsModelInteger.class), "Width", 50);
	}
	
	public DialogComponent getDialogComponentHeight()
	{
		return new DialogComponentNumber(getSetting(CONFIG_IMAGE_HEIGHT, SettingsModelInteger.class), "Height", 50);
	}

}
