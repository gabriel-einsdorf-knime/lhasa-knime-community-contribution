package org.lhasalimited.knime.gpl.rdt.nodes.mapper;

import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;

/**
 * <code>NodeDialog</code> for the "ReactionDecoderMapper" Node.
 * Uses the Reaction Decoder tool to perform reaction mapping
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Lhasa Limited
 */
public class ReactionDecoderMapperNodeDialog extends DefaultNodeSettingsPane {

    /**
     * New pane for configuring the ReactionDecoderMapper node.
     */
    protected ReactionDecoderMapperNodeDialog() {
    	
    	ReactionDecoderMapperSettings settings = new ReactionDecoderMapperSettings();
    	addDialogComponent(settings.getDialogComponentColumnName());
    	
    	createNewGroup("Image settings");
    	setHorizontalPlacement(true);
    	addDialogComponent(settings.getDialogComponentWidth());
    	addDialogComponent(settings.getDialogComponentHeight());

    }
}

