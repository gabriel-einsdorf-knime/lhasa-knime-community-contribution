package org.lhasalimited.knime.gpl.rdt.nodes.mapper;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "ReactionDecoderMapper" Node.
 * Uses the Reaction Decoder tool to perform reaction mapping
 *
 * @author Lhasa Limited
 */
public class ReactionDecoderMapperNodeFactory 
        extends NodeFactory<ReactionDecoderMapperNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public ReactionDecoderMapperNodeModel createNodeModel() {
        return new ReactionDecoderMapperNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<ReactionDecoderMapperNodeModel> createNodeView(final int viewIndex,
            final ReactionDecoderMapperNodeModel nodeModel) {
        return new ReactionDecoderMapperNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new ReactionDecoderMapperNodeDialog();
    }

}

