package org.lhasalimited.knime.gpl.rdt.nodes.mapper;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.knime.chem.types.RxnCellFactory;
import org.knime.chem.types.RxnValue;
import org.knime.chem.types.SmilesValue;
import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.MissingCell;
import org.knime.core.data.container.AbstractCellFactory;
import org.knime.core.data.container.ColumnRearranger;
import org.knime.core.data.image.png.PNGImageCellFactory;
import org.knime.core.data.xml.XMLCellFactory;
import org.knime.core.node.InvalidSettingsException;
import org.lhasalimited.generic.node.config.StreamableLocalSettingsNodeModel;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.InvalidSmilesException;
import org.openscience.cdk.interfaces.IReaction;
import org.openscience.cdk.io.MDLRXNWriter;
import org.openscience.cdk.silent.Reaction;
import org.openscience.cdk.smiles.SmiFlavor;
import org.openscience.cdk.smiles.SmilesGenerator;
import org.openscience.cdk.smiles.SmilesParser;
import org.w3c.dom.Document;

import uk.ac.ebi.aamtool.Annotator;
import uk.ac.ebi.reactionblast.mechanism.ReactionMechanismTool;
import uk.ac.ebi.reactionblast.tools.ImageGenerator;
import uk.ac.ebi.reactionblast.tools.rxnfile.MDLRXNV2000Reader;

/**
 * This is the model implementation of ReactionDecoderMapper.
 * Uses the Reaction Decoder tool to perform reaction mapping
 *
 * @author Lhasa Limited
 */
public class ReactionDecoderMapperNodeModel extends StreamableLocalSettingsNodeModel<ReactionDecoderMapperSettings> {

	@Override
	protected ColumnRearranger createColumnRearrangerImplementation(DataTableSpec spec)
			throws InvalidSettingsException {

		
		ColumnRearranger rearranger = new ColumnRearranger(spec);
		
		rearranger.append(new ReactionDecoderMapperCellFactory(localSettings.getReactionColumnIndex(spec), getOutColSpec(localSettings.getReactionColumnName())));
		
		return rearranger;
	}

	/**
	 * Create the output specification
	 * 
	 * @param reactionColumnName
	 * @return
	 */
	private DataColumnSpec[] getOutColSpec(String reactionColumnName) 
	{		
		DataColumnSpec[] spec = new DataColumnSpec[] {
				new DataColumnSpecCreator(reactionColumnName + " (mapped)", RxnCellFactory.TYPE).createSpec(),
				new DataColumnSpecCreator(reactionColumnName + " (mapped-XML)", XMLCellFactory.TYPE).createSpec(),
				new DataColumnSpecCreator(reactionColumnName + " (mapped-PNG)", PNGImageCellFactory.TYPE).createSpec(),
				new DataColumnSpecCreator(reactionColumnName + " (mapped-PNG-centre)", PNGImageCellFactory.TYPE).createSpec()	
		};
		
		return spec;
	}

	class ReactionDecoderMapperCellFactory extends AbstractCellFactory
	{
		private int index;
		
		 final SmilesGenerator sg = new SmilesGenerator(SmiFlavor.AtomAtomMap);
	     final SmilesParser smilesParser = new SmilesParser(DefaultChemObjectBuilder.getInstance());
	     
		
		public ReactionDecoderMapperCellFactory(int index, DataColumnSpec[] columnSpec)
		{
			super(false, columnSpec);
			this.index = index;
		}

		@Override
		public DataCell[] getCells(DataRow row) 
		{
			DataCell cell = row.getCell(index);
			
			DataCell outCell = null;
			
			IReaction cdkReaction = null;
			Document doc = null;
			byte[] imageInByte = null;
			byte[] imageInByteCenter = null;
			
			try {
				
				if(cell instanceof SmilesValue)
				{
					String rxn = ((SmilesValue) cell).getSmilesValue();
					cdkReaction = smilesParser.parseReactionSmiles(rxn);
				}
				else if(cell instanceof RxnValue)
				{
					String rxn = ((RxnValue) cell).getRxnValue();
					MDLRXNV2000Reader reader = new MDLRXNV2000Reader(new StringReader(rxn));
					cdkReaction = reader.read(new Reaction());
				    reader.close();
				} 
				else
				{
					outCell = new MissingCell("Cell type of " + cell.getType() + " is not compatible with Smiles or RXN");
				}
			} catch (Exception e1) 
			{
				e1.printStackTrace();
				getLogger().debug("Failed to parse the value in the cell to CDK IReaction", e1);
				outCell = new MissingCell(e1.getMessage());
			}
			
			// Should handle this better!
			if(cdkReaction != null)
			{
				try 
				{
					ReactionMechanismTool mapped = performAtomAtomMapping(cdkReaction, row.getKey().getString());
					
					Annotator annotator = new Annotator();
					
					StringWriter out = new StringWriter();
					MDLRXNWriter writer = new MDLRXNWriter(out);
					writer.write(mapped.getSelectedSolution().getReaction());
					writer.close();
					
					
					outCell = RxnCellFactory.create(out.toString());
					out.close();
					
					try {
						doc = getXml(mapped, annotator);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					// Get the highlighted reaction image
					try {
						BufferedImage image = getReactionImageLtRHighlighted(mapped.getSelectedSolution().getReaction(), localSettings.getWidth(), localSettings.getHeight());
						
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						ImageIO.write( image, "png", baos );
						baos.flush();
						imageInByte = baos.toByteArray();
						baos.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					
					// Get the reaction centre image
					try {
						BufferedImage image = getReactionImageLtRReactionCenter(mapped.getSelectedSolution().getReaction(), localSettings.getWidth(), localSettings.getHeight());
						
						ByteArrayOutputStream baos = new ByteArrayOutputStream();
						ImageIO.write( image, "png", baos );
						baos.flush();
						imageInByteCenter = baos.toByteArray();
						baos.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				} catch (Exception e) {
					e.printStackTrace();
					getLogger().error(e);
					outCell = new MissingCell(e.getMessage());
				}
				
			} else
			{
				outCell = new MissingCell("Reaction is missing");
			}
			
			DataCell imageCell;
			try {
				imageCell = imageInByte == null ? new MissingCell("") : PNGImageCellFactory.create(imageInByte);
			} catch (IOException e) {
				e.printStackTrace();
				imageCell = new MissingCell(e.getMessage());
			}
			
			
			DataCell imageCellCentre;
			try {
				imageCellCentre = imageInByteCenter == null ? new MissingCell("") : PNGImageCellFactory.create(imageInByteCenter);
			} catch (IOException e) {
				e.printStackTrace();
				imageCellCentre = new MissingCell(e.getMessage());
			}
			
			
			return new DataCell[] {outCell, doc == null ? new MissingCell("") : XMLCellFactory.create(doc), imageCell, imageCellCentre};
		}

		/**
		 * Get the XML representation of the reaction
		 * @param mapped
		 * @param annotator 
		 * @return
		 * @throws ParserConfigurationException
		 */
		private Document getXml(ReactionMechanismTool mapped, Annotator annotator) throws ParserConfigurationException
		{
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            // root element
            Document doc = docBuilder.newDocument();

            org.w3c.dom.Element rootElement = doc.createElement("EC_BLAST");
            doc.appendChild(rootElement);
            
            annotator.annotateReactionAsXML(mapped, "reaction", doc, rootElement);
			
            return doc;
		}

		
		private BufferedImage getReactionImageLtRHighlighted(IReaction cdkReaction, int width, int height ) throws Exception
		{
			return ImageGenerator.makeLeftToRighHighlightedReaction(cdkReaction, width, height, true, null);
		}
		
		private BufferedImage getReactionImageLtRReactionCenter(IReaction cdkReaction, int width, int height ) throws Exception
		{
			return ImageGenerator.LeftToRightReactionCenterImageDirect(cdkReaction, null, height, width);
		}
		
	}
	
    /**
    *
    * @param cdkReaction
    * @param reactionName
    * @return
    * @throws InvalidSmilesException
    * @throws AssertionError
    * @throws Exception
    */
   public static ReactionMechanismTool performAtomAtomMapping(IReaction cdkReaction, String reactionName) throws InvalidSmilesException, AssertionError, Exception {
       
	   cdkReaction.setID(reactionName);
	   
       /*
        RMT for the reaction mapping
        */
       boolean forceMapping = true;//Overrides any mapping present int the reaction
       boolean generate2D = true;//2D perception of the stereo centers
       boolean generate3D = false;//2D perception of the stereo centers
       boolean complex = false;

       ReactionMechanismTool rmt = new ReactionMechanismTool(cdkReaction, forceMapping, generate2D, generate3D, complex);

       return rmt;
   }

	
   
}

