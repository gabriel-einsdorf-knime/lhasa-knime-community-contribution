package org.lhasalimited.whichcyp;

import java.util.Arrays;
import java.util.List;

import org.openscience.cdk.interfaces.IAtomContainer;

public class WhichCypPropertySupport
{

	/**
	 * A fixed list of WhichCyp properties which can be found set in an {@link IAtomContainer} after processing
	 */
	public static final List<String> PROPERTY_NAMES = Arrays
			.asList(new String[] { "Binder1A2", "Binder2C9", "Binder2C19", "Binder2D6", "Binder3A4",
					"MissingSignatures1a2", "MissingSignatures2c9", "MissingSignatures2c19", 
					"MissingSignatures2d6", "MissingSignatures3a4", "SensitivityWarning1a2", "SensitivityWarning2c9", 
					"SensitivityWarning2c19", "SensitivityWarning2d6", "SensitivityWarning3a4"});

}
