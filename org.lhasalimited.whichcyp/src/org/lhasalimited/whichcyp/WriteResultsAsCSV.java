/* 
 * Copyright (C) 2010-2013  David Gloriam <davidgloriam@googlemail.com> & Patrik Rydberg <patrik.rydberg@gmail.com>
 * 
 * Contact: patrik.rydberg@gmail.com
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 * All we ask is that proper credit is given for our work, which includes
 * - but is not limited to - adding the above copyright notice to the beginning
 * of your source code files, and to any copyright notice that you may distribute
 * with programs based on this work.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.lhasalimited.whichcyp;



import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtomContainerSet;


public class WriteResultsAsCSV {

	PrintWriter outfile;
	String moleculeID;
	private String dateAndTime;
	String[] namesOfInfiles;
	String OutputDir;
	String OutputFile;
	

	DecimalFormat twoDecimalFormat = new DecimalFormat("#.##");
	//DecimalFormat twoDecimalFormat = new DecimalFormat();


	public WriteResultsAsCSV(String dateTime, String[] infileNames, String outputdir, String outputfile){
		dateAndTime = dateTime;
		namesOfInfiles = infileNames;
		OutputDir = outputdir;
		OutputFile = outputfile;

		// DecimalFormat for A
		twoDecimalFormat.setDecimalSeparatorAlwaysShown(false);
		DecimalFormatSymbols decformat = new DecimalFormatSymbols();
		decformat.setDecimalSeparator('.');
		decformat.setGroupingSeparator(',');
		twoDecimalFormat.setMaximumFractionDigits(2);
		twoDecimalFormat.setDecimalFormatSymbols(decformat);
	}



	public void writeCSV(IAtomContainerSet moleculeSet) {
		
		if (OutputFile=="") OutputFile = "WhichCyp_Results_" + this.dateAndTime;
		
		try {
			outfile = new PrintWriter(new BufferedWriter(new FileWriter(this.OutputDir + OutputFile + ".csv")));
		} catch (IOException e) {
			System.out.println("Could not create CSV outfile");
			e.printStackTrace();
		}

		outfile.println("Molecule,BindsTo1A2,BindsTo2C9,BindsTo2C19,BindsTo2D6,BindsTo3A4," + 
				"MissingHeight3Signatures1A2,MissingHeight3Signatures2C9,MissingHeight3Signatures2C19,MissingHeight3Signatures2D6,MissingHeight3Signatures3A4," + 
				"SensitivityWarning1A2,SensitivityWarning2C9,SensitivityWarning2C19,SensitivityWarning2D6,SensitivityWarning3A4");

		// Iterate Molecules
		for (int moleculeIndex=0; moleculeIndex < moleculeSet.getAtomContainerCount(); moleculeIndex++) {

			// Set variables
			IAtomContainer molecule = moleculeSet.getAtomContainer(moleculeIndex);
			moleculeID = molecule.getID();

			outfile.print(moleculeID);				
			if (molecule.getProperty("Binder1A2").equals(1)) outfile.print(",1");
			else outfile.print(",0");
			if (molecule.getProperty("Binder2C9").equals(1)) outfile.print(",1");
			else outfile.print(",0");
			if (molecule.getProperty("Binder2C19").equals(1)) outfile.print(",1");
			else outfile.print(",0");
			if (molecule.getProperty("Binder2D6").equals(1)) outfile.print(",1");
			else outfile.print(",0");
			if (molecule.getProperty("Binder3A4").equals(1)) outfile.print(",1");
			else outfile.print(",0");
			outfile.print("," + molecule.getProperty("MissingSignatures1a2"));
			outfile.print("," + molecule.getProperty("MissingSignatures2c9"));
			outfile.print("," + molecule.getProperty("MissingSignatures2c19"));
			outfile.print("," + molecule.getProperty("MissingSignatures2d6"));
			outfile.print("," + molecule.getProperty("MissingSignatures3a4"));
			outfile.print("," + molecule.getProperty("SensitivityWarning1a2"));
			outfile.print("," + molecule.getProperty("SensitivityWarning2c9"));
			outfile.print("," + molecule.getProperty("SensitivityWarning2c19"));
			outfile.print("," + molecule.getProperty("SensitivityWarning2d6"));
			outfile.print("," + molecule.getProperty("SensitivityWarning3a4"));
			outfile.print("\n");
		}

		outfile.close();
	}
}




