/* 
 * Copyright (C) 2010-2013  David Gloriam <davidgloriam@googlemail.com> & Patrik Rydberg <patrik.rydberg@gmail.com>
 * 
 * Contact: patrik.rydberg@gmail.com
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 * All we ask is that proper credit is given for our work, which includes
 * - but is not limited to - adding the above copyright notice to the beginning
 * of your source code files, and to any copyright notice that you may distribute
 * with programs based on this work.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.lhasalimited.whichcyp;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Arrays;
import java.util.Properties;
import java.util.TreeSet;

import org.openscience.cdk.Atom;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.CDKConstants;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.geometry.GeometryTools;
import org.openscience.cdk.geometry.Projector;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtomContainerSet;
import org.openscience.cdk.io.MDLV2000Writer;
import org.openscience.cdk.io.listener.PropertiesListener;
import org.openscience.cdk.layout.StructureDiagramGenerator;

public class WriteResultsAsChemDoodleHTML
{

	PrintWriter outfile;
	TreeSet<Atom> sortedAtomsTreeSet;
	String moleculeID;
	private String dateAndTime;
	String[] namesOfInfiles;
	DecimalFormat twoDecimalFormat = new DecimalFormat();
	String OutputDir;
	String OutputFile;

	public WriteResultsAsChemDoodleHTML(String dateTime, String[] infileNames, String outputdir, String outputfile)
	{
		dateAndTime = dateTime;
		namesOfInfiles = infileNames;
		OutputDir = outputdir;
		OutputFile = outputfile;

		// DecimalFormat for A
		twoDecimalFormat.setDecimalSeparatorAlwaysShown(false);
		DecimalFormatSymbols decformat = new DecimalFormatSymbols();
		decformat.setDecimalSeparator('.');
		decformat.setGroupingSeparator(',');
		twoDecimalFormat.setMaximumFractionDigits(2);
		twoDecimalFormat.setDecimalFormatSymbols(decformat);
	}

	public void writeHTML(IAtomContainerSet moleculeSet, int[][][] colorlabelatomset)
	{

		if (OutputFile == "")
			OutputFile = "WhichCyp_Results_" + this.dateAndTime;

		try
		{
			outfile = new PrintWriter(new BufferedWriter(new FileWriter(this.OutputDir + OutputFile + ".html")));
		} catch (IOException e)
		{
			System.out.println("Could not create HTML outfile");
			e.printStackTrace();
		}

		this.writeHead(moleculeSet);

		this.writeBody(moleculeSet, colorlabelatomset);

		outfile.close();

	}

	public void writeHead(IAtomContainerSet moleculeSet)
	{

		outfile.println("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
		outfile.println("");
		outfile.println("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
		outfile.println("");
		outfile.println("<head>");
		outfile.println("");
		outfile.println("<title>Results from WhichCyp</title>");
		outfile.println("<style type=\"text/css\">");
		outfile.println("<!--");
		outfile.println("body {");
		outfile.println("\tbackground: #FFFFFF;");
		outfile.println("\tcolor: #000000;");
		outfile.println("\tmargin: 5px;");
		outfile.println("\tfont-family: Verdana, Arial, sans-serif;");
		outfile.println("\tfont-size: 16px;");
		outfile.println("\t}");
		outfile.println(".boldlarge { margin-left: 10px; font-size: 20px; font-weight : bold; }");
		outfile.println(".molecule { margin-left: 20px }");
		outfile.println("table { }");
		outfile.println("th { text-align:center; font-weight : bold; }");
		outfile.println(".highlight1 {  background: rgb(255,204,102); font-weight : bold; } ");
		outfile.println(".highlight2 {  background: rgb(223,189,174); font-weight : bold; } ");
		outfile.println(".highlight3 {  background: rgb(214,227,181); font-weight : bold; } ");
		outfile.println(".hiddenPic {display:none;} ");
		outfile.println("ul#navlist	{");
		outfile.println("font: bold 14px verdana, arial, sans-serif;");
		outfile.println("list-style-type: none;");
		outfile.println("padding-bottom: 26px;");
		outfile.println("border-bottom: 0px;");
		outfile.println("margin: 0; }");
		outfile.println("ul#navlist li {");
		outfile.println("float: left;");
		outfile.println("height: 24px;");
		outfile.println("margin: 2px 4px 0px 4px;");
		outfile.println("border: 0px;");
		outfile.println("border-top-left-radius: 3px;");
		outfile.println("border-top-right-radius: 3px; }");
		outfile.println("ul#navlist li#cyp3A4 {");
		outfile.println("border-bottom: 1px solid rgb(242,238,234);");
		outfile.println("background-color: rgb(242,238,234); }");
		outfile.println("li#cyp3A4 a { color:  rgb(80,80,100); }");
		outfile.println("ul#navlist li#cyp2D6 {");
		outfile.println("border-bottom: 1px solid rgb(228,235,240);");
		outfile.println("background-color: rgb(228,235,240); }");
		outfile.println("li#cyp2D6 a { color: rgb(80,80,100); }");
		outfile.println("ul#navlist li#cyp2C9 {");
		outfile.println("border-bottom: 1px solid rgb(255,235,235);");
		outfile.println("background-color: rgb(255,235,235); }");
		outfile.println("li#cyp2C9 a { color: rgb(80,80,100); }");
		outfile.println("#navlist a	{ ");
		outfile.println("float: left;");
		outfile.println("display: block;");
		outfile.println("text-decoration: none;");
		outfile.println("padding: 4px;");
		outfile.println("font-style: italic; }");
		outfile.println("canvas.ChemDoodleWebComponent {border: none;}");
		outfile.println("div.binder {background-color:rgb(228,220,240); border-radius: 3px; padding: 2px; float:left; margin:4px 2px; font-weight:bold;}");
		outfile.println("div.nobinder {background-color:rgb(255,255,255); border-radius: 3px; padding: 2px; float:left; margin:4px 2px;}");
		outfile.println(
				"div.warningnobinder {background-color:rgb(255,255,255); border-radius: 3px; padding: 2px; float:left; margin:2px 2px; border-style:solid; border-width:2px; border-color: red;}");
		outfile.println(
				"div.warningbinder {background-color:rgb(228,220,240); border-radius: 3px; padding: 2px; float:left; margin:2px 2px; font-weight:bold; border-style:solid; border-width:2px; border-color: red;}");
		outfile.println("-->");
		outfile.println("</style>");
		outfile.println("<script type=\"text/javascript\" src=\"http://www.farma.ku.dk/smartcyp/chemdoodle/ChemDoodleWeb-libs.js\"></script>");
		outfile.println("<script type=\"text/javascript\" src=\"http://www.farma.ku.dk/smartcyp/chemdoodle/ChemDoodleWeb.js\"></script>");
		// outfile.println("<script type=\"text/javascript\"
		// src=\"http://hub.chemdoodle.com/cwc/latest/ChemDoodleWeb-libs.js\"></script>");
		// outfile.println("<script type=\"text/javascript\"
		// src=\"http://hub.chemdoodle.com/cwc/latest/ChemDoodleWeb.js\"></script>");
		outfile.println("<script type=\"text/javascript\">");
		outfile.println("function HideContent(d) {");
		outfile.println("	document.getElementById(d).style.display = 'none';");
		outfile.println("	}");
		outfile.println("function ShowContent(d) {");
		outfile.println("	document.getElementById(d).style.display = 'block';");
		outfile.println("	}");
		outfile.println("function showstandard(mol){");
		outfile.println("	HideContent('molecule'+mol+'CYP1A2div');");
		outfile.println("	HideContent('molecule'+mol+'CYP2C9div');");
		outfile.println("	HideContent('molecule'+mol+'CYP2C19div');");
		outfile.println("	HideContent('molecule'+mol+'CYP2D6div');");
		outfile.println("	HideContent('molecule'+mol+'CYP3A4div');");
		outfile.println("	ShowContent('molecule'+mol+'div');");
		outfile.println("}");
		outfile.println("function show1A2(mol){");
		outfile.println("	HideContent('molecule'+mol+'div');");
		outfile.println("	ShowContent('molecule'+mol+'CYP1A2div');");
		outfile.println("}");
		outfile.println("function show2C9(mol){");
		outfile.println("	HideContent('molecule'+mol+'div');");
		outfile.println("	ShowContent('molecule'+mol+'CYP2C9div');");
		outfile.println("}");
		outfile.println("function show2C19(mol){");
		outfile.println("	HideContent('molecule'+mol+'div');");
		outfile.println("	ShowContent('molecule'+mol+'CYP2C19div');");
		outfile.println("}");
		outfile.println("function show2D6(mol){");
		outfile.println("	HideContent('molecule'+mol+'div');");
		outfile.println("	ShowContent('molecule'+mol+'CYP2D6div');");
		outfile.println("}");
		outfile.println("function show3A4(mol){");
		outfile.println("	HideContent('molecule'+mol+'div');");
		outfile.println("	ShowContent('molecule'+mol+'CYP3A4div');");
		outfile.println("}");
		outfile.println("</script>");
		outfile.println("</head>");
		outfile.println("");

	}

	public void writeBody(IAtomContainerSet moleculeSet, int[][][] colorlabelatomset)
	{

		outfile.println("<body>");
		// error message if problems
		if (moleculeSet.getAtomContainerCount() == 0)
		{
			outfile.println("<h1>There were no molecules in the input</h1>");
		} else
		{
			// no error message, print normal output
			outfile.println("<h1>Results from WhichCyp version 1.2</h1>");
			outfile.println("\n These results were produced: " + this.dateAndTime + ".<br />");
			outfile.println("\n The infiles were: " + Arrays.toString(namesOfInfiles) + ".<br />");
			String errormessage = (String) moleculeSet.getProperty("ErrorMessage");
			if (errormessage != null)
			{
				outfile.println("<br /> \n " + errormessage.replace("\n", "<br />") + "<br />");
				outfile.println("\n Molecule(s) with problems are not written to output, check the log file for details.<br />");
			}

			// Iterate Molecules
			for (int moleculeIndex = 0; moleculeIndex < moleculeSet.getAtomContainerCount(); moleculeIndex++)
			{

				IAtomContainer molecule = moleculeSet.getAtomContainer(moleculeIndex);
				this.writeMoleculeTable(molecule, colorlabelatomset[moleculeIndex]);
			}

		}
		outfile.println("</body>");
		outfile.println("</html>");

	}

	public void writeMoleculeTable(IAtomContainer molecule, int[][] colorlabelatomset)
	{

		moleculeID = molecule.getID();

		outfile.println("");
		outfile.println("<!-- Molecule " + moleculeID + " -->"); // Invisible
																	// code
																	// marker
																	// for
																	// molecule

		String title = moleculeID + ": " + (String) molecule.getProperty(CDKConstants.TITLE);
		if (title == null || title == "")
		{
			title = "Molecule " + moleculeID;
		}

		// preconstruct coordinates and stuff for each molecule
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		MDLV2000Writer writer = new MDLV2000Writer(baos);
		// generate 2D coordinates
		molecule = this.generate2Dcoordinates(molecule);

		// force 2D coordinates even if 3D exists
		Properties customSettings = new Properties();
		customSettings.setProperty("ForceWriteAs2DCoordinates", "true");
		customSettings.setProperty("WriteAromaticBondTypes", "true");

		PropertiesListener listener = new PropertiesListener(customSettings);
		writer.addChemObjectIOListener(listener);
		// end force 2D coordinates
		try
		{
			writer.write(molecule);
		} catch (CDKException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String MoleculeString = baos.toString();
		// now split molecule into multiple lines to enable explicit printout of
		// \n
		String Moleculelines[] = MoleculeString.split("\\r?\\n");

		// define a suitable canvas size in the y-axis
		int ycanvassize = 300;
		double[] molMinMax = GeometryTools.getMinMax(molecule);
		double ylength = molMinMax[3] - molMinMax[1];
		int minLenghtForIncrease = 8;
		if (ylength > minLenghtForIncrease)
		{
			ycanvassize = (int) (300 + (ylength - minLenghtForIncrease) * 20);
		}

		String MolName = "molecule" + moleculeID;

		outfile.println("<br /><span class=\"boldlarge\">" + title + "</span><br />");
		// isoform selectivity output
		writeIsoformSelectivityOutput(molecule, moleculeID);

		outfile.println("<div id='molecule" + moleculeID + "div' style='display:;'>");
		// output chemdoodle molecule here

		outfile.println("<script>");
		outfile.println("var " + MolName + " = new ChemDoodle.ViewerCanvas('" + MolName + "', 400, " + ycanvassize + ");");
		outfile.println(MolName + ".specs.atoms_useJMOLColors = true;");
		outfile.print("var " + MolName + "MolFile = '");
		for (int i = 0; i < Moleculelines.length; i++)
		{
			outfile.print(Moleculelines[i]);
			outfile.print("\\n");
		}
		outfile.print("'; \n");
		outfile.println("var " + MolName + "Mol = ChemDoodle.readMOL(" + MolName + "MolFile); ");
		outfile.println("// get the dimension of the molecule");
		outfile.println("var size = " + MolName + "Mol.getDimension();");
		outfile.println("// find the scale by taking the minimum of the canvas/size ratios");
		outfile.println("var scale = Math.min(" + MolName + ".width/size.x, " + MolName + ".height/size.y);");
		outfile.println("// load the molecule first (this function automatically sets scale, so we need to change specs after");
		outfile.println(MolName + ".loadMolecule(" + MolName + "Mol);");
		outfile.println("// change the specs.scale value to the scale calculated, shrinking it slightly so that text is not cut off");
		outfile.println(MolName + ".specs.scale = scale*.8;");
		outfile.println(MolName + ".drawChildExtras = function(ctx){");
		outfile.println("	ctx.save();");
		outfile.println("	ctx.translate(this.width/2, this.height/2);");
		outfile.println("	ctx.rotate(this.specs.rotateAngle);");
		outfile.println("	ctx.scale(this.specs.scale, this.specs.scale);");
		outfile.println("	ctx.translate(-this.width/2, -this.height/2);");
		outfile.println("	//draw atom numbers and draw circles on ranked atoms");
		outfile.println("	ctx.lineWidth = 2 + 4/this.specs.scale;");
		outfile.println("	radius = 6 + 6/this.specs.scale;");
		outfile.println("	for (var i = 0, ii=" + MolName + "Mol.atoms.length; i<ii; i++) {");
		outfile.println("		var atom = " + MolName + "Mol.atoms[i];");
		outfile.println("	}");
		outfile.println("	ctx.restore();");
		outfile.println("}");
		outfile.println(MolName + ".repaint();");
		outfile.println("</script>");
		// end of chemdoodle molecule

		outfile.println("</div>");
		// end of default output

		String[] isoforms = { "1A2", "2C9", "2C19", "2D6", "3A4" };
		int isoformcount = 0;

		for (String isoform : isoforms)
		{

			// Write isoform output
			MolName = "molecule" + moleculeID + isoform;
			outfile.println("<div id='molecule" + moleculeID + "CYP" + isoform + "div' style='display:none;'>");
			// output chemdoodle molecule here

			outfile.println("<script>");
			outfile.println("var " + MolName + " = new ChemDoodle.ViewerCanvas('" + MolName + "', 400, " + ycanvassize + ");");
			outfile.println(MolName + ".specs.atoms_useJMOLColors = true;");
			outfile.print("var " + MolName + "MolFile = '");
			for (int i = 0; i < Moleculelines.length; i++)
			{
				outfile.print(Moleculelines[i]);
				outfile.print("\\n");
			}
			outfile.print("'; \n");
			outfile.println("var " + MolName + "Mol = ChemDoodle.readMOL(" + MolName + "MolFile); ");
			outfile.println("// get the dimension of the molecule");
			outfile.println("var size = " + MolName + "Mol.getDimension();");
			outfile.println("// find the scale by taking the minimum of the canvas/size ratios");
			outfile.println("var scale = Math.min(" + MolName + ".width/size.x, " + MolName + ".height/size.y);");
			outfile.println("// load the molecule first (this function automatically sets scale, so we need to change specs after");
			outfile.println(MolName + ".loadMolecule(" + MolName + "Mol);");
			outfile.println("// change the specs.scale value to the scale calculated, shrinking it slightly so that text is not cut off");
			outfile.println(MolName + ".specs.scale = scale*.8;");
			outfile.println(MolName + ".drawChildExtras = function(ctx){");
			outfile.println("	ctx.save();");
			outfile.println("	ctx.translate(this.width/2, this.height/2);");
			outfile.println("	ctx.rotate(this.specs.rotateAngle);");
			outfile.println("	ctx.scale(this.specs.scale, this.specs.scale);");
			outfile.println("	ctx.translate(-this.width/2, -this.height/2);");
			outfile.println("	//draw atom numbers and draw circles on ranked atoms");
			outfile.println("	ctx.lineWidth = 2 + 4/this.specs.scale;");
			outfile.println("	radius = 6 + 6/this.specs.scale;");
			outfile.println("	for (var i = 0, ii=" + MolName + "Mol.atoms.length; i<ii; i++) {");
			outfile.println("		var atom = " + MolName + "Mol.atoms[i];");
			// add atom labelling here
			for (int atom = 0; atom < molecule.getAtomCount(); atom++)
			{
				if (colorlabelatomset[isoformcount][atom] == 1)
				{
					outfile.println("		if(i == " + atom + "){");
					outfile.println("			ctx.strokeStyle = 'rgb(255,204,102)';");
					outfile.println("			ctx.beginPath();");
					outfile.println("			ctx.arc(atom.x, atom.y, radius, 0, Math.PI * 2, false);");
					outfile.println("			ctx.stroke();");
					outfile.println("		}");
				}
			}
			outfile.println("	}");
			outfile.println("	ctx.restore();");
			outfile.println("}");
			outfile.println(MolName + ".repaint();");
			outfile.println("</script>");
			// end of chemdoodle molecule

			outfile.println("</div>");
			// end of isoform output

			isoformcount++;

		}

		outfile.println("<hr />");
	}

	public void writeIsoformSelectivityOutput(IAtomContainer molecule, String molid)
	{
		// isoform selectivity output
		String binderclass = "";
		if (molecule.getProperty("Binder1A2").equals(1))
		{
			if (molecule.getProperty("SensitivityWarning1a2").equals(1))
				binderclass = "warningbinder";
			else
				binderclass = "binder";
		} else
		{
			if (molecule.getProperty("SensitivityWarning1a2").equals(1))
				binderclass = "warningnobinder";
			else
				binderclass = "nobinder";
		}
		outfile.print("<div class='" + binderclass + "' onMouseOver=\"show1A2('" + molid + "');\" onMouseOut=\"showstandard('" + molid + "');\">1A2</div>");

		if (molecule.getProperty("Binder2C9").equals(1))
		{
			if (molecule.getProperty("SensitivityWarning2c9").equals(1))
				binderclass = "warningbinder";
			else
				binderclass = "binder";
		} else
		{
			if (molecule.getProperty("SensitivityWarning2c9").equals(1))
				binderclass = "warningnobinder";
			else
				binderclass = "nobinder";
		}
		outfile.print("<div class='" + binderclass + "' onMouseOver=\"show2C9('" + molid + "');\" onMouseOut=\"showstandard('" + molid + "');\">2C9</div>");

		if (molecule.getProperty("Binder2C19").equals(1))
		{
			if (molecule.getProperty("SensitivityWarning2c19").equals(1))
				binderclass = "warningbinder";
			else
				binderclass = "binder";
		} else
		{
			if (molecule.getProperty("SensitivityWarning2c19").equals(1))
				binderclass = "warningnobinder";
			else
				binderclass = "nobinder";
		}
		outfile.print("<div class='" + binderclass + "' onMouseOver=\"show2C19('" + molid + "');\" onMouseOut=\"showstandard('" + molid + "');\">2C19</div>");

		if (molecule.getProperty("Binder2D6").equals(1))
		{
			if (molecule.getProperty("SensitivityWarning2d6").equals(1))
				binderclass = "warningbinder";
			else
				binderclass = "binder";
		} else
		{
			if (molecule.getProperty("SensitivityWarning2d6").equals(1))
				binderclass = "warningnobinder";
			else
				binderclass = "nobinder";
		}
		outfile.print("<div class='" + binderclass + "' onMouseOver=\"show2D6('" + molid + "');\" onMouseOut=\"showstandard('" + molid + "');\">2D6</div>");

		if (molecule.getProperty("Binder3A4").equals(1))
		{
			if (molecule.getProperty("SensitivityWarning3a4").equals(1))
				binderclass = "warningbinder";
			else
				binderclass = "binder";
		} else
		{
			if (molecule.getProperty("SensitivityWarning3a4").equals(1))
				binderclass = "warningnobinder";
			else
				binderclass = "nobinder";
		}
		outfile.print("<div class='" + binderclass + "' onMouseOver=\"show3A4('" + molid + "');\" onMouseOut=\"showstandard('" + molid + "');\">3A4</div>");

		outfile.print("<br /> \n");
	}

	// Generates 2D coordinates of molecules
	public IAtomContainer generate2Dcoordinates(IAtomContainer iAtomContainer)
	{

		// boolean isConnected =
		// ConnectivityChecker.isConnected(iAtomContainer);
		// System.out.println("isConnected " + isConnected);

		final StructureDiagramGenerator structureDiagramGenerator = new StructureDiagramGenerator();

		// Generate 2D coordinates?
		if (GeometryTools.has2DCoordinates(iAtomContainer))
		{
			// System.out.println(iAtomContainer.toString() + " already had 2D
			// coordinates");
			return iAtomContainer; // already has 2D coordinates.
		} else
		{
			// Generate 2D structure diagram (for each connected component).
			final AtomContainer iAtomContainer2d = new AtomContainer();

			synchronized (structureDiagramGenerator)
			{
				structureDiagramGenerator.setMolecule(iAtomContainer, true);
				structureDiagramGenerator.setUseTemplates(true);
				try
				{
					// Generate 2D coords for this molecule.
					structureDiagramGenerator.generateCoordinates();
					iAtomContainer = structureDiagramGenerator.getMolecule();
				} catch (final Exception e)
				{
					// Use projection instead.
					Projector.project2D(iAtomContainer);
					System.out.println("Exception in generating 2D coordinates");
					e.printStackTrace();
				}
			}

			if (GeometryTools.has2DCoordinates(iAtomContainer))
				return iAtomContainer;
			else
			{
				System.out.println("Generating 2D coordinates for " + iAtomContainer2d + " failed.");
				return null;
			}
		}
	}

}