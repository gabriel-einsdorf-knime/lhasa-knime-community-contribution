package org.lhasalimited.whichcyp;

import java.util.HashMap;
import java.util.Map;

import org.openscience.cdk.interfaces.IAtomContainer;

/**
 * A supporting class that provides functionality to interact with {@link WhichCyp}
 * <br><br>
 * Example usage:
 * <br>
 * <pre>
 * <code>
 * IAtomContainer structure = ...;
 * //// Perform structure cleanup: aromatisation, remove explicit hydrogens and add implicit hydrogens
 * process(structure);
 * getWhichCypProperties(structure);
 * 
 * </code>
 * </pre>
 * 
 * 
 * @author Samuel
 *
 */
public class WhichCypProcessor
{
	
	public WhichCypProcessor()
	{
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * The {@link WhichCyp#predictIsoforms} method appears to return a matrix of isphorm vs atom index for the molecule. 
	 * <br><br>
	 * A value in a position of 1 represents the atom is predicted as a important for binding of that isophorm
	 * <br><br>
	 * 
	 * Ordering of isophorms: 1a2, 2c9, 2c19, 2d6, 3a4<br>
	 * result[1][2] identifies if atom 2 is as significant for the binding
	 * 
	 * <br><br>
	 * Properties are set during processing that can be accessed via the {@link #getWhichCypProperties(IAtomContainer)} method
	 * where they are extract as a {@link Map}
	 * <br>
	 * <pre>
	 * <code>
	 * IAtomContainer structure = ...;
	 * //// Perform structure cleanup: aromatisation, remove explicit hydrogens and add implicit hydrogens
	 * process(structure);
	 * getWhichCypProperties(structure);
	 * 
	 * </code>
	 * </pre>
	 * 
	 * @param structure		The structure to predict
	 * 
	 * @return
	 * @throws Exception
	 */
	public int[][] process(IAtomContainer structure) throws Exception
	{		
		return WhichCyp.predictIsoforms(structure);
	}
	
	/**
	 * Runs WhichCyp using the {@link #process(IAtomContainer)} method then returns the result of 
	 * {@link #extractProperties(IAtomContainer)}. Provides a shortcut if the various sites of
	 * metabolism aren't required. 
	 * 
	 * @param structure
	 * @return
	 * @throws Exception
	 */
	public Map<String, Integer> getWhichCypProperties(IAtomContainer structure) throws Exception
	{
		process(structure);
		
		return extractProperties(structure);
	}

	/**
	 * Extract the properties from the predicted structure. See {@link WhichCypPropertySupport#PROPERTY_NAMES} for
	 * more information. 
	 * @param predicted
	 * @return
	 */
	public Map<String, Integer> extractProperties(IAtomContainer predicted)
	{
		Map<String, Integer> results = new HashMap<String, Integer>();
		
		WhichCypPropertySupport.PROPERTY_NAMES.forEach(prop -> 
		{
			results.put(prop, predicted.getProperty(prop));
		});
		
		return results;
	}

}
