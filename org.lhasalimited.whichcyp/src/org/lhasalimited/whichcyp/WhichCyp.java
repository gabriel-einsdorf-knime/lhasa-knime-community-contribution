/* 
 * Copyright (C) 2010-2013  David Gloriam <davidgloriam@googlemail.com> & Patrik Rydberg <patrik.rydberg@gmail.com>
 * 
 * Contact: patrik.rydberg@gmail.com
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 * All we ask is that proper credit is given for our work, which includes
 * - but is not limited to - adding the above copyright notice to the beginning
 * of your source code files, and to any copyright notice that you may distribute
 * with programs based on this work.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.lhasalimited.whichcyp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.lhasalimited.whichcyp.selectivityfiles.WhichCypFileHelper;
import org.openscience.cdk.CDKConstants;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.aromaticity.CDKHueckelAromaticityDetector;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtomContainerSet;
import org.openscience.cdk.interfaces.IChemFile;
import org.openscience.cdk.interfaces.IChemObjectBuilder;
import org.openscience.cdk.io.ISimpleChemObjectReader;
import org.openscience.cdk.io.MDLReader;
import org.openscience.cdk.io.ReaderFactory;
import org.openscience.cdk.io.SMILESReader;
import org.openscience.cdk.signature.AtomSignature;
import org.openscience.cdk.silent.AtomContainer;
import org.openscience.cdk.silent.AtomContainerSet;
import org.openscience.cdk.smiles.DeduceBondSystemTool;
import org.openscience.cdk.tools.CDKHydrogenAdder;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
import org.openscience.cdk.tools.manipulator.ChemFileManipulator;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;

public class WhichCyp {


	public static void main(String[] arguments) throws Exception{

		WhichCyp WhichCypMain = new WhichCyp();

		// Check that the arguments (molecule files) have been given
		if (arguments.length < 1){
			System.out.println("Wrong number of arguments!" + '\n' + "Usage: WhichCyp.jar <One or more moleculeFiles>");
			System.exit(0);			
		}
		
		//check for input flags and copy input files to filenames array
		int nohtml = 0;
		int dirwanted = 0;
		int filewanted = 0;
		int nocsv = 0;
		int smilesstringinput = 0;
		String outputdir = "";
		String outputfile = "";
	    for(int i=0; i < arguments.length; i++){
	    	if (arguments[i].equals("-nohtml")){
	        	nohtml = 1;
	        }
	    	if (arguments[i].equals("-nocsv")){
	        	nocsv = 1;
	        }
	    	if (arguments[i].equals("-outputfile")){
	        	outputfile = arguments[i+1];
	        	filewanted = 1;
	        }
	    	if (arguments[i].equals("-outputdir")){
	        	outputdir = arguments[i+1];
	        	dirwanted = 1;
	        }
	    	if (arguments[i].equals("-smiles")){
	    		smilesstringinput = 1;
	    		//make a file out of arguments[i+1] and modify arguments[i+1] to end with .smiles
	    		String smilesfilename = "smartcypxyz.smiles";
	    		String smilesstring = arguments[i+1]; 
	    		arguments[i+1] = smilesfilename;
	    		PrintWriter smilesfile;
	    		try {
	    			smilesfile = new PrintWriter(new BufferedWriter(new FileWriter("smartcypxyz.smiles")));
	    			smilesfile.println(smilesstring);
	    			smilesfile.close();
	    		} catch (IOException e) {
	    			System.out.println("Could not create temporary smiles file");
	    			e.printStackTrace();
	    		}
	    	}
	    }
	    String[] filenames;
	    if (nohtml == 1 || dirwanted == 1 || filewanted == 1 || nocsv == 1 || smilesstringinput == 1){
	        ArrayList<String> tmplist = new ArrayList<String>();
	        Collections.addAll(tmplist, arguments);
	        if (dirwanted == 1){
	        	//a specific output directory has been requested
	        	tmplist.remove(outputdir);
	        	tmplist.remove("-outputdir");
	        	File dir = new File(outputdir);
	        	//check if the directory exists, otherwise create it
	        	if (!dir.exists()){
	        		dir.mkdir();
	        	}
	        	outputdir = outputdir + File.separator;
	        }
	        if (filewanted == 1){
	        	//a specific filename base has been requested
	        	tmplist.remove(outputfile);
	        	tmplist.remove("-outputfile");
	        }
	        if (nohtml == 1) tmplist.remove("-nohtml");
	        if (nocsv == 1) tmplist.remove("-nocsv");
	        if (smilesstringinput == 1) tmplist.remove("-smiles");
	        filenames = (String[])tmplist.toArray(new String[0]);
	    }
	    else {
	    	filenames = arguments;
	    }
	    //end of input flags

		// Date and Time is used as part of the names of outfiles
		String dateAndTime = WhichCypMain.getDateAndTime();


		// Read in structures/molecules
		System.out.println("\n ************** Reading molecule structures **************");
		IAtomContainerSet moleculeSet = WhichCyp.readInStructures(filenames);

		IAtomContainer molecule;
		int[][][] colorlabelatomset = new int[moleculeSet.getAtomContainerCount()][5][200]; //nr of mols, nr of isoforms, largest possible nr of atoms in a molecule

		for(int moleculeIndex = 0; moleculeIndex < moleculeSet.getAtomContainerCount(); moleculeIndex++){
			molecule = moleculeSet.getAtomContainer(moleculeIndex);
			
			System.out.println("\n ************** Molecule " + molecule.getID() + " **************");
			
			int[][] colorlabelatoms = predictIsoforms(molecule);
			
			colorlabelatomset[moleculeIndex] = colorlabelatoms;			
			
			/*
			// print the colorlabelatoms matrix
			for (int i = 0; i < colorlabelatoms.length; i++){
				for (int j = 0; j < colorlabelatoms[i].length; j++){
					System.out.print(colorlabelatoms[i][j] + ",");
				}
				System.out.print("\n");
			}
			*/
			
		}
		
		//don't write csv files if there are no molecules in the input
		if (moleculeSet.getAtomContainerCount()>0){
			if (nocsv==0){
				// Write results as CSV
				System.out.println("\n ************** Writing Results as CSV **************");
				WriteResultsAsCSV writeResultsAsCSV = new WriteResultsAsCSV(dateAndTime, arguments, outputdir, outputfile);
				writeResultsAsCSV.writeCSV(moleculeSet);
			}
		}
		
		if (nohtml==0){
			//use ChemDoodle HTML output
			System.out.println("\n ************** Writing Results as ChemDoodle HTML **************");
			WriteResultsAsChemDoodleHTML writeResultsAsChemDoodle = new WriteResultsAsChemDoodleHTML(dateAndTime, filenames, outputdir, outputfile);
			writeResultsAsChemDoodle.writeHTML(moleculeSet, colorlabelatomset);
		}


	}

	// The Date and Time is used as part of the output filenames
	private String getDateAndTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		Date date = new Date();
		return dateFormat.format(date);
	}



	// Reads the molecule infiles
	// Stores MoleculeKUs and AtomKUs
	public static IAtomContainerSet readInStructures(String[] inFileNames) throws CloneNotSupportedException, CDKException{

		IAtomContainerSet moleculeSet = new AtomContainerSet();


		List<IAtomContainer> moleculeList;
		IChemObjectBuilder builder = DefaultChemObjectBuilder.getInstance();
		ISimpleChemObjectReader reader;

		File inputFile;
		String infileName;
		ReaderFactory readerFactory;
		IChemFile emptyChemFile;		
		IChemFile chemFile;


		// Iterate over all molecule infiles (it can be a single file)
		int moleculeFileNr;
		int highestMoleculeID = 1;
		for (moleculeFileNr = 0; moleculeFileNr < inFileNames.length; moleculeFileNr++) {
			
			infileName = inFileNames[moleculeFileNr];	
			inputFile = new File(infileName);

			readerFactory = new ReaderFactory();
			
			boolean deducebonds = false;

			try {

				if (infileName.endsWith(".sdf")) {  
					reader = new MDLReader(new FileReader(infileName));
				}
				else if (infileName.endsWith(".smi")){
					reader = new SMILESReader(new FileReader(infileName));
					deducebonds = true;
				}
				else	 reader = readerFactory.createReader(new FileReader(inputFile));


				emptyChemFile = builder.newInstance(IChemFile.class);
				chemFile = (IChemFile) reader.read(emptyChemFile);

				if (chemFile == null) continue;	

				//System.out.println(chemFile.toString());

				// Get Molecules
				moleculeList = ChemFileManipulator.getAllAtomContainers(chemFile);

				// Iterate Molecules
				IAtomContainer molecule;
				IAtomContainer iAtomContainerTmp;		
				IAtomContainer iAtomContainer;	
				CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(DefaultChemObjectBuilder.getInstance());
				for(int atomContainerNr = 0; atomContainerNr < moleculeList.size() ; atomContainerNr++){				
					iAtomContainerTmp = moleculeList.get(atomContainerNr);	
					
					iAtomContainer = AtomContainerManipulator.removeHydrogens(iAtomContainerTmp);
					
					//check number of atoms, if less than 2 don't add molecule
					if (iAtomContainer.getAtomCount()>1){
						//System.out.println(iAtomContainer.getProperties());
			
						try{
							AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(iAtomContainer);
							
							if(deducebonds){
								DeduceBondSystemTool dbst = new DeduceBondSystemTool();
								iAtomContainer = dbst.fixAromaticBondOrders(iAtomContainer);
							}
							
							adder.addImplicitHydrogens(iAtomContainer);
							CDKHueckelAromaticityDetector.detectAromaticity(iAtomContainer);
						
							
							//moleculeKU = new MoleculeKU(iAtomContainer);
							molecule = new AtomContainer(iAtomContainer);
							molecule.setID(Integer.toString(highestMoleculeID));
							//set the molecule title in the moleculeKU object
							if (iAtomContainer.getProperty("SMIdbNAME")!="" && iAtomContainer.getProperty("SMIdbNAME")!=null) {
								iAtomContainer.setProperty(CDKConstants.TITLE, iAtomContainer.getProperty("SMIdbNAME"));
							}
							molecule.setProperty(CDKConstants.TITLE, iAtomContainer.getProperty(CDKConstants.TITLE));
							molecule.setProperties(iAtomContainer.getProperties());
							moleculeSet.addAtomContainer(molecule);
							
						}
						catch(Exception exc) {
							System.out.println("Problems with molecule " + (atomContainerNr + 1));
							System.out.println("This molecule will be ignored and not written to output files.");
							if (moleculeSet.getProperty("ErrorMessage")!=null) moleculeSet.setProperty("ErrorMessage", moleculeSet.getProperty("ErrorMessage") + "\n Problems with molecule " + (atomContainerNr + 1));
							else moleculeSet.setProperty("ErrorMessage", "Problems with molecule " + (atomContainerNr + 1));
				            exc.printStackTrace(System.out);
				        }
						highestMoleculeID++;
					}

				}
				System.out.println(moleculeList.size() + " molecules were read from the file "+ inFileNames[moleculeFileNr]);

			} 
			catch (FileNotFoundException e) {
				System.out.println("File " + inFileNames[moleculeFileNr] + " not found");
				e.printStackTrace();
			} 
			catch (IOException e) {e.printStackTrace();}
		}
		return moleculeSet;
	}

	// predict which isoform a molecule will bind to
	public static int[][] predictIsoforms(IAtomContainer molecule) throws Exception{
		// compute Faulon signatures for the molecule
		int startHeight = 0;
		int endHeight = 3;
		String[] isoforms = {"1a2","2c9","2c19","2d6","3a4"};
		int[] SensitivityWarningLimits = {21,14,18,3,18};  //set to maximum number of allowed missing signatures of height 3 for which the test set gives a sensitivity > 0.6
		int[][] colorlabels = new int[isoforms.length][molecule.getAtomCount()];
		
		int[] MissingSignList = new int[5];
		for(int iso = 0; iso < isoforms.length; iso++){
			
			String modelPath = isoforms[iso] + ".svm";
		    String signaturesPath = isoforms[iso] + "-sign.txt";
		  	
//		    URL modelPath = WhichCypFileHelper.class.getResource("./" + isoforms[iso] + ".txt");
//		    URL signaturesPath = WhichCypFileHelper.class.getResource("./" + isoforms[iso] + "-sign.txt");
		    
//		    URL signaturesPathUrl = FileLocator.toFileURL(WhichCypFileHelper.class.getResource("./" + isoforms[iso] + "-sign.txt").to);
		    
		    //read in the signatures that were used to create the model
		    List<String> signatures;
		    signatures=readSignaturesFile(signaturesPath);
		        
		    //generate signatures for the molecule
			Map<String, Double> moleculeSignatures = new HashMap<String, Double>(); // Contains the signatures for a molecule and the count. We store the count as a double although it is an integer. libsvm wants a double.
			Map<String, Integer> moleculeSignaturesHeight = new HashMap<String, Integer>(); //Contains the height for a specific signature.
			Map<String, List<Integer>> moleculeSignaturesAtomNr = new HashMap<String, List<Integer>>(); //Contains the atomNr for a specific signature.
			
			for (int height = startHeight; height <= endHeight; height++){
				List<String> signs;
				
				signs = generateSignatures( molecule, height ).getSignatures();
							
				Iterator<String> signsIter = signs.iterator();
				int signsIndex = 0;
				int signMissed = 0;
				while (signsIter.hasNext()){
					String currentSignature = signsIter.next();
					if (signatures.contains(currentSignature)){
						if (!moleculeSignaturesAtomNr.containsKey(currentSignature)){
							moleculeSignaturesAtomNr.put(currentSignature, new ArrayList<Integer>());
						}
						moleculeSignaturesHeight.put(currentSignature, height);
						List<Integer> tmpList = moleculeSignaturesAtomNr.get(currentSignature);
						tmpList.add(signsIndex);
						moleculeSignaturesAtomNr.put(currentSignature, tmpList);
						if (moleculeSignatures.containsKey(currentSignature)){
							moleculeSignatures.put(currentSignature, (Double)moleculeSignatures.get(currentSignature)+1.00);
						}
						else{
							moleculeSignatures.put(currentSignature, 1.0);
						}
					}
					else {
                        signMissed++;
                    }
					signsIndex++;
					//if (height==1) System.out.println(currentSignature);
				}
				
				if (height == 3){
					MissingSignList[iso] = signMissed;
				}
			}
			
			
			molecule.setProperty("MissingSignatures" + isoforms[iso], MissingSignList[iso]);
			
			//determine if a sensitivitywarning should be issued
			if(MissingSignList[iso] > SensitivityWarningLimits[iso]){
				molecule.setProperty("SensitivityWarning" + isoforms[iso], 1);
			}
			else molecule.setProperty("SensitivityWarning" + isoforms[iso], 0);
	
			//load the svm model		
			svm_model svmModel;
		    try {
		    	InputStream modelstream = WhichCypFileHelper.class.getResourceAsStream(modelPath);
				svmModel = svm.svm_load_model(new BufferedReader(new InputStreamReader(modelstream)));
	            //svmModel = svm.svm_load_model(modelPath);
	        } catch (IOException e) {
	            throw new Exception("Could not read model file '" + modelPath + "' due to: " + e.getMessage());
	        }
	
	        svm_node[] moleculeArray = new svm_node[moleculeSignatures.size()];
	        
	        //add signatures to moleculeArray
	        Iterator<String> signaturesIter = signatures.iterator();
	        int i = 0;
	        while (signaturesIter.hasNext()){
		        String currentSignature = signaturesIter.next();
		        if (moleculeSignatures.containsKey(currentSignature)){
			        moleculeArray[i] = new svm_node();
			        moleculeArray[i].index = signatures.indexOf(currentSignature)+1; // libsvm assumes that the index starts at one.
			        moleculeArray[i].value = (Double) moleculeSignatures.get(currentSignature);
			        i = i + 1;
		        }
	        }        
			//run the prediction
	        double prediction = 0.0;
	        prediction = svm.svm_predict(svmModel, moleculeArray); //1.0 is non-binder, 2.0 is binder
	        //System.out.println("Prediction: " + prediction + " isoform " + iso);
	        
	        //set molecular properties for binder non/binder        
	        int binds = 1;
	        int dontbinds = 0;
	        
	        if (prediction == 2.0 && iso == 0) {molecule.setProperty("Binder1A2", binds);}
	        else if (iso == 0) {molecule.setProperty("Binder1A2", dontbinds);}
	        if (prediction == 2.0 && iso == 1) {molecule.setProperty("Binder2C9", binds);}
	        else if (iso == 1) {molecule.setProperty("Binder2C9", dontbinds);}
	        if (prediction == 2.0 && iso == 2) {molecule.setProperty("Binder2C19", binds);}
	        else if (iso == 2) {molecule.setProperty("Binder2C19", dontbinds);}
	        if (prediction == 2.0 && iso == 3) {molecule.setProperty("Binder2D6", binds);}
	        else if (iso == 3) {molecule.setProperty("Binder2D6", dontbinds);}
	        if (prediction == 2.0 && iso == 4) {molecule.setProperty("Binder3A4", binds);}
	        else if (iso == 4) {molecule.setProperty("Binder3A4", dontbinds);}
	                
	        // Get the most significant signature for classification or the sum of all gradient components for regression.
	        List<Double> gradientComponents = new ArrayList<Double>();
	        int nOverk = fact(svmModel.nr_class)/(fact(2)*fact(svmModel.nr_class-2)); // The number of decision functions for a classification.
	        double decValues[] = new double[nOverk];
	        double lowerPointValue[] = new double[nOverk];
	        double higherPointValue[] = new double[nOverk];
	        svm.svm_predict_values(svmModel, moleculeArray, decValues);
	        lowerPointValue = decValues.clone();
	        for (int element = 0; element < moleculeArray.length; element++){
		        // Temporarily increase the descriptor value by one to compute the corresponding component of the gradient of the decision function.
		        moleculeArray[element].value = moleculeArray[element].value + 1.00;
		        svm.svm_predict_values(svmModel, moleculeArray, decValues);
		        higherPointValue = decValues.clone();
		        double gradComponentValue = 0.0;
		        
		        for (int curDecisionFunc = 0; curDecisionFunc < nOverk; curDecisionFunc++) {
			        if (svmModel.rho[curDecisionFunc] > 0.0){ // Check if the decision function is reversed.
			        	gradComponentValue = gradComponentValue + higherPointValue[curDecisionFunc]-lowerPointValue[curDecisionFunc];
			        }
			        else{
			        	gradComponentValue = gradComponentValue + lowerPointValue[curDecisionFunc]-higherPointValue[curDecisionFunc];
			        }
		        }
			    
		        gradientComponents.add(gradComponentValue);
		        // Set the value back to what it was.
		        moleculeArray[element].value = moleculeArray[element].value - 1.00;

	        }

	        String significantSignature="";
	        List<Integer> centerAtoms = new ArrayList<Integer>();
	        int height = -1;
	        
	        if (prediction > 1.5){ // Look for most positive component.
	        	double maxComponent = -1.0;
	        	int elementMaxVal = -1;
	        	for (int element = 0; element < moleculeArray.length; element++){
	        			if (gradientComponents.get(element) > maxComponent){
	        				maxComponent = gradientComponents.get(element);
	        				elementMaxVal = element;
	        			}
	        	}
	        	if (maxComponent > 0.0){
	        		//System.out.println("Max atom: " + moleculeSignaturesAtomNr.get(signatures.get(moleculeArray[elementMaxVal].index-1)) + ", max val: " + gradientComponents.get(elementMaxVal) + ", signature: " + signatures.get(moleculeArray[elementMaxVal].index-1) + ", height: " + moleculeSignaturesHeight.get(signatures.get(moleculeArray[elementMaxVal].index-1)));

	        		significantSignature=signatures.get(moleculeArray[elementMaxVal].index-1);
	        		height=moleculeSignaturesHeight.get(signatures.get(moleculeArray[elementMaxVal].index-1));
	        		centerAtoms=moleculeSignaturesAtomNr.get(signatures.get(moleculeArray[elementMaxVal].index-1));

	        	}
	        	else{
	        		//System.out.println("No significant signature.");
	        	}
	        }
	        else{
	        	double minComponent = 1.0;
	        	int elementMinVal = -1;
	        	for (int element = 0; element < moleculeArray.length; element++){
	        		if (gradientComponents.get(element) < minComponent){
	        			minComponent = gradientComponents.get(element);
	        			elementMinVal = element;
	        		}
	        	}
	        	if (minComponent < 0.0){
	        		//System.out.println("Min atom: " + moleculeSignaturesAtomNr.get(signatures.get(moleculeArray[elementMinVal].index-1)) + ", min val: " + gradientComponents.get(elementMinVal) + ", signature: " + signatures.get(moleculeArray[elementMinVal].index-1) + ", height: " + moleculeSignaturesHeight.get(signatures.get(moleculeArray[elementMinVal].index-1)));

	        		significantSignature=signatures.get(moleculeArray[elementMinVal].index-1);
	        		height=moleculeSignaturesHeight.get(signatures.get(moleculeArray[elementMinVal].index-1));
	        		centerAtoms=moleculeSignaturesAtomNr.get(signatures.get(moleculeArray[elementMinVal].index-1));

	        	}
	        	else{
	        		//System.out.println("No significant signature.");
	        	}
	        }
	        
	        //create list of the atoms that should be marked in html figures
            if (significantSignature.length()>0){
            	//OK, we got a significant signature, let's define the atoms that should be colored
                
                for (int centerAtom : centerAtoms){
                	//match.putAtomResult( centerAtom, match.getClassification() );
                	
                	colorlabels[iso][centerAtom] = 1;
                        
                    int currentHeight=0;
                    List<Integer> lastNeighbours=new ArrayList<Integer>();
                    lastNeighbours.add(centerAtom);
                    
                    while (currentHeight<height){
                    
                        List<Integer> newNeighbours=new ArrayList<Integer>();

                        //for all lastNeighbours, get new neighbours
                        for (Integer lastneighbour : lastNeighbours){
                            for (IAtom nbr : molecule.getConnectedAtomsList(molecule.getAtom(lastneighbour))){
                            
                                //Set each neighbour atom to overall match classification
                            	int nbrAtomNr = molecule.getAtomNumber(nbr);
                            	//match.putAtomResult( nbrAtomNr, match.getClassification() );
                            	colorlabels[iso][nbrAtomNr] = 1;
                            
                            	newNeighbours.add(nbrAtomNr);
                            
                            }
                        }
                    
                        lastNeighbours=newNeighbours;

                        currentHeight++;
                    }                     
                 }
	        }
	        
		}
		return colorlabels;
	}	
	
	private static List<String> readSignaturesFile(String signaturesPath) throws Exception {
		//logger.debug("Reading signature file: " + signaturesPath);
		List<String> signatures = new ArrayList<String>(); // Contains signatures. We use the indexOf to retrieve the order of specific signatures in descriptor array.
		try {
//			InputStream signaturestream = WhichCyp.class.getClassLoader().getResourceAsStream(signaturesPath);
			InputStream signaturestream = WhichCypFileHelper.class.getResourceAsStream(signaturesPath);
			
			
			BufferedReader signaturesReader = new BufferedReader(new InputStreamReader(signaturestream));
			String signature;
			while ( (signature = signaturesReader.readLine()) != null ) {
				signatures.add(signature);
			}
		} catch (FileNotFoundException e) {
			//LogUtils.debugTrace(logger, e);
			throw new Exception("Error reading signatures file " + signaturesPath + ": " + e.getMessage());
			} catch (IOException e) {
				//LogUtils.debugTrace(logger, e);
				throw new Exception("Error reading signatures file " + signaturesPath + ": " + e.getMessage());
			}
			//logger.debug("Reading signature file: " + signaturesPath + " completed successfully");
			return signatures;

	}

	public static AtomSignatures generateSignatures(IAtomContainer molecule, int height) throws Exception{

	//Adapt IMolecule to ICDKMolecule
	//ICDKManager cdk = Activator.getDefault().getJavaCDKManager();
	//ICDKMolecule cdkmol = cdk.asCDKMolecule(mol);


	List<String> signatureString=new ArrayList<String>();

	//Create one signature per atom of height h
	for ( int atomNr = 0; atomNr < molecule.getAtomCount(); atomNr++){
	AtomSignature gensign = new AtomSignature(atomNr, height, molecule);
	signatureString.add( gensign.toCanonicalString()); 
	// logger.debug("Sign for atom " + atomNr + ": " +gensign);
	}

	//Test correct number of produced signatures (should not be an issue)
	if (signatureString.size()!=molecule.getAtomCount())
	throw new Exception("Number of atoms and atom signatures " + "differ for molecule: " + molecule +" ; Number of atoms: " +	molecule.getAtomCount() +
	" and number of signatures produced: "	+ signatureString.size());

	return new AtomSignatures(signatureString);

	}
	
	/**
	* Calculate the factorial of n.
	*
	* @param n the number to calculate the factorial of.
	* @return n! - the factorial of n.
	*/
	static int fact(int n) {
		// Base Case:
		// If n <= 1 then n! = 1.
		if (n <= 1) {
			return 1;
		}
		// Recursive Case:
		// If n > 1 then n! = n * (n-1)!
		else {
			return n * fact(n-1);
		}
	}


}



