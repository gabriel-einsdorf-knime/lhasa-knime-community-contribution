package org.lhasalimited.cyp.whichcyp;

import java.util.Map;

public class WhichCypResults
{

	private int[][] isphormPredictions;
	
	private Map<String, Integer> results;
	
	private String molfile;

	public WhichCypResults(int[][] isphormPredictions, Map<String, Integer> results, String molfile)
	{
		this.isphormPredictions = isphormPredictions;
		this.results = results;
		this.molfile = molfile;
	}
	
	public int[][] getIsphormPredictions()
	{
		return isphormPredictions;
	}

	public void setIsphormPredictions(int[][] isphormPredictions)
	{
		this.isphormPredictions = isphormPredictions;
	}

	public Map<String, Integer> getResults()
	{
		return results;
	}

	public void setResults(Map<String, Integer> results)
	{
		this.results = results;
	}

	public String getMolfile()
	{
		return molfile;
	}

	public void setMolfile(String molfile)
	{
		this.molfile = molfile;
	}
	
	
	
	
	
}
