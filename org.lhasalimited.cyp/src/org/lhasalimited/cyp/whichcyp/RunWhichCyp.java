package org.lhasalimited.cyp.whichcyp;

import java.io.File;
import java.nio.file.Files;

import whichcyp.WhichCyp;

/**
 * Method to run SMARTCyp on a collection of SDF files and generating a seperate
 * output folder for each
 * 
 * @author Samuel
 *
 */
public class RunWhichCyp
{

	/**
	 * Arguements expected: output directory, sdf directory
	 * @param args
	 */
	public static void main(String[] args)
	{
		
		File outDir = new File(args[0]);
		File passedSdfDir = new File(outDir.getAbsolutePath() + "//passed");
		passedSdfDir.mkdirs();

		try
		{

			File[] files = new File(args[1]).listFiles();

			for (File file : files)
			{
				try
				{
					WhichCyp.main(new String[] { "-outputdir", new File(outDir.toString() + "\\" + file.getName()).toString(), file.toString() });
					Files.copy(file.toPath(), new File(passedSdfDir + "\\" + file.getName()).toPath());
				} catch (Exception e)
				{
//					e.printStackTrace();
					System.err.println("Error occured moving to next structure: " + e.getMessage());
				}
			}

		} catch (Exception e)
		{
//			e.printStackTrace();
			System.err.println("Could not recover from error: " + e.getMessage());
		}

	}

}
