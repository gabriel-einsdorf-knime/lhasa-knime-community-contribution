package org.lhasalimited.cyp.whichcyp;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lhasalimited.cyp.smartcyp.SMARTSnEnergiesTable;
import org.openscience.old.cdk.CDKConstants;
import org.openscience.old.cdk.DefaultChemObjectBuilder;
import org.openscience.old.cdk.Molecule;
import org.openscience.old.cdk.MoleculeSet;
import org.openscience.old.cdk.aromaticity.CDKHueckelAromaticityDetector;
import org.openscience.old.cdk.exception.CDKException;
import org.openscience.old.cdk.interfaces.IAtomContainer;
import org.openscience.old.cdk.interfaces.IChemFile;
import org.openscience.old.cdk.interfaces.IChemObjectBuilder;
import org.openscience.old.cdk.interfaces.IMolecule;
import org.openscience.old.cdk.io.ISimpleChemObjectReader;
import org.openscience.old.cdk.io.MDLReader;
import org.openscience.old.cdk.io.MDLV2000Writer;
import org.openscience.old.cdk.smiles.DeduceBondSystemTool;
import org.openscience.old.cdk.tools.CDKHydrogenAdder;
import org.openscience.old.cdk.tools.manipulator.AtomContainerManipulator;
import org.openscience.old.cdk.tools.manipulator.ChemFileManipulator;

import whichcyp.WhichCyp;

public class WhichCypProcessor
{

	public WhichCypResults process(String writeMolfile, String identifier) throws Exception
	{

		IMolecule molecule = readInStructures(writeMolfile, identifier);
		
		int[][] results = WhichCyp.predictIsoforms((Molecule) molecule);
		
		return new WhichCypResults(results, extractProperties(molecule), writeMolfile(molecule));
	}
	
	/**
	 * Extract the properties from the predicted structure. See {@link WhichCypPropertySupport#PROPERTY_NAMES} for
	 * more information. 
	 * @param predicted
	 * @return
	 */
	private Map<String, Integer> extractProperties(IAtomContainer predicted)
	{
		Map<String, Integer> results = new HashMap<String, Integer>();
		
		for(String name : WhichCypPropertySupport.PROPERTY_NAMES)
		{
			results.put(name, (int) predicted.getProperty(name));
		}
		
		return results;
	}

	public static String writeMolfile(IAtomContainer structure) throws CDKException, IOException
	{
		StringWriter writer = new StringWriter();
		MDLV2000Writer molfileWriter = new MDLV2000Writer(writer);
		molfileWriter.write(structure);
		molfileWriter.close();
		
		return writer.toString();
	}
	
	public static IMolecule readInStructures(String molfile, String identifier) throws CloneNotSupportedException, CDKException
	{

		SMARTSnEnergiesTable table = new SMARTSnEnergiesTable();
		HashMap<String, Double> SMARTSnEnergiesTable = table.getSMARTSnEnergiesTable();

		MoleculeSet moleculeSet = new MoleculeSet();

		List<IAtomContainer> moleculeList;
		IChemObjectBuilder builder = DefaultChemObjectBuilder.getInstance();
		ISimpleChemObjectReader reader;

		IChemFile emptyChemFile;
		IChemFile chemFile;

		boolean deducebonds = false;

		try
		{

			reader = new MDLReader(new StringReader(molfile));
			emptyChemFile = builder.newInstance(IChemFile.class);
			chemFile = (IChemFile) reader.read(emptyChemFile);

			// System.out.println(chemFile.toString());

			// Get Molecules
			moleculeList = ChemFileManipulator.getAllAtomContainers(chemFile);

			Molecule molecule;
			IAtomContainer iAtomContainerTmp;
			IAtomContainer iAtomContainer;
			CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(DefaultChemObjectBuilder.getInstance());
			for (int atomContainerNr = 0; atomContainerNr < moleculeList.size(); atomContainerNr++)
			{
				iAtomContainerTmp = moleculeList.get(atomContainerNr);

				iAtomContainer = AtomContainerManipulator.removeHydrogens(iAtomContainerTmp);

				// check number of atoms, if less than 2 don't add molecule
				if (iAtomContainer.getAtomCount() > 1)
				{
					// System.out.println(iAtomContainer.getProperties());

					try
					{
						AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(iAtomContainer);

						if (deducebonds)
						{
							DeduceBondSystemTool dbst = new DeduceBondSystemTool();
							iAtomContainer = dbst.fixAromaticBondOrders((IMolecule) iAtomContainer);
						}

						adder.addImplicitHydrogens(iAtomContainer);
						CDKHueckelAromaticityDetector.detectAromaticity(iAtomContainer);

						// moleculeKU = new MoleculeKU(iAtomContainer);
						molecule = new Molecule(iAtomContainer);
						molecule.setID(identifier);
						// set the molecule title in the moleculeKU object
						if (iAtomContainer.getProperty("SMIdbNAME") != "" && iAtomContainer.getProperty("SMIdbNAME") != null)
						{
							iAtomContainer.setProperty(CDKConstants.TITLE, iAtomContainer.getProperty("SMIdbNAME"));
						}
						molecule.setProperty(CDKConstants.TITLE, iAtomContainer.getProperty(CDKConstants.TITLE));
						molecule.setProperties(iAtomContainer.getProperties());
						moleculeSet.addMolecule(molecule);

					} catch (Exception exc)
					{
						System.out.println("Problems with molecule " + (atomContainerNr + 1));
						System.out.println("This molecule will be ignored and not written to output files.");
						if (moleculeSet.getProperty("ErrorMessage") != null)
							moleculeSet.setProperty("ErrorMessage",
									moleculeSet.getProperty("ErrorMessage") + "\n Problems with molecule " + (atomContainerNr + 1));
						else
							moleculeSet.setProperty("ErrorMessage", "Problems with molecule " + (atomContainerNr + 1));
						exc.printStackTrace(System.out);
					}

				}

			}

		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return moleculeSet.getMolecule(0);
	}

}
