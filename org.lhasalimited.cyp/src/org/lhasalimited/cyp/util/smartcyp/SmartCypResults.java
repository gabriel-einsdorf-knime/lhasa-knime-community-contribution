package org.lhasalimited.cyp.util.smartcyp;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains the output from SMARTCyp in the form of the standardised structure and the results for the 
 * properties calculated. 
 * 
 * @author Samuel
 *
 */
public class SmartCypResults
{
	


	private Map<Integer, AtomValues> map;
	private String molfile;
	
	
	public SmartCypResults()
	{
		map = new HashMap<Integer, AtomValues>();
	}
	
	public String getMolfile()
	{
		return molfile;
	}


	public AtomValues getAtom(int i)
	{
		return map.get(i);
	}


	public void writeMolfile(String molfile)
	{
		this.molfile = molfile;
	}


	public void add(int atomIndex, AtomValues values)
	{
		map.put(atomIndex, values);
	}
	
	
}
