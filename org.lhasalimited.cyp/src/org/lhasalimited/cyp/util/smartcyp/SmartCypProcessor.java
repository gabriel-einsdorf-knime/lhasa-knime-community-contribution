package org.lhasalimited.cyp.util.smartcyp;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.lhasalimited.cyp.smartcyp.MoleculeKU;
import org.lhasalimited.cyp.smartcyp.MoleculeKU.SMARTCYP_PROPERTY;
import org.lhasalimited.cyp.smartcyp.SMARTSnEnergiesTable;
import org.openscience.old.cdk.Atom;
import org.openscience.old.cdk.AtomContainer;
import org.openscience.old.cdk.CDKConstants;
import org.openscience.old.cdk.DefaultChemObjectBuilder;
import org.openscience.old.cdk.aromaticity.CDKHueckelAromaticityDetector;
import org.openscience.old.cdk.exception.CDKException;
import org.openscience.old.cdk.graph.ConnectivityChecker;
import org.openscience.old.cdk.interfaces.IAtom;
import org.openscience.old.cdk.interfaces.IAtomContainer;
import org.openscience.old.cdk.interfaces.IChemFile;
import org.openscience.old.cdk.interfaces.IChemObjectBuilder;
import org.openscience.old.cdk.interfaces.IMolecule;
import org.openscience.old.cdk.interfaces.IMoleculeSet;
import org.openscience.old.cdk.io.ISimpleChemObjectReader;
import org.openscience.old.cdk.io.MDLReader;
import org.openscience.old.cdk.io.MDLV2000Writer;
import org.openscience.old.cdk.smiles.DeduceBondSystemTool;
import org.openscience.old.cdk.tools.CDKHydrogenAdder;
import org.openscience.old.cdk.tools.manipulator.AtomContainerManipulator;
import org.openscience.old.cdk.tools.manipulator.ChemFileManipulator;

/**
 * An adaption of the SmartCyp class to handle the processing of Mol
 * representations as well as {@link IAtomContainer} structures. <br>
 * <br>
 * Refactoring has been undertaken to replace {@link Atom} objects with
 * {@link IAtom} and removal of casting to {@link Atom} class. This allows for
 * the use of {@link AtomContainer} and
 * {@link org.openscience.cdk.silent.AtomContainer} <br>
 * <br>
 * The code has been updated to use CDK 1.5.12 as it is intended for use within
 * the KNIME environment.
 *
 * 
 * 
 * @author Samuel
 *
 */
public class SmartCypProcessor
{

	private boolean nOxidationCorrection;

	public SmartCypProcessor(boolean nOxidationCorrection)
	{
		this.nOxidationCorrection = nOxidationCorrection;
	}

	/**
	 * Process a single molstring
	 * 
	 * @param structure
	 *            The molfile string
	 * @param id
	 *            The ID of the structure
	 * 
	 * @return The result form SmartCyp, this is a list as individual components
	 *         may be returned under this API
	 * @throws Exception
	 */
	public List<SmartCypResults> process(String structure, String id) throws Exception
	{
		// Produce SMARTSnEnergiesTable object
		SMARTSnEnergiesTable SMARTSnEnergiesTable = new SMARTSnEnergiesTable();

		// Read in structures/molecules
		List<IAtomContainer> moleculeSet = readInStructures(structure, id);

		List<MoleculeKU> result = process(moleculeSet, SMARTSnEnergiesTable);		
		
		return convertToSmartCypResult(result);
	}

	private List<SmartCypResults> convertToSmartCypResult(List<MoleculeKU> result) throws Exception
	{
		List<SmartCypResults> output = new ArrayList<SmartCypResults>();

		for (MoleculeKU iter : result)
		{

			SmartCypResults iterResult = new SmartCypResults();
			iterResult.writeMolfile(getMolfile(iter));

			for (int atomIndex = 0; atomIndex < iter.getAtomCount(); atomIndex++)
			{

				IAtom currentAtom = iter.getAtom(atomIndex);
				String currentAtomType = currentAtom.getSymbol();

				if (currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P") || currentAtomType.equals("S"))
				{
					AtomValues values = new AtomValues();
					
					values.setRanking((int) SMARTCYP_PROPERTY.Ranking.get(currentAtom));
					
					if (SMARTCYP_PROPERTY.Score.get(currentAtom) != null)
					{
						values.setScore(SMARTCYP_PROPERTY.Score.get(currentAtom).doubleValue());
					}
					
					values.setEnergy(SMARTCYP_PROPERTY.Energy.get(currentAtom).doubleValue());
					
					values.setAccessibility(SMARTCYP_PROPERTY.Accessibility.get(currentAtom).doubleValue());
					
					if (SMARTCYP_PROPERTY.Score2D6.get(currentAtom) != null)
					{
						values.setRanking2d6((int)SMARTCYP_PROPERTY.Ranking2D6.get(currentAtom));
						values.setScore2D6(SMARTCYP_PROPERTY.Score2D6.get(currentAtom).doubleValue());
					}
					
					values.setSpan2end(SMARTCYP_PROPERTY.Span2End.get(currentAtom).doubleValue());
					
					if (SMARTCYP_PROPERTY.Dist2ProtAmine.get(currentAtom) != null)
					{
						values.setDist2ProtAmine((int)SMARTCYP_PROPERTY.Dist2ProtAmine.get(currentAtom));
					} else
					{
						values.setDist2ProtAmine(0);
					}
					
					if (SMARTCYP_PROPERTY.Score2C9.get(currentAtom) != null)
					{
						values.setScore2C9(SMARTCYP_PROPERTY.Score2C9.get(currentAtom).doubleValue());
						values.setRanking2C9((int)SMARTCYP_PROPERTY.Ranking2C9.get(currentAtom));
					}
					
					if (SMARTCYP_PROPERTY.Dist2CarboxylicAcid.get(currentAtom) != null)
					{
						values.setDist2CarboxylicAcid((int)SMARTCYP_PROPERTY.Dist2CarboxylicAcid.get(currentAtom));
					}
					if (SMARTCYP_PROPERTY.SASA2D.get(currentAtom) != null)
					{
						values.setSASA2D(SMARTCYP_PROPERTY.SASA2D.get(currentAtom).doubleValue());
					}
					
					iterResult.add(atomIndex, values);
				}
			}
			output.add(iterResult);
		}

		return output;
	}

	/**
	 * Get the molfile representation 
	 * @param iter
	 * @return
	 * @throws CDKException
	 * @throws IOException
	 */
	private String getMolfile(MoleculeKU iter) throws CDKException, IOException
	{
		StringWriter stringWriter = new StringWriter();
		MDLV2000Writer writer = new MDLV2000Writer(stringWriter);
		writer.write(iter);
		writer.close();

		return stringWriter.toString();
	}

	/**
	 * Process a collection of CDK structures. The ID's should be set as the ID
	 * of the {@link IAtomContainer}
	 * 
	 * @param moleculeSet
	 * @param SMARTSnEnergiesTable
	 * @return
	 * @throws Exception
	 */
	public List<MoleculeKU> process(List<IAtomContainer> moleculeSet, SMARTSnEnergiesTable SMARTSnEnergiesTable) throws Exception
	{

		List<MoleculeKU> results = new ArrayList<MoleculeKU>();

		MoleculeKU moleculeKU;
		for (int moleculeIndex = 0; moleculeIndex < moleculeSet.size(); moleculeIndex++)
		{
			moleculeKU = (MoleculeKU) moleculeSet.get(moleculeIndex);

			// System.out.println("\n ************** Matching SMARTS to assign
			// Energies **************");
			moleculeKU.assignAtomEnergies(SMARTSnEnergiesTable.getSMARTSnEnergiesTable());

			// System.out.println("\n ************** Calculating shortest
			// distance to protonated amine **************");
			moleculeKU.calculateDist2ProtAmine();

			// System.out.println("\n ************** Calculating shortest
			// distance to carboxylic acid **************");
			moleculeKU.calculateDist2CarboxylicAcid();

			// System.out.println("\n ************** Calculating
			// Span2End**************");
			moleculeKU.calculateSpan2End();

			if (nOxidationCorrection)
			{
				// System.out.println("\n ************** Add Empirical Nitrogen
				// Oxidation Corrections **************");
				moleculeKU.unlikelyNoxidationCorrection();
			}

			// System.out.println("\n ************** Calculating Accessabilities
			// and Atom Scores**************");
			moleculeKU.calculateAtomAccessabilities();
			// compute 2DSASA
			double[] SASA = moleculeKU.calculateSASA();
			moleculeKU.calculateAtomScores();
			moleculeKU.calculate2D6AtomScores();
			moleculeKU.calculate2C9AtomScores();

			// System.out.println("\n ************** Identifying, sorting and
			// ranking C, N, P and S atoms **************");
			moleculeKU.sortAtoms();
			moleculeKU.rankAtoms();
			moleculeKU.sortAtoms2D6();
			moleculeKU.rankAtoms2D6();
			moleculeKU.sortAtoms2C9();
			moleculeKU.rankAtoms2C9();

			results.add(moleculeKU);

		}

		return results;

	}

	public static List<IAtomContainer> readInStructures(String molfile, String identifier) throws CloneNotSupportedException, CDKException
	{

		SMARTSnEnergiesTable table = new SMARTSnEnergiesTable();
		HashMap<String, Double> SMARTSnEnergiesTable = table.getSMARTSnEnergiesTable();

		List<IAtomContainer> moleculeSet = new ArrayList<IAtomContainer>();

		List<IAtomContainer> moleculeList;
		IChemObjectBuilder builder = DefaultChemObjectBuilder.getInstance();
		ISimpleChemObjectReader reader;

		IChemFile emptyChemFile;
		IChemFile chemFile;

		boolean deducebonds = false;

		try
		{

			reader = new MDLReader(new StringReader(molfile));
			emptyChemFile = builder.newInstance(IChemFile.class);
			chemFile = (IChemFile) reader.read(emptyChemFile);

			// System.out.println(chemFile.toString());

			// Get Molecules
			moleculeList = ChemFileManipulator.getAllAtomContainers(chemFile);

			// Iterate Molecules
			MoleculeKU moleculeKU;
			IAtomContainer iAtomContainerTmp;
			IAtomContainer iAtomContainer;
			CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(DefaultChemObjectBuilder.getInstance());
			for (int atomContainerNr = 0; atomContainerNr < moleculeList.size(); atomContainerNr++)
			{
				iAtomContainerTmp = moleculeList.get(atomContainerNr);

				// Remove salts or solvents... Keep only the largest molecule
				if (!ConnectivityChecker.isConnected(iAtomContainerTmp))
				{
					// System.out.println(atomContainerNr);
					IMoleculeSet fragments = ConnectivityChecker.partitionIntoMolecules(iAtomContainerTmp);

					int maxID = 0;
					int maxVal = -1;
					for (int i = 0; i < fragments.getMoleculeCount(); i++)
					{
						if (fragments.getMolecule(i).getAtomCount() > maxVal)
						{
							maxID = i;
							maxVal = fragments.getMolecule(i).getAtomCount();
						}
					}
					iAtomContainerTmp = fragments.getMolecule(maxID);
				}
				// end of salt removal

				iAtomContainer = AtomContainerManipulator.removeHydrogens(iAtomContainerTmp);

				// check number of atoms, if less than 2 don't add molecule
				if (iAtomContainer.getAtomCount() > 1)
				{
					// System.out.println(iAtomContainer.getProperties());

					AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(iAtomContainer);

					if (deducebonds)
					{
						DeduceBondSystemTool dbst = new DeduceBondSystemTool();
						iAtomContainer = dbst.fixAromaticBondOrders((IMolecule) iAtomContainer);
					}

					adder.addImplicitHydrogens(iAtomContainer);
					CDKHueckelAromaticityDetector.detectAromaticity(iAtomContainer);

					moleculeKU = new MoleculeKU(iAtomContainer, SMARTSnEnergiesTable);
					moleculeSet.add(moleculeKU);
					moleculeKU.setID(identifier);
					// set the molecule title in the moleculeKU object
					if (iAtomContainer.getProperty("SMIdbNAME") != "" && iAtomContainer.getProperty("SMIdbNAME") != null)
					{
						iAtomContainer.setProperty(CDKConstants.TITLE, iAtomContainer.getProperty("SMIdbNAME"));
					}
					moleculeKU.setProperty(CDKConstants.TITLE, iAtomContainer.getProperty(CDKConstants.TITLE));
					moleculeKU.setProperties(iAtomContainer.getProperties());
				}

			}

		} catch (Exception e)
		{
			e.printStackTrace();
		}

		return moleculeSet;
	}

}
