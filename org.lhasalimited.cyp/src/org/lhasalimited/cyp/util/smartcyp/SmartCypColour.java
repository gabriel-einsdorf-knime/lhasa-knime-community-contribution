package org.lhasalimited.cyp.util.smartcyp;

import java.awt.Color;

/**
 * Provide a colour for a given site of metabolism rank
 * @author Samuel
 *
 */
public class SmartCypColour
{

	
	
	static Color[] colours = new Color[]
			{		new Color(235, 155, 37), 	// Orange
					new Color(252, 117, 117), 	// light red
					new Color(83, 178, 86), 	// light green
					new Color(238, 247, 151)	// yellow
					
			};
	
	/**
	 * Will return a colour for the given rank. Provides a unique colour up to point given by {@link #size()} and then loops back round 
	 * to the start,
	 * 
	 * 
	 * @param rank
	 * @return
	 */
	public static Color getColourForRank(int rank)
	{
		// Correct rank to 0 index
		rank = rank -1;
		
		if(rank >= (colours.length - 1))
			rank = rank % colours.length;
		
		return colours[rank];
	}
	
	
	/**
	 * The number of colours available
	 * @return
	 */
	public static int size()
	{
		return colours.length;
	}
	
}
