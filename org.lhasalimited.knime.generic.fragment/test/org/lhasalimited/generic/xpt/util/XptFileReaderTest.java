//package org.lhasalimited.generic.xpt.util;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertTrue;
//import static org.junit.Assert.fail;
//
//import java.io.File;
//import java.io.IOException;
//import java.net.URL;
//
//import org.eclipse.core.runtime.FileLocator;
//import org.eclipse.core.runtime.Platform;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//import org.osgi.framework.Bundle;
//
//public class XptFileReaderTest {
//	// The number of rows in the test files
//	private final int ROWS_IN_BG = 4099;
//	private final int ROWS_IN_EX = 3668;
//
//	// The number of columns in the test files
//	private final int COLUMNS_IN_BG = 22;
//	private final int COLUMNS_IN_EX = 29;
//
//	XptFileReader readerOne;
//	XptFileReader readerTwo;
//	XptFileReader readerThree;
//
//	@Before
//	public void setup() throws Exception {
//		Bundle bundle = Platform.getBundle("org.lhasalimited.generic");
//
//		System.out.println(bundle.toString());
//
//		
//		
//		File file = new File(FileLocator.resolve(bundle.getEntry("./files/xpt/bg.xpt")).toURI());
//		File b = new File(FileLocator.resolve(bundle.getEntry("./files/xpt/ex.xpt")).toURI());
//		File b2 = new File(FileLocator.resolve(bundle.getEntry("./files/xpt/bg2.xpt")).toURI());
//
//		// File file = new
//		// File(getClass().getResource("./resources/bg.xpt").toURI());
//		// File b = new
//		// File(getClass().getResource("./resources/ex.xpt").toURI());
//		readerOne = new XptFileReader(file);
//		readerTwo = new XptFileReader(b);
//		readerThree = new XptFileReader(b2);
//	}
//
//	@After
//	public void tearDown() throws IOException {
//		readerOne.close();
//		readerTwo.close();
//	}
//
//	@Test
//	public void testXptFileReader() {
//		try {
//			Bundle bundle = Platform.getBundle("org.lhasalimited.generic");
//
//			System.out.println(bundle.toString());
//
//			File a = new File(FileLocator.resolve(bundle.getEntry("./files/xpt/bg.xpt")).toURI());
//			File b = new File(FileLocator.resolve(bundle.getEntry("./files/xpt/ex.xpt")).toURI());
//
//			XptFileReader readerA = new XptFileReader(a);
//			XptFileReader readerB = new XptFileReader(b);
//
//			readerA.close();
//			readerB.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//			fail(e.getMessage());
//		}
//
//	}
//
//	@Test
//	public void testGetDataSetName() {
//		assertEquals("", readerOne.getDataSetName());
//		assertEquals("", readerTwo.getDataSetName());
//		assertEquals("BODY WEIGHT GAIN", readerThree.getDataSetName());
//	}
//
//	@Test
//	public void testGetProgress() {
//		// Check a partial read of the file
//		int count = 0;
//		while (count < 100) {
//			readerOne.next();
//			count++;
//		}
//		assertEquals(0.03021636408853519, readerOne.getProgress(), 0.0002d);
//
//		// Check the end of the file reports 1d
//		while (readerTwo.hasNext())
//			readerTwo.next();
//
//		assertEquals(1d, readerTwo.getProgress(), 0.0002d);
//	}
//
//	@Test
//	public void testReadDataEntry() {
//		try {
//			assertTrue(readerOne.readDataEntry().isPresent());
//			assertTrue(readerTwo.readDataEntry().isPresent());
//
//		} catch (IOException e) {
//			e.printStackTrace();
//			fail(e.getMessage());
//		}
//
//	}
//
//	@Test
//	public void testGetVersion() {
//		assertEquals("9.1", readerOne.getVersion());
//		assertEquals("9.1", readerTwo.getVersion());
//	}
//
//	@Test
//	public void testGetOS() {
//		assertEquals("XP_PRO", readerOne.getOS());
//		assertEquals("XP_PRO", readerTwo.getOS());
//	}
//
//	@Test
//	public void testGetCreated() {
//		assertEquals("14Apr14:14:26:19", readerOne.getCreated());
//		assertEquals("14Apr14:14:26:19", readerTwo.getCreated());
//	}
//
//	@Test
//	public void testGetModified() {
//		assertEquals("14Apr14:14:26:19", readerOne.getModified());
//		assertEquals("14Apr14:14:26:19", readerTwo.getModified());
//	}
//
//	@Test
//	public void testGetNumFields() {
//		assertEquals(COLUMNS_IN_BG, readerOne.getNumFields());
//		assertEquals(COLUMNS_IN_EX, readerTwo.getNumFields());
//	}
//
//	@Test
//	public void testGetFieldDetails() {
//		XptFields fieldsA = readerOne.getFieldDetails();
//		XptFields fieldsB = readerTwo.getFieldDetails();
//
//		assertEquals(COLUMNS_IN_BG, fieldsA.getNumFields());
//		assertEquals(COLUMNS_IN_EX, fieldsB.getNumFields());
//
//	}
//
//	@Test
//	public void testClose() {
//		try {
//			readerOne.close();
//			readerTwo.close();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}
//
//	@Test
//	public void testHasNext() {
//		assertTrue(readerOne.hasNext());
//
//		int iter = 0;
//		while (iter < ROWS_IN_BG) {
//			iter++;
//			readerOne.next();
//		}
//		assertFalse(readerOne.hasNext());
//
//		assertTrue(readerTwo.hasNext());
//
//		iter = 0;
//		while (iter < ROWS_IN_EX) {
//			iter++;
//			readerTwo.next();
//		}
//
//		assertFalse(readerTwo.hasNext());
//	}
//
//	@Test
//	public void testNext() {
//		int count = 0;
//		while (readerOne.hasNext()) {
//			readerOne.next();
//			count++;
//		}
//
//		assertEquals(ROWS_IN_BG, count);
//
//		int count2 = 0;
//		while (readerTwo.hasNext()) {
//			readerTwo.next();
//			count2++;
//		}
//
//		assertEquals(ROWS_IN_EX, count2);
//
//	}
//
//}
