package org.lhasalimited.generic.xpt.util;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class XptUtilsTest
{

	/**
	 * Some example numbers were taken from XPT files
	 */
	@Test
	public void testIbmToIeee()
	{
		
		try
		{			
			assertEquals(1d, 			XptUtils.getIEEfromIBM(new byte[]{65, 16, 0, 0, 0, 0, 0, 0}), 	0.0002d);
			assertEquals(0d, 			XptUtils.getIEEfromIBM(new byte[]{0, 0, 0, 0, 0, 0, 0, 0}), 	0.0002d);
			assertEquals(Double.NaN, 	XptUtils.getIEEfromIBM(new byte[]{46, 0, 0, 0, 0, 0, 0, 0}), 	0.0002d);
			assertEquals(25, 			XptUtils.getIEEfromIBM(new byte[]{66, 25, 0, 0, 0, 0, 0, 0}), 	0.0002d);
			assertEquals(-4, 			XptUtils.getIEEfromIBM(new byte[]{-63, 64, 0, 0, 0, 0, 0, 0}), 	0.0002d);
			assertEquals(-1, 			XptUtils.getIEEfromIBM(new byte[]{-63, 16, 0, 0, 0, 0, 0, 0}), 	0.0002d);
			
		} catch (Exception e)
		{
			e.printStackTrace();
			fail(e.getMessage());
		}
		
	}
	
	@Test
	public void testConcatenate()
	{
		byte[] a = new byte[]{1,2};
		byte[] b = new byte[]{3,4};
		
		byte[] expected = new byte[]{1,2,3,4};
		
		assertTrue(Arrays.equals(expected, XptUtils.concat(a, b)));
	}

}
