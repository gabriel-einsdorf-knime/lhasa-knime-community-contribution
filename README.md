# README #

Lhasa Limited's [KNIME](https://www.knime.org) community contribution can now be found on [Bitbucket](https://bitbucket.org/lhasaLimited/knime-community-contribution)

## What is this repository for? ##

This repository stores the code released for Lhasa Limited's KNIME trusted community plugins. The first plugin represents some generic functionality and the second is
an integration of SMARTCyp and WhichCyp.


## How do I get set up? ##

The nodes can be installed through KNIME's update mechanism. See the [community documentation](https://www.knime.com/community) for more details.

## License ##

These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
any later version.
 
These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 
These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).

These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
results obtained by use of the node. 
 
Lhasa Limited does not provide support services or training in relation to the KNIME nodes.

## Contact ##

knime <at> lhasalimited.org