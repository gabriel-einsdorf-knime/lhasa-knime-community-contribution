package org.lhasalimited.smartcyp;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.renderer.AtomContainerRenderer;
import org.openscience.cdk.renderer.RendererModel;
import org.openscience.cdk.renderer.SymbolVisibility;
import org.openscience.cdk.renderer.color.ModCPKAtomColors;
import org.openscience.cdk.renderer.font.AWTFontManager;
import org.openscience.cdk.renderer.generators.BasicSceneGenerator;
import org.openscience.cdk.renderer.generators.IGenerator;
import org.openscience.cdk.renderer.generators.standard.StandardGenerator;
import org.openscience.cdk.renderer.visitor.AWTDrawVisitor;

public class CdkImageRender
{

	private int width;
	private int height;
	private Color textColour;

	public CdkImageRender(int width, int height)
	{
		this(width, height, Color.BLACK);
	}

	public CdkImageRender(int width, int height, Color col)
	{
		this.width = width;
		this.height = height;
		this.textColour = col;
	}

	/**
	 * Render the SMARTCyp result as an image. This is a workaround the the CDK
	 * Cell not keeping properties and therefore loosing the ranking
	 * annotations.
	 * 
	 * @param result
	 * @return
	 */
	public Image renderResult(IAtomContainer result) throws Exception
	{
		List<IGenerator<IAtomContainer>> generators = new ArrayList<IGenerator<IAtomContainer>>();
		generators.add(new BasicSceneGenerator());
		generators.add(new StandardGenerator(new Font("Verdana", Font.PLAIN, 24)));
		AtomContainerRenderer renderer = new AtomContainerRenderer(generators, new AWTFontManager());

		RendererModel renderer2dModel = renderer.getRenderer2DModel();
		renderer2dModel.set(BasicSceneGenerator.UseAntiAliasing.class, true);
		renderer2dModel.set(StandardGenerator.AtomColor.class, new ModCPKAtomColors());
		renderer2dModel.set(StandardGenerator.AnnotationColor.class, textColour);
		renderer2dModel.set(StandardGenerator.Highlighting.class, StandardGenerator.HighlightStyle.OuterGlow);
		renderer2dModel.set(StandardGenerator.Visibility.class, SymbolVisibility.iupacRecommendations());

		Image image = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
		Graphics2D g = (Graphics2D) image.getGraphics();
		g.setColor(Color.WHITE);
		g.fill(new Rectangle2D.Double(0, 0, width, height));

		renderer.paint(result, new AWTDrawVisitor(g), new Rectangle2D.Double(0, 0, width, height), true);
		g.dispose();

		return image;
	}

//	/**
//	 * Render the SMARTCyp result as an image. This is a workaround the the CDK
//	 * Cell not keeping properties and therefore loosing the ranking
//	 * annotations.
//	 * 
//	 * @param result
//	 * @return
//	 */
//	public Image renderResultSvg(IAtomContainer result) throws Exception
//	{
//		// 26 pt is also okay
//		Font font = new Font("Arial", Font.PLAIN, 24);
//
//		// note the font manager is not used
//		AtomContainerRenderer renderer = new AtomContainerRenderer(Arrays.asList(new BasicSceneGenerator(), new StandardGenerator(font)), new AWTFontManager());
//
//		RendererModel rendererModel = renderer.getRenderer2DModel();
//		rendererModel.set(StandardGenerator.AtomColor.class, new UniColor(Color.BLACK));
//		rendererModel.set(StandardGenerator.Visibility.class, SymbolVisibility.iupacRecommendations());
//		rendererModel.set(StandardGenerator.StrokeRatio.class, 0.85);
//		rendererModel.set(StandardGenerator.SymbolMarginRatio.class, 4d);
//		
//		StandardGenerator generator = new StandardGenerator(new Font("Arial", Font.PLAIN, 24));
//		IRenderingElement element = generator.generate(result, rendererModel);
//		
//
//		return null;
//	}

	/**
	 * Static access to the image rendering
	 * 
	 * @param result
	 * @param width
	 * @param height
	 * @return
	 * @throws Exception
	 */
	public static Image renderImage(IAtomContainer result, int width, int height) throws Exception
	{
		return renderImage(result, width, height, Color.BLACK);
	}

	/**
	 * 
	 * @param result
	 *            Structure / result to highlight. Should have properties set
	 *            for annotations and highlight
	 * @param width
	 *            Render width
	 * @param height
	 *            Render height
	 * @param colorValue
	 *            Text colour
	 * @return
	 * @throws Exception
	 */
	public static Image renderImage(IAtomContainer result, int width, int height, Color colorValue) throws Exception
	{
		CdkImageRender renderer = new CdkImageRender(width, height, colorValue);
		return renderer.renderResult(result);
	}
}
