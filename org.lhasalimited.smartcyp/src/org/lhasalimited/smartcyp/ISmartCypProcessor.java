package org.lhasalimited.smartcyp;

import java.util.List;
import java.util.Map;

import org.openscience.cdk.interfaces.IAtomContainer;

/**
 * Problems using an interface for the processor as the standard and extended versions use different core classes
 * e.g. MoleculeKU, SMARTSnEnergiesTable.
 * 
 * 
 * Correct casting will be required to handle the different versions prior to refactoring to Interfaces in the SmartCyp code
 * 
 * @author Samuel
 *
 */
public interface ISmartCypProcessor
{
	public List<? extends IAtomContainer> process(String structure, String id) throws Exception;
	public List<? extends IAtomContainer> process(Map<String, String> structures) throws Exception;
	public List<? extends IAtomContainer> process(IAtomContainer iAtomContainer, String id) throws Exception;
}
