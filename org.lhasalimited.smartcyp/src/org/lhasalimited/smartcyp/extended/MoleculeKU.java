/*
/*  This program is a modification of the program MoleculeKU by David Gloriam <davidgloriam@googlemail.com> 
/*  & Patrik Rydberg <patrik.rydberg@gmail.com> for the prediction of CYP catalyzed site of metabolism. The modification of
/*  program was made by Ruifeng Liu <rliu@bioanalysis.org>. See the copyright and other information from David Gloriam 
/*  and Patrick Rydberg concerning their original MoleculeKU program below.
/*
/* 
 * Copyright (C) 2010-2011  David Gloriam <davidgloriam@googlemail.com> & Patrik Rydberg <patrik.rydberg@gmail.com>
 * 
 * Contact: smartcyp@farma.ku.dk
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 * All we ask is that proper credit is given for our work, which includes
 * - but is not limited to - adding the above copyright notice to the beginning
 * of your source code files, and to any copyright notice that you may distribute
 * with programs based on this work.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */

package org.lhasalimited.smartcyp.extended;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;



import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.graph.PathTools;
import org.openscience.cdk.graph.invariant.EquivalentClassPartitioner;
import org.openscience.cdk.graph.matrix.AdjacencyMatrix;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.silent.SilentChemObjectBuilder;
import org.openscience.cdk.smiles.smarts.SMARTSQueryTool;

public class MoleculeKU extends AtomContainer 
{

	public enum SMARTCYP_PROPERTY {
		SymmetryNumber,
		IsSymmetric,
		NrofSymmetricSites,
		Score {
			@Override
			public String getLabel() {
				return "S";
			}
		},
		Score2D6 {
			@Override
			public String getLabel() {
				return "S2D6";
			}
		},
		Score1A2 {                    
			@Override
			public String getLabel() {
				return "S1A2";
			}
		},
		Score2C19 {
			@Override
			public String getLabel() {
				return "S2C19";
			}
		},
		Score2C9 {
			@Override
			public String getLabel() {
				return "S2C9";
			}
		},
		Ranking {
			@Override
			public String getLabel() {
				return "R";
			}
		},
		Ranking2D6 {
			@Override
			public String getLabel() {
				return "R";
			}
		},		
		Ranking1A2 {
			@Override
			public String getLabel() {
				return "R";
			}
		},		
		Ranking2C19 {
			@Override
			public String getLabel() {
				return "R";
			}
		},		
		Ranking2C9 {
			@Override
			public String getLabel() {
				return "R";
			}
		},		
		Energy {
			@Override
			public String getLabel() {
				return "E";
			}
		},
		Accessibility {
			@Override
			public String getLabel() {
				return "A";
			}
		},
		Span2End {
			@Override
			public String getLabel() {
				return "S";
			}
		},
		AbsSpan {
			@Override
			public String getLabel() {
				return "AS";
			}
		},
		Dist2ProtAmine {
			@Override
			public String getLabel() {
				return "DP";
			}
		},
		Dist2COr {
			@Override
			public String getLabel() {
				return "DCR";
			}
		},
		Dist2CO {
			@Override
			public String getLabel() {
				return "DCO";
			}
		},
		Dist2COCS {
			@Override
			public String getLabel() {
				return "DCS";
			}
			
		};

		public String  getLabel()  { return "";};

		public void set(IAtom atom, Number value) {
			atom.setProperty(toString(), value);
		}

		public Number get(IAtom atom) {
			Object o = atom.getProperty(toString());
			return (o==null)?null:(Number)o;
		}

		public String atomProperty2String(IAtom atom) {
			return String.format("%s:%s",getLabel(),get(atom));
		}

	}
	// Local variables
	private static final long serialVersionUID = 1L;	
	AtomComparator atomComparator = new AtomComparator();
	private TreeSet<IAtom> atomsSortedByEnA = new TreeSet<IAtom>(atomComparator);
	AtomComparator2D6 atomComparator2D6 = new AtomComparator2D6();
	private TreeSet<IAtom> atomsSortedByEnA2D6 = new TreeSet<IAtom>(atomComparator2D6);
	AtomComparator1A2 atomComparator1A2 = new AtomComparator1A2();
	private TreeSet<IAtom> atomsSortedByEnA1A2 = new TreeSet<IAtom>(atomComparator1A2);
	AtomComparator2C19 atomComparator2C19 = new AtomComparator2C19();
	private TreeSet<IAtom> atomsSortedByEnA2C19 = new TreeSet<IAtom>(atomComparator2C19);
	AtomComparator2C9 atomComparator2C9 = new AtomComparator2C9();
	private TreeSet<IAtom> atomsSortedByEnA2C9 = new TreeSet<IAtom>(atomComparator2C9);
	private int HighestSymmetryNumber = 0;


	// Constructor
	// This constructor also calls the methods that calculate MaxTopDist, Energies and sorts C, N, P and S atoms
	// This constructor is the only way to create MoleculeKU and Atom objects, -there is no add() method
	public MoleculeKU(IAtomContainer iAtomContainer, HashMap<String, Double> SMARTSnEnergiesTable) throws CloneNotSupportedException
	{
		// Calls the constructor in org.openscience.cdk.AtomContainer
		// Atoms are stored in the array atoms[] and accessed by getAtom() and setAtom()
		super(iAtomContainer);			
		int number = 1;
		for (int atomIndex=0; atomIndex < iAtomContainer.getAtomCount(); atomIndex++) {
			iAtomContainer.getAtom(atomIndex).setID(String.valueOf(number));
			number++;
		}
	}



	public void assignAtomEnergies(HashMap<String, Double> SMARTSnEnergiesTable) throws CDKException {

		// Variables
		int numberOfSMARTSmatches = 0;															// Number of SMARTS matches = number of metabolic sites

		// Iterate over the SMARTS in SMARTSnEnergiesTable
		Set<String> keySetSMARTSnEnergies = (Set<String>) SMARTSnEnergiesTable.keySet();
		Iterator<String> keySetIteratorSMARTSnEnergies = keySetSMARTSnEnergies.iterator();

		String currentSMARTS = "C";
		SMARTSQueryTool querytool = new SMARTSQueryTool(currentSMARTS, SilentChemObjectBuilder.getInstance());					// Creates the Query Tool


		while(keySetIteratorSMARTSnEnergies.hasNext()){

			try {
				currentSMARTS = keySetIteratorSMARTSnEnergies.next();
				querytool.setSmarts(currentSMARTS);

				// Check if there are any SMARTS matches
				boolean status = querytool.matches(this);
				if (status) {


					numberOfSMARTSmatches = querytool.countMatches();		// Count the number of matches				
					List<List<Integer>> matchingAtomsIndicesList_1;				// List of List objects each containing the indices of the atoms in the target molecule
					List<Integer> matchingAtomsIndicesList_2 = null;						// List of atom indices
					double energy = SMARTSnEnergiesTable.get(currentSMARTS);		// Energy of currentSMARTS

					//					System.out.println("\n The SMARTS " + currentSMARTS + " has " + numberOfSMARTSmatches + " matches in the molecule " + this.getID());

					matchingAtomsIndicesList_1 = querytool.getMatchingAtoms();													// This list contains the C, N, P and S atom indices

					for(int listObjectIndex = 0; listObjectIndex < numberOfSMARTSmatches; listObjectIndex++){						

						matchingAtomsIndicesList_2 = matchingAtomsIndicesList_1.get(listObjectIndex);							// Contains multiple atoms

						// System.out.println("How many times numberOfSMARTSmatches: " + numberOfSMARTSmatches);							
						// System.out.println("atomID " +this.getAtom(atomNr).getID()+ ", energy " + energy);


						// Set the Energies of the atoms
						int indexOfMatchingAtom;
						IAtom matchingAtom;
						for (int atomNr = 0; atomNr < matchingAtomsIndicesList_2.size(); atomNr++){								// Contains 1 atom
							indexOfMatchingAtom = matchingAtomsIndicesList_2.get(atomNr);

							// An atom can be matched by several SMARTS and thus assigned several energies
							// The if clause assures that atoms will get the lowest possible energy
							matchingAtom = this.getAtom(indexOfMatchingAtom);

							if(SMARTCYP_PROPERTY.Energy.get(matchingAtom) == null 
									|| energy < SMARTCYP_PROPERTY.Energy.get(matchingAtom).doubleValue())
								SMARTCYP_PROPERTY.Energy.set(matchingAtom,energy);
						}
					}
				}
			}	
			catch (CDKException e) {System.out.println("There is something fishy with the SMARTS: " + currentSMARTS); e.printStackTrace();}
		}
		//assign energy 599.99 to all atoms not matching a SMARTS
		for (int testAtomNr=0; testAtomNr < this.getAtomCount(); testAtomNr++){
			IAtom testAtom;
			testAtom = this.getAtom(testAtomNr);
			if(SMARTCYP_PROPERTY.Energy.get(testAtom) == null) {
				SMARTCYP_PROPERTY.Energy.set(testAtom,599.99);
			}
		}
	}

	// Calculates the Accessibilities of all atoms
	public void calculateAtomAccessabilities() throws CloneNotSupportedException{


		int[][] adjacencyMatrix = AdjacencyMatrix.getMatrix(this);

		// Calculate the maximum topology distance
		// Takes an adjacency matrix and outputs and MaxTopDist matrix of the same size
		int[][] minTopDistMatrix = PathTools.computeFloydAPSP(adjacencyMatrix);


		// Find the longest Path of all, "longestMaxTopDistInMolecule"
		double longestMaxTopDistInMolecule = 0;
		double currentMaxTopDist = 0;
		for(int x = 0; x < this.getAtomCount(); x++){
			for(int y = 0; y < this.getAtomCount(); y++){
				currentMaxTopDist =  minTopDistMatrix[x][y];
				if(currentMaxTopDist > longestMaxTopDistInMolecule) longestMaxTopDistInMolecule = currentMaxTopDist;
			}
		}


		// Find the Accessibility value ("longest shortestPath") for each atom

		// ITERATE REFERENCE ATOMS
		for (int refAtomNr=0; refAtomNr < this.getAtomCount(); refAtomNr++){

			// ITERATE COMPARISON ATOMS
			double highestMaxTopDistInMatrixRow = 0;
			IAtom refAtom;
			for (int compAtomNr = 0; compAtomNr < this.getAtomCount(); compAtomNr++){
				if(highestMaxTopDistInMatrixRow < minTopDistMatrix[refAtomNr][compAtomNr]) highestMaxTopDistInMatrixRow = minTopDistMatrix[refAtomNr][compAtomNr];
			}	

			refAtom = this.getAtom(refAtomNr);
			// Set the Accessibility of the Atom
			SMARTCYP_PROPERTY.Accessibility.set(refAtom,(highestMaxTopDistInMatrixRow / longestMaxTopDistInMolecule));
		}
	}

	// Compute the score of 3A4
	public void calculateAtomScores() throws CloneNotSupportedException{

		// ITERATE ATOMS
		for (int refAtomNr=0; refAtomNr < this.getAtomCount(); refAtomNr++){
			IAtom refAtom;
			refAtom = this.getAtom(refAtomNr);
			// Calculate the Atom scores
			if(SMARTCYP_PROPERTY.Accessibility.get(refAtom)!=null) {
				if(SMARTCYP_PROPERTY.Energy.get(refAtom) != null){
					double score = SMARTCYP_PROPERTY.Energy.get(refAtom).doubleValue() - 8 * SMARTCYP_PROPERTY.Accessibility.get(refAtom).doubleValue();
					SMARTCYP_PROPERTY.Score.set(refAtom,score);
				}
			}
		}
	}

	// Compute the 2D6 score of all atoms
	public void calculate2D6AtomScores() throws CloneNotSupportedException{

		// ITERATE ATOMS
		for (int refAtomNr=0; refAtomNr < this.getAtomCount(); refAtomNr++){
			IAtom refAtom;
			refAtom = this.getAtom(refAtomNr);
			double CorrectionDist2ProtAmine;
			double CorrectionSpan2End;
			//double x;
			CorrectionDist2ProtAmine = 0;
			int span2end; 
			int cutoff = 8;
			int s2endcutoff = 4;
			double constant = 6.7;
			// Calculate the Atom scores
			if(SMARTCYP_PROPERTY.Accessibility.get(refAtom)!=null) {
				if(SMARTCYP_PROPERTY.Energy.get(refAtom) != null){
					if(SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom) != null){
						//x = SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom).intValue();
						//if(SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom).intValue()>4 && SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom).intValue()<10) Correction2D6 = 30 * Math.exp(-0.5 * (x - 7.5) * (x - 7.5)) + 15 * Math.exp(-0.05 * (x - 7.5) * (x - 7.5));
						//x = Math.abs(SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom).doubleValue() - 7.5);
						double ProtAmineDist = SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom).doubleValue();
						CorrectionDist2ProtAmine = 0;
						if(ProtAmineDist < cutoff) CorrectionDist2ProtAmine = constant*(cutoff - ProtAmineDist);
					}
					else CorrectionDist2ProtAmine = 0;
					span2end = SMARTCYP_PROPERTY.Span2End.get(refAtom).intValue();
					if(span2end < s2endcutoff){
						CorrectionSpan2End = constant*span2end;
					}
					else CorrectionSpan2End = constant*s2endcutoff + 0.01*span2end;
					double score = SMARTCYP_PROPERTY.Energy.get(refAtom).doubleValue() + CorrectionDist2ProtAmine + CorrectionSpan2End;
					SMARTCYP_PROPERTY.Score2D6.set(refAtom,score);
				}
			}
		}
	}

	// Compute the 2C9 score of all atoms
	public void calculate2C9AtomScores() throws CloneNotSupportedException{

		// ITERATE ATOMS
		for (int refAtomNr=0; refAtomNr < this.getAtomCount(); refAtomNr++){
			IAtom refAtom;
			refAtom = this.getAtom(refAtomNr);
			double CorrectionDist2CO;
			double CorrectionSpan2End;
			//double x;
			CorrectionDist2CO = 0;
			int span2end; 
			int cutoff = 6;
			int s2endcutoff = 4;
			double constant = 10.00;
			// Calculate the Atom scores
			if(SMARTCYP_PROPERTY.Accessibility.get(refAtom)!=null) {
				if(SMARTCYP_PROPERTY.Energy.get(refAtom) != null){
					if(SMARTCYP_PROPERTY.Dist2CO.get(refAtom) != null){
						//x = SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom).intValue();
						//if(SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom).intValue()>4 && SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom).intValue()<10) Correction2D6 = 30 * Math.exp(-0.5 * (x - 7.5) * (x - 7.5)) + 15 * Math.exp(-0.05 * (x - 7.5) * (x - 7.5));
						//x = Math.abs(SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom).doubleValue() - 7.5);
						double CODist = SMARTCYP_PROPERTY.Dist2CO.get(refAtom).doubleValue();
						CorrectionDist2CO = 0;
						if(CODist < cutoff) CorrectionDist2CO = constant*(cutoff - CODist);
					}
					else CorrectionDist2CO = 0;
					span2end = SMARTCYP_PROPERTY.Span2End.get(refAtom).intValue();
					if(span2end < s2endcutoff){
						CorrectionSpan2End = 6.70*span2end;
					}
					else CorrectionSpan2End = 6.70*s2endcutoff + 0.01*span2end;
					double score = SMARTCYP_PROPERTY.Energy.get(refAtom).doubleValue() + CorrectionDist2CO + CorrectionSpan2End;
					SMARTCYP_PROPERTY.Score2C9.set(refAtom,score);
				}
			}
		}
	}

/*	
	// Compute the 1A2 score of all atoms
	public void calculate1A2AtomScores() throws CloneNotSupportedException{

		// ITERATE ATOMS
		for (int refAtomNr=0; refAtomNr < this.getAtomCount(); refAtomNr++){
			IAtom refAtom;
			refAtom = this.getAtom(refAtomNr);

			double CorrectionSpan2End;

			int span2end; 

			int s2endcutoff = 4;
			double constant = 6.7;
			// Calculate the Atom scores
			if(SMARTCYP_PROPERTY.Accessibility.get(refAtom)!=null) {
				if(SMARTCYP_PROPERTY.Energy.get(refAtom) != null){
					span2end = SMARTCYP_PROPERTY.Span2End.get(refAtom).intValue();
					if(span2end < s2endcutoff){
						CorrectionSpan2End = constant*span2end;
					}
					else CorrectionSpan2End = constant*s2endcutoff + 0.01*span2end;
					double score = SMARTCYP_PROPERTY.Energy.get(refAtom).doubleValue() + CorrectionSpan2End;
					SMARTCYP_PROPERTY.Score1A2.set(refAtom,score);
				}
			}
		}
	}
*/
	// Compute the 1A2 score of all atoms with Dist2COr correction
	public void calculate1A2AtomScores() throws CloneNotSupportedException{

		// ITERATE ATOMS
		for (int refAtomNr=0; refAtomNr < this.getAtomCount(); refAtomNr++){
			IAtom refAtom;
			refAtom = this.getAtom(refAtomNr);
			double CorrectionDist2COr;
			double CorrectionSpan2End;
			//double x;
			CorrectionDist2COr = 0;
			int span2end; 
			int cutoff = 6;
			int s2endcutoff = 4;
			double constant = 10.0;
			// Calculate the Atom scores
			if(SMARTCYP_PROPERTY.Accessibility.get(refAtom)!=null) {
				if(SMARTCYP_PROPERTY.Energy.get(refAtom) != null){
					if(SMARTCYP_PROPERTY.Dist2COr.get(refAtom) != null){
						//x = SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom).intValue();
						//if(SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom).intValue()>4 && SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom).intValue()<10) Correction2D6 = 30 * Math.exp(-0.5 * (x - 7.5) * (x - 7.5)) + 15 * Math.exp(-0.05 * (x - 7.5) * (x - 7.5));
						//x = Math.abs(SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom).doubleValue() - 7.5);
						double COrDist = SMARTCYP_PROPERTY.Dist2COr.get(refAtom).doubleValue();
						CorrectionDist2COr = 0;
						if(COrDist < cutoff) CorrectionDist2COr = constant*(cutoff - COrDist);
					}
					else CorrectionDist2COr = 0;
					span2end = SMARTCYP_PROPERTY.Span2End.get(refAtom).intValue();
					if(span2end < s2endcutoff){
						CorrectionSpan2End = 6.70*span2end;
					}
					else CorrectionSpan2End = 6.70*s2endcutoff + 0.01*span2end;
					double score = SMARTCYP_PROPERTY.Energy.get(refAtom).doubleValue() + CorrectionDist2COr + CorrectionSpan2End;
					SMARTCYP_PROPERTY.Score1A2.set(refAtom,score);
				}
			}
		}
	}

	
	// Compute the 2C19 score of all atoms
	public void calculate2C19AtomScores() throws CloneNotSupportedException{

		// ITERATE ATOMS
		for (int refAtomNr=0; refAtomNr < this.getAtomCount(); refAtomNr++){
			IAtom refAtom;
			refAtom = this.getAtom(refAtomNr);
			double CorrectionDist2COCS;
			double CorrectionSpan2End;
			//double x;
			CorrectionDist2COCS = 0;
			int span2end; 
			int cutoff = 4;
			int s2endcutoff = 4;
			double constant = 10.0;
			// Calculate the Atom scores
			if(SMARTCYP_PROPERTY.Accessibility.get(refAtom)!=null) {
				if(SMARTCYP_PROPERTY.Energy.get(refAtom) != null){
					if(SMARTCYP_PROPERTY.Dist2COCS.get(refAtom) != null){
						//x = SMARTCYP_PROPERTY.Dist2COCS.get(refAtom).intValue();
						//if(SMARTCYP_PROPERTY.Dist2COCS.get(refAtom).intValue()>3 && SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom).intValue()<10) Correction2D6 = 30 * Math.exp(-0.5 * (x - 7.5) * (x - 7.5)) + 15 * Math.exp(-0.05 * (x - 7.5) * (x - 7.5));
						//x = Math.abs(SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom).doubleValue() - 7.5);
						double COCSDist = SMARTCYP_PROPERTY.Dist2COCS.get(refAtom).doubleValue();
						CorrectionDist2COCS = 0;
						if(COCSDist < cutoff) CorrectionDist2COCS = constant*(cutoff - COCSDist);
					}
					else CorrectionDist2COCS = 0;
					span2end = SMARTCYP_PROPERTY.Span2End.get(refAtom).intValue();
					if(span2end < s2endcutoff){
						CorrectionSpan2End = 6.7*span2end;
					}
					else CorrectionSpan2End = 6.7*s2endcutoff + 0.01*span2end;
					double score = SMARTCYP_PROPERTY.Energy.get(refAtom).doubleValue() + CorrectionDist2COCS + CorrectionSpan2End;
					SMARTCYP_PROPERTY.Score2C19.set(refAtom,score);
				}
			}
		}
	}

	// Calculates the Span to end of molecule
	public void calculateSpan2End() throws CloneNotSupportedException{


		int[][] adjacencyMatrix = AdjacencyMatrix.getMatrix(this);

		// Calculate the maximum topology distance
		// Takes an adjacency matrix and outputs and MaxTopDist matrix of the same size
		int[][] minTopDistMatrix = PathTools.computeFloydAPSP(adjacencyMatrix);


		// Find the longest Path of all, "longestMaxTopDistInMolecule"
		double longestMaxTopDistInMolecule = 0;
		double currentMaxTopDist = 0;
		for(int x = 0; x < this.getAtomCount(); x++){
			for(int y = 0; y < this.getAtomCount(); y++){
				currentMaxTopDist =  minTopDistMatrix[x][y];
				if(currentMaxTopDist > longestMaxTopDistInMolecule) longestMaxTopDistInMolecule = currentMaxTopDist;
			}
		}


		// Find the Span2End (maxtopdist - currenttopdist) for each atom

		// ITERATE REFERENCE ATOMS
		for (int refAtomNr=0; refAtomNr < this.getAtomCount(); refAtomNr++){

			// ITERATE COMPARISON ATOMS
			double highestMaxTopDistInMatrixRow = 0;
			IAtom refAtom;
			for (int compAtomNr = 0; compAtomNr < this.getAtomCount(); compAtomNr++){
				if(highestMaxTopDistInMatrixRow < minTopDistMatrix[refAtomNr][compAtomNr]) highestMaxTopDistInMatrixRow = minTopDistMatrix[refAtomNr][compAtomNr];
			}	

			refAtom = this.getAtom(refAtomNr);
			// Set the Accessibility of the Atom
			SMARTCYP_PROPERTY.Span2End.set(refAtom,(longestMaxTopDistInMolecule - highestMaxTopDistInMatrixRow));
			SMARTCYP_PROPERTY.AbsSpan.set(refAtom,(highestMaxTopDistInMatrixRow));

		}
	}

	// Calculates the distance to the most distant possibly protonated amine / guanidine nitrogen
	public void calculateDist2ProtAmine() throws CDKException{

		//locate amine nitrogens which could be protonated
		// Variables
		int numberOfSMARTSmatches = 0;	// Number of SMARTS matches = number of protonated amine sites

		//String currentSMARTS = "[$([NX3]),$([NX3][CX3](=[NX2])[NX3]);!$([NX3][#6X3]);!$([NX3][S](=[O])=[O])]";
		String currentSMARTS = "[$([N][CX3](=[N])[N]);!$([NX3][S](=[O])=[O])]";
		String currentSMARTS2 = "[$([NX3]);!$([NX3][#6X3]);!$([NX3][N]=[O]);!$([NX3][S](=[O])=[O])]";
		SMARTSQueryTool querytool = new SMARTSQueryTool(currentSMARTS, SilentChemObjectBuilder.getInstance());		// Creates the Query Tool

		querytool.setSmarts(currentSMARTS);

		// Check if there are any SMARTS matches
		boolean status = querytool.matches(this);
		if (status) {

			numberOfSMARTSmatches = querytool.countMatches();		// Count the number of matches				
			List<List<Integer>> matchingAtomsIndicesList_1;			// List of List objects each containing the indices of the atoms in the target molecule
			List<Integer> matchingAtomsIndicesList_2 = null;		// List of atom indices

			matchingAtomsIndicesList_1 = querytool.getMatchingAtoms();	// This list contains the atom indices of protonated amine nitrogens

			for(int listObjectIndex = 0; listObjectIndex < numberOfSMARTSmatches; listObjectIndex++){						

				matchingAtomsIndicesList_2 = matchingAtomsIndicesList_1.get(listObjectIndex);	// Contains multiple atoms

				// Compute distance for all atoms to the matching atoms
				int indexOfMatchingAtom;
				for (int atomNr = 0; atomNr < matchingAtomsIndicesList_2.size(); atomNr++){		// Contains 1 atom
					indexOfMatchingAtom = matchingAtomsIndicesList_2.get(atomNr);
					//System.out.println("\n" + indexOfMatchingAtom);
					int[][] adjacencyMatrix = AdjacencyMatrix.getMatrix(this);
					int[][] minTopDistMatrix = PathTools.computeFloydAPSP(adjacencyMatrix);
					//iterate over all atoms
					for (int refAtomNr=0; refAtomNr < this.getAtomCount(); refAtomNr++){
						IAtom refAtom;
						refAtom = this.getAtom(refAtomNr);
						int thisdist2protamine;
						thisdist2protamine = minTopDistMatrix[refAtomNr][indexOfMatchingAtom];
						if(SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom) != null){
							if(thisdist2protamine > SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom).intValue()){
								SMARTCYP_PROPERTY.Dist2ProtAmine.set(refAtom,thisdist2protamine);
							}
						}
						else SMARTCYP_PROPERTY.Dist2ProtAmine.set(refAtom,thisdist2protamine);
					}
				}
			}
		}

		querytool.setSmarts(currentSMARTS2);

		// Check if there are any SMARTS matches
		boolean status1 = querytool.matches(this);
		if (status1) {

			numberOfSMARTSmatches = querytool.countMatches();		// Count the number of matches				
			List<List<Integer>> matchingAtomsIndicesList_1;			// List of List objects each containing the indices of the atoms in the target molecule
			List<Integer> matchingAtomsIndicesList_2 = null;		// List of atom indices

			matchingAtomsIndicesList_1 = querytool.getMatchingAtoms();	// This list contains the atom indices of protonated amine nitrogens

			for(int listObjectIndex = 0; listObjectIndex < numberOfSMARTSmatches; listObjectIndex++){						

				matchingAtomsIndicesList_2 = matchingAtomsIndicesList_1.get(listObjectIndex);	// Contains multiple atoms

				// Compute distance for all atoms to the matching atoms
				int indexOfMatchingAtom;
				for (int atomNr = 0; atomNr < matchingAtomsIndicesList_2.size(); atomNr++){		// Contains 1 atom
					indexOfMatchingAtom = matchingAtomsIndicesList_2.get(atomNr);
					//System.out.println("\n" + indexOfMatchingAtom);
					int[][] adjacencyMatrix = AdjacencyMatrix.getMatrix(this);
					int[][] minTopDistMatrix = PathTools.computeFloydAPSP(adjacencyMatrix);
					//iterate over all atoms
					for (int refAtomNr=0; refAtomNr < this.getAtomCount(); refAtomNr++){
						IAtom refAtom;
						refAtom = this.getAtom(refAtomNr);
						int thisdist2protamine;
						thisdist2protamine = minTopDistMatrix[refAtomNr][indexOfMatchingAtom];
						if(SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom) != null){
							if(thisdist2protamine > SMARTCYP_PROPERTY.Dist2ProtAmine.get(refAtom).intValue()){
								SMARTCYP_PROPERTY.Dist2ProtAmine.set(refAtom,thisdist2protamine);
							}
						}
						else SMARTCYP_PROPERTY.Dist2ProtAmine.set(refAtom,thisdist2protamine);
					}
				}
			}
		}
	}
	
// re-calculate COOH
	
	// Calculates the distance to the most distant COOH / CO
	
	public void calculateDist2CO() throws CDKException{

		//locate COOH
		// Variables
		int numberOfSMARTSmatches = 0;	// Number of SMARTS matches = number of protonated amine sites

		//String currentSMARTS = "[$([NX3]),$([NX3][CX3](=[NX2])[NX3]);!$([NX3][#6X3]);!$([NX3][S](=[O])=[O])]";
 
		String currentSMARTS = "[$([CX3](=[O])[OX2H1]),$([CX3](=[O])[OX1H0-])]";
		String currentSMARTS2 = "[$([CX3](=O))]";
		SMARTSQueryTool querytool = new SMARTSQueryTool(currentSMARTS, SilentChemObjectBuilder.getInstance());		// Creates the Query Tool

		querytool.setSmarts(currentSMARTS);

		// Check if there are any SMARTS matches
		boolean status = querytool.matches(this);
		if (status) {

			numberOfSMARTSmatches = querytool.countMatches();		// Count the number of matches				
			List<List<Integer>> matchingAtomsIndicesList_1;			// List of List objects each containing the indices of the atoms in the target molecule
			List<Integer> matchingAtomsIndicesList_2 = null;		// List of atom indices

			matchingAtomsIndicesList_1 = querytool.getMatchingAtoms();	// This list contains the atom indices of protonated amine nitrogens

			for(int listObjectIndex = 0; listObjectIndex < numberOfSMARTSmatches; listObjectIndex++){						

				matchingAtomsIndicesList_2 = matchingAtomsIndicesList_1.get(listObjectIndex);	// Contains multiple atoms

				// Compute distance for all atoms to the matching atoms
				int indexOfMatchingAtom;
				for (int atomNr = 0; atomNr < matchingAtomsIndicesList_2.size(); atomNr++){		// Contains 1 atom
					indexOfMatchingAtom = matchingAtomsIndicesList_2.get(atomNr);
					//System.out.println("\n" + indexOfMatchingAtom);
					int[][] adjacencyMatrix = AdjacencyMatrix.getMatrix(this);
					int[][] minTopDistMatrix = PathTools.computeFloydAPSP(adjacencyMatrix);
					//iterate over all atoms
					for (int refAtomNr=0; refAtomNr < this.getAtomCount(); refAtomNr++){
						IAtom refAtom;
						refAtom = this.getAtom(refAtomNr);
						int thisdist2CO;
						thisdist2CO = minTopDistMatrix[refAtomNr][indexOfMatchingAtom];
						if(SMARTCYP_PROPERTY.Dist2CO.get(refAtom) != null){
							if(thisdist2CO > SMARTCYP_PROPERTY.Dist2CO.get(refAtom).intValue()){
								SMARTCYP_PROPERTY.Dist2CO.set(refAtom,thisdist2CO);
							}
						}
						else SMARTCYP_PROPERTY.Dist2CO.set(refAtom,thisdist2CO);
					}
				}
			}
		}
		else {
		querytool.setSmarts(currentSMARTS2);

		// Check if there are any SMARTS matches
		boolean status1 = querytool.matches(this);
		if (status1) {

			numberOfSMARTSmatches = querytool.countMatches();		// Count the number of matches				
			List<List<Integer>> matchingAtomsIndicesList_1;			// List of List objects each containing the indices of the atoms in the target molecule
			List<Integer> matchingAtomsIndicesList_2 = null;		// List of atom indices

			matchingAtomsIndicesList_1 = querytool.getMatchingAtoms();	// This list contains the atom indices of protonated amine nitrogens

			for(int listObjectIndex = 0; listObjectIndex < numberOfSMARTSmatches; listObjectIndex++){						

				matchingAtomsIndicesList_2 = matchingAtomsIndicesList_1.get(listObjectIndex);	// Contains multiple atoms

				// Compute distance for all atoms to the matching atoms
				int indexOfMatchingAtom;
				for (int atomNr = 0; atomNr < matchingAtomsIndicesList_2.size(); atomNr++){		// Contains 1 atom
					indexOfMatchingAtom = matchingAtomsIndicesList_2.get(atomNr);
					//System.out.println("\n" + indexOfMatchingAtom);
					int[][] adjacencyMatrix = AdjacencyMatrix.getMatrix(this);
					int[][] minTopDistMatrix = PathTools.computeFloydAPSP(adjacencyMatrix);
					//iterate over all atoms
					for (int refAtomNr=0; refAtomNr < this.getAtomCount(); refAtomNr++){
						IAtom refAtom;
						refAtom = this.getAtom(refAtomNr);
						int thisdist2CO;
						thisdist2CO = minTopDistMatrix[refAtomNr][indexOfMatchingAtom];
						if(SMARTCYP_PROPERTY.Dist2CO.get(refAtom) != null){
							if(thisdist2CO > SMARTCYP_PROPERTY.Dist2CO.get(refAtom).intValue()){
								SMARTCYP_PROPERTY.Dist2CO.set(refAtom,thisdist2CO);
							}
						}
						else SMARTCYP_PROPERTY.Dist2CO.set(refAtom,thisdist2CO);
					}
				}
			}
		}
	}
	}
	

	
	// Calculates the distance to the most distant C=O
	public void calculateDist2COCS() throws CDKException{

		//locate C=O
		// Variables
		int numberOfSMARTSmatches = 0;	// Number of SMARTS matches = number of C=O sites

		//String currentSMARTS = "[$([NX3]),$([NX3][CX3](=[NX2])[NX3]);!$([NX3][#6X3]);!$([NX3][S](=[O])=[O])]";
		String currentSMARTS = "[$([CX3](=[O]))]";
		String currentSMARTS2 = "[$([SX3](=[O]))]";
		SMARTSQueryTool querytool = new SMARTSQueryTool(currentSMARTS, SilentChemObjectBuilder.getInstance());		// Creates the Query Tool

		querytool.setSmarts(currentSMARTS);

		// Check if there are any SMARTS matches
		boolean status = querytool.matches(this);
		if (status) {

			numberOfSMARTSmatches = querytool.countMatches();		// Count the number of matches				
			List<List<Integer>> matchingAtomsIndicesList_1;			// List of List objects each containing the indices of the atoms in the target molecule
			List<Integer> matchingAtomsIndicesList_2 = null;		// List of atom indices

			matchingAtomsIndicesList_1 = querytool.getMatchingAtoms();	// This list contains the atom indices of C=O carbons

			for(int listObjectIndex = 0; listObjectIndex < numberOfSMARTSmatches; listObjectIndex++){						

				matchingAtomsIndicesList_2 = matchingAtomsIndicesList_1.get(listObjectIndex);	// Contains multiple atoms

				// Compute distance for all atoms to the matching atoms
				int indexOfMatchingAtom;
				for (int atomNr = 0; atomNr < matchingAtomsIndicesList_2.size(); atomNr++){		// Contains 1 atom
					indexOfMatchingAtom = matchingAtomsIndicesList_2.get(atomNr);
					//System.out.println("\n" + indexOfMatchingAtom);
					int[][] adjacencyMatrix = AdjacencyMatrix.getMatrix(this);
					int[][] minTopDistMatrix = PathTools.computeFloydAPSP(adjacencyMatrix);
					//iterate over all atoms
					for (int refAtomNr=0; refAtomNr < this.getAtomCount(); refAtomNr++){
						IAtom refAtom;
						refAtom = this.getAtom(refAtomNr);
						int thisdist2COCS;
						thisdist2COCS = minTopDistMatrix[refAtomNr][indexOfMatchingAtom];
						if(SMARTCYP_PROPERTY.Dist2COCS.get(refAtom) != null){
							if(thisdist2COCS > SMARTCYP_PROPERTY.Dist2COCS.get(refAtom).intValue()){
								SMARTCYP_PROPERTY.Dist2COCS.set(refAtom,thisdist2COCS);
							}
						}
						else SMARTCYP_PROPERTY.Dist2COCS.set(refAtom,thisdist2COCS);
					}
				}
			}
		}

		querytool.setSmarts(currentSMARTS2);

		// Check if there are any SMARTS matches
		boolean status1 = querytool.matches(this);
		if (status1) {

			numberOfSMARTSmatches = querytool.countMatches();		// Count the number of matches				
			List<List<Integer>> matchingAtomsIndicesList_1;			// List of List objects each containing the indices of the atoms in the target molecule
			List<Integer> matchingAtomsIndicesList_2 = null;		// List of atom indices

			matchingAtomsIndicesList_1 = querytool.getMatchingAtoms();	// This list contains the atom indices of protonated amine nitrogens

			for(int listObjectIndex = 0; listObjectIndex < numberOfSMARTSmatches; listObjectIndex++){						

				matchingAtomsIndicesList_2 = matchingAtomsIndicesList_1.get(listObjectIndex);	// Contains multiple atoms

				// Compute distance for all atoms to the matching atoms
				int indexOfMatchingAtom;
				for (int atomNr = 0; atomNr < matchingAtomsIndicesList_2.size(); atomNr++){		// Contains 1 atom
					indexOfMatchingAtom = matchingAtomsIndicesList_2.get(atomNr);
					//System.out.println("\n" + indexOfMatchingAtom);
					int[][] adjacencyMatrix = AdjacencyMatrix.getMatrix(this);
					int[][] minTopDistMatrix = PathTools.computeFloydAPSP(adjacencyMatrix);
					//iterate over all atoms
					for (int refAtomNr=0; refAtomNr < this.getAtomCount(); refAtomNr++){
						IAtom refAtom;
						refAtom = this.getAtom(refAtomNr);
						int thisdist2COCS;
						thisdist2COCS = minTopDistMatrix[refAtomNr][indexOfMatchingAtom];
						if(SMARTCYP_PROPERTY.Dist2COCS.get(refAtom) != null){
							if(thisdist2COCS > SMARTCYP_PROPERTY.Dist2COCS.get(refAtom).intValue()){
								SMARTCYP_PROPERTY.Dist2COCS.set(refAtom,thisdist2COCS);
							}
						}
						else SMARTCYP_PROPERTY.Dist2COCS.set(refAtom,thisdist2COCS);
					}
				}
			}
		}
	}

	//For 1A2 with COr distance correction
	// 
	public void calculateDist2COr() throws CDKException{

		//locate amine nitrogens which could be protonated
		// Variables
		int numberOfSMARTSmatches = 0;	// Number of SMARTS matches = number of protonated amine sites

		//String currentSMARTS = "[$([NX3]),$([NX3][CX3](=[NX2])[NX3]);!$([NX3][#6X3]);!$([NX3][S](=[O])=[O])]";
		String currentSMARTS = "[$([#6X3r](=[O])),$([#6X3R](=[O]))]";
//		String currentSMARTS2 = "[$([NX3]);!$([NX3][#6X3]);!$([NX3][N]=[O]);!$([NX3][S](=[O])=[O])]";
		SMARTSQueryTool querytool = new SMARTSQueryTool(currentSMARTS, SilentChemObjectBuilder.getInstance());		// Creates the Query Tool

		querytool.setSmarts(currentSMARTS);

		// Check if there are any SMARTS matches
		boolean status = querytool.matches(this);
		if (status) {

			numberOfSMARTSmatches = querytool.countMatches();		// Count the number of matches				
			List<List<Integer>> matchingAtomsIndicesList_1;			// List of List objects each containing the indices of the atoms in the target molecule
			List<Integer> matchingAtomsIndicesList_2 = null;		// List of atom indices

			matchingAtomsIndicesList_1 = querytool.getMatchingAtoms();	// This list contains the atom indices of protonated amine nitrogens

			for(int listObjectIndex = 0; listObjectIndex < numberOfSMARTSmatches; listObjectIndex++){						

				matchingAtomsIndicesList_2 = matchingAtomsIndicesList_1.get(listObjectIndex);	// Contains multiple atoms

				// Compute distance for all atoms to the matching atoms
				int indexOfMatchingAtom;
				for (int atomNr = 0; atomNr < matchingAtomsIndicesList_2.size(); atomNr++){		// Contains 1 atom
					indexOfMatchingAtom = matchingAtomsIndicesList_2.get(atomNr);
					//System.out.println("\n" + indexOfMatchingAtom);
					int[][] adjacencyMatrix = AdjacencyMatrix.getMatrix(this);
					int[][] minTopDistMatrix = PathTools.computeFloydAPSP(adjacencyMatrix);
					//iterate over all atoms
					for (int refAtomNr=0; refAtomNr < this.getAtomCount(); refAtomNr++){
						IAtom refAtom;
						refAtom = this.getAtom(refAtomNr);
						int thisdist2cor;
						thisdist2cor = minTopDistMatrix[refAtomNr][indexOfMatchingAtom];
						if(SMARTCYP_PROPERTY.Dist2COr.get(refAtom) != null){
							if(thisdist2cor > SMARTCYP_PROPERTY.Dist2COr.get(refAtom).intValue()){
								SMARTCYP_PROPERTY.Dist2COr.set(refAtom,thisdist2cor);
							}
						}
						else SMARTCYP_PROPERTY.Dist2COr.set(refAtom,thisdist2cor);
					}
				}
			}
		}


	}
	
	
	//End for 1A2 with COr distance correction
	
	//  This method makes atomsSortedByEnA
	public void sortAtoms() throws CDKException{

		IAtom currentAtom;
		String currentAtomType;					// Atom symbol i.e. C, H, N, P or S

		// The Symmetry Numbers are needed to compare the atoms (Atom class and the compareTo method) before adding them below
		this.setSymmetryNumbers();
		int[] AddedSymmetryNumbers = new int[this.HighestSymmetryNumber];

		for (int atomNr = 0; atomNr < this.getAtomCount(); atomNr++){

			currentAtom = this.getAtom(atomNr);
			int currentSymmetryNumber = SMARTCYP_PROPERTY.SymmetryNumber.get(currentAtom).intValue();

			// Match atom symbol
			currentAtomType = currentAtom.getSymbol();
			if(currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P") || currentAtomType.equals("S")) {
				if (FindInArray(AddedSymmetryNumbers,currentSymmetryNumber) == 0) {
					atomsSortedByEnA.add(currentAtom);
					AddedSymmetryNumbers[currentSymmetryNumber - 1] = currentSymmetryNumber;
				}
			}
		}
	}

	public void sortAtoms2D6() throws CDKException{

		IAtom currentAtom;
		String currentAtomType;					// Atom symbol i.e. C, H, N, P or S

		// The Symmetry Numbers are needed to compare the atoms (Atom class and the compareTo method) before adding them below
		this.setSymmetryNumbers();
		int[] AddedSymmetryNumbers = new int[this.HighestSymmetryNumber];
 
		for (int atomNr = 0; atomNr < this.getAtomCount(); atomNr++){

			currentAtom = this.getAtom(atomNr);
			int currentSymmetryNumber = SMARTCYP_PROPERTY.SymmetryNumber.get(currentAtom).intValue();

			// Match atom symbol
			currentAtomType = currentAtom.getSymbol();
			if(currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P") || currentAtomType.equals("S")) {
				if (FindInArray(AddedSymmetryNumbers,currentSymmetryNumber) == 0) {
					atomsSortedByEnA2D6.add(currentAtom);
					AddedSymmetryNumbers[currentSymmetryNumber - 1] = currentSymmetryNumber;
				}
			}
		}
	}

	public void sortAtoms2C9() throws CDKException{

		IAtom currentAtom;
		String currentAtomType;					// Atom symbol i.e. C, H, N, P or S

		// The Symmetry Numbers are needed to compare the atoms (Atom class and the compareTo method) before adding them below
		this.setSymmetryNumbers();
		int[] AddedSymmetryNumbers = new int[this.HighestSymmetryNumber];
 
		for (int atomNr = 0; atomNr < this.getAtomCount(); atomNr++){

			currentAtom = this.getAtom(atomNr);
			int currentSymmetryNumber = SMARTCYP_PROPERTY.SymmetryNumber.get(currentAtom).intValue();

			// Match atom symbol
			currentAtomType = currentAtom.getSymbol();
			if(currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P") || currentAtomType.equals("S")) {
				if (FindInArray(AddedSymmetryNumbers,currentSymmetryNumber) == 0) {
					atomsSortedByEnA2C9.add(currentAtom);
					AddedSymmetryNumbers[currentSymmetryNumber - 1] = currentSymmetryNumber;
				}
			}
		}
	}
	
	public void sortAtoms1A2() throws CDKException{

		IAtom currentAtom;
		String currentAtomType;					// Atom symbol i.e. C, H, N, P or S

		// The Symmetry Numbers are needed to compare the atoms (Atom class and the compareTo method) before adding them below
		this.setSymmetryNumbers();
		int[] AddedSymmetryNumbers = new int[this.HighestSymmetryNumber];
 
		for (int atomNr = 0; atomNr < this.getAtomCount(); atomNr++){

			currentAtom = this.getAtom(atomNr);
			int currentSymmetryNumber = SMARTCYP_PROPERTY.SymmetryNumber.get(currentAtom).intValue();

			// Match atom symbol
			currentAtomType = currentAtom.getSymbol();
			if(currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P") || currentAtomType.equals("S")) {
				if (FindInArray(AddedSymmetryNumbers,currentSymmetryNumber) == 0) {
					atomsSortedByEnA1A2.add(currentAtom);
					AddedSymmetryNumbers[currentSymmetryNumber - 1] = currentSymmetryNumber;
				}
			}
		}
	}
	
	public void sortAtoms2C19() throws CDKException{

		IAtom currentAtom;
		String currentAtomType;					// Atom symbol i.e. C, H, N, P or S

		// The Symmetry Numbers are needed to compare the atoms (Atom class and the compareTo method) before adding them below
		this.setSymmetryNumbers();
		int[] AddedSymmetryNumbers = new int[this.HighestSymmetryNumber];
 
		for (int atomNr = 0; atomNr < this.getAtomCount(); atomNr++){

			currentAtom = this.getAtom(atomNr);
			int currentSymmetryNumber = SMARTCYP_PROPERTY.SymmetryNumber.get(currentAtom).intValue();

			// Match atom symbol
			currentAtomType = currentAtom.getSymbol();
			if(currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P") || currentAtomType.equals("S")) {
				if (FindInArray(AddedSymmetryNumbers,currentSymmetryNumber) == 0) {
					atomsSortedByEnA2C19.add(currentAtom);
					AddedSymmetryNumbers[currentSymmetryNumber - 1] = currentSymmetryNumber;
				}
			}
		}
	}

	// Symmetric atoms have identical values in the array from getTopoEquivClassbyHuXu
	public void setSymmetryNumbers() throws CDKException{
		IAtom atom;
		//set charges so that they are not null
		for(int atomIndex = 0; atomIndex < this.getAtomCount(); atomIndex++){
			atom = this.getAtom(atomIndex);
			atom.setCharge((double) atom.getFormalCharge());
		}
		//compute symmetry
		EquivalentClassPartitioner symmtest = new EquivalentClassPartitioner((AtomContainer) this);
		int[] symmetryNumbersArray = symmtest.getTopoEquivClassbyHuXu((AtomContainer) this);
		symmetryNumbersArray[0]=0;//so we can count the number of symmetric sites for each atom without double counting for the ones with the highest symmetrynumber
		int symmsites;
		for(int atomIndex = 0; atomIndex < this.getAtomCount(); atomIndex++){
			symmsites = 0;
			atom = this.getAtom(atomIndex);
			SMARTCYP_PROPERTY.SymmetryNumber.set(atom,symmetryNumbersArray[atomIndex+1]);
			// Compute how many symmetric sites the atom has, 1=only itself
			symmsites = FindInArray(symmetryNumbersArray,symmetryNumbersArray[atomIndex+1]);
			SMARTCYP_PROPERTY.NrofSymmetricSites.set(atom,symmsites);

			if (symmetryNumbersArray[atomIndex+1] > HighestSymmetryNumber) HighestSymmetryNumber = symmetryNumbersArray[atomIndex+1];
		}
	}



	// This method makes the ranking
	public void rankAtoms() throws CDKException{

		// Iterate over the Atoms in this sortedAtomsTreeSet
		int rankNr = 1;
		int loopNr = 1;
		IAtom previousAtom = null;
		IAtom currentAtom;
		Iterator<IAtom> atomsSortedByEnAiterator = this.getAtomsSortedByEnA().iterator();
		while(atomsSortedByEnAiterator.hasNext()){

			currentAtom = atomsSortedByEnAiterator.next();

			// First Atom
			if(previousAtom == null){}				// Do nothing												

			// Atoms have no score, compare Accessibility instead
			else if(SMARTCYP_PROPERTY.Score.get(currentAtom) == null){
				if(SMARTCYP_PROPERTY.Accessibility.get(currentAtom) != SMARTCYP_PROPERTY.Accessibility.get(previousAtom)) rankNr = loopNr;
			} 

			// Compare scores
			else if(SMARTCYP_PROPERTY.Score.get(currentAtom).doubleValue() > SMARTCYP_PROPERTY.Score.get(previousAtom).doubleValue()) rankNr = loopNr;

			// Else, Atoms have the same score
			SMARTCYP_PROPERTY.Ranking.set(currentAtom,rankNr);
			previousAtom = currentAtom;	
			loopNr++;
		}

		this.rankSymmetricAtoms();
	}

	// This method makes the ranking
	public void rankAtoms2D6() throws CDKException{

		// Iterate over the Atoms in this sortedAtomsTreeSet
		int rankNr = 1;
		int loopNr = 1;
		IAtom previousAtom = null;
		IAtom currentAtom;
		Iterator<IAtom> atomsSortedByEnAiterator = this.getAtomsSortedByEnA2D6().iterator();
		while(atomsSortedByEnAiterator.hasNext()){

			currentAtom = atomsSortedByEnAiterator.next();

			// First Atom
			if(previousAtom == null){}				// Do nothing												

			// Atoms have no score, compare Accessibility instead
			//else if(SMARTCYP_PROPERTY.Score2D6.get(currentAtom) == null){
			//	if(SMARTCYP_PROPERTY.Accessibility.get(currentAtom) != SMARTCYP_PROPERTY.Accessibility.get(previousAtom)) rankNr = loopNr;
			//} 

			// Compare scores
			else if(SMARTCYP_PROPERTY.Score2D6.get(currentAtom).doubleValue() > SMARTCYP_PROPERTY.Score2D6.get(previousAtom).doubleValue()) rankNr = loopNr;

			// Else, Atoms have the same score
			SMARTCYP_PROPERTY.Ranking2D6.set(currentAtom,rankNr);
			previousAtom = currentAtom;	
			loopNr++;
		}

		this.rankSymmetricAtoms2D6();
	}

	// This method makes the ranking
		public void rankAtoms2C9() throws CDKException{

			// Iterate over the Atoms in this sortedAtomsTreeSet
			int rankNr = 1;
			int loopNr = 1;
			IAtom previousAtom = null;
			IAtom currentAtom;
			Iterator<IAtom> atomsSortedByEnAiterator = this.getAtomsSortedByEnA2C9().iterator();
			while(atomsSortedByEnAiterator.hasNext()){

				currentAtom = atomsSortedByEnAiterator.next();

				// First Atom
				if(previousAtom == null){}				// Do nothing												

				// Atoms have no score, compare Accessibility instead
				//else if(SMARTCYP_PROPERTY.Score2D6.get(currentAtom) == null){
				//	if(SMARTCYP_PROPERTY.Accessibility.get(currentAtom) != SMARTCYP_PROPERTY.Accessibility.get(previousAtom)) rankNr = loopNr;
				//} 

				// Compare scores
				else if(SMARTCYP_PROPERTY.Score2C9.get(currentAtom).doubleValue() > SMARTCYP_PROPERTY.Score2C9.get(previousAtom).doubleValue()) rankNr = loopNr;

				// Else, Atoms have the same score
				SMARTCYP_PROPERTY.Ranking2C9.set(currentAtom,rankNr);
				previousAtom = currentAtom;	
				loopNr++;
			}

			this.rankSymmetricAtoms2C9();
		}

	
	// This method makes the ranking
	public void rankAtoms2C19() throws CDKException{

		// Iterate over the Atoms in this sortedAtomsTreeSet
		int rankNr = 1;
		int loopNr = 1;
		IAtom previousAtom = null;
		IAtom currentAtom;
		Iterator<IAtom> atomsSortedByEnAiterator = this.getAtomsSortedByEnA2C19().iterator();
		while(atomsSortedByEnAiterator.hasNext()){

			currentAtom = atomsSortedByEnAiterator.next();

			// First Atom
			if(previousAtom == null){}				// Do nothing												

			// Atoms have no score, compare Accessibility instead
			//else if(SMARTCYP_PROPERTY.Score2C19.get(currentAtom) == null){
			//	if(SMARTCYP_PROPERTY.Accessibility.get(currentAtom) != SMARTCYP_PROPERTY.Accessibility.get(previousAtom)) rankNr = loopNr;
			//} 

			// Compare scores
			else if(SMARTCYP_PROPERTY.Score2C19.get(currentAtom).doubleValue() > SMARTCYP_PROPERTY.Score2C19.get(previousAtom).doubleValue()) rankNr = loopNr;

			// Else, Atoms have the same score
			SMARTCYP_PROPERTY.Ranking2C19.set(currentAtom,rankNr);
			previousAtom = currentAtom;	
			loopNr++;
		}

		this.rankSymmetricAtoms2C19();
	}
	
	// This method makes the ranking
	public void rankAtoms1A2() throws CDKException{

		// Iterate over the Atoms in this sortedAtomsTreeSet
		int rankNr = 1;
		int loopNr = 1;
		IAtom previousAtom = null;
		IAtom currentAtom;
		Iterator<IAtom> atomsSortedByEnAiterator = this.getAtomsSortedByEnA1A2().iterator();
		while(atomsSortedByEnAiterator.hasNext()){

			currentAtom = atomsSortedByEnAiterator.next();

			// First Atom
			if(previousAtom == null){}				// Do nothing												

			// Atoms have no score, compare Accessibility instead
			//else if(SMARTCYP_PROPERTY.Score1A2.get(currentAtom) == null){
			//	if(SMARTCYP_PROPERTY.Accessibility.get(currentAtom) != SMARTCYP_PROPERTY.Accessibility.get(previousAtom)) rankNr = loopNr;
			//} 

			// Compare scores
			else if(SMARTCYP_PROPERTY.Score1A2.get(currentAtom).doubleValue() > SMARTCYP_PROPERTY.Score1A2.get(previousAtom).doubleValue()) rankNr = loopNr;

			// Else, Atoms have the same score
			SMARTCYP_PROPERTY.Ranking1A2.set(currentAtom,rankNr);
			previousAtom = currentAtom;	
			loopNr++;
		}

		this.rankSymmetricAtoms1A2();
	}


	// This method makes the ranking of symmetric atoms
	public void rankSymmetricAtoms() throws CDKException{

		IAtom currentAtom;
		String currentAtomType;					// Atom symbol i.e. C, H, N, P or S

		for (int atomNr = 0; atomNr < this.getAtomCount(); atomNr++){

			currentAtom = this.getAtom(atomNr);

			// Match atom symbol
			currentAtomType = currentAtom.getSymbol();
			if(currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P") || currentAtomType.equals("S")) {			

				//This clause finds symmetric atoms which have not been assigned a ranking
				if(SMARTCYP_PROPERTY.Ranking.get(currentAtom) == null){

					// AtomsSortedByEnA contains the ranked atoms
					// We just need to find the symmetric atom and use its ranking for the unranked symmetric atom
					Iterator<IAtom> atomsSortedByEnAiterator = this.getAtomsSortedByEnA().iterator();
					IAtom rankedAtom;
					Number rankNr;
					while(atomsSortedByEnAiterator.hasNext()){

						rankedAtom = atomsSortedByEnAiterator.next();

						if(SMARTCYP_PROPERTY.SymmetryNumber.get(currentAtom).intValue() == SMARTCYP_PROPERTY.SymmetryNumber.get(rankedAtom).intValue()){

							rankNr = SMARTCYP_PROPERTY.Ranking.get(rankedAtom);
							SMARTCYP_PROPERTY.Ranking.set(currentAtom,rankNr);
							SMARTCYP_PROPERTY.IsSymmetric.set(currentAtom,1);

						}
					}

				}

			}
		}
	}

	// This method makes the ranking of symmetric atoms
	public void rankSymmetricAtoms2D6() throws CDKException{

		IAtom currentAtom;
		String currentAtomType;					// Atom symbol i.e. C, H, N, P or S

		for (int atomNr = 0; atomNr < this.getAtomCount(); atomNr++){

			currentAtom = this.getAtom(atomNr);

			// Match atom symbol
			currentAtomType = currentAtom.getSymbol();
			if(currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P") || currentAtomType.equals("S")) {			

				//This clause finds symmetric atoms which have not been assigned a ranking
				if(SMARTCYP_PROPERTY.Ranking2D6.get(currentAtom) == null){

					// AtomsSortedByEnA contains the ranked atoms
					// We just need to find the symmetric atom and use its ranking for the unranked symmetric atom
					Iterator<IAtom> atomsSortedByEnAiterator = this.getAtomsSortedByEnA2D6().iterator();
					IAtom rankedAtom;
					Number rankNr;
					while(atomsSortedByEnAiterator.hasNext()){

						rankedAtom = atomsSortedByEnAiterator.next();

						if(SMARTCYP_PROPERTY.SymmetryNumber.get(currentAtom).intValue() == SMARTCYP_PROPERTY.SymmetryNumber.get(rankedAtom).intValue()){

							rankNr = SMARTCYP_PROPERTY.Ranking2D6.get(rankedAtom);
							SMARTCYP_PROPERTY.Ranking2D6.set(currentAtom,rankNr);
							SMARTCYP_PROPERTY.IsSymmetric.set(currentAtom,1);

						}
					}

				}

			}
		}
	}	

	// This method makes the ranking of symmetric atoms
	public void rankSymmetricAtoms2C9() throws CDKException{

		IAtom currentAtom;
		String currentAtomType;					// Atom symbol i.e. C, H, N, P or S

		for (int atomNr = 0; atomNr < this.getAtomCount(); atomNr++){

			currentAtom = this.getAtom(atomNr);

			// Match atom symbol
			currentAtomType = currentAtom.getSymbol();
			if(currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P") || currentAtomType.equals("S")) {			

				//This clause finds symmetric atoms which have not been assigned a ranking
				if(SMARTCYP_PROPERTY.Ranking2C9.get(currentAtom) == null){

					// AtomsSortedByEnA contains the ranked atoms
					// We just need to find the symmetric atom and use its ranking for the unranked symmetric atom
					Iterator<IAtom> atomsSortedByEnAiterator = this.getAtomsSortedByEnA2C9().iterator();
					IAtom rankedAtom;
					Number rankNr;
					while(atomsSortedByEnAiterator.hasNext()){

						rankedAtom = atomsSortedByEnAiterator.next();

						if(SMARTCYP_PROPERTY.SymmetryNumber.get(currentAtom).intValue() == SMARTCYP_PROPERTY.SymmetryNumber.get(rankedAtom).intValue()){

							rankNr = SMARTCYP_PROPERTY.Ranking2C9.get(rankedAtom);
							SMARTCYP_PROPERTY.Ranking2C9.set(currentAtom,rankNr);
							SMARTCYP_PROPERTY.IsSymmetric.set(currentAtom,1);

						}
					}

				}

			}
		}
	}	

	
	// This method makes the ranking of symmetric atoms
	public void rankSymmetricAtoms2C19() throws CDKException{

		IAtom currentAtom;
		String currentAtomType;					// Atom symbol i.e. C, H, N, P or S

		for (int atomNr = 0; atomNr < this.getAtomCount(); atomNr++){

			currentAtom = this.getAtom(atomNr);

			// Match atom symbol
			currentAtomType = currentAtom.getSymbol();
			if(currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P") || currentAtomType.equals("S")) {			

				//This clause finds symmetric atoms which have not been assigned a ranking
				if(SMARTCYP_PROPERTY.Ranking2C19.get(currentAtom) == null){

					// AtomsSortedByEnA contains the ranked atoms
					// We just need to find the symmetric atom and use its ranking for the unranked symmetric atom
					Iterator<IAtom> atomsSortedByEnAiterator = this.getAtomsSortedByEnA2C19().iterator();
					IAtom rankedAtom;
					Number rankNr;
					while(atomsSortedByEnAiterator.hasNext()){

						rankedAtom = atomsSortedByEnAiterator.next();

						if(SMARTCYP_PROPERTY.SymmetryNumber.get(currentAtom).intValue() == SMARTCYP_PROPERTY.SymmetryNumber.get(rankedAtom).intValue()){

							rankNr = SMARTCYP_PROPERTY.Ranking2C19.get(rankedAtom);
							SMARTCYP_PROPERTY.Ranking2C19.set(currentAtom,rankNr);
							SMARTCYP_PROPERTY.IsSymmetric.set(currentAtom,1);

						}
					}

				}

			}
		}
	}	

	// This method makes the ranking of symmetric atoms
	public void rankSymmetricAtoms1A2() throws CDKException{

		IAtom currentAtom;
		String currentAtomType;					// Atom symbol i.e. C, H, N, P or S

		for (int atomNr = 0; atomNr < this.getAtomCount(); atomNr++){

			currentAtom = this.getAtom(atomNr);

			// Match atom symbol
			currentAtomType = currentAtom.getSymbol();
			if(currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P") || currentAtomType.equals("S")) {			

				//This clause finds symmetric atoms which have not been assigned a ranking
				if(SMARTCYP_PROPERTY.Ranking1A2.get(currentAtom) == null){

					// AtomsSortedByEnA contains the ranked atoms
					// We just need to find the symmetric atom and use its ranking for the unranked symmetric atom
					Iterator<IAtom> atomsSortedByEnAiterator = this.getAtomsSortedByEnA1A2().iterator();
					IAtom rankedAtom;
					Number rankNr;
					while(atomsSortedByEnAiterator.hasNext()){

						rankedAtom = atomsSortedByEnAiterator.next();

						if(SMARTCYP_PROPERTY.SymmetryNumber.get(currentAtom).intValue() == SMARTCYP_PROPERTY.SymmetryNumber.get(rankedAtom).intValue()){

							rankNr = SMARTCYP_PROPERTY.Ranking1A2.get(rankedAtom);
							SMARTCYP_PROPERTY.Ranking1A2.set(currentAtom,rankNr);
							SMARTCYP_PROPERTY.IsSymmetric.set(currentAtom,1);

						}
					}

				}

			}
		}
	}	

	// Get the TreeSet containing the sorted C, N, P and S atoms
	public TreeSet<IAtom> getAtomsSortedByEnA(){
		return this.atomsSortedByEnA;
	}

	public TreeSet<IAtom> getAtomsSortedByEnA2D6(){
		return this.atomsSortedByEnA2D6;
	}
	
	public TreeSet<IAtom> getAtomsSortedByEnA2C9(){
		return this.atomsSortedByEnA2C9;
	}
	
	public TreeSet<IAtom> getAtomsSortedByEnA2C19(){
		return this.atomsSortedByEnA2C19;
	}
	
	public TreeSet<IAtom> getAtomsSortedByEnA1A2(){
		return this.atomsSortedByEnA1A2;
	}

	public void setID(String id){
		super.setID(id);
	}

	public String toString(){
		for(int atomNr=0; atomNr < this.getAtomCount(); atomNr++) System.out.println(this.getAtom(atomNr).toString());
		return "MoleculeKU " + super.toString();
	}

	public static int FindInArray(int[] arr, int numToFind) {
		int occurence=0;
		for (int i = 0; i < arr.length; i++) { 
			if (arr[i] == numToFind) occurence++;
		}
		return occurence;
	}

	public int[] convertStringArraytoIntArray(String[] sarray) throws Exception {
		if (sarray != null) {
			int intarray[] = new int[sarray.length];
			for (int i = 0; i < sarray.length; i++) {
				intarray[i] = Integer.parseInt(sarray[i]);
			}
			return intarray;
		}
		return null;
	}

	public static int [] concatAll(int[] first, int[]... rest) {
		int totalLength = first.length;
		for (int [] array : rest) {
			totalLength += array.length;
		}
		int [] result = Arrays.copyOf(first, totalLength);
		int offset = first.length;
		for (int [] array : rest) {
			System.arraycopy(array, 0, result, offset, array.length);
			offset += array.length;
		}
		return result;
	}
	
	
	/**
	 * Taken from SMARTCyp 2.4.2 during lhasa refactoring
	 * 
	 * @throws CDKException
	 */
	public void unlikelyNoxidationCorrection() throws CDKException
	{

		ArrayList<String> EmpiricalSMARTS = new ArrayList<String>();
		// select nitrogen oxidations never or almost never seen
		EmpiricalSMARTS.add("[$([NX3H0]([CX4])([CX4])[CX4]);!$([NX3]@[C])]"); // tertiary
																				// amine
																				// nitrogen
																				// not
																				// in
																				// ring
		EmpiricalSMARTS.add("[$([NX3H0]1([CX4])[CX4][CX4][CX4][CX4][CX4]1);!$([NX3]1(@[C])[C][C][C][C][C]1);!$([NX3]1[C](@[*])[C][C][C][C]1)]"); // tertiary
																																					// amine
																																					// piperidine
																																					// nitrogen
		EmpiricalSMARTS.add("[$([NR2H0]1([CX4])[CX4]2[CX4][CX4][CX4][CX4]1[CX4][CX4][CX4]2)]"); // tertiary
																								// amine
																								// in
																								// bridged
																								// dual
																								// 6-rings
		EmpiricalSMARTS.add("[$([NX3H0]1([CX4])[CX3][C][NX3][C][CX4]1);!$([NX3]1([C])[C][C][NX3]([#6X3])[C][C]1);!$([NX3]1([#6X3])[C][C][NX3][C][C]1)]"); // piperazine
																																							// nitrogen
		EmpiricalSMARTS.add("[$([N^3H0]);$([NR2r6](@[CX4])(@[CX4])@[CX4])]"); // nitrogen
																				// bridging
																				// two
																				// six
																				// rings
																				// as
																				// in
																				// octahydroquinolizine

		// Variables
		double correction = 100.0; // the penalty (in kJ/mol) which is added to
									// the score of the atoms chosen
		int numberOfSMARTSmatches = 0;
		String currentSMARTS = "C";
		SMARTSQueryTool querytool = new SMARTSQueryTool(currentSMARTS, SilentChemObjectBuilder.getInstance()); // Creates
																												// the
																												// Query
																												// Tool

		// Iterate over the SMARTS in the array above
		Iterator<String> itr = EmpiricalSMARTS.iterator();
		while (itr.hasNext())
		{
			try
			{
				currentSMARTS = itr.next();
				querytool.setSmarts(currentSMARTS);

				// Check if there are any SMARTS matches
				boolean status = querytool.matches(this);
				if (status)
				{
					numberOfSMARTSmatches = querytool.countMatches(); // Count
																		// the
																		// number
																		// of
																		// matches
					List<List<Integer>> matchingAtomsIndicesList_1; // List of
																	// List
																	// objects
																	// each
																	// containing
																	// the
																	// indices
																	// of the
																	// atoms in
																	// the
																	// target
																	// molecule
					List<Integer> matchingAtomsIndicesList_2 = null; // List of
																		// atom
																		// indices
					// System.out.println("\n The SMARTS " + currentSMARTS + "
					// has " + numberOfSMARTSmatches + " matches in the molecule
					// " + this.getID());

					matchingAtomsIndicesList_1 = querytool.getMatchingAtoms(); // This
																				// list
																				// contains
																				// the
																				// C,
																				// N,
																				// P
																				// and
																				// S
																				// atom
																				// indices

					for (int listObjectIndex = 0; listObjectIndex < numberOfSMARTSmatches; listObjectIndex++)
					{

						matchingAtomsIndicesList_2 = matchingAtomsIndicesList_1.get(listObjectIndex); // Contains
																										// multiple
																										// atoms

						// Set the descriptorvalue of the atoms which should be
						// empirically corrected
						int indexOfMatchingAtom;
						IAtom matchingAtom;
						double newenergy = 0.0;
						for (int atomNr = 0; atomNr < matchingAtomsIndicesList_2.size(); atomNr++)
						{ // Contains 1 atom
							indexOfMatchingAtom = matchingAtomsIndicesList_2.get(atomNr);
							matchingAtom = this.getAtom(indexOfMatchingAtom);
							newenergy = SMARTCYP_PROPERTY.Energy.get(matchingAtom).doubleValue() + correction;
							SMARTCYP_PROPERTY.Energy.set(matchingAtom, newenergy);
							// System.out.println(indexOfMatchingAtom);
						}
					}
				}
			} catch (CDKException e)
			{
				System.out.println("There is something fishy with the SMARTS: " + currentSMARTS);
				e.printStackTrace();
			}
		}
	}

}





