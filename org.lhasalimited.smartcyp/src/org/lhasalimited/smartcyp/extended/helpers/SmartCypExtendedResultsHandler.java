package org.lhasalimited.smartcyp.extended.helpers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.lhasalimited.smartcyp.extended.MoleculeKU;
import org.lhasalimited.smartcyp.extended.MoleculeKU.SMARTCYP_PROPERTY;
import org.openscience.cdk.interfaces.IAtom;



public class SmartCypExtendedResultsHandler 
{
	
	private static double ERROR_VALUE = 999d;
	
	public static void writeCsv(File csv, List<MoleculeKU> results)
	{
		
		try 
		{
			FileWriter writer = new FileWriter(csv);
			
			CSVPrinter printer = CSVFormat.DEFAULT.withHeader(
					"Molecule",
					"Atom",
					"Atom ID",
					"Ranking",
					"Score",
					"Energy",
					"Accessibility",
					"2D6ranking",
					"2D6score",
					"Span2End",
					"N+Dist",
					"2C9ranking",
					"2C9score",
					"CODist",
					"2C19ranking",
					"2C19score",
					"COCSDist",
					"1A2Ranking",
					"1A2score",
					"D2rCO").print(writer);
			
			
			IAtom currentAtom;
			String currentAtomType;					// Atom symbol i.e. C, H, N, P or S

			
			int moleculeIndex = 0;
			for(MoleculeKU moleculeKU : results)
			{
				
				String ID = moleculeKU.getID();
				
				for(int atomIndex = 0; atomIndex < moleculeKU.getAtomCount()  ; atomIndex++ )
				{
					currentAtom = moleculeKU.getAtom(atomIndex);
					currentAtomType = currentAtom.getSymbol();
					
					if(currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P") || currentAtomType.equals("S"))
					{
						
						Number ranking = SMARTCYP_PROPERTY.Ranking.get(currentAtom);
						
						Number score = ERROR_VALUE;
						Number energy = ERROR_VALUE;
						if(SMARTCYP_PROPERTY.Score.get(currentAtom) != null)
						{
							score = SMARTCYP_PROPERTY.Score.get(currentAtom);
							energy = SMARTCYP_PROPERTY.Energy.get(currentAtom);
						}
						
						Number accessibility = SMARTCYP_PROPERTY.Accessibility.get(currentAtom);
						
						Number score2d6 = ERROR_VALUE;
						Number ranking2d6 = ERROR_VALUE;
						if(SMARTCYP_PROPERTY.Score2D6.get(currentAtom) != null) 
						{
							ranking2d6 = SMARTCYP_PROPERTY.Ranking2D6.get(currentAtom);
							score2d6 = SMARTCYP_PROPERTY.Score2D6.get(currentAtom);
						}
						
						Number span2end = SMARTCYP_PROPERTY.Span2End.get(currentAtom);
						
						Number dist2protamine = 0;
						if(SMARTCYP_PROPERTY.Dist2ProtAmine.get(currentAtom) != null)
						{
							dist2protamine = SMARTCYP_PROPERTY.Dist2ProtAmine.get(currentAtom);
						}
						
						Number ranking2c9 = ERROR_VALUE;
						Number score2c9 = ERROR_VALUE;
						if(SMARTCYP_PROPERTY.Score2C9.get(currentAtom) != null) 
						{
							ranking2c9 = SMARTCYP_PROPERTY.Ranking2C9.get(currentAtom);
							score2c9 = SMARTCYP_PROPERTY.Score2C9.get(currentAtom);
						}
						
						Number dist2CO = 0;
						if(SMARTCYP_PROPERTY.Dist2CO.get(currentAtom) != null)
						{
							dist2CO = SMARTCYP_PROPERTY.Dist2CO.get(currentAtom);
						}

						
						Number Ranking2C19 = ERROR_VALUE;
						Number Score2C19 = ERROR_VALUE;
						if(SMARTCYP_PROPERTY.Score2C19.get(currentAtom) != null) 
						{
							Ranking2C19 = SMARTCYP_PROPERTY.Ranking2C19.get(currentAtom);
							Score2C19 = SMARTCYP_PROPERTY.Score2C19.get(currentAtom);
						}
						
						Number Dist2COCS = 0;
						if(SMARTCYP_PROPERTY.Dist2COCS.get(currentAtom) != null)
						{
							Dist2COCS = SMARTCYP_PROPERTY.Dist2COCS.get(currentAtom);
						}

						
						Number Ranking1A2 = ERROR_VALUE;
						Number Score1A2 = ERROR_VALUE;
						if(SMARTCYP_PROPERTY.Score1A2.get(currentAtom) != null) 
						{
							Ranking2C19 = SMARTCYP_PROPERTY.Ranking1A2.get(currentAtom);
							Score2C19 = SMARTCYP_PROPERTY.Score1A2.get(currentAtom);
						}
						
						Number Dist2COr = 0;
						if(SMARTCYP_PROPERTY.Dist2COr.get(currentAtom) != null)
						{
							Dist2COCS = SMARTCYP_PROPERTY.Dist2COr.get(currentAtom);
						}
							
						printer.printRecord(moleculeIndex, currentAtom.getSymbol(), currentAtom.getID(), ranking, 
								score, energy, accessibility, ranking2d6, score2d6, span2end, dist2protamine, ranking2c9, score2c9, 
								dist2CO, Ranking2C19, Score2C19, Dist2COCS, Ranking1A2, Score1A2, Dist2COr);
						
					}
				}
				
				moleculeIndex++;
			}
						
			writer.close();
			
		} catch (IOException e) 
		{
			e.printStackTrace();
		}

	}
	

}
