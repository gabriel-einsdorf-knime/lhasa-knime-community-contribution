package org.lhasalimited.smartcyp.extended.helpers;

import java.io.IOException;
import java.io.StringReader;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.lhasalimited.smartcyp.ISmartCypProcessor;
import org.lhasalimited.smartcyp.extended.MoleculeKU;
import org.lhasalimited.smartcyp.extended.SMARTSnEnergiesTable;
import org.openscience.cdk.Atom;
import org.openscience.cdk.AtomContainer;
import org.openscience.cdk.CDKConstants;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.aromaticity.Aromaticity;
import org.openscience.cdk.aromaticity.ElectronDonation;
import org.openscience.cdk.exception.CDKException;
import org.openscience.cdk.graph.ConnectivityChecker;
import org.openscience.cdk.graph.Cycles;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IAtomContainerSet;
import org.openscience.cdk.interfaces.IChemFile;
import org.openscience.cdk.interfaces.IChemObjectBuilder;
import org.openscience.cdk.io.ISimpleChemObjectReader;
import org.openscience.cdk.io.MDLReader;
import org.openscience.cdk.silent.SilentChemObjectBuilder;
import org.openscience.cdk.tools.CDKHydrogenAdder;
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator;
import org.openscience.cdk.tools.manipulator.ChemFileManipulator;

/**
 * An adaption of the SmartCyp class to handle the processing of Mol
 * representations as well as {@link IAtomContainer} structures. <br>
 * <br>
 * Refactoring has been undertaken to replace {@link Atom} objects with
 * {@link IAtom} and removal of casting to {@link Atom} class. This allows for
 * the use of {@link AtomContainer} and
 * {@link org.openscience.cdk.silent.AtomContainer} <br>
 * <br>
 * The code has been updated to use CDK 1.5.12 as it is intended for use within
 * the KNIME environment.
 *
 * 
 * 
 * @author Samuel
 *
 */
public class SmartCypExtendedProcessor implements ISmartCypProcessor
{

	private boolean nOxidationCorrection;

	public SmartCypExtendedProcessor(boolean nOxidationCorrection)
	{
		this.nOxidationCorrection = nOxidationCorrection;
	}

	/**
	 * Process a single molstring
	 * 
	 * @param structure
	 *            The molfile string
	 * @param id
	 *            The ID of the structure
	 * 
	 * @return The result form SmartCyp, this is a list as individual components
	 *         may be returned under this API
	 * @throws Exception
	 */
	public List<MoleculeKU> process(String structure, String id) throws Exception
	{
		// Produce SMARTSnEnergiesTable object
		System.out.println("\n ************** Processing SMARTS and Energies **************");
		SMARTSnEnergiesTable SMARTSnEnergiesTable = new SMARTSnEnergiesTable();

		// Read in structures/molecules
		System.out.println("\n ************** Reading molecule structures **************");
		List<IAtomContainer> moleculeSet = readInStructures(structure, SMARTSnEnergiesTable.getSMARTSnEnergiesTable(), id);

		return process(moleculeSet, SMARTSnEnergiesTable);
	}

	/**
	 * The Map represents a collection of molfiles with ids. The ID should be
	 * the key and the structure the value
	 * 
	 * 
	 * @param structures
	 * @return
	 * @throws Exception
	 */
	public List<MoleculeKU> process(Map<String, String> structures) throws Exception
	{
		SMARTSnEnergiesTable SMARTSnEnergiesTable = new SMARTSnEnergiesTable();

		List<IAtomContainer> moleculeSet = new ArrayList<IAtomContainer>();

		structures.forEach((id, structure) ->
		{
			try
			{
				moleculeSet.addAll(readInStructures(structure, SMARTSnEnergiesTable.getSMARTSnEnergiesTable(), id));
			} catch (Exception e)
			{
				e.printStackTrace();
				throw new UncheckedIOException(new IOException(e));
			}
		});

		return process(moleculeSet, SMARTSnEnergiesTable);
	}

	/**
	 * Process a collection of CDK structures. The ID's should be set as the ID
	 * of the {@link IAtomContainer}
	 * 
	 * @param moleculeSet
	 * @param SMARTSnEnergiesTable
	 * @return
	 * @throws Exception
	 */
	public List<MoleculeKU> process(List<IAtomContainer> moleculeSet, SMARTSnEnergiesTable SMARTSnEnergiesTable) throws Exception
	{

		List<MoleculeKU> results = new ArrayList<MoleculeKU>();

		MoleculeKU moleculeKU;
		for(int moleculeIndex = 0; moleculeIndex < moleculeSet.size(); moleculeIndex++){
			moleculeKU = (MoleculeKU) moleculeSet.get(moleculeIndex);
						
			//System.out.println("\n ************** Matching SMARTS to assign Energies **************");
			moleculeKU.assignAtomEnergies(SMARTSnEnergiesTable.getSMARTSnEnergiesTable());	

			//System.out.println("\n ************** Calculating shortest distance to protonated amine **************");
			moleculeKU.calculateDist2ProtAmine();
			
			//System.out.println("\n ************** Calculating shortest distance to COOH **************");
			moleculeKU.calculateDist2CO();
			
			//System.out.println("\n ************** Calculating shortest distance to ring C=O **************");
			moleculeKU.calculateDist2COr();
			
			//RL2C19: System.out.println("\n ************** Calculating shortest distance to C=O/S=O **************");
			moleculeKU.calculateDist2COCS();
			
			//System.out.println("\n ************** Calculating Span2End**************");
			moleculeKU.calculateSpan2End();
			
			//System.out.println("\n ************** Calculating Accessabilities and Atom Scores**************");
			moleculeKU.calculateAtomAccessabilities();
			moleculeKU.calculateAtomScores();
	 		moleculeKU.calculate2D6AtomScores();
	 		moleculeKU.calculate1A2AtomScores();       //RL1A2
	 		moleculeKU.calculate2C19AtomScores();       //RL2C19
	 		moleculeKU.calculate2C9AtomScores();       //RL2C9
			
			//System.out.println("\n ************** Identifying, sorting and ranking C, N, P and S atoms **************");
			moleculeKU.sortAtoms();
			moleculeKU.rankAtoms();
			moleculeKU.sortAtoms2D6();
			moleculeKU.rankAtoms2D6();
			moleculeKU.sortAtoms1A2();  //RL1A2
			moleculeKU.rankAtoms1A2();  //RL1A2
			moleculeKU.sortAtoms2C19();  //RL2C19
			moleculeKU.rankAtoms2C19();  //RL2C19
			moleculeKU.sortAtoms2C9();
			moleculeKU.rankAtoms2C9();
			
			
			if (nOxidationCorrection)
			{
				// System.out.println("\n ************** Add Empirical Nitrogen
				// Oxidation Corrections **************");
				moleculeKU.unlikelyNoxidationCorrection();
			}
			
			results.add(moleculeKU);
		}
		
		
		return results;

	}

	public static List<IAtomContainer> readInStructures(String molFile, HashMap<String, Double> SMARTSnEnergiesTable, String id)
			throws CloneNotSupportedException, CDKException
	{

		List<IAtomContainer> moleculeSet = new ArrayList<IAtomContainer>();

		List<IAtomContainer> moleculeList;
		IChemObjectBuilder builder = SilentChemObjectBuilder.getInstance();
		ISimpleChemObjectReader reader;

		IChemFile emptyChemFile;
		IChemFile chemFile;

		int highestMoleculeID = 1;

		try
		{

			reader = new MDLReader(new StringReader(molFile));

			emptyChemFile = builder.newInstance(IChemFile.class);
			chemFile = (IChemFile) reader.read(emptyChemFile);

			// System.out.println(chemFile.toString());

			// Get Molecules
			moleculeList = ChemFileManipulator.getAllAtomContainers(chemFile);

			// Iterate Molecules
			MoleculeKU moleculeKU;
			IAtomContainer iAtomContainerTmp;
			IAtomContainer iAtomContainer;
			CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(DefaultChemObjectBuilder.getInstance());
			for (int atomContainerNr = 0; atomContainerNr < moleculeList.size(); atomContainerNr++)
			{
				iAtomContainerTmp = moleculeList.get(atomContainerNr);

				// Remove salts or solvents... Keep only the largest molecule
				if (!ConnectivityChecker.isConnected(iAtomContainerTmp))
				{
					// System.out.println(atomContainerNr);
					IAtomContainerSet fragments = ConnectivityChecker.partitionIntoMolecules(iAtomContainerTmp);

					int maxID = 0;
					int maxVal = -1;
					for (int i = 0; i < fragments.getAtomContainerCount(); i++)
					{
						if (fragments.getAtomContainer(i).getAtomCount() > maxVal)
						{
							maxID = i;
							maxVal = fragments.getAtomContainer(i).getAtomCount();
						}
					}
					iAtomContainerTmp = fragments.getAtomContainer(maxID);
				}
				// end of salt removal

				iAtomContainer = AtomContainerManipulator.removeHydrogens(iAtomContainerTmp);

				// check number of atoms, if less than 2 don't add molecule
				if (iAtomContainer.getAtomCount() > 1)
				{
					// System.out.println(iAtomContainer.getProperties());

					AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(iAtomContainer);

					// if(deducebonds){
					// DeduceBondSystemTool dbst = new DeduceBondSystemTool();
					// iAtomContainer = dbst.fixAromaticBondOrders((IMolecule)
					// iAtomContainer);
					// }

					adder.addImplicitHydrogens(iAtomContainer);
					// CDKHueckelAromaticityDetector.detectAromaticity(iAtomContainer);
					// Replaced deprecated aromaticity perception
					Aromaticity aromaticity = new Aromaticity(ElectronDonation.cdk(), Cycles.cdkAromaticSet());
					AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(iAtomContainer);
					aromaticity.apply(iAtomContainer);

					System.out.println("Structure class:" + iAtomContainer.getClass().toString());
					System.out.println("Structure classLoader:" + iAtomContainer.getClass().getClassLoader().toString());
					System.out.println(
							"Structure classLoader location:" + iAtomContainer.getClass().getResource(iAtomContainer.getClass().getSimpleName() + ".class"));

					moleculeKU = new MoleculeKU(iAtomContainer, SMARTSnEnergiesTable);
					moleculeSet.add(moleculeKU);
					moleculeKU.setID(id + ", component " + Integer.toString(highestMoleculeID));
					// set the molecule title in the moleculeKU object
					if (iAtomContainer.getProperty("SMIdbNAME") != "" && iAtomContainer.getProperty("SMIdbNAME") != null)
					{
						iAtomContainer.setProperty(CDKConstants.TITLE, iAtomContainer.getProperty("SMIdbNAME"));
					}
					moleculeKU.setProperty(CDKConstants.TITLE, iAtomContainer.getProperty(CDKConstants.TITLE));
					moleculeKU.setProperties(iAtomContainer.getProperties());
					highestMoleculeID++;
				}

			}
			// System.out.println(moleculeList.size() + " molecules were read
			// from the file "+ inFileNames[moleculeFileNr]);

		}

		catch (Exception e)
		{
			e.printStackTrace();
		}

		return moleculeSet;
	}

	public List<MoleculeKU> process(IAtomContainer iAtomContainer, String id) throws Exception
	{
		// Normalise structure
		// CDKHydrogenAdder adder =
		// CDKHydrogenAdder.getInstance(DefaultChemObjectBuilder.getInstance());
		// iAtomContainer =
		// AtomContainerManipulator.removeHydrogens(iAtomContainer);
		// adder.addImplicitHydrogens(iAtomContainer);
		// CDKHueckelAromaticityDetector.detectAromaticity(iAtomContainer);

		IAtomContainer standardized = standardize(iAtomContainer);
		
		SMARTSnEnergiesTable SMARTSnEnergiesTable = new SMARTSnEnergiesTable();

		// TODO: handle mixtures
		List<IAtomContainer> structures = new ArrayList<IAtomContainer>();
		structures.add(new MoleculeKU(standardized, SMARTSnEnergiesTable.getSMARTSnEnergiesTable()));

		return process(structures, SMARTSnEnergiesTable);
	}
	
	public IAtomContainer standardize(IAtomContainer molecule) throws CDKException
	{
		molecule = stripSalts(molecule);
		normaliseStructure(molecule);
		return molecule;
	}

	private void normaliseStructure(IAtomContainer molecule) throws CDKException
	{
		molecule = AtomContainerManipulator.removeHydrogens(molecule);
		CDKHydrogenAdder adder = CDKHydrogenAdder.getInstance(DefaultChemObjectBuilder.getInstance());
		
		AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(molecule);

		adder.addImplicitHydrogens(molecule);
		// CDKHueckelAromaticityDetector.detectAromaticity(iAtomContainer);
		// Replaced deprecated aromaticity perception
		Aromaticity aromaticity = new Aromaticity(ElectronDonation.cdk(), Cycles.cdkAromaticSet());
		AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(molecule);
		aromaticity.apply(molecule);
	}
	
	private IAtomContainer stripSalts(IAtomContainer molecule)
	{
		// Remove salts or solvents... Keep only the largest molecule
		if (!ConnectivityChecker.isConnected(molecule))
		{
			// System.out.println(atomContainerNr);
			IAtomContainerSet fragments = ConnectivityChecker.partitionIntoMolecules(molecule);

			int maxID = 0;
			int maxVal = -1;
			for (int i = 0; i < fragments.getAtomContainerCount(); i++)
			{
				if (fragments.getAtomContainer(i).getAtomCount() > maxVal)
				{
					maxID = i;
					maxVal = fragments.getAtomContainer(i).getAtomCount();
				}
			}
			molecule = fragments.getAtomContainer(maxID);
		}
		
		return molecule;
		// end of salt removal
	}
}
