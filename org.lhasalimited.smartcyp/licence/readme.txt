Licence

SMARTCyp was developed by the Department of Drug Design and Pharmacology at the University of Copenhagen.

SMARTCyp 2.4.2 (funded by Lhasa Limited) binaries and sources have been downloaded from http://www.farma.ku.dk/smartcyp/download.php


If you use this program for publications, please cite:
1) P. Rydberg, D. E. Gloriam, J. Zaretzki, C. Breneman and L. Olsen, ACS Med. Chem. Lett., 2010, 1, 96-100
2) P. Rydberg, D. E. Gloriam and L. Olsen, Bioinformatics, 2010, 26, 2988-2989
3) P. Rydberg and L. Olsen, ACS Med. Chem. Lett., 2012, 3, 69-73, P. Rydberg and L. Olsen, ChemMedChem, 2012, 7, 1202-1209
4) P. Rydberg et al., Angew. Chem, Int. Ed. 2013, 52, 993-997
5) P. Rydberg et al., Mol. Pharmaceutics 2013, 10, 1216-1223.

SMARTCyp has been extended to add models for additional isophorms. These additions are provided by the Biotechnology HPC Software
applications institute. Again licenced under LGPL and binaries and sources were acquired from: http://bhsai.org/downloads/smartcyp_ext/ 


Changes

SMARTCyp 2.4.2 and SMARTCyp_ext have been refactored to use the CDK 1.5.12

- Usage of Molecule class replaced with IAtomContainer
- Use of Atom replaced with IAtom and removal of explicit casting: allows use of Silent atoms/bonds/atom containers
- MoleculeSet replaces with List<IAtomContainer> (IAtomContainerSet could also be used)
- Added the N-Oxidation correction method to SMARTCyp_ext (may not be appropriate for version of SMARTCyp, required validation)
- Created SmartCypProcessor and SmartCypResultsHandler to replace running SmartCyp and printing CSV files
- Created SmartCypImageRender to produce a CDK based image with highlighting and annotations of sites and an Image object