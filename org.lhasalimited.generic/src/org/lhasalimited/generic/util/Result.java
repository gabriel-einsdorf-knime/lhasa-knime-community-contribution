/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.util;

/**
 * Various result types for prediction (active/inactive) and TP, FP etc.
 * 
 * TODO: ACTIVE/INACTIVE is not the standard terminology. Replace references to these words
 * with POSITIVE and NEGATIVE. 
 * 
 * @author Samuel
 *
 */
public enum Result
{
	
	TRUE_POSITIVE, 
	
	TRUE_NEGATIVE, 
	
	FALSE_POSITIVE, 
	
	FALSE_NEGATIVE, 
	
	EQUIVOCAL, 
	
	OUT_OF_DOMAIN, 
	
	/** aka POSITIVE */
	ACTIVE, 
	
	/** aka NEGATIVE */
	INACTIVE, 
	
	UNKNOWN;
}
