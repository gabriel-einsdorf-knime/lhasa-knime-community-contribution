package org.lhasalimited.generic.node.config;

import java.lang.reflect.ParameterizedType;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.MissingCell;
import org.knime.core.data.container.ColumnRearranger;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.streamable.simple.SimpleStreamableFunctionNodeModel;

/**
 * 
 * This abstract class extends {@link SimpleStreamableFunctionNodeModel} and uses an {@link INodeSettingCollection}
 * object to handle loading, validating and savings of settings.
 * <br><br>
 * The settings are available through the localSettings object
 * 
 * @author Samuel, Lhasa Limited
 * @since 1.2.000
 * 
 * @param <T> 	An instance of {@link NodeSettingCollection} which should be used for handling of tall the 
 * 				node settings.
 */
public abstract class StreamableLocalSettingsNodeModel<T extends NodeSettingCollection>
		extends SimpleStreamableFunctionNodeModel
{
	protected T localSettings;

	protected StreamableLocalSettingsNodeModel()
	{
		super();
		setSettingsObject();
	}

	protected void setSettingsObject()
	{

		ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();

		try
		{
			localSettings = (T) Class
					.forName(parameterizedType.getActualTypeArguments()[0].toString().replaceAll("class ", ""), true, getClass().getClassLoader())
					.newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e)
		{
			e.printStackTrace();
		}

	}

	@Override
	protected void saveSettingsTo(NodeSettingsWO settings)
	{
		localSettings.saveSettingsTo(settings);

	}

	@Override
	protected void validateSettings(NodeSettingsRO settings) throws InvalidSettingsException
	{
		localSettings.validateSettings(settings);

	}

	@Override
	protected void loadValidatedSettingsFrom(NodeSettingsRO settings) throws InvalidSettingsException
	{
		localSettings.loadValidatedSettingsFrom(settings);
	}
	
	@Override
	protected DataTableSpec[] configure(DataTableSpec[] inSpecs) throws InvalidSettingsException
	{
		localSettings.configureValidations();
		return super.configure(inSpecs);
	}

	@Override
	protected ColumnRearranger createColumnRearranger(DataTableSpec spec) throws InvalidSettingsException
	{
		localSettings.configureValidations();
		return createColumnRearrangerImplementation(spec);
	}
	
	protected abstract ColumnRearranger createColumnRearrangerImplementation(DataTableSpec spec) throws InvalidSettingsException;
	
	/**
	 * Create an array of missing cells
	 * 
	 * @param reason	the reason the cell is missing
	 * @param size		the number size of the array to create
	 * @return
	 */
	protected DataCell[] createMissing(String reason, int size)
	{
		DataCell[] cells = new DataCell[size];
		
		for(int i = 0; i < size; i++)
			cells[i] = new MissingCell(reason);
		
		return cells;
	}

}
