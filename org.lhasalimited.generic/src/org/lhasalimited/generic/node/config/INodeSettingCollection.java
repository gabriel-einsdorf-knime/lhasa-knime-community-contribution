package org.lhasalimited.generic.node.config;

import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModel;

/**
 * Objects implementing this interface store settings for a node and manage validation, loading and saving.
 * 
 * Particular implementations may provide more functionality such as the generation of node dialog components
 * 
 * @author Samuel
 */
public interface INodeSettingCollection
{

	/**
	 * Get a specific settings model
	 * 
	 * @param key		The key for the settings model
	 * @param type		The type of the settings model
	 * @return
	 */
	public <T> T getSetting(String key, Class<T> type);
	
	/**
	 * Validate all of the settings against the {@link NodeSettingsRO} object. This method
	 * iterates through all settings calling {@link SettingsModel#validateSettings(NodeSettingsRO)}
	 * 
	 * @param settings
	 */
	public void validateSettings(NodeSettingsRO settings);
	
	/**
	 * Load validated settings for all of the settings against the {@link NodeSettingsRO} object. This method
	 * iterates through all settings calling {@link SettingsModel#loadSettingsFrom(NodeSettingsRO)}
	 * 
	 * @param settings
	 */
	public void loadValidatedSettingsFrom(NodeSettingsRO settings);
	
	/**
	 * Save the all the settings to the {@link NodeSettingsWO} object. This method
	 * iterates through all settings calling {@link SettingsModel#saveSettingsTo(NodeSettingsWO)}
	 * 
	 * @param settings
	 */
	public void saveSettingsTo(NodeSettingsWO settings);
	
}
