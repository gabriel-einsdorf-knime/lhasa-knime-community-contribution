/**
 * Copyright (C) 2017  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.xpt.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * Store the {@link FieldDetails} for a given XPT file
 * 
 * @author Samuel, Lhasa Limited
 *
 */
public class XptFields implements Iterable<FieldDetails> {
	private List<FieldDetails> fields;

	/**
	 * Initiialise with an empty collection of {@link FieldDetails}
	 */
	public XptFields() {
		this.fields = new ArrayList<FieldDetails>();
	}

	/**
	 * Initialise with the given list of {@link FieldDetails}
	 * 
	 * @param fields
	 */
	public XptFields(List<FieldDetails> fields) {
		this.fields = fields;
	}

	public void addField(FieldDetails field) {
		try {
			addField(field, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Add a new field
	 * 
	 * @param field
	 *            The field
	 * @param check
	 *            If true checks a field with the given varnum or name is not
	 *            already present
	 * 
	 * @throws Exception
	 *             Thrown if duplicate name or varnum found
	 */
	public void addField(FieldDetails field, boolean check) throws Exception {
		if (check) {

			if (fields.stream().filter(value -> field.getVarNum() == value.getVarNum()).findAny().isPresent()) {
				throw new Exception("Field already present with given varnum " + field.getVarNum());
			}

			if (fields.stream().filter(value -> field.getName().equals(value.getName())).findAny().isPresent()) {
				throw new Exception("Field already present with given name " + field.getName());
			}

		} else {
			fields.add(field);
		}
	}

	/**
	 * Retreive a filed based on index
	 * 
	 * @param index
	 *            0 based index of the column
	 * @return
	 */
	public FieldDetails getFieldByIndex(int index) {
		FieldDetails field = fields.get(index);

		return field;
	}

	/**
	 * Get a field by the given VARNUMBER
	 */
	public Optional<FieldDetails> getFieldByNumber(int number) {
		return fields.stream().filter(value -> value.getVarNum() == number).findAny();
	}

	/**
	 * Get field by given name
	 * 
	 * @param name
	 * @return
	 */
	public Optional<FieldDetails> getField(String name) {
		return fields.stream().filter(value -> value.getName().equals(name)).findAny();
	}

	public int getNumFields() {
		return fields.size();
	}

	public int getTotalBytes() {
		int bytes = 0;

		for (FieldDetails field : fields) {
			bytes += field.getLength();
		}

		bytes += XptDataRow.PADDING;

		return bytes;
	}

	@Override
	public Iterator<FieldDetails> iterator() {
		return fields.iterator();
	}
}
