/**
 * Copyright (C) 2017  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.xpt.util;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.google.common.util.concurrent.UncheckedExecutionException;

public class FieldDetails {
	// byte[] data;

	/** variable type: 1=NUMERIC 2=CHAR */
	short ntype;
	/** Length of variable in observation */
	short length;
	/** VARNUM */
	short varnum;
	/** NAME OF VARIABLE */
	String nameOfVariable;
	/** label of variable */
	String label;
	/** NAME OF FORMAT */
	String format;

	/** FORMAT FIELD LENGTH OR 0 */
	short formatFieldLength;
	/** FORMAT NUMBER OF DECIMALS */
	short formatNumberOfDecimals;
	/** 0=LEFT JUSTIFICATION, 1=RIGHT JUST */
	short formatJustification;

	/** NAME OF INPUT FORMAT */
	String nameOfInputFormat;
	/** INFORMAT LENGTH ATTRIBUTE */
	short informatLengthAttribute;
	/** INFORMAT NUMBER OF DECIMALS */
	short informatNumberOfDecimals;
	/** POSITION OF VALUE IN OBSERVATION */
	long positionOfValueInObservation;

	public FieldDetails(byte[] data) {
		// this.data = data;
		process(data);
	}

	private void process(byte[] data) {
		int position = 0;

		ntype = getShort(XptUtils.subArray(data, position, 2));
		position += 2;

		short nameHash = getShort(XptUtils.subArray(data, position, 2));
		position += 2;

		length = getShort(XptUtils.subArray(data, position, 2));
		position += 2;

		varnum = getShort(XptUtils.subArray(data, position, 2));
		position += 2;

		nameOfVariable = XptUtils.getStringFromBytes(XptUtils.subArray(data, position, 8));
		position += 8;

		label = XptUtils.getStringFromBytes(XptUtils.subArray(data, position, 40));
		position += 40;

		format = XptUtils.getStringFromBytes(XptUtils.subArray(data, position, 8));
		position += 8;

		formatFieldLength = getShort(XptUtils.subArray(data, position, 2));
		position += 2;

		formatNumberOfDecimals = getShort(XptUtils.subArray(data, position, 2));
		position += 2;

		formatJustification = getShort(XptUtils.subArray(data, position, 2));
		position += 2;

		// Need to progress past the blank space
		position += 2;

		nameOfInputFormat = XptUtils.getStringFromBytes(XptUtils.subArray(data, position, 8));
		position += 8;

		informatLengthAttribute = getShort(XptUtils.subArray(data, position, 2));
		position += 2;

		informatNumberOfDecimals = getShort(XptUtils.subArray(data, position, 2));
		position += 2;

		positionOfValueInObservation = (long) XptUtils.getIEEfromIBM(XptUtils.subArray(data, position, 8));
		position += 4;

	}

	/**
	 * Identifies if the field is numeric
	 * 
	 * @return
	 */
	public boolean isNumeric() {
		if (!(ntype != 1 || ntype != 2))
			throw new UncheckedExecutionException(new IOException("Allowed types: 1, 2 but read " + ntype));

		return ntype == 1;
	}

	private short getShort(byte[] bytes) {
		ByteBuffer bb = ByteBuffer.allocate(2);
		bb.order(ByteOrder.BIG_ENDIAN);
		bb.put(bytes);
		return bb.getShort(0);
	}

	// @Override
	// public String toString()
	// {
	// return "FieldDetails [ntype=" + ntype + ", length=" + length + ",
	// varnum="
	// + varnum + ", nameOfVariable=" + nameOfVariable + ", label=" + label + ",
	// format=" + format
	// + ", formatFieldLength=" + formatFieldLength + ",
	// formatNumberOfDecimals=" + formatNumberOfDecimals
	// + ", formatJustification=" + formatJustification + ", nameOfInputFormat="
	// + nameOfInputFormat
	// + ", informatLengthAttribute=" + informatLengthAttribute + ",
	// informatNumberOfDecimals="
	// + informatNumberOfDecimals + ", positionOfValueInObservation=" +
	// positionOfValueInObservation + "]";
	// }

	/**
	 * Get the length of the variable
	 * 
	 * @return
	 */
	public int getLength() {
		return length;
	}

	/**
	 * Get the name of the variable
	 * 
	 * @return
	 */
	public String getName() {
		return nameOfVariable;
	}

	/**
	 * Get the variable number (index from 1)
	 * 
	 * @return
	 */
	public int getVarNum() {
		return varnum;
	}

	/**
	 * Get the label of the field
	 * 
	 * @return
	 */
	public String getLabel() {
		return label;
	}

	@Override
	public String toString() {
		return "FieldDetails [length=" + length + ", varnum=" + varnum + ", nameOfVariable=" + nameOfVariable
				+ ", label=" + label + ", isNumeric()=" + isNumeric() + "]";
	}

}
