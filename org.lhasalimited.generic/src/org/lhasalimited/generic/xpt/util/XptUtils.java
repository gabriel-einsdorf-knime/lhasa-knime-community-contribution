/**
 * Copyright (C) 2017  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.xpt.util;

import java.io.File;
import java.nio.ByteBuffer;

/**
 * A collection of methods to support processing of XPT files
 * 
 * @author Samuel
 *
 */
public class XptUtils {

	/**
	 * Concatenate two byte arrays
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static byte[] concat(byte[] a, byte[] b) {
		int aLen = a.length;
		int bLen = b.length;
		byte[] c = new byte[aLen + bLen];
		System.arraycopy(a, 0, c, 0, aLen);
		System.arraycopy(b, 0, c, aLen, bLen);
		return c;
	}

	/**
	 * Get a reader for the given file
	 * 
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public static XptFileReader getReaderForFile(File file) throws Exception {
		return new XptFileReader(file);
	}

	/**
	 * Get the string encoded in the byte[]
	 * 
	 * @param bytes
	 * @return
	 */
	public static String getStringFromBytes(byte[] bytes) {
		return new String(bytes).trim();
	}

	/**
	 * Create a subet of a byte[]
	 * 
	 * @param data
	 *            The full array
	 * @param start
	 *            the start index
	 * @param length
	 *            The number of elements to be copied
	 * @return
	 */
	public static byte[] subArray(byte[] data, int start, int length) {
		byte[] sub = new byte[length];
		System.arraycopy(data, start, sub, 0, length);

		return sub;
	}

	/**
	 * Convert the IBM number (stored in the bytes) to an IEE representation and
	 * return as a double
	 * 
	 * @param bytes
	 * @return
	 */
	public static double getIEEfromIBM(byte[] bytes) {
		Double field = null;

		ByteBuffer observation = ByteBuffer.wrap(bytes);

		int adjust = 8 - observation.capacity();

		if (adjust > 0) {
			ByteBuffer tempBuffer = observation;
			observation = ByteBuffer.allocate(8);

			for (int j = 0; j < tempBuffer.capacity(); ++j) {
				observation.put(tempBuffer.get());
			}

			tempBuffer.rewind();

			for (int j = 0; j < adjust; ++j) {
				observation.put((byte) 0);
			}
		}

		// Rewind the buffer, just in case we made modifications
		observation.rewind();

		boolean hasValue = false;
		byte[] dataBytes = observation.array();

		for (int j = 1; j < dataBytes.length; ++j) {
			if (dataBytes[j] != 0) {
				hasValue = true;
				break;
			}
		}

		if (hasValue == false) {
			if (dataBytes[0] != 0x5F && dataBytes[0] != 0x2E && (dataBytes[0] < 0x41 || dataBytes[0] > 0x5A)) {
				hasValue = dataBytes[0] != 0;
			}
		}

		if (hasValue) {
			long xport1 = observation.getInt() & 0x00000000FFFFFFFFL;
			long xport2 = observation.getInt() & 0x00000000FFFFFFFFL;

			observation.rewind();

			byte exponent = observation.get();
			long ieee1 = xport1 & 0x00FFFFFF;
			long ieee2 = xport2;
			short shift;
			long nib = (int) xport1;

			if ((nib & 0x00800000) != 0) {
				shift = 3;
			} else if ((nib & 0x00400000) != 0) {
				shift = 2;
			} else if ((nib & 0x00200000) != 0) {
				shift = 1;
			} else {
				shift = 0;
			}

			if (shift > 0) {
				ieee1 >>= shift;
				ieee2 = (xport2 >>> shift) | ((xport1 & 0x00000007) << (29 + (3 - shift)));
			}

			ieee1 &= 0xFFEFFFFF;
			ieee1 |= (((((long) (exponent & 0x7F) - 65) << 2) + shift + 1023) << 20) | (xport1 & 0x80000000);

			int ieee1conv = (int) ieee1;
			int ieee2conv = (int) ieee2;
			observation.rewind();
			observation.putInt(ieee1conv);
			observation.putInt(ieee2conv);
			observation.rewind();

			field = observation.getDouble();

		} else {
			// The field doesn't have data in it
			if (dataBytes[0] != 0) {
				field = Double.NaN;
			} else {
				field = 0d;
			}
		}

		return field;
	}

}
