/**
 * Copyright (C) 2017  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.xpt.util;

import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Optional;

/**
 * Reads a single XPT file. Fields can be accessed via
 * {@link #getFieldDetails()} and then rows can be iteratively read using
 * {@link #readDataEntry()}. Conversion is done for IBM floating point numbers
 * to IEEE floating point numbers, some precision is lost.
 * 
 * @author Samuel
 *
 */
public class XptFileReader implements Closeable, Iterator<XptDataRow> {
	/** the number of bytes per entry / line in the file */
	protected static int LINE_LENGTH = 80;
	/** the number of bytes used to store the field records */
	protected static int HEADER_RECORD_LENGTH = 140;

	protected File file;
	protected FileInputStream reader;
	protected long length;
	protected long readLength;

	protected String version;
	protected String os;
	protected String created;
	protected String modified;
	protected String[] headers;
	private XptFields fields;
	private String datasetName;

	/**
	 * Create a new XPT file reader with the given file. Initial reading will be
	 * performed to read the header records. <br>
	 * </br>
	 * The file will be read to the point {@link #readDataEntry()} can be called
	 * sequentially.
	 * 
	 * @param file
	 * @throws Exception
	 */
	public XptFileReader(File file) throws Exception {
		this.file = file;
		length = file.length();
		reader = new FileInputStream(file);

		readHeader();
		readHeaderRecord();
	}

	/**
	 * Get the name of the dataset
	 * 
	 * @return
	 */
	public String getDataSetName() {
		return datasetName;
	}

	/**
	 * Increment the counted bytes
	 * 
	 * @param increment
	 */
	private void incrementReadByteCount(long increment) {
		readLength += increment;
	}

	/**
	 * Get the progress of the file read
	 * 
	 * @return
	 */
	public double getProgress() {
		return readLength / (double) length;
	}

	/**
	 * Find the number of bytes to be read per data row
	 * 
	 * @return
	 */
	private int dataRowByteCount() {

		return fields.getTotalBytes();
		// bytes = (int) Math.ceil((double) bytes / LINE_LENGTH);

		// return bytes * LINE_LENGTH;
	}

	/**
	 * Read the next row. Returns an empty optional if no rows left. This
	 * progresses the stream and will continue to read until the end of the file
	 * or an error is thrown. <br>
	 * </br>
	 * Check {@link Optional#isPresent()} to determine the end of the stream
	 * 
	 * @return
	 * @throws IOException
	 *             will be thrown if there is an error reading the data. The end
	 *             of the file will be indicated by an empty {@link Optional}
	 */
	public Optional<XptDataRow> readDataEntry() throws IOException {
		try {
			byte[] bytes = read(dataRowByteCount());

			return Optional.of(new XptDataRow(getFieldDetails(), bytes));
		} catch (EOFException e) {
			// e.printStackTrace();
			return Optional.empty();
		}
	}

	/**
	 * Read the First 3 header lines. A rough check on format is done. This
	 * method sets the os, version, created and modified variables.
	 * 
	 * @throws IOException
	 */
	private void readHeader() throws IOException {
		// Read line 1

		String lineOne = getText();

		if (!lineOne.contains("HEADER RECORD*******LIBRARY HEADER RECORD!!!!!!!"))
			throw new IOException("XPORT format unexpected");

		// System.out.println(lineOne);

		// Read line 2 : version, os, _, created
		String lineTwo = getText();
		version = lineTwo.substring(23, 31).trim();
		os = lineTwo.substring(32, 41).trim();
		created = lineTwo.substring(64, 80);

		// System.out.println(lineTwo);

		// Read line 3
		String line3 = getText();

		modified = line3.trim();

		// System.out.println(line3);
	}

	/**
	 * Read a specific number of bytes
	 * 
	 * @param length
	 * @return
	 * @throws IOException
	 */
	private byte[] read(int length) throws IOException {
		byte[] line = new byte[length];

		int bytes = reader.read(line);

		incrementReadByteCount(bytes);

		if (bytes != length) {
			if (readLength == this.length) {
				throw new EOFException("No more data to read");
			} else {
				throw new IOException("Incorrect number of bytes read. Requested " + length + " read " + bytes);
			}

		}

		return line;
	}

	/**
	 * convert the byte array to a String
	 * 
	 * @param array
	 * @return
	 */
	private String byteArrayAsString(byte[] array) {
		return new String(array, StandardCharsets.UTF_8);
	}

	/**
	 * Extract the next 80 bytes as a String
	 * 
	 * @return
	 * @throws IOException
	 */
	private String getText() throws IOException {
		return byteArrayAsString(read(LINE_LENGTH));
	}

	/**
	 * Read header and populate fields
	 * 
	 * @throws Exception
	 */
	private void readHeaderRecord() throws Exception {
		// Parse over the first two lines
		String headRecord1 = getText(); // Member header record
		String headRecord2 = getText(); // Descriptor header record
		String headRecord3 = getText(); // Member header data

		String line = readNextLine(); // NAMESTR HEADER RECORD
		datasetName = line.substring(32, 72).trim();
		// System.out.println(line);
		line = readNextLine(); // NAMESTR HEADER RECORD
		// System.out.println(line);

		int variables = Integer.valueOf(line.substring(54, 58));

		byte[] header = read(HEADER_RECORD_LENGTH);
		for (int i = 1; i < variables; i++) {
			byte[] record = read(HEADER_RECORD_LENGTH);
			header = XptUtils.concat(header, record);
		}

		// read filler bits
		String filler = null;
		int offset = (int) (Math.ceil(header.length / (double) LINE_LENGTH) * LINE_LENGTH) - header.length;
		if (offset != 0) {
			filler = byteArrayAsString(read(offset));
		}

		// Read remaining buffer

		String obsLine = getText();

		if (!obsLine.equals("HEADER RECORD*******OBS     HEADER RECORD!!!!!!!000000000000000000000000000000  "))
			throw new IOException("Incorrect position for OBS HEADER RECORD line");

		fields = new XptFields();

		double iterations = header.length / (double) HEADER_RECORD_LENGTH;

		for (int i = 0; i < (int) Math.floor(iterations); i++) {
			byte[] fieldBytes = new byte[HEADER_RECORD_LENGTH];

			System.arraycopy(header, i * HEADER_RECORD_LENGTH, fieldBytes, 0, HEADER_RECORD_LENGTH);
			fields.addField(new FieldDetails(fieldBytes), false);
		}
	}

	protected void readMemberHeaderRecord() throws IOException {
		String headRecord1 = getText();
		String headRecord2 = getText();
		System.out.println(headRecord1);
		System.out.println(headRecord2);
	}

	public String[] getHeaders() throws IOException {
		return headers;
	}

	/**
	 * Read a full line from the XPT file. A line is a full set of bytes as
	 * defined by {@link #LINE_LENGTH}
	 * 
	 * @return
	 * @throws IOException
	 */
	private String readNextLine() throws IOException {
		return getText();
	}

	/**
	 * Specifies the version of SAS(r) System under which the file was created
	 * 
	 * @return
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Specifies the operating system that creates the record
	 * 
	 * @return
	 */
	public String getOS() {
		return os;
	}

	/**
	 * Specifies the date and time created, formatted as ddMMMyy:hh:mm:ss. Note
	 * that only a 2-digit year appears, be prepared to deal with dates in the
	 * 1900s and the 2000s
	 * 
	 * @return
	 */
	public String getCreated() {
		return created;
	}

	/**
	 * In this record, the string is the datetime modified. Most often, the
	 * datetime created and datetime modified will always be the same. Pad with
	 * ASCII blanks to 80 bytes. Note that only a 2-digit year appears. If any
	 * program needs to read in this 2-digit year, be prepared to deal with
	 * dates in the 1900s or the 2000s.
	 * 
	 * @return
	 */
	public String getModified() {
		return modified;
	}

	/**
	 * Get the number of fields in the file
	 * 
	 * @return
	 */
	public int getNumFields() {
		return fields.getNumFields();
	}

	/**
	 * Get the field details
	 * 
	 * @return
	 */
	public XptFields getFieldDetails() {
		return fields;
	}

	@Override
	public void close() throws IOException {
		reader.close();

	}

	/**
	 * Checks that there are sufficient bytes left in the file to read another
	 * row
	 * 
	 * @return
	 */
	@Override
	public boolean hasNext() {
		long bytesRemaining = length - readLength;

		long required = fields.getTotalBytes();

		boolean next = (bytesRemaining - required >= 0);

		if (!next) {
			try {
				read((int) bytesRemaining);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return next;
	}

	/**
	 * Read the next {@link XptDataRow} from the file
	 * 
	 * @return
	 */
	@Override
	public XptDataRow next() {
		XptDataRow row = null;
		try {
			row = readDataEntry().get();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return row;
	}

}
