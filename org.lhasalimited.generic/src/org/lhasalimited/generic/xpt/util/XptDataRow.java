/**
 * Copyright (C) 2017  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.xpt.util;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

/**
 * This class allows the extraction of specific data points from an XPT row by
 * querying using {@link #getDataForField(int)}
 * 
 * @author Samuel
 *
 */
public class XptDataRow {

	public static int PADDING = 0;
	private XptFields fields;
	private byte[] data;
	public final static byte BLANK = (byte) 32;

	/**
	 * Create a new XptDataRow with the given byte data representing all bytes
	 * for the entry.
	 * 
	 * @param fields
	 *            The fields contained in the XPT file
	 * @param data
	 *            The data row in the XPT
	 */
	public XptDataRow(XptFields fields, byte[] data) {
		this.data = data;
		this.fields = fields;
	}

	/**
	 * Get the data stored in the field
	 * 
	 * @param index
	 * @return
	 */
	public Optional<Object> getDataForField(int index) {
		int start = PADDING + getStart(index);
		// int end = start + fields.get(index).length;

		byte[] array = XptUtils.subArray(data, start, fields.getFieldByIndex(index).getLength());

		Optional<Object> output;

		if (isBlank(array)) {
			output = Optional.empty();
		} else {
			Object value;
			if (fields.getFieldByIndex(index).isNumeric()) {
				// value = "numeric not parsed";
				value = getNumber(array);
			} else {
				value = getString(array, true);
			}

			output = Optional.of(value);
		}

		return output;
	}

	private boolean isBlank(byte[] array) {
		int blankCount = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] == BLANK)
				blankCount++;
		}

		return blankCount == array.length;
	}

	/**
	 * Get the start byte for the given field.
	 * 
	 * @param index
	 * @return
	 */
	private int getStart(int index) {
		int start = 0;

		for (int i = 0; i < index; i++) {
			start += fields.getFieldByIndex(i).getLength();
		}

		return start;
	}

	/**
	 * Get the number of columns
	 * 
	 * @return
	 */
	public int getNumColumns() {
		return fields.getNumFields();
	}

	/**
	 * Convert the byte array representing a non numeric results to a String
	 * 
	 * @param subArray
	 * @param trim
	 * @return
	 */
	private String getString(byte[] subArray, boolean trim) {
		return new String(subArray, StandardCharsets.UTF_8).trim();
	}

	private Double getNumber(byte[] bytes) {

		return XptUtils.getIEEfromIBM(bytes);
	}

}
