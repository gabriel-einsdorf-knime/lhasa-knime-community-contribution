/**
 * Copyright (C) 2017  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.xpt.reader;

import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleContext;

/**
 * This is the eclipse bundle activator. Note: KNIME node developers probably
 * won't have to do anything in here, as this class is only needed by the
 * eclipse platform/plugin mechanism. If you want to move/rename this file, make
 * sure to change the plugin.xml file in the project root directory accordingly.
 *
 * @author Samuel, Lhasa Limited
 */
public class XptReaderNodePlugin extends Plugin {
	// The shared instance.
	private static XptReaderNodePlugin plugin;

	/**
	 * The constructor.
	 */
	public XptReaderNodePlugin() {
		super();
		plugin = this;
	}

	/**
	 * This method is called upon plug-in activation.
	 * 
	 * @param context
	 *            The OSGI bundle context
	 * @throws Exception
	 *             If this plugin could not be started
	 */
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);

	}

	/**
	 * This method is called when the plug-in is stopped.
	 * 
	 * @param context
	 *            The OSGI bundle context
	 * @throws Exception
	 *             If this plugin could not be stopped
	 */
	@Override
	public void stop(final BundleContext context) throws Exception {
		super.stop(context);
		plugin = null;
	}

	/**
	 * Returns the shared instance.
	 * 
	 * @return Singleton instance of the Plugin
	 */
	public static XptReaderNodePlugin getDefault() {
		return plugin;
	}

}
