/**
 * Copyright (C) 2017  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.xpt.reader;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.DataTableSpecCreator;
import org.knime.core.data.MissingCell;
import org.knime.core.data.RowKey;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.DoubleCell.DoubleCellFactory;
import org.knime.core.data.def.IntCell.IntCellFactory;
import org.knime.core.data.def.StringCell.StringCellFactory;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeCreationContext;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.util.FileUtil;
import org.lhasalimited.generic.xpt.util.FieldDetails;
import org.lhasalimited.generic.xpt.util.XptDataRow;
import org.lhasalimited.generic.xpt.util.XptFields;
import org.lhasalimited.generic.xpt.util.XptFileReader;

/**
 * This is the model implementation of XptReader. Read XPT (SAS XPORT) file
 * format
 *
 * @author Samuel, Lhasa Limited
 */
public class XptReaderNodeModel extends NodeModel {
	public static final String CONFIG_XPT_FILE = "cfgXptFile";
	public SettingsModelString settingXptFile = new SettingsModelString(CONFIG_XPT_FILE, "");

	public static final String CONFIG_ROW_LIMIT = "cfgRowLimit";
	public SettingsModelInteger settingRowLimit = new SettingsModelInteger(CONFIG_ROW_LIMIT, 100000);

	public static final String CONFIG_ENABLE_LIMIT = "cfgEnableLimit";
	public SettingsModelBoolean settingEnableLimit = new SettingsModelBoolean(CONFIG_ENABLE_LIMIT, false);

	public static final String CONFIG_MISSING_AS_BLANK = "cfgEnableMissingAsBlank";
	public SettingsModelBoolean settingAsBlank = new SettingsModelBoolean(CONFIG_MISSING_AS_BLANK, false);

	private XptFileReader reader;

	/**
	 * Constructor for the node model.
	 */
	protected XptReaderNodeModel() {
		super(0, 3);
		settingRowLimit.setEnabled(settingEnableLimit.getBooleanValue());
	}

	public XptReaderNodeModel(NodeCreationContext context) {
		this();
		settingXptFile.setStringValue(context.getUrl().toString());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected BufferedDataTable[] execute(final BufferedDataTable[] inData, final ExecutionContext exec)
			throws Exception {
		BufferedDataContainer data = exec.createDataContainer(getSpec(reader.getFieldDetails()));
		BufferedDataContainer details = exec.createDataContainer(getSpecDetails());
		BufferedDataContainer overview = exec.createDataContainer(getOverviewSpec());

		createOverviewTable(overview, reader);
		createFieldsTable(details, reader);

		int track = 0;
		while (reader.hasNext()) {
			XptDataRow current = reader.next();

			DataCell cells[] = new DataCell[current.getNumColumns()];

			for (int i = 0; i < reader.getNumFields(); i++) {
				Optional<Object> value = current.getDataForField(i);

				if (value.isPresent()) {
					Object str = value.get();

					if (reader.getFieldDetails().getFieldByIndex(i).isNumeric()) {
						cells[i] = DoubleCellFactory.create((Double) str);
					} else {
						cells[i] = StringCellFactory.create((String) str);
					}
				} else {
					if (settingAsBlank.getBooleanValue()) {
						cells[i] = StringCellFactory.create("");
					} else {
						cells[i] = new MissingCell("empty");
					}

				}

			}

			data.addRowToTable(new DefaultRow(new RowKey("Row" + data.size()), cells));

			exec.setProgress(reader.getProgress(), "Processed " + data.size() + " rows");

			exec.checkCanceled();

			track++;

			if (settingRowLimit.isEnabled() && track >= settingRowLimit.getIntValue())
				break;

		}

		data.close();
		details.close();
		overview.close();
		reader.close();

		return new BufferedDataTable[] { data.getTable(), details.getTable(), overview.getTable() };
	}

	private void createOverviewTable(BufferedDataContainer overview, XptFileReader reader2) {
		overview.addRowToTable(new DefaultRow(new RowKey("Row" + overview.size()), new DataCell[] {
				StringCellFactory.create("Version"), StringCellFactory.create(reader2.getVersion()) }));

		overview.addRowToTable(new DefaultRow(new RowKey("Row" + overview.size()), new DataCell[] {
				StringCellFactory.create("Created"), StringCellFactory.create(reader2.getCreated()) }));

		overview.addRowToTable(new DefaultRow(new RowKey("Row" + overview.size()), new DataCell[] {
				StringCellFactory.create("Modified"), StringCellFactory.create(reader2.getModified()) }));

		overview.addRowToTable(new DefaultRow(new RowKey("Row" + overview.size()),
				new DataCell[] { StringCellFactory.create("OS"), StringCellFactory.create(reader2.getOS()) }));

		overview.addRowToTable(new DefaultRow(new RowKey("Row" + overview.size()), new DataCell[] {
				StringCellFactory.create("Dataset name"), StringCellFactory.create(reader2.getDataSetName()) }));

	}

	private void createFieldsTable(BufferedDataContainer details, XptFileReader reader2) {
		for (FieldDetails field : reader.getFieldDetails()) {
			details.addRowToTable(new DefaultRow(new RowKey("Row" + details.size()),
					new DataCell[] { IntCellFactory.create(field.getVarNum()),
							StringCellFactory.create(field.getName()),
							StringCellFactory.create(field.isNumeric() ? "NUMERIC" : "CHAR"),
							StringCellFactory.create(field.getLabel()), IntCellFactory.create(field.getLength()) }));
		}

	}

	private DataTableSpec getOverviewSpec() {
		DataTableSpecCreator creator = new DataTableSpecCreator();
		creator.addColumns(new DataColumnSpecCreator("Key", StringCellFactory.TYPE).createSpec());
		creator.addColumns(new DataColumnSpecCreator("Value", StringCellFactory.TYPE).createSpec());

		return creator.createSpec();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void reset() {
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DataTableSpec[] configure(final DataTableSpec[] inSpecs) throws InvalidSettingsException {

		if (settingXptFile.getStringValue().equals(""))
			throw new InvalidSettingsException("No file specified");

		File file = getFile(settingXptFile.getStringValue());

		if (!file.exists())
			throw new InvalidSettingsException("File does not exist: " + file.toString());

		try {
			reader = new XptFileReader(file);
		} catch (Exception e) {
			e.printStackTrace();
			throw new InvalidSettingsException(e);
		}

		return new DataTableSpec[] { getSpec(reader.getFieldDetails()), getSpecDetails(), getOverviewSpec() };
	}

	/**
	 * Get the column specification for the data table
	 * 
	 * @param fieldDetails
	 * @return
	 */
	private DataTableSpec getSpec(XptFields fieldDetails) {
		DataTableSpecCreator creator = new DataTableSpecCreator();

		for (FieldDetails details : fieldDetails)
			creator.addColumns(new DataColumnSpecCreator(details.getName(),
					details.isNumeric() ? DoubleCellFactory.TYPE : StringCellFactory.TYPE).createSpec());

		return creator.createSpec();
	}

	/**
	 * Get the table specification for the details table
	 * 
	 * @return
	 */
	private DataTableSpec getSpecDetails() {
		DataTableSpecCreator creator = new DataTableSpecCreator();

		creator.addColumns(new DataColumnSpecCreator("VARNUM", IntCellFactory.TYPE).createSpec());
		creator.addColumns(new DataColumnSpecCreator("Name", StringCellFactory.TYPE).createSpec());
		creator.addColumns(new DataColumnSpecCreator("Type", StringCellFactory.TYPE).createSpec());
		creator.addColumns(new DataColumnSpecCreator("Label", StringCellFactory.TYPE).createSpec());
		creator.addColumns(new DataColumnSpecCreator("Value", IntCellFactory.TYPE).createSpec());

		return creator.createSpec();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveSettingsTo(final NodeSettingsWO settings) {
		settingXptFile.saveSettingsTo(settings);
		settingEnableLimit.saveSettingsTo(settings);
		settingRowLimit.saveSettingsTo(settings);
		settingAsBlank.saveSettingsTo(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadValidatedSettingsFrom(final NodeSettingsRO settings) throws InvalidSettingsException {
		settingXptFile.loadSettingsFrom(settings);
		settingEnableLimit.loadSettingsFrom(settings);
		settingRowLimit.loadSettingsFrom(settings);
		settingAsBlank.loadSettingsFrom(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validateSettings(final NodeSettingsRO settings) throws InvalidSettingsException {
		settingXptFile.validateSettings(settings);
		settingEnableLimit.validateSettings(settings);
		settingRowLimit.validateSettings(settings);
		settingAsBlank.validateSettings(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadInternals(final File internDir, final ExecutionMonitor exec)
			throws IOException, CanceledExecutionException {
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveInternals(final File internDir, final ExecutionMonitor exec)
			throws IOException, CanceledExecutionException {
		// TODO: generated method stub
	}

	private File getFile(String str) {
		File file;
		try {
			file = FileUtil.getFileFromURL(new URL(str));
		} catch (MalformedURLException e) {
			file = new File(str);
		}

		return file;
	}

}
