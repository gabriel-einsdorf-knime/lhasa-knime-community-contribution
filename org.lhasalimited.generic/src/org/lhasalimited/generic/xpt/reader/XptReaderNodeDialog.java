/**
 * Copyright (C) 2017  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.xpt.reader;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponent;
import org.knime.core.node.defaultnodesettings.DialogComponentBoolean;
import org.knime.core.node.defaultnodesettings.DialogComponentFileChooser;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelString;

/**
 * <code>NodeDialog</code> for the "XptReader" Node. Read XPT (SAS XPORT) file
 * format
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more
 * complex dialog please derive directly from
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Samuel, Lhasa Limited
 */
public class XptReaderNodeDialog extends DefaultNodeSettingsPane {

	public static String XPT_HISTORY = "XPT_HISTORY";

	/**
	 * New pane for configuring the XptReader node.
	 */
	protected XptReaderNodeDialog() {
		SettingsModelString settingXptFile = new SettingsModelString(XptReaderNodeModel.CONFIG_XPT_FILE, "");
		SettingsModelBoolean cutoffEnabled = new SettingsModelBoolean(XptReaderNodeModel.CONFIG_ENABLE_LIMIT, false);
		SettingsModelInteger cutoff = new SettingsModelInteger(XptReaderNodeModel.CONFIG_ROW_LIMIT, 100000);
		SettingsModelBoolean handleMissingAsBlank = new SettingsModelBoolean(XptReaderNodeModel.CONFIG_MISSING_AS_BLANK,
				false);

		addDialogComponent(new DialogComponentFileChooser(settingXptFile, XPT_HISTORY, ".xpt"));

		setHorizontalPlacement(true);

		cutoffEnabled.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				cutoff.setEnabled(cutoffEnabled.getBooleanValue());
			}
		});
		cutoffEnabled.setEnabled(cutoffEnabled.getBooleanValue());

		addDialogComponent(new DialogComponentBoolean(cutoffEnabled, "Limit rows"));

		DialogComponent limitCounter = new DialogComponentNumber(cutoff, "Import limit", 100);
		addDialogComponent(limitCounter);

		addDialogComponent(new DialogComponentBoolean(handleMissingAsBlank, "Missing as blank"));

	}
}
