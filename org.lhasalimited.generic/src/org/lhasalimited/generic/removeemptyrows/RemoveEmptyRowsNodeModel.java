package org.lhasalimited.generic.removeemptyrows;

import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.container.CloseableRowIterator;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.port.PortObjectSpec;
import org.knime.core.node.streamable.InputPortRole;
import org.knime.core.node.streamable.OutputPortRole;
import org.knime.core.node.streamable.PartitionInfo;
import org.knime.core.node.streamable.PortInput;
import org.knime.core.node.streamable.PortOutput;
import org.knime.core.node.streamable.RowInput;
import org.knime.core.node.streamable.RowOutput;
import org.knime.core.node.streamable.StreamableOperator;
import org.knime.core.node.streamable.StreamableOperatorInternals;
import org.lhasalimited.generic.node.config.LocalSettingsNodeModel;

/**
 * This is the model implementation of RemoveEmptyRows. Removes empty rows from
 * the input table
 *
 * @author Samuel, Lhasa Limited
 */
public class RemoveEmptyRowsNodeModel extends LocalSettingsNodeModel<RemoveEmptyRowsSettings>
{

	/**
	 * Constructor for the node model.
	 */
	protected RemoveEmptyRowsNodeModel()
	{
		super(1, 1);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected BufferedDataTable[] execute(final BufferedDataTable[] inData, final ExecutionContext exec)
			throws Exception
	{

		BufferedDataContainer container = exec.createDataContainer(inData[0].getDataTableSpec());
		
		CloseableRowIterator iterator = inData[0].iterator();
		
		while(iterator.hasNext())
		{
			DataRow row = iterator.next();
			
			if(!rowIsEmpty(row))
			{
				container.addRowToTable(row);
			}
		}
		
		container.close();
		
		return new BufferedDataTable[] {container.getTable()};
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void reset()
	{
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DataTableSpec[] configure(final DataTableSpec[] inSpecs) throws InvalidSettingsException
	{
		return inSpecs;
	}

//	/**
//	 * {@inheritDoc}
//	 */
//	@Override
//	protected void saveSettingsTo(final NodeSettingsWO settings)
//	{
//		// TODO: generated method stub
//	}
//
//	/**
//	 * {@inheritDoc}
//	 */
//	@Override
//	protected void loadValidatedSettingsFrom(final NodeSettingsRO settings) throws InvalidSettingsException
//	{
//		// TODO: generated method stub
//	}
//
//	/**
//	 * {@inheritDoc}
//	 */
//	@Override
//	protected void validateSettings(final NodeSettingsRO settings) throws InvalidSettingsException
//	{
//		// TODO: generated method stub
//	}
//
//	/**
//	 * {@inheritDoc}
//	 */
//	@Override
//	protected void loadInternals(final File internDir, final ExecutionMonitor exec)
//			throws IOException, CanceledExecutionException
//	{
//		// TODO: generated method stub
//	}
//
//	/**
//	 * {@inheritDoc}
//	 */
//	@Override
//	protected void saveInternals(final File internDir, final ExecutionMonitor exec)
//			throws IOException, CanceledExecutionException
//	{
//		// TODO: generated method stub
//	}
	
	/** {@inheritDoc} */
	@Override
	public InputPortRole[] getInputPortRoles()
	{
		return new InputPortRole[] { InputPortRole.NONDISTRIBUTED_STREAMABLE };
	}

	/** {@inheritDoc} */
	@Override
	public OutputPortRole[] getOutputPortRoles()
	{
		return new OutputPortRole[] { OutputPortRole.NONDISTRIBUTED };
	}

	/** {@inheritDoc} */
	@Override
	public StreamableOperator createStreamableOperator(final PartitionInfo partitionInfo,
			final PortObjectSpec[] inSpecs) throws InvalidSettingsException
	{
		return new StreamableOperator()
		{

			@Override
			public StreamableOperatorInternals saveInternals()
			{
				return null;
			}

			@Override
			public void runFinal(final PortInput[] inputs, final PortOutput[] outputs, final ExecutionContext ctx)
					throws Exception
			{
				RowInput in = (RowInput) inputs[0];
				RowOutput out = (RowOutput) outputs[0];

				RemoveEmptyRowsNodeModel.this.execute(in, out, ctx);
			}
		};
	}

	protected void execute(RowInput inData, RowOutput outData, ExecutionContext ctx) throws InterruptedException
	{
		DataRow row;
		while ((row = inData.poll()) != null)
		{
			if(!rowIsEmpty(row))
				outData.push(row);
		}
		inData.close();
		outData.close();
	}
	
	/**
	 * 
	 * @param row
	 * @return
	 */
	private boolean rowIsEmpty(DataRow row)
	{
		boolean empty = true;
		
		if(localSettings.isRemoveNoColumns()) 
		{
			if(rowHasNoColumns(row))
			{
				empty = true;
			} else
			{
				if(localSettings.isRemoveAllMissing()) 
				{
					if(hasNonMissingCell(row))
						empty = false;
				} 
				else
				{
					// We have columns and we aren't checking data
					empty = false;
				}
			}
		} else
		{
			if(rowHasNoColumns(row))
			{
				// If we don't have any columns there's no data
				// to check. User has specified that no columns
				// does not mean empty
				empty = false;
			} else
			{
				if(localSettings.isRemoveAllMissing()) 
				{
					if(hasNonMissingCell(row))
						empty = false;
				}
				else
				{
					// This config doesn't really make sense to choose
					// but is possible
					empty = false;
				}

			}
		}
				
		return empty;
	}
	
	/**
	 * Identifier if there are no cells in the row
	 * @param row
	 * @return
	 */
	private boolean rowHasNoColumns(DataRow row)
	{
		return row.getNumCells() == 0;
	}
	
	private boolean hasNonMissingCell(DataRow row)
	{
		boolean empty = true;
		
		for(int i = 0; i < row.getNumCells() && empty; i++)
		{
			if(!row.getCell(i).isMissing())
				empty = false;
		}
		
		return !empty;
	}

}
