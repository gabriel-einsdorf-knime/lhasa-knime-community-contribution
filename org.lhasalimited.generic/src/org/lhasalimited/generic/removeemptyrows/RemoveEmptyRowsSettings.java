package org.lhasalimited.generic.removeemptyrows;

import org.knime.core.node.defaultnodesettings.DialogComponent;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.lhasalimited.generic.node.config.NodeSettingCollection;

public class RemoveEmptyRowsSettings extends NodeSettingCollection 
{

	public static final String CONFIG_REMOVE_ALL_MISSING = "cfgRemoveAllMissing";
	public static final String CONFIG_REMOVE_NO_COLUMNS = "cfgRemoveNoColumns";
	
	
	@Override
	protected void addSettings() 
	{
		addSetting(CONFIG_REMOVE_ALL_MISSING, new SettingsModelBoolean(CONFIG_REMOVE_ALL_MISSING, true));
		addSetting(CONFIG_REMOVE_NO_COLUMNS, new SettingsModelBoolean(CONFIG_REMOVE_NO_COLUMNS, true));
	}
	
	public boolean isRemoveAllMissing()
	{
		return getBooleanValue(CONFIG_REMOVE_ALL_MISSING);
	}
	
	public boolean isRemoveNoColumns()
	{
		return getBooleanValue(CONFIG_REMOVE_NO_COLUMNS);
	}

	
	public DialogComponent createDialogComponentAllMissing()
	{
		return getBooleanComponent(CONFIG_REMOVE_ALL_MISSING, "Remove entirely Missing Cell rows");
	}
	
	public DialogComponent createDialogComponentNoColumns()
	{
		return getBooleanComponent(CONFIG_REMOVE_NO_COLUMNS, "Remove row where no columns are present");
	}
}
