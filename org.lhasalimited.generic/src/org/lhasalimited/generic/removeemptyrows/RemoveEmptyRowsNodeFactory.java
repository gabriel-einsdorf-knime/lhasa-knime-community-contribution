package org.lhasalimited.generic.removeemptyrows;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "RemoveEmptyRows" Node.
 * Removes empty rows from the input table
 *
 * @author Samuel, Lhasa Limited
 */
public class RemoveEmptyRowsNodeFactory 
        extends NodeFactory<RemoveEmptyRowsNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public RemoveEmptyRowsNodeModel createNodeModel() {
        return new RemoveEmptyRowsNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<RemoveEmptyRowsNodeModel> createNodeView(final int viewIndex,
            final RemoveEmptyRowsNodeModel nodeModel) {
        return new RemoveEmptyRowsNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new RemoveEmptyRowsNodeDialog();
    }

}

