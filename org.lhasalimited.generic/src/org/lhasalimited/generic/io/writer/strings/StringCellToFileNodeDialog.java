package org.lhasalimited.generic.io.writer.strings;

import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;

/**
 * <code>NodeDialog</code> for the "StringCellToFile" Node. Writes the contents
 * of a StringValue to a file
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more
 * complex dialog please derive directly from
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Samuel, Lhasa Limited
 */
public class StringCellToFileNodeDialog extends DefaultNodeSettingsPane
{

	/**
	 * New pane for configuring the StringCellToFile node.
	 */
	protected StringCellToFileNodeDialog()
	{
		
		
		StringCellToFileSettings settings = new StringCellToFileSettings();
		
		createNewGroup("File settings");
		addDialogComponent(settings.getDialogComponentDirectory());
		setHorizontalPlacement(true);
		addDialogComponent(settings.getDialogComponentExtension());
		addDialogComponent(settings.getDialogComponentOveride());
		
		createNewGroup("Instance settings");
		addDialogComponent(settings.getDialogComponentFilenameColumn());
		addDialogComponent(settings.getDialogComponentStringColumn());
	}
}
