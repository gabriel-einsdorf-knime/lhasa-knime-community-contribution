package org.lhasalimited.generic.io.writer.strings;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JFileChooser;

import org.knime.core.data.StringValue;
import org.knime.core.node.defaultnodesettings.DialogComponent;
import org.knime.core.node.defaultnodesettings.DialogComponentBoolean;
import org.knime.core.node.defaultnodesettings.DialogComponentFileChooser;
import org.knime.core.node.defaultnodesettings.DialogComponentString;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnName;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.util.FileUtil;
import org.lhasalimited.generic.node.config.NodeSettingCollection;

public class StringCellToFileSettings extends NodeSettingCollection {

	public static String CONFIG_STRING_COLUMN = "cfgStringColumn";
	public static String CONFIG_FILENAME_COLUMN = "cfgFileNameColumn";
	public static String CONFIG_EXTENSION = "cfgExtension";
	public static String CONFIG_DIRECTORY = "cfgDirectory";
	public static String CONFIG_OVERIDE = "cfgOveride";

	public static String CONFIG_STRING_CELL_TO_FILE_HISTORY = "CONFIG_STRING_CELL_TO_FILE_HISTORY";

	@Override
	protected void addSettings() {
		addSetting(CONFIG_DIRECTORY, new SettingsModelString(CONFIG_DIRECTORY, ""));
		addSetting(CONFIG_EXTENSION, new SettingsModelString(CONFIG_EXTENSION, ".txt"));
		addSetting(CONFIG_STRING_COLUMN, new SettingsModelColumnName(CONFIG_STRING_COLUMN, ""));
		addSetting(CONFIG_FILENAME_COLUMN, new SettingsModelColumnName(CONFIG_FILENAME_COLUMN, ""));
		addSetting(CONFIG_OVERIDE, new SettingsModelBoolean(CONFIG_OVERIDE, false));
	}

	/**
	 * Get the extension specified in the settings
	 * 
	 * @return
	 */
	public String getExtension() {
		return getSetting(CONFIG_EXTENSION, SettingsModelString.class).getStringValue();
	}

	/**
	 * Get the directory specified in the settings
	 * 
	 * @return
	 */
	public String getDirectory() {
		// TODO: handle final \ in String?
		return getSetting(CONFIG_DIRECTORY, SettingsModelString.class).getStringValue();
	}

	/**
	 * Returns a file object for the given file name whith the directory and
	 * extension specified in the settings.
	 * 
	 * @param filename
	 * @return
	 */
	public File getFile(String filename) {
		return getKnimeFile(getDirectory() + "/" + filename + getExtension());
	}

	private File getKnimeFile(String filename) {
		File file;
		try {
			file = FileUtil.getFileFromURL(new URL(filename));
		} catch (MalformedURLException e) {
			file = new File(filename);
		}

		return file;
	}

	/**
	 * Returns the column name for the column selected that stores the content
	 * to be written.
	 * 
	 * @return
	 */
	public String getStringColumnName() {
		return getColumnName(CONFIG_STRING_COLUMN);
	}

	/**
	 * Returns the column name of the column containing the value of the
	 * filename to be saved
	 * 
	 * @return
	 */
	public String getFilenameColumnName() {
		return getColumnName(CONFIG_FILENAME_COLUMN);
	}
	
	public boolean useRowID()
	{
		return getSetting(CONFIG_FILENAME_COLUMN, SettingsModelColumnName.class).useRowID();
	}

	/**
	 * Whether an existing file can be written over
	 * 
	 * @return
	 */
	public boolean getIsOveride() {
		return getSetting(CONFIG_OVERIDE, SettingsModelBoolean.class).getBooleanValue();
	}

	/**
	 * Returns a column selector linked to the string value setting
	 * 
	 * @return
	 */
	public DialogComponent getDialogComponentStringColumn() {
		return getDialogColumnNameSelection(CONFIG_STRING_COLUMN, "Value to write", 0, StringValue.class);
	}

	/**
	 * Returns a column selector linked to the filename setting
	 * 
	 * @return
	 */
	public DialogComponent getDialogComponentFilenameColumn() {
		return getDialogColumnNameSelection(CONFIG_FILENAME_COLUMN, "Filename", 0, StringValue.class);
	}

	/**
	 * Returns a dialog component for entering a string value
	 * 
	 * @return
	 */
	public DialogComponent getDialogComponentExtension() {
		return new DialogComponentString(getSetting(CONFIG_EXTENSION, SettingsModelString.class), "Extension");
	}

	/**
	 * Returns a dialog component for entering a string value
	 * 
	 * @return
	 */
	public DialogComponent getDialogComponentDirectory() {
		return new DialogComponentFileChooser(getSetting(CONFIG_DIRECTORY, SettingsModelString.class),
				CONFIG_STRING_CELL_TO_FILE_HISTORY, JFileChooser.OPEN_DIALOG, true);
	}

	public DialogComponent getDialogComponentOveride() {
		return new DialogComponentBoolean(getSetting(CONFIG_OVERIDE, SettingsModelBoolean.class), "Overide existing");
	}

	public File getDirectoryFile() {
		// TODO Auto-generated method stub
		return getKnimeFile(getDirectory());
	}

}
