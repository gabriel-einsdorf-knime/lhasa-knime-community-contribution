/**
* Copyright (C) 2015  Lhasa Limited
* 
* This program is free software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* These nodes extend the KNIME core which is free software developed
* by  KNIME Gmbh (http://www.knime.org/).
* 
*/
package org.lhasalimited.generic.htmltotable;

import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentBoolean;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnFilter;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnFilter2;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;
import org.knime.core.node.defaultnodesettings.DialogComponentString;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnFilter2;
import org.knime.core.node.defaultnodesettings.SettingsModelFilterString;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.lhasalimited.generic.htmltotable.util.TableToHtml;

/**
 * <code>NodeDialog</code> for the "TableToHtml" Node.
 * Convert a complete table to a single HTML cell
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Lhasa Limited
 * @deprecated
 */
public class TableToHtmlNodeDialog extends DefaultNodeSettingsPane 
{

    /**
     * New pane for configuring the TableToHtml node.
     */
    protected TableToHtmlNodeDialog() 
    {    	
    	
    	createNewGroup("Settings");
    	setHorizontalPlacement(true);
    	addDialogComponent(new DialogComponentNumber(new SettingsModelInteger(TableToHtmlNodeModel.CFG_NUM_ROWS, 1000), "Number of rows", 100));
    	
    	addDialogComponent(new DialogComponentBoolean(new SettingsModelBoolean(TableToHtmlNodeModel.CFG_INLCUDE_ROWID, true), "Include RowID"));
    	
    	DialogComponentBoolean formatDouble = new DialogComponentBoolean(new SettingsModelBoolean(TableToHtmlNodeModel.CFG_FORMAT_DOUBLE, true), "Format double");
    	addDialogComponent(formatDouble);
    	
    	DialogComponentString doubleFormat = new DialogComponentString(new SettingsModelString(TableToHtmlNodeModel.CFG_DOUBLE_FORMAT, "#.00"), "Double format");
    	addDialogComponent(doubleFormat);
    	
    	DialogComponentBoolean addBasicStyle = new DialogComponentBoolean(new SettingsModelBoolean(TableToHtmlNodeModel.CFG_ADD_BASIC_STYLE, false), "Add basic style");
    	addDialogComponent(addBasicStyle);
    		
    	
    	createNewGroup("Column selection:");
    	addDialogComponent(new DialogComponentColumnFilter(new SettingsModelFilterString(TableToHtmlNodeModel.CFG_COLUMN_SELECTION), 0, true, TableToHtml.APPROVED_TYPES));

//    	SettingsModelColumnFilter2 setting = new SettingsModelColumnFilter2(TableToHtmlNodeModel.CFG_COLUMN_SELECTION, TableToHtml.APPROVED_TYPES);
//    	addDialogComponent(new DialogComponentColumnFilter2(setting, 0));
    	
    }
}

