/**
* Copyright (C) 2015  Lhasa Limited
* 
* This program is free software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* These nodes extend the KNIME core which is free software developed
* by  KNIME Gmbh (http://www.knime.org/).
* 
*/
package org.lhasalimited.generic.htmltotable;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "TableToHtml" Node. Convert a complete table
 * to a single HTML cell
 * 
 * @author Lhasa Limited
 * @deprecated
 */
public class TableToHtmlNodeFactory extends NodeFactory<TableToHtmlNodeModel>
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TableToHtmlNodeModel createNodeModel()
	{
		return new TableToHtmlNodeModel();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getNrNodeViews()
	{
		return 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public NodeView<TableToHtmlNodeModel> createNodeView(final int viewIndex,
			final TableToHtmlNodeModel nodeModel)
	{
		return new TableToHtmlNodeView(nodeModel);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasDialog()
	{
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public NodeDialogPane createNodeDialogPane()
	{
		return new TableToHtmlNodeDialog();
	}

}
