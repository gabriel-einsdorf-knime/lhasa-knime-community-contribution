/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.multicolumnrowsplitter;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.knime.base.node.preproc.filter.row.RowFilterNodeDialogPane;
import org.knime.base.node.preproc.filter.row.rowfilter.AttrValueRowFilter;
import org.knime.base.node.preproc.filter.row.rowfilter.ColValFilterOldObsolete;
import org.knime.base.node.preproc.filter.row.rowfilter.IRowFilter;
import org.knime.base.node.preproc.filter.row.rowfilter.RowFilterFactory;
import org.knime.core.data.DataTableSpec;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.NotConfigurableException;

/**
 * This class is heavily based on {@link RowFilterNodeDialogPane} and has had
 * the inappropriate filter criteria panels removed
 * 
 * @author Samuel
 * 
 */
public class MultiColumnRowFilterDialogPane extends NodeDialogPane
{

	private JRadioButton m_colValInclRadio;

	private JRadioButton m_colValExclRadio;

	private ColumnFilterRowPanel m_colValPanel;

	private JPanel m_filterPanel;
	
//	private boolean m_matchAllValue;
//	
//	private JCheckBox m_matchAll;

	/**
	 * Creates a new panel for the row filter node dialog.
	 */
	public MultiColumnRowFilterDialogPane()
	{
		super();
		JPanel dlg = createDialogPanel();
		// the actual filter panel instantiations happen during loadSettings.
		addTab("Filter Criteria", dlg);
	}

	protected JPanel createDialogPanel()
	{

		JPanel result = new JPanel();
		result.setLayout(new BoxLayout(result, BoxLayout.Y_AXIS));

		// the panel on the left side for the filter selection
		
		Box selectionBox = Box.createHorizontalBox();
		selectionBox.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(), "Matching type:"));
		
		// We only want to do attribute matching here
		// remove the other matching options
		m_colValInclRadio = new JRadioButton("include rows by attribute value");
		m_colValExclRadio = new JRadioButton("exclude rows by attribute value");
		m_colValInclRadio.setActionCommand("colval");
		m_colValExclRadio.setActionCommand("colval");
		addActionListener(m_colValInclRadio);
		addActionListener(m_colValExclRadio);


		// only one at a time should be selected
		ButtonGroup group = new ButtonGroup();
		group.add(m_colValInclRadio);
		group.add(m_colValExclRadio);
		selectionBox.add(Box.createVerticalGlue());
		selectionBox.add(m_colValInclRadio);
		selectionBox.add(m_colValExclRadio);
		selectionBox.add(Box.createVerticalStrut(3));
		selectionBox.add(Box.createVerticalStrut(3));
		selectionBox.add(Box.createVerticalGlue());
		
//		m_matchAll = new JCheckBox("Match all");
//		m_matchAll.addItemListener(new ItemListener()
//		{
//			@Override
//			public void itemStateChanged(ItemEvent e)
//			{
//				matchAllChange();
//				
//			}
//		});
//	
//		Box matchAllBox = Box.createHorizontalBox();
//		matchAllBox.add(m_matchAll);
//		selectionBox.add(matchAllBox);

		// the panel containing the filter panels. They will be instantiated
		// in loadSettings
		m_filterPanel = new JPanel();
		m_filterPanel.setLayout(new BoxLayout(m_filterPanel, BoxLayout.Y_AXIS));
		m_filterPanel.setBorder(BorderFactory.createTitledBorder(
		BorderFactory.createEtchedBorder(), "Set filter parameter:"));
//		m_filterPanel.setMaximumSize(new Dimension(800, 800));
		
		result.add(selectionBox);
		result.add(Box.createHorizontalStrut(7));
		result.add(m_filterPanel);

		return result;
	}
	
//	protected void matchAllChange()
//	{
//		m_matchAllValue = m_matchAll.isSelected();	
//	}

	private void addActionListener(final JRadioButton rb)
	{
		rb.addActionListener(new ActionListener()
		{
			public void actionPerformed(final ActionEvent e)
			{
				filterSelectionChanged(e.getActionCommand());
			}
		});
	}

	/**
	 * Activates the corresponding panel. Called by the radio button listeners.
	 * 
	 * @param activeFilterMethod
	 *            the active filter method
	 */
	protected void filterSelectionChanged(final String activeFilterMethod)
	{

		// remove the old filter panels
		m_filterPanel.remove(m_colValPanel);

		if (activeFilterMethod.equals("colval"))
		{
			m_filterPanel.add(m_colValPanel);
		}

		m_filterPanel.invalidate();
		m_filterPanel.validate();
		m_filterPanel.repaint();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadSettingsFrom(final NodeSettingsRO settings, final DataTableSpec[] specs) throws NotConfigurableException
	{

		if ((specs[0] == null) || (specs[0].getNumColumns() < 1))
		{
			throw new NotConfigurableException("Cannot be configured without" + " input table");
		}

		if (m_colValPanel != null)
		{
			m_filterPanel.remove(m_colValPanel);
		}
		// now create new filter panels
		m_colValPanel = new ColumnFilterRowPanel(this, specs[0]);

		try
		{
			m_colValPanel.setMatchAllSelection(settings.getBoolean(MultiColumnRowSplitterNodeModel.CONFIG_MATCH_ALL));
		} catch (InvalidSettingsException e)
		{
			e.printStackTrace();
		}

		try
		{
			if(settings.getStringArray(MultiColumnRowSplitterNodeModel.CONFIG_SELECTED_COLUMNS) == null)
			{
				m_colValPanel.setIncludedColumns(specs[0], settings.getStringArray(MultiColumnRowSplitterNodeModel.CONFIG_SELECTED_COLUMNS));
			} else
			{
				m_colValPanel.setIncludedColumns(specs[0], settings.getStringArray(MultiColumnRowSplitterNodeModel.CONFIG_SELECTED_COLUMNS));
			}
			
		} catch (Exception e)
		{
			e.printStackTrace();
		}

		/*
		 * now read the filters. We support three different filters:
		 * RowIDFilter, AttrValfilter, and RowNumberFilter. But only one at a
		 * time.
		 */
		IRowFilter filter = null;
		try
		{
			// get the filter
			filter = RowFilterFactory.createRowFilter(settings.getNodeSettings(MultiColumnRowSplitterNodeModel.CFGFILTER));
		} catch (InvalidSettingsException ise)
		{
			ise.printStackTrace();
		}

		String actionCommand = "colval";

		if (filter == null)
		{
			// set the default
			m_colValInclRadio.setSelected(true);
			filterSelectionChanged("colval");
			return;
		}

		if (filter instanceof ColValFilterOldObsolete)
		{
			// support the obsolete filter for backward compatibility
			ColValFilterOldObsolete f = (ColValFilterOldObsolete) filter;
			actionCommand = "colval";
			// activate the corresponding radio button
			if (f.includeMatchingLines())
			{
				m_colValInclRadio.setSelected(true);
			} else
			{
				m_colValExclRadio.setSelected(true);
			}
			try
			{
				m_colValPanel.loadSettingsFromFilter(f);
			} catch (InvalidSettingsException ise)
			{
				// ignore failure
			}
		} else if (filter instanceof AttrValueRowFilter)
		{
			// this covers all the attribute value filters:
			// range, string compare, missing value filter
			AttrValueRowFilter f = (AttrValueRowFilter) filter;
			actionCommand = "colval";
			// activate the corresponding radio button
			if (f.getInclude())
			{
				m_colValInclRadio.setSelected(true);
			} else
			{
				m_colValExclRadio.setSelected(true);
			}
			try
			{
				m_colValPanel.loadSettingsFromFilter(f);
			} catch (InvalidSettingsException ise)
			{
				// ignore failure
			}
		} else
		{
			// we silently ignore unsupported filter and leave default settings.
		}

		filterSelectionChanged(actionCommand);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveSettingsTo(final NodeSettingsWO settings) throws InvalidSettingsException
	{

		List<IRowFilter> theFilter = null;

		if (m_colValInclRadio.isSelected())
		{
			theFilter = m_colValPanel.createFilters(true);
		}
		if (m_colValExclRadio.isSelected())
		{
			theFilter = m_colValPanel.createFilters(false);
		}

		assert theFilter != null;

		settings.addInt(MultiColumnRowSplitterNodeModel.CONFIG_NUM_FILTERS, theFilter.size());

		System.out.println("Saving filters: " + theFilter);
		
//		int i = 0;
//		for (RowFilter filter : theFilter)
//		{
//			filter.saveSettingsTo(settings.addNodeSettings(MultiColumnRowSplitterNodeModel.CFGFILTER + i));
//			i++;
//		}
		
		// Kludge, TODO: this should be removed
		theFilter.get(0).saveSettingsTo(settings.addNodeSettings(MultiColumnRowSplitterNodeModel.CFGFILTER));

		// New variables
		boolean exclude = m_colValExclRadio.isSelected();
		settings.addBoolean(MultiColumnRowSplitterNodeModel.CONFIG_EXCLUDE, exclude);
		
		boolean single = m_colValPanel.getMatchAllFiltering();
		Set<String> includedColumns = m_colValPanel.getColumnFilterPanel().getIncludedColumnSet();

		settings.addBoolean(MultiColumnRowSplitterNodeModel.CONFIG_MATCH_ALL, single);
		settings.addStringArray(MultiColumnRowSplitterNodeModel.CONFIG_SELECTED_COLUMNS, includedColumns.toArray(new String[] {}));

	}
}