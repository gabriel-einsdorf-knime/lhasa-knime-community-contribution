/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.discretise;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.DoubleValue;
import org.knime.core.data.IntValue;
import org.knime.core.data.LongValue;
import org.knime.core.data.MissingCell;
import org.knime.core.data.container.CellFactory;
import org.knime.core.data.container.ColumnRearranger;
import org.knime.core.data.container.SingleCellFactory;
import org.knime.core.data.def.IntCell;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelDouble;
import org.knime.core.node.defaultnodesettings.SettingsModelFilterString;
import org.knime.core.node.streamable.simple.SimpleStreamableFunctionNodeModel;

/**
 * This is the model implementation of Discretise.
 * Discretise the selected columns
 *
 * @author Lhasa Limited
 */
public class DiscretiseNodeModel extends SimpleStreamableFunctionNodeModel 
{
    
//	private static NodeLogger LOGGER = NodeLogger.getLogger(DiscretiseNodeModel.class);
	
    public static final String CONFIG_DISCRETISE_COLUMNS = "CONFIG_DISCRETISE";
	public static final String CONFIG_BOUNDARY = "CONFIG_BOUNDARY";
	
	private SettingsModelDouble settingBoundary = new SettingsModelDouble(CONFIG_BOUNDARY, 1d);
	private SettingsModelFilterString settingColumnFilter = new SettingsModelFilterString(CONFIG_DISCRETISE_COLUMNS);
	

	

//	/**
//     * Constructor for the node model.
//     */
//    protected DiscretiseNodeModel() 
//    {
//        super(1, 1);
//    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BufferedDataTable[] execute(final BufferedDataTable[] inData,final ExecutionContext exec) throws Exception 
    {
    	// Get the input data table and spec, the output spec is the same as in input spec
    	BufferedDataTable data = inData[0];
    	DataTableSpec inSpec = data.getDataTableSpec();
    	
    	// Get the columns from the include list
    	List<String> includedColumns = settingColumnFilter.getIncludeList();
    	
    	// We create a column rearranger iterating through each column we wish to apply
    	// the method to. We take the previous column rearranger to add the new column
    	// to allowing us to do multiple columns. 
    	ColumnRearranger outputTable = new ColumnRearranger(inSpec);
    	for(int i = 0; i < includedColumns.size(); i++)
    	{
    		outputTable = addNewColumnDiscretisation(outputTable, inSpec, inSpec.findColumnIndex(includedColumns.get(i)), 
    												settingBoundary.getDoubleValue());
    	}

    	// Get the output table (this allows the multi threading)
        BufferedDataTable out = exec.createColumnRearrangeTable(data, outputTable, exec);

        return new BufferedDataTable[]{out};
    }

    /**
     * TODO: LongValue, IntValue and DoubleValue are handled seperated in the same way. If we treat them all as
     * DoubleValue can we just do this the once?
     * <br><br>
     * Usage: create the first column rearranger (ColumnRearranger outputTable = new ColumnRearranger(inSpec);) and then iterate
     * through each column to be discretised calling this method with the created column rearranger. 
     * 
     * <br><br>
     * <br> 		ColumnRearranger outputTable = new ColumnRearranger(inSpec);
     *	<br>		for(int i = 0; i < includedColumns.size(); i++)
     *	<br>		{
     *	<br>			outputTable = createColumnRearranger(outputTable, inSpec, inSpec.findColumnIndex(includedColumns.get(i)), 
     * 	<br>											settingBoundary.getDoubleValue());
     *	<br>		}
     * 
     * 
     * 
	 * @param rearanger				The initial column rearranger
	 * @param inSpec				The table specification
	 * @param index					The index of the column to replace
	 * @param boundary				The boundary for the disceretisation value >= boundary ? true : false
	 * 
	 * @return						The column the ColumnRearranger
	 */
	private ColumnRearranger addNewColumnDiscretisation(ColumnRearranger rearanger, DataTableSpec inSpec, final int index, final double boundary)
	{

		DataColumnSpec spec = new DataColumnSpecCreator(inSpec.getColumnSpec(index).getName(), IntCell.TYPE).createSpec();
		
        CellFactory factory = new SingleCellFactory(true, spec)
        {
            public DataCell getCell(DataRow row) 
            {
            	DataCell cell = row.getCell(index);
            	
            	if(cell instanceof MissingCell)
            	{
            		// leave the cell alone
            	} else if(cell instanceof LongValue)
            	{
            		int value = ((LongValue)cell).getLongValue() >= boundary ? 1 : 0;	
            		cell = new IntCell(value);
            	}
            	else if(cell instanceof IntValue)
            	{
            		int value = ((IntValue)cell).getIntValue() >= boundary ? 1 : 0;
            		cell = new IntCell(value);
            	} else if(cell instanceof DoubleValue)
            	{
            		int value = ((DoubleValue)cell).getDoubleValue() >= boundary ? 1 : 0;	
            		cell = new IntCell(value);
            	} else
            	{
            		// Will this exception be caught? 
            		throw new IllegalArgumentException("Cannot handle cell of type: " + cell.getType().toString());
            	}
            	
				return cell;
            }
        };
               
        rearanger.replace(factory, inSpec.getColumnSpec(index).getName());

        return rearanger;
	}

	/**
     * {@inheritDoc}
     */
    @Override
    protected void reset() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataTableSpec[] configure(final DataTableSpec[] inSpecs) throws InvalidSettingsException 
    {
    	ColumnRearranger outputTable = createColumnRearranger(inSpecs[0]);
    	
    	return new DataTableSpec[]{outputTable.createSpec()};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings) {
         settingBoundary.saveSettingsTo(settings);
         settingColumnFilter.saveSettingsTo(settings);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
            throws InvalidSettingsException {
    	settingBoundary.loadSettingsFrom(settings);
    	settingColumnFilter.loadSettingsFrom(settings);
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings)
            throws InvalidSettingsException {
        settingBoundary.validateSettings(settings);
        settingColumnFilter.validateSettings(settings);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
        // TODO: generated method stub
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File internDir,
            final ExecutionMonitor exec) throws IOException,
            CanceledExecutionException {
        // TODO: generated method stub
    }

	@Override
	protected ColumnRearranger createColumnRearranger(DataTableSpec spec) throws InvalidSettingsException
	{
    	// Get the columns from the include list
    	List<String> includedColumns = settingColumnFilter.getIncludeList();
    	
    	// We create a column rearranger iterating through each column we wish to apply
    	// the method to. We take the previous column rearranger to add the new column
    	// to allowing us to do multiple columns. 
    	ColumnRearranger outputTable = new ColumnRearranger(spec);
    	for(int i = 0; i < includedColumns.size(); i++)
    	{
    		outputTable = addNewColumnDiscretisation(outputTable, spec, spec.findColumnIndex(includedColumns.get(i)), 
    												settingBoundary.getDoubleValue());
    	}
    	
    	return outputTable;
	}

}

