package org.lhasalimited.generic.htmltotable2;

import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentBoolean;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnFilter;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;
import org.knime.core.node.defaultnodesettings.DialogComponentString;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelFilterString;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.lhasalimited.generic.htmltotable.util.TableToHtml2;

/**
 * <code>NodeDialog</code> for the "HtmlToTableString2" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Lhasa Limited
 */
public class HtmlToTableString2NodeDialog extends DefaultNodeSettingsPane 
{

    /**
     * New pane for configuring the HtmlToTableString2 node.
     */
    protected HtmlToTableString2NodeDialog() 
    {
    	TableToHtmlString2Settings settings = new TableToHtmlString2Settings();
    	
    	createNewGroup("Settings");
    	setHorizontalPlacement(true);
    	addDialogComponent(new DialogComponentNumber(new SettingsModelInteger(TableToHtmlString2Settings.CFG_NUM_ROWS, 1000), "Number of rows", 100));
    	
    	addDialogComponent(new DialogComponentBoolean(new SettingsModelBoolean(TableToHtmlString2Settings.CFG_INLCUDE_ROWID, true), "Include RowID"));
    	
    	DialogComponentBoolean formatDouble = new DialogComponentBoolean(new SettingsModelBoolean(TableToHtmlString2Settings.CFG_FORMAT_DOUBLE, true), "Format double");
    	addDialogComponent(formatDouble);
    	
    	DialogComponentString doubleFormat = new DialogComponentString(new SettingsModelString(TableToHtmlString2Settings.CFG_DOUBLE_FORMAT, "#.00"), "Double format");
    	addDialogComponent(doubleFormat);
    	
    	DialogComponentBoolean addBasicStyle = new DialogComponentBoolean(new SettingsModelBoolean(TableToHtmlString2Settings.CFG_ADD_BASIC_STYLE, false), "Add basic style");
    	addDialogComponent(addBasicStyle);
    		
    	
    	createNewGroup("Column selection:");
    	addDialogComponent(new DialogComponentColumnFilter(new SettingsModelFilterString(TableToHtmlString2Settings.CFG_COLUMN_SELECTION), 0, true, TableToHtml2.APPROVED_TYPES));

//    	SettingsModelColumnFilter2 setting = new SettingsModelColumnFilter2(TableToHtmlNodeModel.CFG_COLUMN_SELECTION, TableToHtml.APPROVED_TYPES);
//    	addDialogComponent(new DialogComponentColumnFilter2(setting, 0));
    	
    	createNewTab("Styles");
    	
    	createNewGroup("Table");
    	DialogComponentString styleTable = new DialogComponentString(settings.getSetting(TableToHtmlString2Settings.CFG_STYLE_TABLE, SettingsModelString.class), "table style=");
    	addDialogComponent(styleTable);
    	
    	createNewGroup("Header");
    	DialogComponentString styleHeaderTr = new DialogComponentString(settings.getSetting(TableToHtmlString2Settings.CFG_STYLE_HEADER_TR, SettingsModelString.class), "tr style=");
    	addDialogComponent(styleHeaderTr);

    	
    	createNewGroup("Row");
    	setHorizontalPlacement(false);
    	DialogComponentString styleTr = new DialogComponentString(settings.getSetting(TableToHtmlString2Settings.CFG_STYLE_TR, SettingsModelString.class), "All row tr style=");
    	addDialogComponent(styleTr);
    	
    	DialogComponentString styleTrEven = new DialogComponentString(settings.getSetting(TableToHtmlString2Settings.CFG_STYLE_TR_EVEN_ROW, SettingsModelString.class), "Even row additional tr style=");
    	addDialogComponent(styleTrEven);

    	DialogComponentString styleTrOdd = new DialogComponentString(settings.getSetting(TableToHtmlString2Settings.CFG_STYLE_TR_ODD_ROW, SettingsModelString.class), "Odd row additional tr style=");
    	addDialogComponent(styleTrOdd);
    	
    	DialogComponentString styleHeaderTd = new DialogComponentString(settings.getSetting(TableToHtmlString2Settings.CFG_STYLE_TD, SettingsModelString.class), "td style=");
    	addDialogComponent(styleHeaderTd);
    	
    	
    	createNewGroup("Missing cells");
    	DialogComponentString styleMissing = new DialogComponentString(settings.getSetting(TableToHtmlString2Settings.CFG_STYLE_MISSIG, SettingsModelString.class), "td style=");
    	addDialogComponent(styleMissing);
    }
}

