package org.lhasalimited.generic.htmltotable2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.RowKey;
import org.knime.core.data.collection.CollectionCellFactory;
import org.knime.core.data.collection.ListCell;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.StringCell;
import org.knime.core.data.def.StringCell.StringCellFactory;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.defaultnodesettings.SettingsModelFilterString;
import org.lhasalimited.generic.htmltotable.util.TableToHtml2;
import org.lhasalimited.generic.node.config.LocalSettingsNodeModel;

/**
 * This is the model implementation of HtmlToTableString2.
 * 
 *
 * @author Lhasa Limited
 */
public class HtmlToTableString2NodeModel extends LocalSettingsNodeModel<TableToHtmlString2Settings>
{	
//	private TableToHtml2 streamConverter;
//	private int streamTrack;
	
	/**
	 * Constructor for the node model.
	 */
	protected HtmlToTableString2NodeModel()
	{
		super(1, 1);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected BufferedDataTable[] execute(final BufferedDataTable[] inData, final ExecutionContext exec) throws Exception
	{	
		// Get the input table
		BufferedDataTable table = inData[0];

		// Create the table converter
		TableToHtml2 converter = new TableToHtml2(localSettings.getNumRows(), 
												localSettings.isFormatDouble() ? localSettings.getDoubleFormat() : null, 
												localSettings.getIncludeRowId(), 
												localSettings.isAddStyle());
		
		// Add the style config
		converter.setTableStyle(localSettings.getStyleTable());
		
		converter.setTrStyleHeader(localSettings.getStyleHeaderTr());
		
		converter.setTdStyle(localSettings.getStyleTd());
		
		converter.setAdditionalStyleMissingCell(localSettings.getStyleMissing());
		
		converter.setTrStyle(localSettings.getStyleTr());
		converter.setEvenRowStyle(localSettings.getStyleTrEven());
		converter.setOddRowStyle(localSettings.getStyleTrOdd());

		
		List<String> included = getIncludedColumns(localSettings.getColumnFilter(), table.getDataTableSpec());
		
		
		// Convert it to HTML
		String htmlTable;
		try 
		{
			htmlTable = converter.convertToHtml(table, included);
		} catch (Exception e) 
		{
			e.printStackTrace();
			throw new Exception("Failed to convert to HTML", e);
		}

		// Create the spec
		DataTableSpec outputSpec = createTableSpec();
		BufferedDataContainer container = exec.createDataContainer(outputSpec);
		List<String> images = converter.getImages();
		
		// Add row to table
		container.addRowToTable(createRow(htmlTable, images));
		container.close();
		

		
//		FlowVariable variable = new FlowVariable("html-content", htmlTable);
		pushFlowVariableString("html-content", htmlTable);
		
		// Return the table
		return new BufferedDataTable[] { container.getTable() };
	}

	/**
	 * 
	 * @param htmlTable
	 * @return
	 */
	private DefaultRow createRow(String htmlTable, List<String> images)
	{
		DataCell[] cells = new DataCell[2];

		cells[0] = new StringCell(htmlTable);
		cells[1] = createImagesCell(images);

		return new DefaultRow(new RowKey("HTML"), cells);
	}

	private DataCell createImagesCell(List<String> images) 
	{
		DataCell[] cells = new DataCell[images.size()];
		
		for(int i = 0; i < images.size(); i++)
		{
			cells[i] = StringCellFactory.create(images.get(i));
		}
		
		return CollectionCellFactory.createListCell(Arrays.asList(cells));
	}

	/**
	 * 
	 * @return
	 */
	private DataTableSpec createTableSpec()
	{
		DataColumnSpec[] allColSpecs = new DataColumnSpec[2];

		allColSpecs[0] = new DataColumnSpecCreator("HTML", StringCell.TYPE).createSpec();
		allColSpecs[1] = new DataColumnSpecCreator("Images", ListCell.getCollectionType(StringCellFactory.TYPE)).createSpec();

		return new DataTableSpec(allColSpecs);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void reset()
	{
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
			throws InvalidSettingsException
	{
		pushFlowVariableString("html-content", "");
		return new DataTableSpec[] { createTableSpec() };
	}

	
	
//    /** {@inheritDoc} */
//    @Override
//    public InputPortRole[] getInputPortRoles() {
//        return new InputPortRole[] {InputPortRole.NONDISTRIBUTED_STREAMABLE};
//    }
//
//    /** {@inheritDoc} */
//    @Override
//    public OutputPortRole[] getOutputPortRoles() {
//        return new OutputPortRole[] {OutputPortRole.NONDISTRIBUTED};
//    }
    
    /**
     * Identify which columns should be used to generate the table.
     * 
     * @param selected
     * @param spec
     * @return
     */
    protected List<String> getIncludedColumns(SettingsModelFilterString selected, DataTableSpec spec)
    {
		List<String> included = null;
		if(localSettings.getColumnFilter().isKeepAllSelected()) {
			String[] names = spec.getColumnNames();
			
			included = new ArrayList<String>();
			
			for(String colName : names)
			{
				if(TableToHtml2.colSpecCompatible(spec.getColumnSpec(colName))) {
					included.add(colName);
				}
			}
			
		} else {
			included = selected.getIncludeList();
		}
		
		return included;
    }

//	/** {@inheritDoc} */
//	@Override
//	public StreamableOperator createStreamableOperator(final PartitionInfo partitionInfo,
//			final PortObjectSpec[] inSpecs) throws InvalidSettingsException
//	{
//		return new StreamableOperator()
//		{
//
//			@Override
//			public StreamableOperatorInternals saveInternals()
//			{
//				return null;
//			}
//
//			@Override
//			public void runFinal(final PortInput[] inputs, final PortOutput[] outputs, final ExecutionContext ctx)
//					throws Exception
//			{
//				
//				List<String> included = getIncludedColumns(localSettings.getColumnFilter(), ((RowInput)inputs[0]).getDataTableSpec());
//				
//
//				streamConverter = new TableToHtml2(localSettings.getNumRows(), 
//						localSettings.isFormatDouble() ? localSettings.getDoubleFormat() : null, 
//						localSettings.getIncludeRowId(), 
//						localSettings.isAddStyle());
//				
//				streamTrack = 0;
//				
//				RowInput in = (RowInput) inputs[0];
//				streamConverter.convertToHtmlStreamStart(in.getDataTableSpec(), included);
//				
//				
//				RowOutput out = (RowOutput) outputs[0];
//				HtmlToTableString2NodeModel.this.execute(in, ctx);
//				
//				String html = streamConverter.convertToHtmlStreamEnd();
//				
//				out.push(createRow(html, streamConverter.getImages()));
//				out.close();
//				
//				pushFlowVariableString("html-content", html);
//			}
//		};
//	}
//	
//	private void execute(final RowInput inData, final ExecutionContext exec) throws Exception
//	{
//		
//		DataRow row;
//		while ((row = inData.poll()) != null && streamTrack < localSettings.getNumRows())
//		{
//			streamConverter.addRow(row);
//			streamTrack++;
//		}
//		
//		inData.close();
//	}

}
