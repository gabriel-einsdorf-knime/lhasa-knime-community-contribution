package org.lhasalimited.generic.htmltotable2;

import org.knime.core.node.NodeView;

/**
 * <code>NodeView</code> for the "HtmlToTableString2" Node.
 * 
 *
 * @author Lhasa Limited
 */
public class HtmlToTableString2NodeView extends NodeView<HtmlToTableString2NodeModel> {

    /**
     * Creates a new view.
     * 
     * @param nodeModel The model (class: {@link HtmlToTableString2NodeModel})
     */
    protected HtmlToTableString2NodeView(final HtmlToTableString2NodeModel nodeModel) {
        super(nodeModel);
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void modelChanged() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onClose() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onOpen() {
        // TODO: generated method stub
    }

}

