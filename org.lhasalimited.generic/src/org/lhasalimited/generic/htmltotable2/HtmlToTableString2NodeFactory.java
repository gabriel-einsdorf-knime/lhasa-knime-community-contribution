package org.lhasalimited.generic.htmltotable2;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "HtmlToTableString2" Node.
 * 
 *
 * @author Lhasa Limited
 */
public class HtmlToTableString2NodeFactory 
        extends NodeFactory<HtmlToTableString2NodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public HtmlToTableString2NodeModel createNodeModel() {
        return new HtmlToTableString2NodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<HtmlToTableString2NodeModel> createNodeView(final int viewIndex,
            final HtmlToTableString2NodeModel nodeModel) {
        return new HtmlToTableString2NodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new HtmlToTableString2NodeDialog();
    }

}

