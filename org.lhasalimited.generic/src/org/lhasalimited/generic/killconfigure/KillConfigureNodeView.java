package org.lhasalimited.generic.killconfigure;

import org.knime.core.node.NodeView;

/**
 * <code>NodeView</code> for the "KillConfigure" Node.
 * 
 *
 * @author Samuel, Lhasa Limited
 */
public class KillConfigureNodeView extends NodeView<KillConfigureNodeModel> {

    /**
     * Creates a new view.
     * 
     * @param nodeModel The model (class: {@link KillConfigureNodeModel})
     */
    protected KillConfigureNodeView(final KillConfigureNodeModel nodeModel) {
        super(nodeModel);
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void modelChanged() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onClose() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onOpen() {
        // TODO: generated method stub
    }

}

