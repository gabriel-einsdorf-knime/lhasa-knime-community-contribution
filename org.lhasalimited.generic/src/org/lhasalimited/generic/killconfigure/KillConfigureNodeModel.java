package org.lhasalimited.generic.killconfigure;

import java.io.File;
import java.io.IOException;

import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.port.PortObject;
import org.knime.core.node.port.PortObjectSpec;
import org.knime.core.node.port.inactive.InactiveBranchPortObjectSpec;

/**
 * This is the model implementation of KillConfigure.
 * 
 *
 * @author Samuel, Lhasa Limited
 */
public class KillConfigureNodeModel extends NodeModel
{
	
	protected KillConfigureNodeModel()
	{
		super(1, 1);
	}

	public static String CONFIG_KILL_CONFIGURE = "cfgKillConfigure";
	public static SettingsModelBoolean settingKillConfigure = new SettingsModelBoolean(CONFIG_KILL_CONFIGURE, true);
	
	



	@Override
	protected PortObject[] execute(PortObject[] inObjects, ExecutionContext exec) throws Exception
	{
		return inObjects;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected PortObjectSpec[] configure(final PortObjectSpec[] inSpecs) throws InvalidSettingsException
	{
		boolean condition = false;
		
		if(settingKillConfigure.isEnabled())
		{
			condition = settingKillConfigure.getBooleanValue();
		}
		

		
		if(condition)
		{
			return new PortObjectSpec[]{null};
		} else
		{
//			return new DataTableSpec[] {createColumnRearranger(inSpecs[0]).createSpec()};
			return inSpecs;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveSettingsTo(final NodeSettingsWO settings)
	{
		settingKillConfigure.saveSettingsTo(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadValidatedSettingsFrom(final NodeSettingsRO settings) throws InvalidSettingsException
	{
		settingKillConfigure.loadSettingsFrom(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validateSettings(final NodeSettingsRO settings) throws InvalidSettingsException
	{
		settingKillConfigure.validateSettings(settings);
	}

//	@Override
//	protected ColumnRearranger createColumnRearranger(DataTableSpec spec) throws InvalidSettingsException
//	{
//		return new ColumnRearranger(spec);
//	}

	@Override
	protected void loadInternals(File nodeInternDir, ExecutionMonitor exec)
			throws IOException, CanceledExecutionException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void saveInternals(File nodeInternDir, ExecutionMonitor exec)
			throws IOException, CanceledExecutionException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void reset()
	{
		// TODO Auto-generated method stub
		
	}

}
