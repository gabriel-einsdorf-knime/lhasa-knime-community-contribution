package org.lhasalimited.generic.killconfigure;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "KillConfigure" Node.
 * 
 *
 * @author Samuel, Lhasa Limited
 */
public class KillConfigureNodeFactory 
        extends NodeFactory<KillConfigureNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public KillConfigureNodeModel createNodeModel() {
        return new KillConfigureNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<KillConfigureNodeModel> createNodeView(final int viewIndex,
            final KillConfigureNodeModel nodeModel) {
        return new KillConfigureNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new KillConfigureNodeDialog();
    }

}

