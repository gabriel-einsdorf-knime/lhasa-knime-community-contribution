package org.lhasalimited.knime.metabolism.encapsulated.testing.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.lhasalimited.cyp.util.smartcyp.SmartCypColour;
import org.lhasalimited.cyp.util.smartcyp.SmartCypProcessor;
import org.lhasalimited.cyp.util.smartcyp.SmartCypResults;
import org.lhasalimited.knime.metabolism.encapsulated.util.CdkImageRender;
import org.lhasalimited.knime.metabolism.encapsulated.util.StructureConversion;
import org.openscience.cdk.interfaces.IAtom;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.renderer.generators.standard.StandardGenerator;

public class SmartCypResultsTest
{

	public static final String molfile = "Paracetamol\r\n" + "  Mrv0541 01291609432D          \r\n" + "\r\n"
			+ " 11 11  0  0  0  0            999 V2000\r\n"
			+ "    0.0000    3.3000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n"
			+ "    0.0000    2.4750    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n"
			+ "   -0.7145    2.0625    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\r\n"
			+ "    0.7145    2.0625    0.0000 N   0  0  0  0  0  0  0  0  0  0  0  0\r\n"
			+ "    0.7145    1.2375    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n"
			+ "    1.4289    0.8250    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n"
			+ "    1.4289   -0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n"
			+ "    0.7145   -0.4125    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n"
			+ "    0.7145   -1.2375    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\r\n"
			+ "    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n"
			+ "    0.0000    0.8250    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + "  1  2  1  0  0  0  0\r\n"
			+ "  2  3  2  0  0  0  0\r\n" + "  2  4  1  0  0  0  0\r\n" + "  4  5  1  0  0  0  0\r\n"
			+ "  5  6  2  0  0  0  0\r\n" + "  6  7  1  0  0  0  0\r\n" + "  7  8  2  0  0  0  0\r\n"
			+ "  8  9  1  0  0  0  0\r\n" + "  8 10  1  0  0  0  0\r\n" + " 10 11  2  0  0  0  0\r\n"
			+ "  5 11  1  0  0  0  0\r\n" + "M  END\r\n" + "";

	@Test
	public void testRenderStructureSvg()
	{
		SmartCypProcessor processor = new SmartCypProcessor(true);
		try
		{

			int rankingCutoff = 3;

			SmartCypResults result = processor.process(molfile, "paracetemol").get(0);
			String molfile = result.getMolfile();

			IAtomContainer structure = StructureConversion.readMolfile(molfile);

			IAtomContainer structureCloned = structure.clone();
			for (int atomIndex = 0; atomIndex < structureCloned.getAtomCount(); atomIndex++)
			{
				IAtom currentAtom = structureCloned.getAtom(atomIndex);
				String currentAtomType = currentAtom.getSymbol();

				if (currentAtomType.equals("C") || currentAtomType.equals("N") || currentAtomType.equals("P")
						|| currentAtomType.equals("S"))
				{
					int ranking = result.getAtom(atomIndex).getRanking();
					if (ranking <= rankingCutoff || rankingCutoff == 0)
					{
						structureCloned.getAtom(atomIndex).setProperty(StandardGenerator.ANNOTATION_LABEL,
								ranking + "");
						structureCloned.getAtom(atomIndex).setProperty(StandardGenerator.HIGHLIGHT_COLOR,
								SmartCypColour.getColourForRank(ranking));
					}
				}
			}

			String expectedSvg = FileUtils.readFileToString(new File(SmartCypResultsTest.class.getResource("./test2.svg").getFile()));
//			String expectedSvg = readFile(SmartCypResultsTest.class.getResource("./test.svg").getFile(), StandardCharsets.UTF_8);

			String svg = CdkImageRender.renderSvg(structureCloned, 250, 250, Color.BLACK);
//			FileUtils.writeStringToFile(new File("c:/container/test2.svg"), svg);

			assertEquals(expectedSvg, svg);

		} catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	static String readFile(String path, Charset encoding) throws IOException
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

}
