package org.lhasalimited.knime.metabolism.encapsulated.testing.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.lhasalimited.cyp.util.smartcyp.SmartCypProcessor;
import org.lhasalimited.cyp.util.smartcyp.SmartCypResults;
import org.openscience.old.cdk.interfaces.IAtomContainer;

/**
 * Simply tests that SMARTCyp runs
 * TODO: proper test cases before approving branching into 3.1  
 *
 * @author Samuel
 *
 */
public class SmartCypRunTest
{
	
	public static final String molfile = "Paracetamol\r\n" + 
			"  Mrv0541 01291609432D          \r\n" + 
			"\r\n" + 
			" 11 11  0  0  0  0            999 V2000\r\n" + 
			"    0.0000    3.3000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    0.0000    2.4750    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"   -0.7145    2.0625    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    0.7145    2.0625    0.0000 N   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    0.7145    1.2375    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    1.4289    0.8250    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    1.4289   -0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    0.7145   -0.4125    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    0.7145   -1.2375    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    0.0000    0.8250    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"  1  2  1  0  0  0  0\r\n" + 
			"  2  3  2  0  0  0  0\r\n" + 
			"  2  4  1  0  0  0  0\r\n" + 
			"  4  5  1  0  0  0  0\r\n" + 
			"  5  6  2  0  0  0  0\r\n" + 
			"  6  7  1  0  0  0  0\r\n" + 
			"  7  8  2  0  0  0  0\r\n" + 
			"  8  9  1  0  0  0  0\r\n" + 
			"  8 10  1  0  0  0  0\r\n" + 
			" 10 11  2  0  0  0  0\r\n" + 
			"  5 11  1  0  0  0  0\r\n" + 
			"M  END\r\n" + 
			"";
	
	public static final String molfile2 = "\r\n" + 
			"  CDK     0127161450\r\n" + 
			"\r\n" + 
			" 22 22  0  0  0  0  0  0  0  0999 V2000\r\n" + 
			"   -1.8200    0.1896    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"   -0.5208   -0.5602    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"   -0.5206   -2.0602    0.0000 O   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    0.7781    0.1900    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    2.0772   -0.5598    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    3.3762    0.1904    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    4.6753   -0.5594    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    5.9742    0.1908    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    7.2734   -0.5590    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    8.5723    0.1912    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    9.8715   -0.5585    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"   11.1704    0.1917    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"   12.4704   -0.5583    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"   13.7704    0.1917    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"   13.7704    1.6917    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"   12.4704    2.4417    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"   11.1704    1.6917    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    9.8715    2.4419    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"   11.5063   -1.7075    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"   13.4344   -1.7075    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    7.2736   -2.0590    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"    2.0775   -2.0598    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0\r\n" + 
			"  1  2  1  0  0  0  0 \r\n" + 
			"  2  3  2  0  0  0  0 \r\n" + 
			"  2  4  1  0  0  0  0 \r\n" + 
			"  4  5  2  0  0  0  0 \r\n" + 
			"  5  6  1  0  0  0  0 \r\n" + 
			"  6  7  2  0  0  0  0 \r\n" + 
			"  7  8  1  4  0  0  0 \r\n" + 
			"  8  9  2  0  0  0  0 \r\n" + 
			"  9 10  1  4  0  0  0 \r\n" + 
			" 10 11  2  0  0  0  0 \r\n" + 
			" 11 12  1  0  0  0  0 \r\n" + 
			" 12 13  1  0  0  0  0 \r\n" + 
			" 13 14  1  0  0  0  0 \r\n" + 
			" 14 15  1  0  0  0  0 \r\n" + 
			" 15 16  1  0  0  0  0 \r\n" + 
			" 16 17  1  0  0  0  0 \r\n" + 
			" 12 17  2  0  0  0  0 \r\n" + 
			" 17 18  1  0  0  0  0 \r\n" + 
			" 13 19  1  0  0  0  0 \r\n" + 
			" 13 20  1  0  0  0  0 \r\n" + 
			"  9 21  1  0  0  0  0 \r\n" + 
			"  5 22  1  4  0  0  0 \r\n" + 
			"M  END\r\n" + 
			"";

	@Test
	public void testSmartCypProcessor()
	{
		SmartCypProcessor processor = new SmartCypProcessor(true);
		
		assertNotNull(processor);
	}

	@Test
	public void testProcessStringString()
	{
		SmartCypProcessor processor = new SmartCypProcessor(true);
				try
		{
			List<SmartCypResults> results = processor.process(molfile, "paracetemol");
			List<SmartCypResults> results2 = processor.process(molfile2, "second");
			
			assertEquals(1, results.size());
			assertEquals(1, results2.size());
		} catch (Exception e)
		{
			e.printStackTrace();
			fail(e.getMessage());
		}
		
	}

	@Test
	public void testReadInStructures()
	{
		SmartCypProcessor processor = new SmartCypProcessor(true);
		try
		{
			List<IAtomContainer> structures  = SmartCypProcessor.readInStructures(molfile, "paracetemol");
			assertEquals(1, structures.size());
		} catch (Exception e)
		{
			e.printStackTrace();
			fail(e.getMessage());
		}

	}
	


}
